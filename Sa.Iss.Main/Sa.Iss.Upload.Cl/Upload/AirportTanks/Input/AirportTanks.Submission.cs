﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss.AirportTanks.Input
{
    public class Upload
    {
        internal static Submission Submission(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission, String sheetName)
        {
            submission = new Submission();

            Excel.Worksheet wks = wkb.Worksheets[sheetName];
            Excel.Range rng = null;

            int r = 0;
            int c = 0;

            try
            {
                using (SqlCommand cmd = new SqlCommand("[stg].[Insert_Submissions]", cn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    //@tsModified		DATETIMEOFFSET
                    cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

                    //@CompanyName		NVARCHAR(48),
                    r = 9;
                    c = 4;
                    rng = wks.Cells[r, c];
                    cmd.Parameters.Add("@CompanyName", SqlDbType.NVarChar, 48).Value = XL.Return.String(rng, 48);

                    //@FacilityName		NVARCHAR(48),
                    r = 10;
                    c = 4;
                    rng = wks.Cells[r, c];
                    cmd.Parameters.Add("@FacilityName", SqlDbType.NVarChar, 48).Value = XL.Return.String(rng, 48);

                    //@DataSpan			CHAR(1),
                    r = 10;
                    c = 9;
                    rng = wks.Cells[r, c];
                    cmd.Parameters.Add("@DataSpan", SqlDbType.Char, 1).Value = XL.Return.String(rng, 1);

                    //@DataYear			SMALLINT,
                    r = 12;
                    c = 10;
                    rng = wks.Cells[r, c];
                    cmd.Parameters.Add("@DataYear", SqlDbType.SmallInt).Value = XL.Return.UInt16(rng);

                    //@DataMonth		TINYINT,
                    r = 10;
                    c = 10;
                    rng = wks.Cells[r, c];
                    string month = XL.Return.String(rng, 3);

                    cmd.Parameters.Add("@DataMonth", SqlDbType.SmallInt).Value = DateTime.ParseExact(month, "MMM", System.Globalization.CultureInfo.CurrentCulture).Month;

                    //@PeriodBeg		DATE,
                    r = 12;
                    rng = wks.Cells[r, c];
                    string year = XL.Return.String(rng, 4);

                    cmd.Parameters.Add("@PeriodBeg", SqlDbType.Date).Value = year + " " + month;

                    //@PeriodEnd		DATE,
                    cmd.Parameters.Add("@PeriodEnd", SqlDbType.Date).Value = DBNull.Value;

                    //@Duration_Days	SMALLINT
                    cmd.Parameters.Add("@Duration_Days", SqlDbType.SmallInt).Value = DBNull.Value;

                    //@ReturnValue		INT (Stored Procedure Return Value)
                    string pName = "@ReturnValue";
                    cmd.Parameters.Add(new SqlParameter(pName, SqlDbType.Int, 0, ParameterDirection.ReturnValue, false, 0, 0, string.Empty, DataRowVersion.Current, null));

                    try
                    {
                        cmd.ExecuteNonQuery();
                        submission.Id = (int)cmd.Parameters[pName].Value;
                    }
                    catch (SqlException)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorHandler.Insert_UpLoadError(queue, "Bafco", "Bafco.Submission", "0", wkb, wks, rng, r, c, "[stg].[Insert_Submissions]", ex);
            }
            finally
            {
                wks = null;
                rng = null;
            }
            return submission;
        }
    }
}
