﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Input
		{
			public partial class Upload
			{
				public static void EnergyElectricity(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["Complex-Level Input"];
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					const int cName = 8;
					const int cQuant = 12;
					const int cEff = 14;

					try
					{
						for (int i = 31; i <= 33; i++)
						{
							r = i;

							using (SqlCommand cmd = new SqlCommand("[stg].[Insert_ChemEnergyElectricity]", cn))
							{
								cmd.CommandType = CommandType.StoredProcedure;
								SqlParameter p = new SqlParameter();

								//@SubmissionId						INT
								cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

								//@tsModified						DATETIMEOFFSET
								cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

								//@ElectricityName					VARCHAR(40),
								c = cName;
								rng = wks.Cells[r, c];
								cmd.Parameters.Add("@ElectricityName", SqlDbType.VarChar, 40).Value = XL.Return.String(rng, 40);

								//@Quantity_MWhMonth				FLOAT,
								c = cQuant;
								rng = wks.Cells[r, c];
								cmd.Parameters.Add("@Quantity_MWhMonth", SqlDbType.Float).Value = XL.Return.Double(rng);

								//@Efficiency_MJMWh					FLOAT,
								c = cEff;
								rng = wks.Cells[r, c];
								p = cmd.Parameters.Add("@Efficiency_MJMWh", SqlDbType.Float);
								if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

								try
								{
									cmd.ExecuteNonQuery();
								}
								catch (SqlException)
								{
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "GPIC", "GPIC.ChemEnergyElectricity", "0", wkb, wks, rng, r, c, "[stg].[Insert_ChemEnergyElectricity]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}