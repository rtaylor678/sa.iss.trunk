﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Input
		{
			public partial class Upload
			{
				public static void Losses(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["Hydrocarbon Loss"];
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					const int cName = 3;
					const int cQuant = 5;

					try
					{
						for (int i = 7; i <= 8; i++)
						{
							r = i;

							using (SqlCommand cmd = new SqlCommand("[stg].[Insert_ChemLosses]", cn))
							{
								cmd.CommandType = CommandType.StoredProcedure;

								//@SubmissionId						INT
								cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

								//@tsModified						DATETIMEOFFSET
								cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

								//@LossDetail						VARCHAR(96),
								c = cName;
								rng = wks.Cells[r, c];
								cmd.Parameters.Add("@LossDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

								//@Quantity_TonneMonth				FLOAT,
								c = cQuant;
								rng = wks.Cells[r, c];
								cmd.Parameters.Add("@Quantity_TonneMonth", SqlDbType.Float).Value = XL.Return.Double(rng);

								try
								{
									cmd.ExecuteNonQuery();
								}
								catch (SqlException)
								{
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "GPIC", "GPIC.ChemLosses", "0", wkb, wks, rng, r, c, "[stg].[Insert_ChemLosses]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}