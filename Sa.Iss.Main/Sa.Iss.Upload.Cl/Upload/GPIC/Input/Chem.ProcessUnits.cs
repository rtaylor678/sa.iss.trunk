﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Input
		{
			public partial class Upload
			{
				public static void ProcessUnits(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["Process-Level Input"];
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					const int rProcNum = 9;
					const int rName = 11;
					const int rProcId = 12;
					const int rProcType = 13;
					const int rUnits = 15;
					const int rCap = 16;
					const int rUtil = 17;

					try
					{
						for (int i = 2; i <= 28; i++)
						{
							c = i;

							r = rName;
							rng = wks.Cells[r, i];

							if (XL.RangeHasValue(rng))
							{
								using (SqlCommand cmd = new SqlCommand("[stg].[Insert_ChemProcessUnits]", cn))
								{
									cmd.CommandType = CommandType.StoredProcedure;
									SqlParameter p = new SqlParameter();

									//@SubmissionId						INT
									cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

									//@tsModified						DATETIMEOFFSET
									cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

									//@ProcessUnitName					VARCHAR(48),
									r = rName;
									rng = wks.Cells[r, c];
									cmd.Parameters.Add("@ProcessUnitName", SqlDbType.VarChar, 48).Value = XL.Return.String(rng, 48);

									//@ProcessUnitNumber				INT				= NULL,
									r = rProcNum;
									rng = wks.Cells[r, c];
									p = cmd.Parameters.Add("@ProcessUnitNumber", SqlDbType.Int);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.Int32(rng);

									//@ProcessId						VARCHAR(12)		= NULL,
									r = rProcId;
									rng = wks.Cells[r, c];
									p = cmd.Parameters.Add("@ProcessId", SqlDbType.VarChar, 12);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.String(rng, 12);

									//@ProcessType						VARCHAR(12)		= NULL,
									r = rProcType;
									rng = wks.Cells[r, c];
									p = cmd.Parameters.Add("@ProcessType", SqlDbType.VarChar, 12);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.String(rng, 12);

									//@Capacity_Units					VARCHAR(32)		= NULL,
									r = rUnits;
									rng = wks.Cells[r, c];
									p = cmd.Parameters.Add("@Capacity_Units", SqlDbType.VarChar, 32);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.String(rng, 32);

									//@Capacity							FLOAT			= NULL,
									r = rCap;
									rng = wks.Cells[r, c];
									p = cmd.Parameters.Add("@Capacity", SqlDbType.Float);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

									//@CapacityUtilization_Pcnt			FLOAT			= NULL,
									r = rUtil;
									rng = wks.Cells[r, c];
									p = cmd.Parameters.Add("@CapacityUtilization_Pcnt", SqlDbType.Float);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

									try
									{
										cmd.ExecuteNonQuery();
									}
									catch (SqlException)
									{
									}
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "GPIC", "GPIC.ChemProcessUnits", "0", wkb, wks, rng, r, c, "[stg].[Insert_ChemProcessUnits]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}