﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Input
		{
			public partial class Upload
			{
				public static void ProcessUnitsQuantity(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["Process-Level Input"];
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					const int rName = 11;
					const int rQty = 21;

					try
					{
						for (int i = 2; i <= 9; i++)
						{
							c = i;

							r = rQty;
							rng = wks.Cells[r, c];

							if (XL.RangeHasValue(rng))
							{
								using (SqlCommand cmd = new SqlCommand("[stg].[Insert_ChemProcessUnitsQuantity]", cn))
								{
									cmd.CommandType = CommandType.StoredProcedure;

									//@SubmissionId						INT
									cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

									//@tsModified						DATETIMEOFFSET
									cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

									//@ProcessUnitName					VARCHAR(48),
									r = rName;
									rng = wks.Cells[r, c];
									cmd.Parameters.Add("@ProcessUnitName", SqlDbType.VarChar, 48).Value = XL.Return.String(rng, 48);

									//@Quantity_kNm3Day					FLOAT
									r = rQty;
									rng = wks.Cells[r, c];
									cmd.Parameters.Add("@Quantity_kNm3Day", SqlDbType.Float).Value = XL.Return.Double(rng);

									try
									{
										cmd.ExecuteNonQuery();
									}
									catch (SqlException)
									{
									}
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "GPIC", "GPIC.ChemProcessUnitsQuantity", "0", wkb, wks, rng, r, c, "[stg].[Insert_ChemProcessUnitsQuantity]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}