﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Calcs
		{
			public partial class Upload
			{
				public static void Divisors(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["Opex"]; ;
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					try
					{
						for (int i = 49; i <= 53; i++)
						{
							r = i;

							using (SqlCommand cmd = new SqlCommand("[xls].[Insert_Divisors]", cn))
							{
								cmd.CommandType = CommandType.StoredProcedure;

								//@SubmissionId					INT,
								cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;
								//@tsModified					DATETIMEOFFSET
								cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

								//@DivisorDetail				NVARCHAR(96),
								c = 1;
								rng = wks.Cells[r, c];
								cmd.Parameters.Add("@DivisorDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

								//@Value						FLOAT
								c = 2;
								rng = wks.Cells[r, c];
								cmd.Parameters.Add("@Value", SqlDbType.Float).Value = XL.Return.Double(rng);

								try
								{
									cmd.ExecuteNonQuery();
								}
								catch (SqlException)
								{
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "Divisors", "Divisors", "0", wkb, wks, rng, r, c, "[xls].[Insert_Divisors]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}