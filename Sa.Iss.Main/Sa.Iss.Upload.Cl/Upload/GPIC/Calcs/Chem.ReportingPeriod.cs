﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Calcs
		{
			public partial class Upload
			{
				public static void ReportingPeriod(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = null;
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					try
					{
						using (SqlCommand cmd = new SqlCommand("[xls].[Insert_ReportingPeriod]", cn))
						{
							cmd.CommandType = CommandType.StoredProcedure;

							//@SubmissionId					INT,
							cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

							//@tsModified					DATETIMEOFFSET
							cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

							//@PeriodName					VARCHAR(24),
							r = 3;
							c = 11;
							wks = wkb.Worksheets["Complex-Level Input"];
							rng = wks.Cells[r, c];
							cmd.Parameters.Add("@PeriodName", SqlDbType.VarChar, 24).Value = rng.Text; // XL.Return.String(rng, 24);

							//@PeriodBeg_Date				DATE			= NULL,
							r = 10;
							c = 10;
							rng = wks.Cells[r, c];
							string month = XL.Return.String(rng, 3);

							r = 12;
							rng = wks.Cells[r, c];
							string year = XL.Return.String(rng, 4);

							cmd.Parameters.Add("@PeriodBeg_Date", SqlDbType.Date).Value = year + " " + month;

							//@Duration_Days				INT				= NULL,
							r = 55;
							c = 2;
							wks = wkb.Worksheets["Opex"];
							rng = wks.Cells[r, c];
							cmd.Parameters.Add("@Duration_Days", SqlDbType.Int).Value = XL.Return.UInt16(rng);

							//@Duration_PcntYear			FLOAT			= NULL
							r = 56;
							c = 2;
							wks = wkb.Worksheets["Opex"];
							rng = wks.Cells[r, c];
							cmd.Parameters.Add("@Duration_PcntYear", SqlDbType.Float).Value = XL.Return.Double(rng);

							try
							{
								cmd.ExecuteNonQuery();
							}
							catch (SqlException)
							{
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "ReportingPeriod", "ReportingPeriod", "0", wkb, wks, rng, r, c, "[xls].[Insert_ReportingPeriod]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}