﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Calcs
		{
			public partial class Upload
			{
				public static void FactorsAndStandards(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["Factors&Standards"];
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					try
					{
						int cName = 1;
						int cCap = 2;
						int cUom = 3;
						int ckEdc = 4;
						int cEii = 5;
						int cPes = 6;
						int cMes = 7;
						int cCes = 8;

						for (int i = 14; i <= 30; i++)
						{
							r = i;

							using (SqlCommand cmd = new SqlCommand("[xls].[Insert_FactorsAndStandards]", cn))
							{
								if (XL.RangeHasValue(wks.Cells[r, cName]) &&
									XL.RangeHasValue(wks.Cells[r, ckEdc]) &&
									XL.RangeHasValue(wks.Cells[r, cPes]) &&
									XL.RangeHasValue(wks.Cells[r, cMes]) &&
									XL.RangeHasValue(wks.Cells[r, cCes]))
								{
									cmd.CommandType = CommandType.StoredProcedure;
									SqlParameter p = new SqlParameter();

									//@SubmissionId				INT,
									cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

									//@tsModified				DATETIMEOFFSET
									cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

									//@ProcessUnitDetail		NVARCHAR(96),
									rng = wks.Cells[r, cName];
									cmd.Parameters.Add("@ProcessUnitDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

									//@Capacity					FLOAT			= NULL,
									rng = wks.Cells[r, cCap];
									p = cmd.Parameters.Add("@Capacity", SqlDbType.Float);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

									//@Capacity_UomName			NVARCHAR(48)	= NULL,
									rng = wks.Cells[r, cUom];
									p = cmd.Parameters.Add("@Capacity_UomName", SqlDbType.NVarChar, 48);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.String(rng, 48);

									//@kEdc						FLOAT,
									rng = wks.Cells[r, ckEdc];
									cmd.Parameters.Add("@kEdc", SqlDbType.Float).Value = XL.Return.Double(rng);

									//@Eii_MBtuDay				FLOAT = NULL,
									rng = wks.Cells[r, cEii];
									p = cmd.Parameters.Add("@Eii_MBtuDay", SqlDbType.Float);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

									//@Pes_kHours				FLOAT,
									rng = wks.Cells[r, cPes];
									cmd.Parameters.Add("@Pes_kHours", SqlDbType.Float).Value = XL.Return.Double(rng);

									//@Mes_kUsd					FLOAT,
									rng = wks.Cells[r, cMes];
									cmd.Parameters.Add("@Mes_kUsd", SqlDbType.Float).Value = XL.Return.Double(rng);

									//@NeCes_kUsd				FLOAT,
									rng = wks.Cells[r, cCes];
									cmd.Parameters.Add("@NeCes_kUsd", SqlDbType.Float).Value = XL.Return.Double(rng);

									try
									{
										cmd.ExecuteNonQuery();
									}
									catch (SqlException)
									{
									}
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "Factors&Standards", "Factors&Standards", "0", wkb, wks, rng, r, c, "[xls].[Insert_FactorsAndStandards]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}