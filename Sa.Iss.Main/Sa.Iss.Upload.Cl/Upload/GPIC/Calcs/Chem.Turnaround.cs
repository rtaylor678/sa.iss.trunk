﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Calcs
		{
			public partial class Upload
			{
				public static void Turnaround(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["Turnaround"];
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					try
					{
						int cName = 1;
						int cInterval = 2;
						int cDownTime = 3;
						int cCost = 4;
						int cWork = 5;

						List<string[]> TaType = new List<string[]>();

						TaType.Add(new string[2] { "0", "Reported" });
						TaType.Add(new string[2] { "3", "Annualized" });

						foreach (string[] str in TaType)
						{
							int offset = int.Parse(str[0]);
							string durationType = str[1];

							for (int i = 12; i <= 38; i++)
							{
								r = i;

								using (SqlCommand cmd = new SqlCommand("[xls].[Insert_Turnaround]", cn))
								{
									if (XL.RangeHasValue(wks.Cells[r, cName]) &&
										XL.RangeHasValue(wks.Cells[r, cCost + offset]) &&
										XL.RangeHasValue(wks.Cells[r, cWork + offset]))
									{
										cmd.CommandType = CommandType.StoredProcedure;
										SqlParameter p = new SqlParameter();

										//@SubmissionId					INT,
										cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

										//@tsModified					DATETIMEOFFSET
										cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

										//@ProcessUnitDetail			VARCHAR(96),
										rng = wks.Cells[r, cName];
										cmd.Parameters.Add("@ProcessUnitDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

										//@Interval_Years				FLOAT			= NULL,
										rng = wks.Cells[r, cInterval];
										p = cmd.Parameters.Add("@Interval_Years", SqlDbType.Float);
										if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

										//@Duration_Type				VARCHAR(12),
										cmd.Parameters.Add("@Duration_Type", SqlDbType.VarChar, 12).Value = durationType;

										//@Downtime_Hours				FLOAT			= NULL,
										rng = wks.Cells[r, cDownTime + offset];
										p = cmd.Parameters.Add("@Downtime_Hours", SqlDbType.Float);
										if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

										//@Cost_kUsd					FLOAT,
										rng = wks.Cells[r, cCost + offset];
										cmd.Parameters.Add("@Cost_kUsd", SqlDbType.Float).Value = XL.Return.Double(rng);

										//@Work_Hours					FLOAT,
										rng = wks.Cells[r, cWork + offset];
										cmd.Parameters.Add("@Work_Hours", SqlDbType.Float).Value = XL.Return.Double(rng);

										try
										{
											cmd.ExecuteNonQuery();
										}
										catch (SqlException)
										{
										}
									}
								}
							}
						}

						using (SqlCommand cmd = new SqlCommand("[xls].[Insert_TurnaroundEdcAnn]", cn))
						{
							cmd.CommandType = CommandType.StoredProcedure;

							r = 28;

							//@SubmissionId					INT,
							cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;
							//@tsModified					DATETIMEOFFSET
							cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

							//@ProcessUnitDetail			VARCHAR(96),
							rng = wks.Cells[r, cName];
							cmd.Parameters.Add("@ProcessUnitDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

							//@Downtime_HoursEdc			FLOAT,
							rng = wks.Cells[r, 6];
							cmd.Parameters.Add("@Downtime_HoursEdc", SqlDbType.Float).Value = XL.Return.Double(rng);

							try
							{
								cmd.ExecuteNonQuery();
							}
							catch (SqlException)
							{
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "Turnaround", "Turnaround", "0", wkb, wks, rng, r, c, "[xls].[Insert_Turnaround]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}