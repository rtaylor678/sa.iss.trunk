﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Calcs
		{
			public partial class Upload
			{
				public static void Yield(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["Yield"];
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					try
					{
						int cName = 1;
						int cMole = 2;
						int cMass = 3;
						int cWeight = 4;
						int cLHV = 5;

						for (int i = 13; i <= 23; i++)
						{
							r = i;

							using (SqlCommand cmd = new SqlCommand("[xls].[Insert_Yield]", cn))
							{
								cmd.CommandType = CommandType.StoredProcedure;
								SqlParameter p = new SqlParameter();

								//@SubmissionId					INT,
								cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

								//@tsModified					DATETIMEOFFSET
								cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

								//@ComponentDetail				VARCHAR(96),
								rng = wks.Cells[r, cName];
								cmd.Parameters.Add("@ComponentDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

								//@Mole_Pcnt					FLOAT,
								rng = wks.Cells[r, cMole];
								cmd.Parameters.Add("@Mole_Pcnt", SqlDbType.Float).Value = XL.Return.Double(rng);

								//@Mass_MolecularWeight			FLOAT,
								rng = wks.Cells[r, cMass];
								cmd.Parameters.Add("@Mass_MolecularWeight", SqlDbType.Float).Value = XL.Return.Double(rng);

								//@Weight_Pcnt					FLOAT,
								rng = wks.Cells[r, cWeight];
								cmd.Parameters.Add("@Weight_Pcnt", SqlDbType.Float).Value = XL.Return.Double(rng);

								//@LowerHeatingValue_kJNm3		FLOAT,
								rng = wks.Cells[r, cLHV];
								cmd.Parameters.Add("@LowerHeatingValue_kJNm3", SqlDbType.Float).Value = XL.Return.Double(rng);

								try
								{
									cmd.ExecuteNonQuery();
								}
								catch (SqlException)
								{
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "Yield", "Yield", "0", wkb, wks, rng, r, c, "[xls].[Insert_Yield]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}