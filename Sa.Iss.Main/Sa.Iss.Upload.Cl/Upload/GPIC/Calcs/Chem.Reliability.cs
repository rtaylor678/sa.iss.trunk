﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Calcs
		{
			public partial class Upload
			{
				public static void Reliability(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["Reliability"];
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					try
					{
						int cName = 1;
						int cRegulatory = 2;
						int cMechanical = 3;
						int cTotal = 4;
						SqlParameter p = new SqlParameter();

						for (int i = 13; i <= 25; i++)
						{
							r = i;

							using (SqlCommand cmd = new SqlCommand("[xls].[Insert_Reliability]", cn))
							{
								if (XL.RangeHasValue(wks.Cells[r, cName]) &&
									XL.RangeHasValue(wks.Cells[r, cMechanical]) &&
									XL.RangeHasValue(wks.Cells[r, cTotal]))
								{
									cmd.CommandType = CommandType.StoredProcedure;

									//@SubmissionId					INT,
									cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

									//@tsModified					DATETIMEOFFSET
									cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

									//@ProcessUnitDetail			VARCHAR(96),
									rng = wks.Cells[r, cName];
									cmd.Parameters.Add("@ProcessUnitDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

									//@Regulatory_Hours				FLOAT,
									rng = wks.Cells[r, cRegulatory];
									p = cmd.Parameters.Add("@Regulatory_Hours", SqlDbType.Float);
									if (XL.RangeHasValue(rng)) p.Value = XL.Return.Double(rng);

									//@Mechanical_Hours				FLOAT,
									rng = wks.Cells[r, cMechanical];
									cmd.Parameters.Add("@Mechanical_Hours", SqlDbType.Float).Value = XL.Return.Double(rng);

									//@Total_Hours					FLOAT
									rng = wks.Cells[r, cTotal];
									cmd.Parameters.Add("@Total_Hours", SqlDbType.Float).Value = XL.Return.Double(rng);

									try
									{
										cmd.ExecuteNonQuery();
									}
									catch (SqlException)
									{
									}
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "Reliability", "Reliability", "0", wkb, wks, rng, r, c, "[xls].[Insert_Reliability]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}