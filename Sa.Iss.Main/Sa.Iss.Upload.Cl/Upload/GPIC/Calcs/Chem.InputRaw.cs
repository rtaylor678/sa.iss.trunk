﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Chem
	{
		public partial class Calcs
		{
			public partial class Upload
			{
				public static void InputRaw(Queue.Upload queue, SqlConnection cn, Excel.Workbook wkb, Submission submission)
				{
					Excel.Worksheet wks = wkb.Worksheets["Yield"]; ;
					Excel.Range rng = null;

					int r = 0;
					int c = 0;

					const int cName = 1;
					const int cQuantity = 2;

					try
					{
						for (int i = 119; i <= 123; i++)
						{
							r = i;

							using (SqlCommand cmd = new SqlCommand("[xls].[Insert_InputRaw]", cn))
							{
								cmd.CommandType = CommandType.StoredProcedure;

								//@SubmissionId					INT,
								cmd.Parameters.Add("@SubmissionId", SqlDbType.Int).Value = submission.Id;

								//@tsModified					DATETIMEOFFSET
								cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = queue.UploadTime;

								//@InputRawDetail				NVARCHAR(96),
								c = cName;
								rng = wks.Cells[r, c];
								cmd.Parameters.Add("@InputRawDetail", SqlDbType.NVarChar, 96).Value = XL.Return.String(rng, 96);

								//@Quantity_TonnesDay			FLOAT
								c = cQuantity;
								rng = wks.Cells[r, c];
								cmd.Parameters.Add("@Quantity_TonnesDay", SqlDbType.Float).Value = XL.Return.Double(rng);

								try
								{
									cmd.ExecuteNonQuery();
								}
								catch (SqlException)
								{
								}
							}
						}
					}
					catch (Exception ex)
					{
						ErrorHandler.Insert_UpLoadError(queue, "InputRaw", "InputRaw", "0", wkb, wks, rng, r, c, "[xls].[Insert_InputRaw]", ex);
					}
					finally
					{
						wks = null;
						rng = null;
					}
				}
			}
		}
	}
}