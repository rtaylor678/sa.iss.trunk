﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Sa.Iss;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Fuel
	{
		public class InputForm : IStgUpload
		{
			private readonly CpaFile studyFile;

			public InputForm(CpaFile studyFile)
			{
				this.studyFile = studyFile;
			}

			public Submission UploadStudyFile()
			{
				Submission submission = new Submission();

				Excel.Application xla = XL.NewExcelApplication(false);
				Excel.Workbook wkb = XL.OpenWorkbook_ReadOnly(xla, this.studyFile.FullyQualifiedPath, this.studyFile.Password);

				Queue.Upload queue = new Queue.Upload();
				queue.Id = 0;
				queue.UploadTime = DateTimeOffset.Now;                
                /*
                //web svc
                Sa.Iss.Upload.Cl.Upload.ProfileFuelDataset.ProfileFuelDataset profileFuel = new Sa.Iss.Upload.Cl.Upload.ProfileFuelDataset.ProfileFuelDataset();
                Excel.Worksheet wks = wkb.Sheets["Product Yield"];
                Excel.Range rng = null;
                DataRow energyRow = profileFuel.Energy.NewRow();
                rng = wks.Cells[133,7];
                Single butane = XL.Return.Single(rng);           
                rng = wks.Cells[134, 7];
                Single co2 =XL.Return.Single(rng);
                energyRow["Butane"] = butane;
                energyRow["CO2"] = co2;
                profileFuel.Energy.Rows.Add(energyRow);
               
               */
                
                // Close workbook, dispose.
                
                              

                //staging table
				using (SqlConnection cn = new SqlConnection(Sa.Common.cnString()))
				{
					cn.Open();

					cn.Close();
				}

				XL.CloseWorkbook(ref wkb);
				XL.CloseApplication(ref xla);

				return submission;
			}
		}

        private Boolean LoadRecord(DataSet dsProfileFuel,String tableName, DataRow dataRow)
        {
            try
            {
                dsProfileFuel.Tables[tableName].Rows.Add(dataRow);
            }
            catch (Exception ex)
            {
                //need to log error here
                return false;
            }
            return true;
        }

		public class Calculations : IXlsUpload
		{
			private readonly CpaFile studyFile;

			public Calculations(CpaFile studyFile)
			{
				this.studyFile = studyFile;
			}

			public void UploadStudyFile(Submission submission)
			{
				string pathFileCalc = @"ProfileLite";

				Excel.Application xla = XL.NewExcelApplication(false);
				Excel.Workbook wkbInput = XL.OpenWorkbook_ReadOnly(xla, this.studyFile.FullyQualifiedPath, this.studyFile.Password);
				Excel.Workbook wkbCalc = XL.OpenWorkbook_ReadOnly(xla, pathFileCalc);

				Queue.Upload queue = new Queue.Upload();
				queue.Id = 0;
				queue.UploadTime = DateTimeOffset.Now;

				XL.CopyWorkbookValues(wkbInput, wkbCalc);

				using (SqlConnection cn = new SqlConnection(Sa.Common.cnString()))
				{
					cn.Open();

					cn.Close();
				}

				XL.CloseWorkbook(ref wkbInput);
				XL.CloseWorkbook(ref wkbCalc);
				XL.CloseApplication(ref xla);
			}
		}
	}
}