﻿using System;
using System.Data;
using System.Data.SqlClient;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public partial class Deployment
	{
		public partial class GPIC
		{
			string pathFile = @".\Testing\Noga Gpic May 2015.xlsm";
			string password = "lonestar";

			public void UploadSummary()
			{
				DateTime dtBeg = DateTime.Now;

				const int rDate = 10;
				const int cEntity = 1;

				const int rBeg = 11;
				const int rEnd = 44;

				const int cBeg = 14;
				const int cEnd = 41;

				string entity;
				string date;
				double value;

				DateTimeOffset dto = DateTimeOffset.Now;

				Excel.Application xla = XL.NewExcelApplication(false);
				Excel.Workbook wkb = XL.OpenWorkbook_ReadOnly(xla, this.pathFile, this.password);

				Excel.Worksheet wks = wkb.Worksheets["Complex-Level Input"];
				Excel.Range rng = null;

				rng = wks.Cells[9, 4];
				string company = XL.Return.String(rng, 48);

				rng = wks.Cells[10, 4];
				string facility = XL.Return.String(rng, 48);

				using (SqlConnection cn = new SqlConnection(Sa.Common.cnString()))
				{
					cn.Open();

					wks = wkb.Worksheets["Summary"];

					for (int r = rBeg; r <= rEnd; r++)
					{
						rng = wks.Cells[r, cEntity];

						if (XL.RangeHasValue(rng))
						{
							entity = XL.Return.String(rng, 96);

							for (int c = cBeg; c <= cEnd; c++)
							{
								rng = wks.Cells[rDate, c];

								if (XL.RangeHasValue(rng))
								{
									date = XL.Return.String(rng, 24);

									rng = null;
									rng = wks.Cells[r, c];
									value = XL.Return.Double(rng);

									using (SqlCommand cmd = new SqlCommand("[dep].[Insert_Summary]", cn))
									{
										cmd.CommandType = CommandType.StoredProcedure;
										cmd.Parameters.Add("@CompanyName", SqlDbType.NVarChar, 48).Value = company;
										cmd.Parameters.Add("@FacilityName", SqlDbType.NVarChar, 48).Value = facility;

										cmd.Parameters.Add("@EntityDetail", SqlDbType.NVarChar, 96).Value = entity;
										cmd.Parameters.Add("@HistDate", SqlDbType.VarChar, 24).Value = date;
										cmd.Parameters.Add("@Value", SqlDbType.VarChar, 12).Value = value;
										cmd.Parameters.Add("@tsModified", SqlDbType.DateTimeOffset).Value = dto;

										try
										{
											cmd.ExecuteNonQuery();
										}
										catch (SqlException)
										{
										}
									}
								}
							}
						}
					}

					using (SqlCommand cmd = new SqlCommand("[dep].[Inject_OutputFacilities]", cn))
					{
						cmd.CommandType = CommandType.StoredProcedure;
						try
						{
							cmd.ExecuteNonQuery();
						}
						catch (SqlException)
						{
						}
					}
					cn.Close();
				}
				rng = null;
				wks = null;

				XL.CloseWorkbook(ref wkb);
				XL.CloseApplication(ref xla);

				DateTime dtEnd = DateTime.Now;
				System.Windows.Forms.MessageBox.Show("DONE" + "\n\r" + dtBeg.ToString() + "\n\r" + dtEnd.ToString());
			}
		}
	}
}
