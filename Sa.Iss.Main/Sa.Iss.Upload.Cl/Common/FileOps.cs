﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public class FileOps
	{
		//public const string Osim  = "OSIM";
		//public const string Spsl = "SPSL";
		//public const string Pyps = "PYPS";
		//public const string Seec = "SEEC";

		//public static string GetFileType(string PathFile)
		//{
		//	List<string[]> l = new List<string[]>();

		//	string xlsExtension = "(\\.xls)";

		//	l.Add(new string[2] { "(PYPS)(([0-9]{4})(PCH)([0-9]{2}[0-9a-z]))" + xlsExtension, Common.Pyps });
		//	l.Add(new string[2] { "(SPSL)(([0-9]{4})(PCH)([0-9]{2}[0-9a-z]))" + xlsExtension, Common.Spsl });
		//	l.Add(new string[2] { "(OSIM)(([0-9]{4})(PCH)([0-9]{2}[0-9a-z]))" + xlsExtension, Common.Osim });

		//	l.Add(new string[2] { "(PYPS)(([0-9]{4})(PCH)([0-9]{2}[0-9a-z]{2}))" + xlsExtension, Common.Pyps });
		//	l.Add(new string[2] { "(SPSL)(([0-9]{4})(PCH)([0-9]{2}[0-9a-z]{2}))" + xlsExtension, Common.Spsl });
		//	l.Add(new string[2] { "(OSIM)(([0-9]{4})(PCH)([0-9]{2}[0-9a-z]{2}))" + xlsExtension, Common.Osim });

		//	l.Add(new string[2] { "(OSIM)(([0-9]{4})(SEEC)([0-9]{2}[0-9a-z]))" + xlsExtension, Common.Seec });
		//	l.Add(new string[2] { "(OSIM)(([0-9]{4})(SEEC)([0-9]{2}[0-9a-z]{2}))" + xlsExtension, Common.Seec });

		//	System.Text.RegularExpressions.Regex r;

		//	foreach (string[] s in l)
		//	{
		//		r = new Regex(s[0], RegexOptions.IgnoreCase);
		//		if (r.IsMatch(PathFile))
		//		{
		//			return s[1];
		//		}
		//	}
		//	return string.Empty;
		//}

		//public static string GetPathRoot(string PathFile)
		//{
		//	string FileType = GetFileType(PathFile);
		//	string PathRoot = string.Empty;

		//	if (FileType == Common.Osim || FileType == Common.Spsl || FileType == Common.Pyps)
		//	{
		//		PathRoot = "Olefins";
		//	}
		//	else if (FileType == Common.Seec)
		//	{
		//		PathRoot = "SEEC";
		//	}

		//	PathRoot = "K:\\STUDY\\" + PathRoot + "\\";

		//	return PathRoot;
		//}

		//public static void CopyFile(string PathFile, string Refnum, uint StudyYear)
		//{
		//	string PathRoot = GetPathRoot(PathFile);

		//	CopyFileToCorrespondence(PathRoot, PathFile, Refnum, StudyYear);
		//	CopyFileToPlants(PathRoot, PathFile, Refnum, StudyYear);
		//}

		//public static void CopyFileToCorrespondence(string PathRoot, string PathFile, string Refnum, uint StudyYear)
		//{
		//	string PathTarget = PathRoot + StudyYear.ToString() + "\\Correspondence\\" + Sa.Common.GetRefnumSmall(Refnum) + "\\Uploads\\";
		//	string FileTarget = System.IO.Path.GetFileNameWithoutExtension(PathFile) + " " + Sa.Common.GetDateTimeStamp() + "." + System.IO.Path.GetExtension(PathFile);

		//	System.IO.Directory.CreateDirectory(PathTarget);
		//	System.IO.File.Copy(PathFile, PathTarget + FileTarget, false);
		//}

		//public static void CopyFileToPlants(string PathRoot, string PathFile, string Refnum, uint StudyYear)
		//{
		//	string PathTarget = PathRoot + StudyYear.ToString() + "\\Plants\\" + Common.GetRefnumSmall(Refnum) + "\\";
		//	string FileTarget = System.IO.Path.GetFileName(PathFile);

		//	System.IO.Directory.CreateDirectory(PathTarget);
		//	System.IO.File.Copy(PathFile, PathTarget + FileTarget, true);
		//}
	}
}