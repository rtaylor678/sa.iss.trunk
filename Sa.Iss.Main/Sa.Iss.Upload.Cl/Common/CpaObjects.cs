﻿using System;
using System.Collections.Generic;

using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;

namespace Sa.Iss
{
	public class CpaFile
	{
		private readonly string fullyQualifiedPath;
		private readonly string password;
		private CpaForm.CpaFormType cpaFormType = CpaForm.CpaFormType.None;

		public CpaFile(string fullyQualifiedPath)
		{
			this.fullyQualifiedPath = fullyQualifiedPath;
		}

		public CpaFile(string fullyQualifiedPath, string password)
		{
			this.fullyQualifiedPath = fullyQualifiedPath;
			this.password = password;
		}

		public string Path
		{
			get
			{
				return System.IO.Path.GetDirectoryName(this.fullyQualifiedPath);
			}
		}

		public string FullyQualifiedPath
		{
			get
			{
				return this.fullyQualifiedPath;
			}
		}

		public string Extension
		{
			get
			{
				return System.IO.Path.GetExtension(this.fullyQualifiedPath);
			}
		}

		public string FileNameExtension
		{
			get
			{
				return System.IO.Path.GetFileName(this.fullyQualifiedPath);
			}
		}

		public string FileName
		{
			get
			{
				return System.IO.Path.GetFileNameWithoutExtension(this.fullyQualifiedPath);
			}
		}

		public string Password
		{
			get
			{
				return this.password;
			}
		}

		public CpaForm.CpaFormType CpaFormType
		{
			get
			{
				if (this.cpaFormType != CpaForm.CpaFormType.None)
				{
					return this.cpaFormType;
				}
				else
				{
					this.cpaFormType = CpaForm.FormType(this);
					return this.cpaFormType;
				}
			}
		}
	}

	public class CpaForm
	{
		public enum CpaFormType
		{
			@None,
			@Chem,
			@Fuel,
			@Lube
		}

		public static CpaFormType FormType(CpaFile file)
		{
			CpaFormType cpaFormType = CpaFormType.None;

			Excel.Application xla = XL.NewExcelApplication(false);
			Excel.Workbook wkb = XL.OpenWorkbook_ReadOnly(xla, file.FullyQualifiedPath, file.Password);

			if (IsChem(wkb)) cpaFormType = CpaFormType.Chem;
			if (IsFuel(wkb)) cpaFormType = CpaFormType.Fuel;
			if (IsLube(wkb)) cpaFormType = CpaFormType.Lube;

			XL.CloseWorkbook(ref wkb);
			XL.CloseApplication(ref xla);

			return cpaFormType;
		}

		private static bool IsChem(Excel.Workbook wkb)
		{
			List<string> Tabs = new List<string>();
			Tabs.Add("Complex-Level Input");
			Tabs.Add("Process-Level Input");
			Tabs.Add("Raw Material Input");
			Tabs.Add("Product Yield");
			Tabs.Add("Hydrocarbon Loss");
			Tabs.Add("Sheet1");
			Tabs.Add("Reference");
			Tabs.Add("Upload");
			Tabs.Add("Download");

			int i = 0;

			foreach (string t in Tabs)
			{
				if (XL.WorksheetExists(wkb, t)) i++;
			}

			return (Tabs.Count == i && Tabs.Count == wkb.Worksheets.Count);
		}

		private static bool IsFuel(Excel.Workbook wkb)
		{
			List<string> Tabs = new List<string>();
			Tabs.Add("Refinery-Level Input");
			Tabs.Add("Process-Level Input");
			Tabs.Add("Crude Charge Details");
			Tabs.Add("Raw Material Input");
			Tabs.Add("Product Yield");
			Tabs.Add("Hydrocarbon Loss");

			int i = 0;

			foreach (string t in Tabs)
			{
				if (XL.WorksheetExists(wkb, t)) i++;
			}

			return (Tabs.Count == i && Tabs.Count == wkb.Worksheets.Count);
		}

		private static bool IsLube(Excel.Workbook wkb)
		{
			List<string> Tabs = new List<string>();
			Tabs.Add("Refinery-Level Input");
			Tabs.Add("Process-Level Input");
			Tabs.Add("Hydrocarbon Loss");
			Tabs.Add("Workspace");

			int i = 0;

			foreach (string t in Tabs)
			{
				if (XL.WorksheetExists(wkb, t)) i++;
			}

			return (Tabs.Count == i && Tabs.Count == wkb.Worksheets.Count);
		}
	}

	public class CpaInputChem : CpaForm
	{
	}

	public class CpaInputFuel : CpaForm
	{
	}

	public class CpaInputLube : CpaForm
	{
	}

	public interface IStgUpload
	{
		Submission UploadStudyFile();
	}

	public interface IXlsUpload
	{
		void UploadStudyFile(Submission submission);
	}
}
