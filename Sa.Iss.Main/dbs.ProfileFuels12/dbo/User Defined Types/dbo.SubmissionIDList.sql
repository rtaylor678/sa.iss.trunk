﻿CREATE TYPE [dbo].[SubmissionIDList] AS TABLE (
    [SubmissionID] INT NOT NULL,
    PRIMARY KEY CLUSTERED ([SubmissionID] ASC));

