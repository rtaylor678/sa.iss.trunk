﻿CREATE TABLE [dbo].[CurrenciesToCalc] (
    [RefineryID] CHAR (6)    NOT NULL,
    [Currency]   VARCHAR (5) NOT NULL,
    CONSTRAINT [PK_CurrenciesToCalc] PRIMARY KEY CLUSTERED ([RefineryID] ASC, [Currency] ASC) WITH (FILLFACTOR = 90)
);

