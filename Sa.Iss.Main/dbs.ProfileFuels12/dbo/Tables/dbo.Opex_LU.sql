﻿CREATE TABLE [dbo].[OpEx_LU] (
    [Description]   VARCHAR (256) NULL,
    [OpExID]        VARCHAR (50)  NULL,
    [SortKey]       INT           NULL,
    [Indent]        TINYINT       NULL,
    [ParentID]      VARCHAR (50)  NULL,
    [DetailStudy]   CHAR (5)      NULL,
    [DetailProfile] CHAR (5)      NULL
);

