﻿CREATE TABLE [dbo].[OpExRpt] (
    [OpExID]         INT             IDENTITY (1, 1) NOT NULL,
    [SubmissionID]   INT             NOT NULL,
    [OpExCategory]   VARCHAR (50)    NOT NULL,
    [RptValue]       NUMERIC (18, 4) NULL,
    [OthDescription] NVARCHAR (400)  NULL,
    CONSTRAINT [PK_OpExRpt] PRIMARY KEY NONCLUSTERED ([OpExID] ASC)
);

