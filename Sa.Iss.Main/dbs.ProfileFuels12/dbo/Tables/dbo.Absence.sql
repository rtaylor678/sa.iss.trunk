﻿CREATE TABLE [dbo].[Absence] (
    [SubmissionID] INT      NOT NULL,
    [SortKey]      SMALLINT NOT NULL,
    [CategoryID]   CHAR (6) NOT NULL,
    [OCCAbs]       REAL     NULL,
    [MPSAbs]       REAL     NULL,
    [TotAbs]       REAL     NULL,
    [OCCPcnt]      REAL     NULL,
    [MPSPcnt]      REAL     NULL,
    [TotPcnt]      REAL     NULL,
    CONSTRAINT [PK_Absence_1__12] PRIMARY KEY NONCLUSTERED ([SubmissionID] ASC, [CategoryID] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE CLUSTERED INDEX [AbsSortKey]
    ON [dbo].[Absence]([SubmissionID] ASC, [SortKey] ASC) WITH (FILLFACTOR = 90);

