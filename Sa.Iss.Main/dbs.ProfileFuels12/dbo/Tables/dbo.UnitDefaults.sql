﻿CREATE TABLE [dbo].[UnitDefaults] (
    [RefineryID] CHAR (6)       NOT NULL,
    [UnitID]     [dbo].[UnitID] NOT NULL,
    [EffDate]    SMALLDATETIME  NOT NULL,
    [EffUntil]   SMALLDATETIME  NOT NULL,
    [Ratio]      REAL           NOT NULL,
    CONSTRAINT [PK_UnitDefaults] PRIMARY KEY CLUSTERED ([RefineryID] ASC, [UnitID] ASC, [EffDate] ASC) WITH (FILLFACTOR = 90)
);

