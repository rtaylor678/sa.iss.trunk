﻿CREATE TABLE [dbo].[MaintTotCost] (
    [SubmissionID]      INT                  NOT NULL,
    [Currency]          [dbo].[CurrencyCode] NOT NULL,
    [AnnTACost]         REAL                 NULL,
    [AnnRoutCost]       REAL                 NULL,
    [AnnMaintCost]      REAL                 NULL,
    [AnnTAMatl]         REAL                 NULL,
    [AnnRoutMatl]       REAL                 NULL,
    [AnnMaintMatl]      REAL                 NULL,
    [CurrTACost]        REAL                 NULL,
    [CurrRoutCost]      REAL                 NULL,
    [CurrMaintCost]     REAL                 NULL,
    [CurrTAMatl]        REAL                 NULL,
    [CurrRoutMatl]      REAL                 NULL,
    [CurrMaintMatl]     REAL                 NULL,
    [AllocAnnTACost]    REAL                 NULL,
    [AllocAnnTAMatl]    REAL                 NULL,
    [AllocCurrRoutCost] REAL                 NULL,
    [AllocCurrRoutMatl] REAL                 NULL,
    CONSTRAINT [PK_MaintTotCost] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Currency] ASC) WITH (FILLFACTOR = 90)
);

