﻿CREATE TABLE [dbo].[EnergyTransType_LU] (
    [TransType]   CHAR (3)     NOT NULL,
    [Description] VARCHAR (30) NULL,
    CONSTRAINT [PK_EnergyTransType_LU_1__10] PRIMARY KEY CLUSTERED ([TransType] ASC) WITH (FILLFACTOR = 90)
);

