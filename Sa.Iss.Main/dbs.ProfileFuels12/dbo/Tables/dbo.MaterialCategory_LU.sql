﻿CREATE TABLE [dbo].[MaterialCategory_LU] (
    [Category]     [dbo].[YieldCategory] NOT NULL,
    [CategoryName] VARCHAR (40)          NULL,
    [GrossGroup]   CHAR (2)              NULL,
    [SortKey]      SMALLINT              NULL,
    [NetGroup]     CHAR (2)              NULL,
    [FuelsInput]   BIT                   DEFAULT ((1)) NOT NULL,
    [LubesInput]   BIT                   DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MatCategory_LU_1__14] PRIMARY KEY NONCLUSTERED ([Category] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE CLUSTERED INDEX [MatCategorySortKey]
    ON [dbo].[MaterialCategory_LU]([SortKey] ASC) WITH (FILLFACTOR = 90);

