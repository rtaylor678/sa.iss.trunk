﻿CREATE TABLE [dbo].[GeneralMisc] (
    [SubmissionID] INT  NOT NULL,
    [FlareLossMT]  REAL NULL,
    [TotLossMT]    REAL NULL,
    CONSTRAINT [PK_GeneralMisc] PRIMARY KEY CLUSTERED ([SubmissionID] ASC) WITH (FILLFACTOR = 70)
);

