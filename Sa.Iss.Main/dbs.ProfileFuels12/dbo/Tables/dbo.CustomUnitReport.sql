﻿CREATE TABLE [dbo].[CustomUnitReport] (
    [CompanyID]      VARCHAR (10)      NOT NULL,
    [ProcessID]      [dbo].[ProcessID] NOT NULL,
    [SortKey]        SMALLINT          NOT NULL,
    [Property]       VARCHAR (30)      NOT NULL,
    [USDescription]  VARCHAR (100)     NOT NULL,
    [USDecPlaces]    TINYINT           NOT NULL,
    [MetDescription] VARCHAR (100)     NOT NULL,
    [MetDecPlaces]   TINYINT           NOT NULL,
    CONSTRAINT [PK_CustomUnitReport] PRIMARY KEY CLUSTERED ([CompanyID] ASC, [ProcessID] ASC, [Property] ASC) WITH (FILLFACTOR = 90)
);

