﻿CREATE TABLE [dbo].[AbsenceTot] (
    [SubmissionID] INT  NOT NULL,
    [OCCAbs]       REAL NULL,
    [MPSAbs]       REAL NULL,
    [TotAbs]       REAL NULL,
    [OCCPcnt]      REAL NULL,
    [MPSPcnt]      REAL NULL,
    [TotPcnt]      REAL NULL,
    CONSTRAINT [PK_AbsenceTot] PRIMARY KEY CLUSTERED ([SubmissionID] ASC) WITH (FILLFACTOR = 90)
);

