﻿CREATE TABLE [dbo].[Resid] (
    [SubmissionID] INT                NOT NULL,
    [BlendID]      INT                NOT NULL,
    [Grade]        [dbo].[MaterialID] NULL,
    [Gravity]      REAL               NULL,
    [Density]      REAL               NULL,
    [Sulfur]       REAL               NULL,
    [SulfurSpec]   REAL               NULL,
    [PourPT]       REAL               NULL,
    [ViscCS]       REAL               NULL,
    [ViscCSAtTemp] REAL               NULL,
    [ViscTemp]     REAL               NULL,
    [ViscCSSpec]   REAL               NULL,
    [Vanadium]     REAL               NULL,
    [KBbl]         REAL               NULL,
    [KMT]          REAL               NULL,
    CONSTRAINT [PK_Resid_1__14] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [BlendID] ASC) WITH (FILLFACTOR = 70)
);

