﻿CREATE TABLE [dbo].[RPFResid] (
    [SubmissionID] INT                NOT NULL,
    [EnergyType]   [dbo].[EnergyType] NOT NULL,
    [Gravity]      REAL               NULL,
    [Sulfur]       REAL               NULL,
    [ViscCS]       REAL               NULL,
    [ViscTemp]     REAL               NULL,
    [ViscCSAtTemp] REAL               NULL,
    [Density]      REAL               NULL,
    CONSTRAINT [PK_RPFResid] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [EnergyType] ASC) WITH (FILLFACTOR = 70)
);

