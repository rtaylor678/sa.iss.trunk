﻿CREATE TABLE [dbo].[MaterialTotCost] (
    [SubmissionID]           INT                  NOT NULL,
    [Scenario]               [dbo].[Scenario]     NOT NULL,
    [Currency]               [dbo].[CurrencyCode] NOT NULL,
    [RawMatCost]             REAL                 NULL,
    [ProdValue]              REAL                 NULL,
    [ThirdPartyTerminalRM]   REAL                 NULL,
    [ThirdPartyTerminalProd] REAL                 NULL,
    [POXO2]                  REAL                 NULL,
    [CogenCredit]            REAL                 NULL,
    [ARCPremium]             REAL                 NULL,
    [NetLWSRawMatCost]       REAL                 NULL,
    [LWSProdValue]           REAL                 NULL,
    [ByProdValue]            REAL                 NULL,
    [LWSRawMatCost]          REAL                 NULL,
    CONSTRAINT [PK_MatTotCost] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Scenario] ASC, [Currency] ASC) WITH (FILLFACTOR = 90)
);

