﻿CREATE TABLE [dbo].[CrudeTotPrice] (
    [SubmissionID] INT              NOT NULL,
    [Scenario]     [dbo].[Scenario] NOT NULL,
    [AvgCost]      REAL             NULL,
    CONSTRAINT [PK_CrudeTotPrice] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Scenario] ASC) WITH (FILLFACTOR = 90)
);

