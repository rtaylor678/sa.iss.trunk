﻿CREATE TABLE [dbo].[GasProperties] (
    [Property]  VARCHAR (12) NOT NULL,
    [Hydrogen]  REAL         NULL,
    [Methane]   REAL         NULL,
    [Ethane]    REAL         NULL,
    [Ethylene]  REAL         NULL,
    [Propane]   REAL         NULL,
    [Propylene] REAL         NULL,
    [Butane]    REAL         NULL,
    [Isobutane] REAL         NULL,
    [Butylenes] REAL         NULL,
    [C5Plus]    REAL         NULL,
    [CO]        REAL         NULL,
    [CO2]       REAL         NULL,
    [N2]        REAL         NULL,
    [OthInerts] REAL         NULL,
    [Inerts]    REAL         NULL,
    CONSTRAINT [PK_GasProperties] PRIMARY KEY CLUSTERED ([Property] ASC) WITH (FILLFACTOR = 90)
);

