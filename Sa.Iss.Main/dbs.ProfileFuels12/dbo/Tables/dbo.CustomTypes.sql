﻿CREATE TABLE [dbo].[CustomTypes] (
    [CustomType] VARCHAR (3)  NOT NULL,
    [CustomDesc] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_CustomTypes] PRIMARY KEY CLUSTERED ([CustomType] ASC) WITH (FILLFACTOR = 90)
);

