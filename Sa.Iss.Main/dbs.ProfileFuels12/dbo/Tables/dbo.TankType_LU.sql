﻿CREATE TABLE [dbo].[TankType_LU] (
    [TankType]    CHAR (3)  NOT NULL,
    [Description] CHAR (40) NULL,
    [SortKey]     TINYINT   NULL,
    CONSTRAINT [PK___5__15] PRIMARY KEY CLUSTERED ([TankType] ASC) WITH (FILLFACTOR = 90)
);

