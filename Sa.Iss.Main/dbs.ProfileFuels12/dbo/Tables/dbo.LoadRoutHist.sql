﻿CREATE TABLE [dbo].[LoadRoutHist] (
    [SubmissionID]  INT           NOT NULL,
    [PeriodStart]   SMALLDATETIME NOT NULL,
    [RoutCostLocal] REAL          NULL,
    [RoutMatlLocal] REAL          NULL,
    CONSTRAINT [PK_LoadRoutHist_1] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [PeriodStart] ASC)
);

