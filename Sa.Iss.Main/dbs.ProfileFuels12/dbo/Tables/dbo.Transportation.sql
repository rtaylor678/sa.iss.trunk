﻿CREATE TABLE [dbo].[Transportation] (
    [StudyYear]   SMALLINT      NOT NULL,
    [CrudeOrigin] SMALLINT      NOT NULL,
    [CrudeDest]   SMALLINT      NOT NULL,
    [AdjType]     CHAR (2)      NOT NULL,
    [AdjValue]    REAL          NULL,
    [SaveDate]    SMALLDATETIME CONSTRAINT [DF_Trans_SaveDate_3__13] DEFAULT (getdate()) NOT NULL,
    [AdjValueMT]  REAL          NULL,
    CONSTRAINT [PK_Trans_2__13] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [CrudeOrigin] ASC, [CrudeDest] ASC, [AdjType] ASC) WITH (FILLFACTOR = 90)
);

