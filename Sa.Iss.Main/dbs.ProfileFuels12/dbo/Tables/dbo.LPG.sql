﻿CREATE TABLE [dbo].[LPG] (
    [SubmissionID] INT      NOT NULL,
    [BlendID]      INT      NOT NULL,
    [MolOrVol]     CHAR (1) NULL,
    [VolC2Lt]      REAL     NULL,
    [VolC3]        REAL     NULL,
    [VolC3ene]     REAL     NULL,
    [VoliC4]       REAL     NULL,
    [VolnC4]       REAL     NULL,
    [VolC4ene]     REAL     NULL,
    [VolC5Plus]    REAL     NULL,
    [MolC2Lt]      REAL     NULL,
    [MolC3]        REAL     NULL,
    [MolC3ene]     REAL     NULL,
    [MoliC4]       REAL     NULL,
    [MolnC4]       REAL     NULL,
    [MolC4ene]     REAL     NULL,
    [MolC5Plus]    REAL     NULL,
    [KBbl]         REAL     NULL,
    [KMT]          REAL     NULL,
    CONSTRAINT [PK_LPG] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [BlendID] ASC)
);

