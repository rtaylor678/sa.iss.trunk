﻿CREATE TABLE [dbo].[InventoryTot] (
    [SubmissionID]  INT        NOT NULL,
    [TotStorage]    FLOAT (53) NULL,
    [MktgStorage]   FLOAT (53) NULL,
    [MandStorage]   FLOAT (53) NULL,
    [FuelsStorage]  FLOAT (53) NULL,
    [NumTank]       SMALLINT   NULL,
    [Inven]         REAL       NULL,
    [LeasedPcnt]    REAL       NULL,
    [AvgLevel]      REAL       NULL,
    [AvgVesselSize] REAL       NULL,
    CONSTRAINT [PK_InventoryTot] PRIMARY KEY CLUSTERED ([SubmissionID] ASC) WITH (FILLFACTOR = 90)
);

