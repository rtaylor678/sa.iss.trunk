﻿CREATE TABLE [dbo].[UnitList] (
    [RefineryID] CHAR (6)       NOT NULL,
    [UnitID]     [dbo].[UnitID] NOT NULL,
    [TAID]       INT            NOT NULL,
    CONSTRAINT [PK_UnitList] PRIMARY KEY CLUSTERED ([RefineryID] ASC, [UnitID] ASC) WITH (FILLFACTOR = 90)
);

