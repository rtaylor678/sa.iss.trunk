﻿CREATE TABLE [dbo].[ProfileLiteScorecardItems] (
    [RefineryID] VARCHAR (6)   NOT NULL,
    [ChartTitle] VARCHAR (100) NOT NULL,
    [SortKey]    SMALLINT      NULL,
    CONSTRAINT [PK_ProfileLiteScorecardItems] PRIMARY KEY CLUSTERED ([RefineryID] ASC, [ChartTitle] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_ProfileLiteScorecardItems_Chart_LU] FOREIGN KEY ([ChartTitle]) REFERENCES [dbo].[Chart_LU] ([ChartTitle])
);

