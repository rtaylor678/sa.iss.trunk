﻿


CREATE   PROC [dbo].[spReportGazpromKPICalc] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15), 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @UOM varchar(5),
	@EII real = NULL OUTPUT, @EII_QTR real = NULL OUTPUT, @EII_Avg real = NULL OUTPUT, 
	@EnergyUseDay real = NULL OUTPUT, @EnergyUseDay_QTR real = NULL OUTPUT, @EnergyUseDay_Avg real = NULL OUTPUT, 
	@TotStdEnergy real = NULL OUTPUT, @TotStdEnergy_QTR real = NULL OUTPUT, @TotStdEnergy_Avg real = NULL OUTPUT, 
	@RefUtilPcnt real = NULL OUTPUT, @RefUtilPcnt_QTR real = NULL OUTPUT, @RefUtilPcnt_Avg real = NULL OUTPUT, 
	@EDC real = NULL OUTPUT, @EDC_QTR real = NULL OUTPUT, @EDC_Avg real = NULL OUTPUT, 
	@UEDC real = NULL OUTPUT, @UEDC_QTR real = NULL OUTPUT, @UEDC_Avg real = NULL OUTPUT, 
	@VEI real = NULL OUTPUT, @VEI_QTR real = NULL OUTPUT, @VEI_Avg real = NULL OUTPUT, 
	@ReportLossGain real = NULL OUTPUT, @ReportLossGain_QTR real = NULL OUTPUT, @ReportLossGain_Avg real = NULL OUTPUT, 
	@EstGain real = NULL OUTPUT, @EstGain_QTR real = NULL OUTPUT, @EstGain_Avg real = NULL OUTPUT, 
	@OpAvail real = NULL OUTPUT, @OpAvail_QTR real = NULL OUTPUT, @OpAvail_Avg real = NULL OUTPUT, 
	@MechUnavailTA real = NULL OUTPUT, @MechUnavailTA_QTR real = NULL OUTPUT, @MechUnavailTA_Avg real = NULL OUTPUT, 
	@NonTAUnavail real = NULL OUTPUT, @NonTAUnavail_QTR real = NULL OUTPUT, @NonTAUnavail_Avg real = NULL OUTPUT, 
	@RoutIndex real = NULL OUTPUT, @RoutIndex_QTR real = NULL OUTPUT, @RoutIndex_Avg real = NULL OUTPUT,
	@RoutCost real = NULL OUTPUT, @RoutCost_QTR real = NULL OUTPUT, @RoutCost_Avg real = NULL OUTPUT, 
	@PersIndex real = NULL OUTPUT, @PersIndex_QTR real = NULL OUTPUT, @PersIndex_Avg real = NULL OUTPUT, 
	@AnnTAWHr real = NULL OUTPUT, @AnnTAWHr_QTR real = NULL OUTPUT, @AnnTAWHr_Avg real = NULL OUTPUT,
	@NonTAWHr real = NULL OUTPUT, @NonTAWHr_QTR real = NULL OUTPUT, @NonTAWHr_Avg real = NULL OUTPUT,
	@NEOpExEDC real = NULL OUTPUT, @NEOpExEDC_QTR real = NULL OUTPUT, @NEOpExEDC_Avg real = NULL OUTPUT, 
	@NEOpEx real = NULL OUTPUT, @NEOpEx_QTR real = NULL OUTPUT, @NEOpEx_Avg real = NULL OUTPUT, 
	@OpExUEDC real = NULL OUTPUT, @OpExUEDC_QTR real = NULL OUTPUT, @OpExUEDC_Avg real = NULL OUTPUT, 
	@TAAdj real = NULL OUTPUT, @TAAdj_QTR real = NULL OUTPUT, @TAAdj_Avg real = NULL OUTPUT,
	@EnergyCost real = NULL OUTPUT, @EnergyCost_QTR real = NULL OUTPUT, @EnergyCost_Avg real = NULL OUTPUT, 
	@TotCashOpEx real = NULL OUTPUT, @TotCashOpEx_QTR real = NULL OUTPUT, @TotCashOpEx_Avg real = NULL OUTPUT
	)
AS

SELECT @EII = NULL, @EII_QTR = NULL, @EII_Avg = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_QTR = NULL, @EnergyUseDay_Avg = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_QTR = NULL, @TotStdEnergy_Avg = NULL, 
	@RefUtilPcnt = NULL, @RefUtilPcnt_QTR = NULL, @RefUtilPcnt_Avg = NULL, 
	@EDC = NULL, @EDC_QTR = NULL, @EDC_Avg = NULL, 
	@UEDC = NULL, @UEDC_QTR = NULL, @UEDC_Avg = NULL, 
	@VEI = NULL, @VEI_QTR = NULL, @VEI_Avg = NULL, 
	@ReportLossGain = NULL, @ReportLossGain_QTR = NULL, @ReportLossGain_Avg = NULL, 
	@EstGain = NULL, @EstGain_QTR = NULL, @EstGain_Avg = NULL, 
	@OpAvail = NULL, @OpAvail_QTR = NULL, @OpAvail_Avg = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_QTR = NULL, @MechUnavailTA_Avg = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_QTR = NULL, @NonTAUnavail_Avg = NULL, 
	@RoutIndex = NULL, @RoutIndex_QTR = NULL, @RoutIndex_Avg = NULL,
	@RoutCost = NULL, @RoutCost_QTR = NULL, @RoutCost_Avg = NULL, 
	@PersIndex = NULL, @PersIndex_QTR = NULL, @PersIndex_Avg = NULL, 
	@AnnTAWHr = NULL, @AnnTAWHr_QTR = NULL, @AnnTAWHr_Avg = NULL,
	@NonTAWHr = NULL, @NonTAWHr_QTR = NULL, @NonTAWHr_Avg = NULL,
	@NEOpExEDC = NULL, @NEOpExEDC_QTR = NULL, @NEOpExEDC_Avg = NULL, 
	@NEOpEx = NULL, @NEOpEx_QTR = NULL, @NEOpEx_Avg = NULL, 
	@OpExUEDC = NULL, @OpExUEDC_QTR = NULL, @OpExUEDC_Avg = NULL, 
	@TAAdj = NULL, @TAAdj_QTR = NULL, @TAAdj_Avg = NULL,
	@EnergyCost = NULL, @EnergyCost_QTR = NULL, @EnergyCost_Avg = NULL, 
	@TotCashOpEx = NULL, @TotCashOpEx_QTR = NULL, @TotCashOpEx_Avg = NULL

SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
SELECT @Start3Mo = DATEADD(mm, -3, @PeriodEnd), @Start12Mo = DATEADD(mm, -12, @PeriodEnd), @StartYTD = dbo.BuildDate(DATEPART(yy, @PeriodStart), 1, 1), @Start24Mo = DATEADD(mm, -24, @PeriodEnd)
IF DATEPART(yy, @Start3Mo) < 2010
	SET @Start3Mo = '12/31/2009'
IF DATEPART(yy, @Start12Mo) < 2010
	SET @Start12Mo = '12/31/2009'
DECLARE	
	@ProcessUtilPcnt real, @ProcessUtilPcnt_QTR real, @ProcessUtilPcnt_Avg real, 
	@TotProcessEDC real, @TotProcessEDC_QTR real, @TotProcessEDC_Avg real, 
	@TotProcessUEDC real, @TotProcessUEDC_QTR real, @TotProcessUEDC_Avg real, 

	@TotMaintForceWHrEDC real, @TotMaintForceWHrEDC_QTR real, @TotMaintForceWHrEDC_Avg real, 
	@MaintTAWHr real, @MaintTAWHr_QTR real, @MaintTAWHr_Avg real, 
	@MaintNonTAWHr real, @MaintNonTAWHr_QTR real, @MaintNonTAWHr_Avg real, 

	@MaintIndex real, @MaintIndex_QTR real, @MaintIndex_Avg real, 
	@AnnTACost real, @AnnTACost_QTR real, @AnnTACost_Avg real


--- Everything Already Available in GenSum
SELECT	@RefUtilPcnt = UtilPcnt, @RefUtilPcnt_Avg = UtilPcnt_Avg,
	@ProcessUtilPcnt = ProcessUtilPcnt, @ProcessUtilPcnt_Avg = ProcessUtilPcnt_Avg,
	@OpAvail = OpAvail, @OpAvail_Avg = OpAvail_Avg,
	@EII = EII, @EII_Avg = EII_Avg,
	@VEI = VEI, @VEI_Avg = VEI_Avg,
	@PersIndex = TotWHrEDC, @PersIndex_Avg = TotWHrEDC_Avg,
	@EDC = EDC/1000, @EDC_Avg = EDC_Avg/1000,
	@UEDC = UEDC/1000, @UEDC_Avg = UEDC_Avg/1000,
	@TotMaintForceWHrEDC = TotMaintForceWHrEDC, @TotMaintForceWHrEDC_Avg = TotMaintForceWHrEDC_Avg,
	@MaintIndex = RoutIndex + TAIndex_Avg, @MaintIndex_Avg = MaintIndex_Avg,
	@RoutIndex = RoutIndex, @RoutIndex_Avg = RoutIndex_Avg,
	@NEOpExEDC = NEOpExEDC, @NEOpExEDC_Avg = NEOpExEDC_Avg
FROM GenSum
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = 'CLIENT'

--- Everything Already Available in MaintAvailCalc
SELECT	@MechUnavailTA = MechUnavailTA_Ann, @MechUnavailTA_QTR = MechUnavailTA_Ann, @MechUnavailTA_Avg = MechUnavailTA_Ann,
	@NonTAUnavail = 100 - OpAvail_Ann - MechUnavailTA_Ann
FROM MaintAvailCalc
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

SELECT @NonTAUnavail_QTR = SUM((100 - OpAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	--, @MechUnavailTA_QTR=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet

SELECT @NonTAUnavail_Avg = SUM((100 - OpAvail_Ann - MechUnavailTA_Ann)*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
	--, @MechUnavailTA_Avg=SUM(MechUnavailTA_Ann*f.TotProcessEDC*s.FractionOfYear)/SUM(f.TotProcessEDC*s.FractionOfYear)
FROM MaintAvailCalc m INNER JOIN FactorTotCalc f ON f.SubmissionID = m.SubmissionID AND f.FactorSet = m.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND m.FactorSet = @FactorSet

IF @NonTAUnavail < 0.05
	SET @NonTAUnavail = 0
IF @NonTAUnavail_QTR < 0.05
	SET @NonTAUnavail_QTR = 0
IF @NonTAUnavail_Avg < 0.05
	SET @NonTAUnavail_Avg = 0
SELECT @OpAvail = 100 - @NonTAUnavail - @MechUnavailTA, 
		@OpAvail_QTR = 100 - @NonTAUnavail_QTR - @MechUnavailTA, 
		@OpAvail_Avg = 100 - @NonTAUnavail_Avg - @MechUnavailTA
	
EXEC spAverageFactors @RefineryID, @DataSet, @Start3Mo, @PeriodEnd, @FactorSet, 
		@EII = @EII_QTR OUTPUT, @VEI = @VEI_QTR OUTPUT, @UtilPcnt = @RefUtilPcnt_QTR OUTPUT, @UtilOSTA = NULL, @EDC = @EDC_QTR OUTPUT, @UEDC = @UEDC_QTR OUTPUT, 
		@ProcessUtilPcnt = @ProcessUtilPcnt_QTR OUTPUT, @TotProcessEDC = @TotProcessEDC_QTR OUTPUT, @TotProcessUEDC = @TotProcessUEDC_QTR OUTPUT

SELECT @TotProcessEDC = TotProcessEDC, @TotProcessUEDC = TotProcessUEDC,
	@EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy,
	@ReportLossGain = ReportLossGain/s.NumDays, @EstGain = EstGain/s.NumDays
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE f.SubmissionID = @SubmissionID AND f.FactorSet = @FactorSet

SELECT @TotProcessEDC_QTR = SUM(f.TotProcessEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotProcessUEDC_QTR = SUM(f.TotProcessUEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@EnergyUseDay_QTR = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_QTR = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@ReportLossGain_QTR = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
	@EstGain_QTR = SUM(EstGain)/SUM(s.NumDays*1.0)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet

SELECT @TotProcessEDC_Avg = SUM(f.TotProcessEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotProcessUEDC_Avg = SUM(f.TotProcessUEDC*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@EnergyUseDay_Avg = SUM(EnergyUseDay*s.NumDays*1.0)/SUM(s.NumDays*1.0), 
	@TotStdEnergy_Avg = SUM(TotStdEnergy*s.NumDays*1.0)/SUM(s.NumDays*1.0),
	@ReportLossGain_Avg = SUM(ReportLossGain)/SUM(s.NumDays*1.0), 
	@EstGain_Avg = SUM(EstGain)/SUM(s.NumDays*1.0)
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND f.FactorSet = @FactorSet

SELECT @TotMaintForceWHrEDC_QTR = SUM(p.TotMaintForceWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor)
, @PersIndex_QTR = SUM(g.TotWHrEDC*p.WHrEDCDivisor)/SUM(p.WHrEDCDivisor)
, @MaintIndex_QTR = SUM((g.RoutIndex + g.TAIndex_Avg)*g.EDC)/SUM(g.EDC)
, @RoutIndex_QTR = SUM(g.RoutIndex*g.EDC)/SUM(g.EDC)
, @NEOpExEDC_QTR = SUM(g.NEOpExEDC*g.EDC)/SUM(g.EDC)
FROM PersTotCalc p INNER JOIN GenSum g ON g.SubmissionID = p.SubmissionID AND g.FactorSet = p.FactorSet AND g.Scenario = p.Scenario AND g.Currency = p.Currency
INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND p.FactorSet = @FactorSet AND g.UOM = @UOM AND s.UseSubmission = 1
AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND p.Currency = @Currency AND p.Scenario = 'CLIENT'

SELECT @AnnTAWHr = SUM(TotWHr)/1000 FROM Pers WHERE SubmissionID = @SubmissionID AND PersID IN ('OCCTAADJ','MPSTAADJ')
SELECT @NonTAWHr = TotNonTAWHr/1000
FROM PersTot
WHERE SubmissionID = @SubmissionID

SELECT @AnnTAWHr_QTR = SUM(TotWHr)/1000 
FROM Pers p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND p.PersID IN ('OCCTAADJ','MPSTAADJ')
SELECT @NonTAWHr_QTR = SUM(TotNonTAWHr)/1000
FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1

SELECT @AnnTAWHr_Avg = SUM(TotWHr)/1000 
FROM Pers p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND p.PersID IN ('OCCTAADJ','MPSTAADJ')
SELECT @NonTAWHr_Avg = SUM(TotNonTAWHr)/1000
FROM PersTot p INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1

SELECT @AnnTACost = AllocAnnTACost/1000, @RoutCost = CurrRoutCost/1000
FROM MaintTotCost mtc 
WHERE mtc.SubmissionID = @SubmissionID AND mtc.Currency = @Currency

SELECT @AnnTACost_QTR = SUM(AllocAnnTACost)/1000, @RoutCost_QTR = SUM(CurrRoutCost)/1000
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND mtc.Currency = @Currency

SELECT @AnnTACost_Avg = SUM(AllocAnnTACost)/1000, @RoutCost_Avg = SUM(CurrRoutCost)/1000
FROM MaintTotCost mtc INNER JOIN Submissions s ON s.SubmissionID = mtc.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1 AND mtc.Currency = @Currency

SELECT @NEOpEx = NEOpEx*Divisor/1000, @EnergyCost = EnergyCost*Divisor/1000, @TotCashOpEx = TotCashOpEx*Divisor/1000, @TAAdj = TAAdj*Divisor/1000
FROM OpExCalc o 
WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'

SELECT @OpExUEDC = TotCashOpEx
FROM OpExCalc o 
WHERE o.SubmissionID = @SubmissionID AND o.FactorSet = @FactorSet AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEDC'

SELECT @NEOpEx_QTR = SUM(NEOpEx*Divisor)/1000, @EnergyCost_QTR = SUM(EnergyCost*Divisor)/1000
	, @TotCashOpEx_QTR = SUM(TotCashOpEx*Divisor)/1000, @TAAdj_QTR = SUM(TAAdj*Divisor)/1000
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'

SELECT @OpExUEDC_QTR = SUM(TotCashOpEx*Divisor)/SUM(Divisor)
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start3Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEDC'

SELECT @NEOpEx_Avg = SUM(NEOpEx*Divisor)/1000, @EnergyCost_Avg = SUM(EnergyCost*Divisor)/1000
	, @TotCashOpEx_Avg = SUM(TotCashOpEx*Divisor)/1000, @TAAdj_Avg = SUM(TAAdj*Divisor)/1000
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'EDC'

SELECT @OpExUEDC_Avg = SUM(TotCashOpEx*Divisor)/SUM(Divisor)
FROM OpExCalc o INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND o.FactorSet = @FactorSet
AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND o.Currency = @Currency AND o.Scenario = 'CLIENT' AND o.DataType = 'UEDC'

IF @UOM = 'MET'
	SELECT @EnergyUseDay = @EnergyUseDay * 1.055, @EnergyUseDay_QTR = @EnergyUseDay_QTR * 1.055, @EnergyUseDay_Avg = @EnergyUseDay_Avg * 1.055, 
			@TotStdEnergy = @TotStdEnergy * 1.055, @TotStdEnergy_QTR = @TotStdEnergy_QTR * 1.055, @TotStdEnergy_Avg = @TotStdEnergy_Avg * 1.055

SELECT 
	@EnergyUseDay = @EnergyUseDay/1000, @EnergyUseDay_QTR = @EnergyUseDay_QTR/1000, @EnergyUseDay_Avg = @EnergyUseDay_Avg/1000, 
	@TotStdEnergy = @TotStdEnergy/1000, @TotStdEnergy_QTR = @TotStdEnergy_QTR/1000, @TotStdEnergy_Avg = @TotStdEnergy_Avg/1000, 

	@EDC_QTR = @EDC_QTR/1000, @UEDC_QTR = @UEDC_QTR/1000,
	@TotProcessEDC = @TotProcessEDC/1000, @TotProcessEDC_QTR = @TotProcessEDC_QTR/1000, @TotProcessEDC_Avg = @TotProcessEDC_Avg/1000, 
	@TotProcessUEDC = @TotProcessUEDC/1000, @TotProcessUEDC_QTR = @TotProcessUEDC_QTR/1000, @TotProcessUEDC_Avg = @TotProcessUEDC_Avg/1000
	





