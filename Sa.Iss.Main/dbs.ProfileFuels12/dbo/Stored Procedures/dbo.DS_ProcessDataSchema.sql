﻿CREATE PROCEDURE [dbo].[DS_ProcessDataSchema]
	
AS
BEGIN
SELECT UnitID, UnitID as SortKey, RTRIM(Property) as ProcessID, RTRIM(Property) as UnitName, RTRIM(Property) as Property, RptValue FROM ProcessData WHERE SubmissionID = 0
END
