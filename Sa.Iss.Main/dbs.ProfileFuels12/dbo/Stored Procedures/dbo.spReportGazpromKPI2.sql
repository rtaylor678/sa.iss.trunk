﻿




CREATE   PROC [dbo].[spReportGazpromKPI2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
SET @FactorSet = '2012'

DECLARE	@RefUtilPcnt real, @RefUtilPcnt_QTR real, @RefUtilPcnt_Avg real, @RefUtilPcnt_YTD real, 
	@ProcessUtilPcnt real, @ProcessUtilPcnt_QTR real, @ProcessUtilPcnt_Avg real, @ProcessUtilPcnt_YTD real, 
	@TotProcessEDC real, @TotProcessEDC_QTR real, @TotProcessEDC_Avg real, @TotProcessEDC_YTD real, 
	@TotProcessUEDC real, @TotProcessUEDC_QTR real, @TotProcessUEDC_Avg real, @TotProcessUEDC_YTD real, 
	
	@OpAvail real, @OpAvail_QTR real, @OpAvail_Avg real, @OpAvail_YTD real, 
	@MechUnavailTA real, @MechUnavailTA_QTR real, @MechUnavailTA_Avg real, @MechUnavailTA_YTD real, 
	@NonTAUnavail real, @NonTAUnavail_QTR real, @NonTAUnavail_Avg real, @NonTAUnavail_YTD real, 

	@EII real, @EII_QTR real, @EII_Avg real, @EII_YTD real, 
	@EnergyUseDay real, @EnergyUseDay_QTR real, @EnergyUseDay_Avg real, @EnergyUseDay_YTD real, 
	@TotStdEnergy real, @TotStdEnergy_QTR real, @TotStdEnergy_Avg real, @TotStdEnergy_YTD real, 

	@VEI real, @VEI_QTR real, @VEI_Avg real, @VEI_YTD real, 
	@ReportLossGain real, @ReportLossGain_QTR real, @ReportLossGain_Avg real, @ReportLossGain_YTD real, 
	@EstGain real, @EstGain_QTR real, @EstGain_Avg real, @EstGain_YTD real, 

	@Gain real, @Gain_QTR real, @Gain_Avg real, @Gain_YTD real, 
	@RawMatl real, @RawMatl_QTR real, @RawMatl_Avg real, @RawMatl_YTD real, 
	@ProdYield real, @ProdYield_QTR real, @ProdYield_Avg real, @ProdYield_YTD real, 

	@PersIndex real, @PersIndex_QTR real, @PersIndex_Avg real, @PersIndex_YTD real, 
	@AnnTAWHr real, @AnnTAWHr_QTR real, @AnnTAWHr_Avg real, @AnnTAWHr_YTD real,
	@NonTAWHr real, @NonTAWHr_QTR real, @NonTAWHr_Avg real, @NonTAWHr_YTD real,
	@EDC real, @EDC_QTR real, @EDC_Avg real, @EDC_YTD real, 
	@UEDC real, @UEDC_QTR real, @UEDC_Avg real, @UEDC_YTD real, 

	@TotMaintForceWHrEDC real, @TotMaintForceWHrEDC_QTR real, @TotMaintForceWHrEDC_Avg real, @TotMaintForceWHrEDC_YTD real, 
	@MaintTAWHr real, @MaintTAWHr_QTR real, @MaintTAWHr_Avg real, @MaintTAWHr_YTD real, 
	@MaintNonTAWHr real, @MaintNonTAWHr_QTR real, @MaintNonTAWHr_Avg real, @MaintNonTAWHr_YTD real, 

	@MaintIndex real, @MaintIndex_QTR real, @MaintIndex_Avg real, @MaintIndex_YTD real, 
	@RoutIndex real, @RoutIndex_QTR real, @RoutIndex_Avg real, @RoutIndex_YTD real,
	@AnnTACost real, @AnnTACost_QTR real, @AnnTACost_Avg real, @AnnTACost_YTD real, 
	@RoutCost real, @RoutCost_QTR real, @RoutCost_Avg real, @RoutCost_YTD real, 

	@NEOpExEDC real, @NEOpExEDC_QTR real, @NEOpExEDC_Avg real, @NEOpExEDC_YTD real, 
	@NEOpEx real, @NEOpEx_QTR real, @NEOpEx_Avg real, @NEOpEx_YTD real, 

	@OpExUEDC real, @OpExUEDC_QTR real, @OpExUEDC_Avg real, @OpExUEDC_YTD real, 
	@EnergyCost real, @EnergyCost_QTR real, @EnergyCost_Avg real, @EnergyCost_YTD real, 
	@TAAdj real, @TAAdj_QTR real, @TAAdj_Avg real, @TAAdj_YTD real,
	@TotCashOpEx real, @TotCashOpEx_QTR real, @TotCashOpEx_Avg real , @TotCashOpEx_YTD real
DECLARE @spResult smallint
EXEC @spResult = [dbo].[spReportGazpromKPICalc2] @RefineryID, @PeriodYear, @PeriodMonth, @DataSet, 
	@FactorSet, @Scenario, @Currency, @UOM,
	@EII = @EII OUTPUT, @EII_QTR = @EII_QTR OUTPUT, @EII_Avg = @EII_Avg OUTPUT, @EII_YTD = @EII_YTD OUTPUT, 
	@EnergyUseDay = @EnergyUseDay OUTPUT, @EnergyUseDay_QTR = @EnergyUseDay_QTR OUTPUT, @EnergyUseDay_Avg = @EnergyUseDay_Avg OUTPUT, @EnergyUseDay_YTD = @EnergyUseDay_YTD OUTPUT, 
	@TotStdEnergy = @TotStdEnergy OUTPUT, @TotStdEnergy_QTR = @TotStdEnergy_QTR OUTPUT, @TotStdEnergy_Avg = @TotStdEnergy_Avg OUTPUT, @TotStdEnergy_YTD = @TotStdEnergy_YTD OUTPUT, 
	@RefUtilPcnt = @RefUtilPcnt OUTPUT, @RefUtilPcnt_QTR = @RefUtilPcnt_QTR OUTPUT, @RefUtilPcnt_Avg = @RefUtilPcnt_Avg OUTPUT, @RefUtilPcnt_YTD = @RefUtilPcnt_YTD OUTPUT, 
	@EDC = @EDC OUTPUT, @EDC_QTR = @EDC_QTR OUTPUT, @EDC_Avg = @EDC_Avg OUTPUT, @EDC_YTD = @EDC_YTD OUTPUT, 
	@UEDC = @UEDC OUTPUT, @UEDC_QTR = @UEDC_QTR OUTPUT, @UEDC_Avg = @UEDC_Avg OUTPUT, @UEDC_YTD = @UEDC_YTD OUTPUT, 
	@VEI = @VEI OUTPUT, @VEI_QTR = @VEI_QTR OUTPUT, @VEI_Avg = @VEI_Avg OUTPUT, @VEI_YTD = @VEI_YTD OUTPUT, 
	@ReportLossGain = @ReportLossGain OUTPUT, @ReportLossGain_QTR = @ReportLossGain_QTR OUTPUT, @ReportLossGain_Avg = @ReportLossGain_Avg OUTPUT, @ReportLossGain_YTD = @ReportLossGain_YTD OUTPUT, 
	@EstGain = @EstGain OUTPUT, @EstGain_QTR = @EstGain_QTR OUTPUT, @EstGain_Avg = @EstGain_Avg OUTPUT, @EstGain_YTD = @EstGain_YTD OUTPUT, 
	@OpAvail = @OpAvail OUTPUT, @OpAvail_QTR = @OpAvail_QTR OUTPUT, @OpAvail_Avg = @OpAvail_Avg OUTPUT, @OpAvail_YTD = @OpAvail_YTD OUTPUT, 
	@MechUnavailTA = @MechUnavailTA OUTPUT, @MechUnavailTA_QTR = @MechUnavailTA_QTR OUTPUT, @MechUnavailTA_Avg = @MechUnavailTA_Avg OUTPUT, @MechUnavailTA_YTD = @MechUnavailTA_YTD OUTPUT, 
	@NonTAUnavail = @NonTAUnavail OUTPUT, @NonTAUnavail_QTR = @NonTAUnavail_QTR OUTPUT, @NonTAUnavail_Avg = @NonTAUnavail_Avg OUTPUT, @NonTAUnavail_YTD = @NonTAUnavail_YTD OUTPUT, 
	@RoutIndex = @RoutIndex OUTPUT, @RoutIndex_QTR = @RoutIndex_QTR OUTPUT, @RoutIndex_Avg = @RoutIndex_Avg OUTPUT, @RoutIndex_YTD = @RoutIndex_YTD OUTPUT,
	@RoutCost = @RoutCost OUTPUT, @RoutCost_QTR = @RoutCost_QTR OUTPUT, @RoutCost_Avg = @RoutCost_Avg OUTPUT, @RoutCost_YTD = @RoutCost_YTD OUTPUT, 
	@PersIndex = @PersIndex OUTPUT, @PersIndex_QTR = @PersIndex_QTR OUTPUT, @PersIndex_Avg = @PersIndex_Avg OUTPUT, @PersIndex_YTD = @PersIndex_YTD OUTPUT, 
	@AnnTAWHr = @AnnTAWHr OUTPUT, @AnnTAWHr_QTR = @AnnTAWHr_QTR OUTPUT, @AnnTAWHr_Avg = @AnnTAWHr_Avg OUTPUT, @AnnTAWHr_YTD = @AnnTAWHr_YTD OUTPUT,
	@NonTAWHr = @NonTAWHr OUTPUT, @NonTAWHr_QTR = @NonTAWHr_QTR OUTPUT, @NonTAWHr_Avg = @NonTAWHr_Avg OUTPUT, @NonTAWHr_YTD = @NonTAWHr_YTD OUTPUT,
	@NEOpExEDC = @NEOpExEDC OUTPUT, @NEOpExEDC_QTR = @NEOpExEDC_QTR OUTPUT, @NEOpExEDC_Avg = @NEOpExEDC_Avg OUTPUT, @NEOpExEDC_YTD = @NEOpExEDC_YTD OUTPUT, 
	@NEOpEx = @NEOpEx OUTPUT, @NEOpEx_QTR = @NEOpEx_QTR OUTPUT, @NEOpEx_Avg = @NEOpEx_Avg OUTPUT, @NEOpEx_YTD = @NEOpEx_YTD OUTPUT, 
	@OpExUEDC = @OpExUEDC OUTPUT, @OpExUEDC_QTR = @OpExUEDC_QTR OUTPUT, @OpExUEDC_Avg = @OpExUEDC_Avg OUTPUT, @OpExUEDC_YTD = @OpExUEDC_YTD OUTPUT, 
	@TAAdj = @TAAdj OUTPUT, @TAAdj_QTR = @TAAdj_QTR OUTPUT, @TAAdj_Avg = @TAAdj_Avg OUTPUT, @TAAdj_YTD = @TAAdj_YTD OUTPUT,
	@EnergyCost = @EnergyCost OUTPUT, @EnergyCost_QTR = @EnergyCost_QTR OUTPUT, @EnergyCost_Avg = @EnergyCost_Avg OUTPUT, @EnergyCost_YTD = @EnergyCost_YTD OUTPUT, 
	@TotCashOpEx = @TotCashOpEx OUTPUT, @TotCashOpEx_QTR = @TotCashOpEx_QTR OUTPUT, @TotCashOpEx_Avg = @TotCashOpEx_Avg OUTPUT, @TotCashOpEx_YTD = @TotCashOpEx_YTD OUTPUT

IF @spResult > 0	
	RETURN @spResult
ELSE 

SELECT 
	EII = @EII, EII_QTR = @EII_QTR, EII_Avg = @EII_Avg, EII_YTD = @EII_YTD, 
	EnergyUseDay = @EnergyUseDay, EnergyUseDay_QTR = @EnergyUseDay_QTR, EnergyUseDay_Avg = @EnergyUseDay_Avg, EnergyUseDay_YTD = @EnergyUseDay_YTD, 
	TotStdEnergy = @TotStdEnergy, TotStdEnergy_QTR = @TotStdEnergy_QTR, TotStdEnergy_Avg = @TotStdEnergy_Avg, TotStdEnergy_YTD = @TotStdEnergy_YTD, 

	UtilPcnt = @RefUtilPcnt, UtilPcnt_QTR = @RefUtilPcnt_QTR, UtilPcnt_Avg = @RefUtilPcnt_Avg, UtilPcnt_YTD = @RefUtilPcnt_YTD, 
	EDC = @EDC, EDC_QTR = @EDC_QTR, EDC_Avg = @EDC_Avg, EDC_YTD = @EDC_YTD, 
	UtilUEDC = @EDC*@RefUtilPcnt/100, UtilUEDC_QTR = @EDC_QTR*@RefUtilPcnt_QTR/100, UtilUEDC_Avg = @EDC_Avg*@RefUtilPcnt_Avg/100, UtilUEDC_YTD = @EDC_YTD*@RefUtilPcnt_YTD/100, 
	
--	ProcessUtilPcnt = @ProcessUtilPcnt, ProcessUtilPcnt_QTR = @ProcessUtilPcnt_QTR, ProcessUtilPcnt_Avg = @ProcessUtilPcnt_Avg,
--	TotProcessEDC = @TotProcessEDC, TotProcessEDC_QTR = @TotProcessEDC_QTR, TotProcessEDC_Avg = @TotProcessEDC_Avg, 
--	TotProcessUEDC = @TotProcessUEDC, TotProcessUEDC_QTR = @TotProcessUEDC_QTR, TotProcessUEDC_Avg = @TotProcessUEDC_Avg, 
	
	VEI = @VEI, VEI_QTR = @VEI_QTR, VEI_Avg = @VEI_Avg, VEI_YTD = @VEI_YTD, 
	ReportLossGain = @ReportLossGain, ReportLossGain_QTR = @ReportLossGain_QTR, ReportLossGain_Avg = @ReportLossGain_Avg, ReportLossGain_YTD = @ReportLossGain_YTD, 
	EstGain = @EstGain, EstGain_QTR = @EstGain_QTR, EstGain_Avg = @EstGain_Avg, EstGain_YTD = @EstGain_YTD, 

	OpAvail = @OpAvail, OpAvail_QTR = @OpAvail_QTR, OpAvail_Avg = @OpAvail_Avg, OpAvail_YTD = @OpAvail_YTD, 
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_QTR = @MechUnavailTA_QTR, MechUnavailTA_Avg = @MechUnavailTA_Avg, MechUnavailTA_YTD = @MechUnavailTA_YTD, 
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_QTR = @NonTAUnavail_QTR, NonTAUnavail_Avg = @NonTAUnavail_Avg, NonTAUnavail_YTD = @NonTAUnavail_YTD, 

	MaintIndex = @MaintIndex, MaintIndex_QTR = @MaintIndex_QTR, MaintIndex_Avg = @MaintIndex_Avg, MaintIndex_YTD = @MaintIndex_YTD, 
	AnnTACost = @AnnTACost, AnnTACost_QTR = @AnnTACost_QTR, AnnTACost_Avg = @AnnTACost_Avg, AnnTACost_YTD = @AnnTACost_YTD, 
	RoutCost = @RoutCost, RoutCost_QTR = @RoutCost_QTR, RoutCost_Avg = @RoutCost_Avg, RoutCost_YTD = @RoutCost_YTD, 
	RoutIndex = @RoutIndex, RoutIndex_QTR = @RoutIndex_QTR, RoutIndex_Avg = @RoutIndex_Avg, RoutIndex_YTD = @RoutIndex_YTD, 

	TotWHrEDC = @PersIndex, TotWHrEDC_QTR = @PersIndex_QTR, TotWHrEDC_Avg = @PersIndex_Avg, TotWHrEDC_YTD = @PersIndex_YTD, 
	AnnTAWHr = @AnnTAWHr, AnnTAWHr_QTR = @AnnTAWHr_QTR, AnnTAWHr_Avg = @AnnTAWHr_Avg, AnnTAWHr_YTD = @AnnTAWHr_YTD,
	NonTAWHr = @NonTAWHr, NonTAWHr_QTR = @NonTAWHr_QTR, NonTAWHr_Avg = @NonTAWHr_Avg, NonTAWHr_YTD = @NonTAWHr_YTD, 

	NEOpExEDC = @NEOpExEDC, NEOpExEDC_QTR = @NEOpExEDC_QTR, NEOpExEDC_Avg = @NEOpExEDC_Avg, NEOpExEDC_YTD = @NEOpExEDC_YTD, 
	NEOpEx = @NEOpEx, NEOpEx_QTR = @NEOpEx_QTR, NEOpEx_Avg = @NEOpEx_Avg, NEOpEx_YTD = @NEOpEx_YTD, 

	TotCashOpExUEDC = @OpExUEDC, TotCashOpExUEDC_QTR = @OpExUEDC_QTR, TotCashOpExUEDC_Avg = @OpExUEDC_Avg, TotCashOpExUEDC_YTD = @OpExUEDC_YTD, 
	TAAdj = @TAAdj, TAAdj_QTR = @TAAdj_QTR, TAAdj_Avg = @TAAdj_Avg, TAAdj_YTD = @TAAdj_YTD,
	EnergyCost = @EnergyCost, EnergyCost_QTR = @EnergyCost_QTR, EnergyCost_Avg = @EnergyCost_Avg, EnergyCost_YTD = @EnergyCost_YTD, 
	TotCashOpEx = @TotCashOpEx, TotCashOpEx_QTR = @TotCashOpEx_QTR, TotCashOpEx_Avg = @TotCashOpEx_Avg, TotCashOpEx_YTD = @TotCashOpEx_YTD, 
	UEDC = @UEDC, UEDC_QTR = @UEDC_QTR, UEDC_Avg = @UEDC_Avg, UEDC_YTD = @UEDC_YTD



