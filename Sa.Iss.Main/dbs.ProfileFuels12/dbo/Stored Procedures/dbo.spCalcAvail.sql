﻿
CREATE      PROC [dbo].[spCalcAvail](@SubmissionID int)
AS
SET NOCOUNT ON
DECLARE @PeriodHrs int, @RefineryID varchar(6), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @DataSet varchar(15)
SELECT @RefineryID = RefineryID, @PeriodHrs = DATEDIFF(hh, PeriodStart, PeriodEnd), @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @DataSet = DataSet
FROM SubmissionsAll WHERE SubmissionID = @SubmissionID
UPDATE MaintRout
SET PeriodHrs = @PeriodHrs
WHERE SubmissionID = @SubmissionID
UPDATE MaintRout
SET PeriodHrs = PeriodHrs * c.InServicePcnt/100
FROM MaintRout INNER JOIN Config c 
ON c.SubmissionID = MaintRout.SubmissionID AND c.UnitID = MaintRout.UnitID
WHERE MaintRout.SubmissionID = @SubmissionID AND c.InServicePcnt <> 100

;WITH units AS (
	SELECT r.SubmissionID, r.UnitID, r.PeriodHrs, c.InServicePcnt, c.UtilPcnt, RegDown, RegSlow, MaintDown, MaintSlow, OthDown, OthSlow
	, (SELECT HrsDownInPeriod FROM TAForSubmission ta WHERE ta.SubmissionID = r.SubmissionID AND ta.UnitID = r.UnitID) AS TADown
	FROM MaintRout r INNER JOIN SubmissionsAll s ON s.SubmissionID = r.SubmissionID
	INNER JOIN Config c ON c.SubmissionID = r.SubmissionID AND c.UnitID = r.UnitID
	WHERE s.SubmissionID = @SubmissionID
)
UPDATE MaintRout SET OthSlow = ROUND(CASE WHEN ISNULL(u.UtilPcnt, 0) > 0 THEN (ISNULL(u.InServicePcnt/100,1.0)*(100-u.UtilPcnt)/100 - (ISNULL(u.TADown, 0) + ISNULL(u.RegDown, 0) + ISNULL(u.RegSlow,0) + ISNULL(u.MaintDown, 0) + ISNULL(u.MaintSlow, 0) + ISNULL(u.OthDown, 0))/u.PeriodHrs) * u.PeriodHrs ELSE 0 END, 0)
FROM MaintRout INNER JOIN units u ON MaintRout.SubmissionID = u.SubmissionID AND MaintRout.UnitID = u.UnitID

UPDATE MaintRout SET OthSlow = 0 WHERE SubmissionID = @SubmissionID AND OthSlow < 0

DELETE FROM MaintCalc WHERE SubmissionID = @SubmissionID
DELETE FROM MaintProcess WHERE SubmissionID = @SubmissionID
DELETE FROM MaintAvailCalc WHERE SubmissionID = @SubmissionID
INSERT INTO MaintCalc(SubmissionID, UnitID, ProcessID, PeriodHrs)
SELECT SubmissionID, UnitID, ProcessID, PeriodHrs
FROM MaintRout WHERE SubmissionID = @SubmissionID

UPDATE MaintCalc
SET 	MechUnavailTA_Act = a.MechUnavailTA_Act, MechAvailOSTA = a.MechAvailOSTA, 
	MechAvail_Act = a.MechAvail_Act, MechAvailSlow_Act = a.MechAvailSlow_Act, 
	OpAvail_Act = a.OpAvail_Act, OpAvailSlow_Act = a.OpAvailSlow_Act, 
	OnStream_Act = a.OnStream_Act, OnStreamSlow_Act = a.OnStreamSlow_Act,
	MechUnavailTA_Ann = a.MechUnavailTA_Ann, PeriodHrsOSTA = a.PeriodHrsOSTA,
	MechAvail_Ann = a.MechAvail_Ann, MechAvailSlow_Ann = a.MechAvailSlow_Ann, 
	OpAvail_Ann = a.OpAvail_Ann, OpAvailSlow_Ann = a.OpAvailSlow_Ann, 
	OnStream_Ann = a.OnStream_Ann, OnStreamSlow_Ann = a.OnStreamSlow_Ann,
	MechUnavailPlan	= a.MechUnavailPlan, MechUnavailUnp = a.MechUnavailUnp,
	MechUnavail_Ann	= a.MechUnavail_Ann, MechUnavail_Act = a.MechUnavail_Act,
	RegUnavail = a.RegUnavail, RegUnavailPlan = a.RegUnavailPlan, RegUnavailUnp = a.RegUnavailUnp,
	OpUnavail_Ann = a.OpUnavail_Ann, OpUnavail_Act = a.OpUnavail_Act,
	OthUnavailEconomic = a.OthUnavailEconomic, OthUnavailExternal = a.OthUnavailExternal,
	OthUnavailUnitUpsets = a.OthUnavailUnitUpsets, OthUnavailOffsiteUpsets = a.OthUnavailOffsiteUpsets,
	OthUnavailOther = a.OthUnavailOther, 
	OthUnavail = a.OthUnavail, OthUnavailPlan = a.OthUnavailPlan, OthUnavailUnp = a.OthUnavailUnp,
	TotUnavail_Ann = a.TotUnavail_Ann, TotUnavail_Act = a.TotUnavail_Act, TotUnavailUnp = a.TotUnavailUnp
FROM MaintCalc INNER JOIN dbo.GetUnitAvailability(@RefineryID, @DataSet, @PeriodStart, @PeriodEnd) a ON a.UnitID = MaintCalc.UnitID AND a.FactorSet = dbo.GetCurrentFactorSet()
WHERE SubmissionID = @SubmissionID

INSERT INTO MaintProcess(SubmissionID, FactorSet, ProcessID, MechUnavailTA_Ann, MechUnavailTA_Act, MechAvail_Ann, MechAvail_Act, MechAvailSlow_Ann, MechAvailSlow_Act, MechAvailOSTA, OpAvail_Ann, OpAvail_Act, OpAvailSlow_Ann, OpAvailSlow_Act, OnStream_Ann, OnStream_Act, OnStreamSlow_Ann, OnStreamSlow_Act,
MechUnavailPlan, MechUnavailUnp, MechUnavail_Ann, MechUnavail_Act, RegUnavail, RegUnavailPlan, RegUnavailUnp, OpUnavail_Ann, OpUnavail_Act,
OthUnavailEconomic, OthUnavailExternal, OthUnavailUnitUpsets, OthUnavailOffsiteUpsets, OthUnavailOther, OthUnavail, OthUnavailPlan, OthUnavailUnp,
TotUnavail_Ann, TotUnavail_Act, TotUnavailUnp)
SELECT @SubmissionID, FactorSet, ProcessID, MechUnavailTA_Ann, MechUnavailTA_Act, MechAvail_Ann, MechAvail_Act, MechAvailSlow_Ann, MechAvailSlow_Act, MechAvailOSTA, OpAvail_Ann, OpAvail_Act, OpAvailSlow_Ann, OpAvailSlow_Act, OnStream_Ann, OnStream_Act, OnStreamSlow_Ann, OnStreamSlow_Act,
MechUnavailPlan, MechUnavailUnp, MechUnavail_Ann, MechUnavail_Act, RegUnavail, RegUnavailPlan, RegUnavailUnp, OpUnavail_Ann, OpUnavail_Act,
OthUnavailEconomic, OthUnavailExternal, OthUnavailUnitUpsets, OthUnavailOffsiteUpsets, OthUnavailOther, OthUnavail, OthUnavailPlan, OthUnavailUnp,
TotUnavail_Ann, TotUnavail_Act, TotUnavailUnp
FROM dbo.GetAvailability(@RefineryID, @DataSet, @PeriodStart, @PeriodEnd)

INSERT INTO MaintAvailCalc(SubmissionID, FactorSet, MechUnavailTA_Ann, MechUnavailTA_Act, MechAvail_Ann, MechAvail_Act, MechAvailSlow_Ann, MechAvailSlow_Act, MechAvailOSTA, OpAvail_Ann, OpAvail_Act, OpAvailSlow_Ann, OpAvailSlow_Act, OnStream_Ann, OnStream_Act, OnStreamSlow_Ann, OnStreamSlow_Act,
	OthProcessMechUnavailTA_Ann, OthProcessMechAvail_Ann, OthProcessMechAvailSlow_Ann, OthProcessMechAvailOSTA, OthProcessOpAvail_Ann, OthProcessOpAvailSlow_Ann, OthProcessOnStream_Ann, OthProcessOnStreamSlow_Ann, 
	OthProcessMechUnavailTA_Act, OthProcessMechAvail_Act, OthProcessMechAvailSlow_Act, OthProcessOpAvail_Act, OthProcessOpAvailSlow_Act, OthProcessOnStream_Act, OthProcessOnStreamSlow_Act)
SELECT @SubmissionID, fs.FactorSet, 
	m.MechUnavailTA_Ann, m.MechUnavailTA_Act, m.MechAvail_Ann, m.MechAvail_Act, m.MechAvailSlow_Ann, m.MechAvailSlow_Act, m.MechAvailOSTA, m.OpAvail_Ann, m.OpAvail_Act, m.OpAvailSlow_Ann, m.OpAvailSlow_Act, m.OnStream_Ann, m.OnStream_Act, m.OnStreamSlow_Ann, m.OnStreamSlow_Act,
	o.MechUnavailTA_Ann, o.MechAvail_Ann, o.MechAvailSlow_Ann, o.MechAvailOSTA, o.OpAvail_Ann, o.OpAvailSlow_Ann, o.OnStream_Ann, o.OnStreamSlow_Ann, 
	o.MechUnavailTA_Act, o.MechAvail_Act, o.MechAvailSlow_Act, o.OpAvail_Act, o.OpAvailSlow_Act, o.OnStream_Act, o.OnStreamSlow_Act
FROM FactorSets fs LEFT JOIN MaintProcess m ON m.SubmissionID = @SubmissionID AND m.FactorSet = fs.FactorSet AND m.ProcessID = CASE WHEN fs.IdleUnitsInProcessResults = 'Y' THEN 'TotProc' ELSE 'OperProc' END
LEFT JOIN MaintProcess o ON o.SubmissionID = @SubmissionID AND o.FactorSet = fs.FactorSet AND o.ProcessID = 'OthProc'
WHERE fs.RefineryType = dbo.GetRefineryType(@RefineryID) AND fs.Calculate = 'Y'




