﻿CREATE PROCEDURE [dbo].[DS_GetUnitList]
	@RefineryID nvarchar(10),
	@DataSet nvarchar(20)='ACTUAL'
AS
BEGIN
	SELECT c.UnitID, TAID = ISNULL(max(m.TAID) , 0) 
               FROM Config c INNER JOIN dbo.Submissions s ON s.SubmissionID = c.SubmissionID 
               LEFT JOIN MaintTA m ON m.RefineryID = s.RefineryID AND m.DataSet = s.DataSet AND m.UnitID = c.UnitID 
               WHERE s.RefineryID =  @RefineryID and s.DataSet = @DataSet and UseSubmission=1
               GROUP BY c.UnitID
END

