﻿


CREATE   PROC [dbo].[spReportGazpromChartData2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
SET @FactorSet = '2012'

DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
SELECT @PeriodEnd = PeriodEnd
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)
IF @PeriodEnd IS NULL
	RETURN 1
IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND CalcsNeeded IS NOT NULL)
	RETURN 2
SELECT @PeriodStart12Mo = DATEADD(mm, -12, @PeriodEnd)

DECLARE @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	EII real NULL, 
	UtilPcnt real NULL, 
	VEI real NULL, 
	OpAvail real NULL, 
	RoutIndex real NULL,
	PersIndex real NULL, 
	NEOpExEDC real NULL,
	OpExUEDC real NULL,

	EII_YTD real NULL, 
	UtilPcnt_YTD real NULL, 
	VEI_YTD real NULL, 
	OpAvail_YTD real NULL, 
	RoutIndex_YTD real NULL,
	PersIndex_YTD real NULL, 
	NEOpExEDC_YTD real NULL,
	OpExUEDC_YTD real NULL,

	EII_LastYTD real NULL, 
	UtilPcnt_LastYTD real NULL, 
	VEI_LastYTD real NULL, 
	OpAvail_LastYTD real NULL, 
	RoutIndex_LastYTD real NULL,
	PersIndex_LastYTD real NULL, 
	NEOpExEDC_LastYTD real NULL,
	OpExUEDC_LastYTD real NULL,

	ProcessUtilPcnt real NULL, 
	MaintIndex real NULL
)

--- Everything Already Available in GenSum
INSERT INTO @Data (PeriodStart, PeriodEnd, UtilPcnt, ProcessUtilPcnt, OpAvail, EII, VEI, PersIndex, RoutIndex, MaintIndex, NEOpExEDC, OpExUEDC
	, UtilPcnt_YTD, OpAvail_YTD, EII_YTD, VEI_YTD, PersIndex_YTD, RoutIndex_YTD, NEOpExEDC_YTD, OpExUEDC_YTD)
SELECT	s.PeriodStart, s.PeriodEnd, UtilPcnt, ProcessUtilPcnt, OpAvail, EII, VEI, PersIndex = TotWHrEDC, RoutIndex, MaintIndex, NEOpExEDC, TotCashOpExUEDC
	, UtilPcnt_YTD, OpAvail_YTD, EII_YTD, VEI_YTD, TotWHrEDC_YTD, RoutIndex_YTD, NEOpExEDC_YTD, TotCashOpExUEDC_YTD
FROM GenSum g INNER JOIN Submissions s ON s.SubmissionID = g.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @PeriodStart12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND g.FactorSet = @FactorSet AND g.Currency = 'USD' AND g.UOM = @UOM AND g.Scenario = @Scenario


IF (SELECT COUNT(*) FROM @Data) < 12
BEGIN
	DECLARE @Period smalldatetime
	SELECT @Period = DATEADD(mm, -1, @PeriodEnd)
	WHILE @Period >= @PeriodStart12Mo
	BEGIN
		IF NOT EXISTS (SELECT * FROM @Data WHERE PeriodStart = @Period)
			INSERT @Data (PeriodStart) VALUES (@Period)
		SELECT @Period = DATEADD(mm, -1, @Period)
	END
END

UPDATE @Data
SET EII = NULL, UtilPcnt = NULL, VEI = NULL, RoutIndex = NULL, PersIndex = NULL, NEOpExEDC = NULL, OpExUEDC = NULL, OpAvail = NULL,
	EII_YTD = NULL, UtilPcnt_YTD = NULL, VEI_YTD = NULL, RoutIndex_YTD = NULL, PersIndex_YTD = NULL, NEOpExEDC_YTD = NULL, OpExUEDC_YTD = NULL, OpAvail_YTD = NULL
WHERE DATEPART(yy, PeriodStart) < 2011

UPDATE @Data
SET EII_LastYTD = EII_YTD, UtilPcnt_LastYTD = UtilPcnt_YTD, VEI_LastYTD = VEI_YTD, RoutIndex_LastYTD = RoutIndex_YTD, PersIndex_LastYTD = PersIndex_YTD
	, NEOpExEDC_LastYTD = NEOpExEDC_YTD, OpExUEDC_LastYTD = OpExUEDC_YTD, OpAvail_LastYTD = OpAvail_YTD
	, EII_YTD = NULL, UtilPcnt_YTD = NULL, VEI_YTD = NULL, RoutIndex_YTD = NULL, PersIndex_YTD = NULL, NEOpExEDC_YTD = NULL, OpExUEDC_YTD = NULL, OpAvail_YTD = NULL
WHERE DATEPART(yy, PeriodStart) < @PeriodYear

SELECT Period = CAST(DATEPART(mm, PeriodStart) as varchar(2)) + '/' + CAST(DATEPART(yy, PeriodStart) as varchar(4))
	, EII, UtilPcnt, VEI, OpAvail, RoutIndex, PersIndex, NEOpExEDC, OpExUEDC
	, EII_YTD, UtilPcnt_YTD, VEI_YTD, OpAvail_YTD, RoutIndex_YTD, PersIndex_YTD, NEOpExEDC_YTD, OpExUEDC_YTD
	, EII_LastYTD, UtilPcnt_LastYTD, VEI_LastYTD, OpAvail_LastYTD, RoutIndex_LastYTD, PersIndex_LastYTD, NEOpExEDC_LastYTD, OpExUEDC_LastYTD
FROM @Data
ORDER BY PeriodStart ASC








