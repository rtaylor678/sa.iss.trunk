﻿CREATE PROC [dbo].[SS_GetInputUnitTargets]
	@RefineryID nvarchar(10),
	@DataSet nvarchar(20)='ACTUAL'
AS

SELECT s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            u.UnitID,u.MechAvail,u.OpAvail,u.OnStream,u.UtilPcnt, 
            u.RoutCost,u.TACost,ISNULL(RTRIM(u.CurrencyCode),'USD') as CurrencyCode, 
            RTRIM(cfg.ProcessID)AS ProcessID,cfg.SortKey,RTRIM(cfg.UnitName) AS UnitName 
            FROM  
            dbo.UnitTargets u , Config cfg  
            ,dbo.Submissions s  
            WHERE   
            u.SubmissionID = s.SubmissionID AND 
            u.UnitID = cfg.UnitID AND u.SubmissionID=cfg.SubmissionID AND u.SubmissionID IN  
            (SELECT DISTINCT SubmissionID FROM dbo.Submissions
            WHERE RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1)

