﻿


CREATE   PROC [dbo].[spReportGazpromKPICalc2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15), 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @UOM varchar(5),
	@EII real = NULL OUTPUT, @EII_QTR real = NULL OUTPUT, @EII_Avg real = NULL OUTPUT, @EII_YTD real = NULL OUTPUT, 
	@EnergyUseDay real = NULL OUTPUT, @EnergyUseDay_QTR real = NULL OUTPUT, @EnergyUseDay_Avg real = NULL OUTPUT, @EnergyUseDay_YTD real = NULL OUTPUT, 
	@TotStdEnergy real = NULL OUTPUT, @TotStdEnergy_QTR real = NULL OUTPUT, @TotStdEnergy_Avg real = NULL OUTPUT, @TotStdEnergy_YTD real = NULL OUTPUT, 
	@RefUtilPcnt real = NULL OUTPUT, @RefUtilPcnt_QTR real = NULL OUTPUT, @RefUtilPcnt_Avg real = NULL OUTPUT, @RefUtilPcnt_YTD real = NULL OUTPUT, 
	@EDC real = NULL OUTPUT, @EDC_QTR real = NULL OUTPUT, @EDC_Avg real = NULL OUTPUT, @EDC_YTD real = NULL OUTPUT, 
	@UEDC real = NULL OUTPUT, @UEDC_QTR real = NULL OUTPUT, @UEDC_Avg real = NULL OUTPUT, @UEDC_YTD real = NULL OUTPUT, 
	@VEI real = NULL OUTPUT, @VEI_QTR real = NULL OUTPUT, @VEI_Avg real = NULL OUTPUT, @VEI_YTD real = NULL OUTPUT, 
	@ReportLossGain real = NULL OUTPUT, @ReportLossGain_QTR real = NULL OUTPUT, @ReportLossGain_Avg real = NULL OUTPUT, @ReportLossGain_YTD real = NULL OUTPUT, 
	@EstGain real = NULL OUTPUT, @EstGain_QTR real = NULL OUTPUT, @EstGain_Avg real = NULL OUTPUT, @EstGain_YTD real = NULL OUTPUT, 
	@OpAvail real = NULL OUTPUT, @OpAvail_QTR real = NULL OUTPUT, @OpAvail_Avg real = NULL OUTPUT, @OpAvail_YTD real = NULL OUTPUT, 
	@MechUnavailTA real = NULL OUTPUT, @MechUnavailTA_QTR real = NULL OUTPUT, @MechUnavailTA_Avg real = NULL OUTPUT, @MechUnavailTA_YTD real = NULL OUTPUT, 
	@NonTAUnavail real = NULL OUTPUT, @NonTAUnavail_QTR real = NULL OUTPUT, @NonTAUnavail_Avg real = NULL OUTPUT, @NonTAUnavail_YTD real = NULL OUTPUT, 
	@RoutIndex real = NULL OUTPUT, @RoutIndex_QTR real = NULL OUTPUT, @RoutIndex_Avg real = NULL OUTPUT, @RoutIndex_YTD real = NULL OUTPUT,
	@RoutCost real = NULL OUTPUT, @RoutCost_QTR real = NULL OUTPUT, @RoutCost_Avg real = NULL OUTPUT, @RoutCost_YTD real = NULL OUTPUT, 
	@PersIndex real = NULL OUTPUT, @PersIndex_QTR real = NULL OUTPUT, @PersIndex_Avg real = NULL OUTPUT, @PersIndex_YTD real = NULL OUTPUT, 
	@AnnTAWHr real = NULL OUTPUT, @AnnTAWHr_QTR real = NULL OUTPUT, @AnnTAWHr_Avg real = NULL OUTPUT, @AnnTAWHr_YTD real = NULL OUTPUT,
	@NonTAWHr real = NULL OUTPUT, @NonTAWHr_QTR real = NULL OUTPUT, @NonTAWHr_Avg real = NULL OUTPUT, @NonTAWHr_YTD real = NULL OUTPUT,
	@NEOpExEDC real = NULL OUTPUT, @NEOpExEDC_QTR real = NULL OUTPUT, @NEOpExEDC_Avg real = NULL OUTPUT, @NEOpExEDC_YTD real = NULL OUTPUT, 
	@NEOpEx real = NULL OUTPUT, @NEOpEx_QTR real = NULL OUTPUT, @NEOpEx_Avg real = NULL OUTPUT, @NEOpEx_YTD real = NULL OUTPUT, 
	@OpExUEDC real = NULL OUTPUT, @OpExUEDC_QTR real = NULL OUTPUT, @OpExUEDC_Avg real = NULL OUTPUT, @OpExUEDC_YTD real = NULL OUTPUT, 
	@TAAdj real = NULL OUTPUT, @TAAdj_QTR real = NULL OUTPUT, @TAAdj_Avg real = NULL OUTPUT, @TAAdj_YTD real = NULL OUTPUT,
	@EnergyCost real = NULL OUTPUT, @EnergyCost_QTR real = NULL OUTPUT, @EnergyCost_Avg real = NULL OUTPUT, @EnergyCost_YTD real = NULL OUTPUT, 
	@TotCashOpEx real = NULL OUTPUT, @TotCashOpEx_QTR real = NULL OUTPUT, @TotCashOpEx_Avg real = NULL OUTPUT, @TotCashOpEx_YTD real = NULL OUTPUT
	)
AS

SELECT @EII = NULL, @EII_QTR = NULL, @EII_Avg = NULL, @EII_YTD = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_QTR = NULL, @EnergyUseDay_Avg = NULL, @EnergyUseDay_YTD = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_QTR = NULL, @TotStdEnergy_Avg = NULL, @TotStdEnergy_YTD = NULL, 
	@RefUtilPcnt = NULL, @RefUtilPcnt_QTR = NULL, @RefUtilPcnt_Avg = NULL, @RefUtilPcnt_YTD = NULL, 
	@EDC = NULL, @EDC_QTR = NULL, @EDC_Avg = NULL, @EDC_YTD = NULL, 
	@UEDC = NULL, @UEDC_QTR = NULL, @UEDC_Avg = NULL, @UEDC_YTD = NULL, 
	@VEI = NULL, @VEI_QTR = NULL, @VEI_Avg = NULL, @VEI_YTD = NULL, 
	@ReportLossGain = NULL, @ReportLossGain_QTR = NULL, @ReportLossGain_Avg = NULL, @ReportLossGain_YTD = NULL,
	@EstGain = NULL, @EstGain_QTR = NULL, @EstGain_Avg = NULL, @EstGain_YTD = NULL, 
	@OpAvail = NULL, @OpAvail_QTR = NULL, @OpAvail_Avg = NULL, @OpAvail_YTD = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_QTR = NULL, @MechUnavailTA_Avg = NULL, @MechUnavailTA_YTD = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_QTR = NULL, @NonTAUnavail_Avg = NULL, @NonTAUnavail_YTD = NULL, 
	@RoutIndex = NULL, @RoutIndex_QTR = NULL, @RoutIndex_Avg = NULL, @RoutIndex_YTD = NULL,
	@RoutCost = NULL, @RoutCost_QTR = NULL, @RoutCost_Avg = NULL, @RoutCost_YTD = NULL, 
	@PersIndex = NULL, @PersIndex_QTR = NULL, @PersIndex_Avg = NULL, @PersIndex_YTD = NULL, 
	@AnnTAWHr = NULL, @AnnTAWHr_QTR = NULL, @AnnTAWHr_Avg = NULL, @AnnTAWHr_YTD = NULL,
	@NonTAWHr = NULL, @NonTAWHr_QTR = NULL, @NonTAWHr_Avg = NULL, @NonTAWHr_YTD = NULL,
	@NEOpExEDC = NULL, @NEOpExEDC_QTR = NULL, @NEOpExEDC_Avg = NULL, @NEOpExEDC_YTD = NULL, 
	@NEOpEx = NULL, @NEOpEx_QTR = NULL, @NEOpEx_Avg = NULL, @NEOpEx_YTD = NULL, 
	@OpExUEDC = NULL, @OpExUEDC_QTR = NULL, @OpExUEDC_Avg = NULL, @OpExUEDC_YTD = NULL, 
	@TAAdj = NULL, @TAAdj_QTR = NULL, @TAAdj_Avg = NULL, @TAAdj_YTD = NULL,
	@EnergyCost = NULL, @EnergyCost_QTR = NULL, @EnergyCost_Avg = NULL, @EnergyCost_YTD = NULL, 
	@TotCashOpEx = NULL, @TotCashOpEx_QTR = NULL, @TotCashOpEx_Avg = NULL, @TotCashOpEx_YTD = NULL

SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
SELECT @Start3Mo = Start3Mo, @Start12Mo = Start12Mo, @StartYTD = StartYTD, @Start24Mo = Start24Mo
FROM dbo.GetPeriods(@SubmissionID)

IF DATEPART(yy, @Start3Mo) < 2010
	SET @Start3Mo = '12/31/2009'
IF DATEPART(yy, @Start12Mo) < 2010
	SET @Start12Mo = '12/31/2009'
	
DECLARE @SubListMonth dbo.SubmissionIDList, @SubList3Mo dbo.SubmissionIDList, @SubListYTD dbo.SubmissionIDList, @SubList12Mo dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
INSERT @SubListMonth SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @PeriodStart, @PeriodEnd)
INSERT @SubList3Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start3Mo, @PeriodEnd)
INSERT @SubListYTD SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartYTD, @PeriodEnd)
INSERT @SubList12Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd)
INSERT @SubList24Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start24Mo, @PeriodEnd)

SELECT @EII = m.EII, @EII_QTR = avg3mo.EII, @EII_Avg = avg12mo.EII, @EII_YTD = avgYTD.EII, 
	@EnergyUseDay = m.EnergyUseDay/1000, @EnergyUseDay_QTR = avg3mo.EnergyUseDay/1000, @EnergyUseDay_Avg = avg12mo.EnergyUseDay/1000, @EnergyUseDay_YTD = avgYTD.EnergyUseDay/1000, 
	@TotStdEnergy = m.TotStdEnergy/1000, @TotStdEnergy_QTR = avg3mo.TotStdEnergy/1000, @TotStdEnergy_Avg = avg12mo.TotStdEnergy/1000, @TotStdEnergy_YTD = avgYTD.TotStdEnergy/1000, 
	@RefUtilPcnt = m.RefUtilPcnt, @RefUtilPcnt_QTR = avg3mo.RefUtilPcnt, @RefUtilPcnt_Avg = avg12mo.RefUtilPcnt, @RefUtilPcnt_YTD = avgYTD.RefUtilPcnt, 
	@EDC = m.EDC/1000, @EDC_QTR = avg3mo.EDC/1000, @EDC_Avg = avg12mo.EDC/1000, @EDC_YTD = avgYTD.EDC/1000, 
	@UEDC = m.UEDC/1000, @UEDC_QTR = avg3mo.UEDC/1000, @UEDC_Avg = avg12mo.UEDC/1000, @UEDC_YTD = avgYTD.UEDC/1000, 
	@VEI = m.VEI, @VEI_QTR = avg3mo.VEI, @VEI_Avg = avg12mo.VEI, @VEI_YTD = avgYTD.VEI, 
	@ReportLossGain = m.ReportLossGain, @ReportLossGain_QTR = avg3mo.ReportLossGain, @ReportLossGain_Avg = avg12mo.ReportLossGain, @ReportLossGain_YTD = avgYTD.ReportLossGain, 
	@EstGain = m.EstGain, @EstGain_QTR = avg3mo.EstGain, @EstGain_Avg = avg12mo.EstGain, @EstGain_YTD = avgYTD.EstGain, 
	@OpAvail = m.OpAvail, @OpAvail_QTR = avg3mo.OpAvail, @OpAvail_Avg = avg24mo.OpAvail, @OpAvail_YTD = avgYTD.OpAvail, 
	@MechUnavailTA = m.MechUnavailTA, @MechUnavailTA_QTR = avg3mo.MechUnavailTA, @MechUnavailTA_Avg = avg24mo.MechUnavailTA, @MechUnavailTA_YTD = avgYTD.MechUnavailTA, 
	@NonTAUnavail = m.MechUnavailRout + m.RegUnavail, @NonTAUnavail_QTR = avg3mo.MechUnavailRout + avg3mo.RegUnavail, @NonTAUnavail_Avg = avg24mo.MechUnavailRout + avg24mo.RegUnavail, @NonTAUnavail_YTD = avgYTD.MechUnavailRout + avgYTD.RegUnavail, 
	@RoutIndex = m.RoutIndex, @RoutIndex_QTR = avg3mo.RoutIndex, @RoutIndex_Avg = mi24.RoutIndex, @RoutIndex_YTD = avgYTD.RoutIndex,
	@RoutCost = m.RoutCost/1000, @RoutCost_QTR = avg3mo.RoutCost/1000, @RoutCost_Avg = mi24.RoutEffIndex*avg24mo.MaintEffDiv/100/1e6, @RoutCost_YTD = avgYTD.RoutCost/1000, 
	@PersIndex = m.PersIndex, @PersIndex_QTR = avg3mo.PersIndex, @PersIndex_Avg = avg12mo.PersIndex, @PersIndex_YTD = avgYTD.PersIndex, 
	@AnnTAWHr = m.MaintTAWHr_k, @AnnTAWHr_QTR = avg3mo.MaintTAWHr_k, @AnnTAWHr_Avg = avg12mo.MaintTAWHr_k, @AnnTAWHr_YTD = avgYTD.MaintTAWHr_k,
	@NonTAWHr = m.TotNonTAWHr_k, @NonTAWHr_QTR = avg3mo.TotNonTAWHr_k, @NonTAWHr_Avg = avg12mo.TotNonTAWHr_k, @NonTAWHr_YTD = avgYTD.TotNonTAWHr_k,
	@NEOpExEDC = m.NEOpExEDC, @NEOpExEDC_QTR = avg3mo.NEOpExEDC, @NEOpExEDC_Avg = avg12mo.NEOpExEDC, @NEOpExEDC_YTD = avgYTD.NEOpExEDC, 
	@NEOpEx = m.NEOpEx/1000, @NEOpEx_QTR = avg3mo.NEOpEx/1000, @NEOpEx_Avg = avg12mo.NEOpEx/1000, @NEOpEx_YTD = avgYTD.NEOpEx/1000, 
	@OpExUEDC = m.OpExUEDC, @OpExUEDC_QTR = avg3mo.OpExUEDC, @OpExUEDC_Avg = avg12mo.OpExUEDC, @OpExUEDC_YTD = avgYTD.OpExUEDC, 
	@TAAdj = m.AnnTACost/1000, @TAAdj_QTR = avg3mo.AnnTACost/1000, @TAAdj_Avg = avg12mo.AnnTACost/1000, @TAAdj_YTD = avgYTD.AnnTACost/1000,
	@EnergyCost = m.EnergyCost/1000, @EnergyCost_QTR = avg3mo.EnergyCost/1000, @EnergyCost_Avg = avg12mo.EnergyCost/1000, @EnergyCost_YTD = avgYTD.EnergyCost/1000, 
	@TotCashOpEx = m.TotCashOpEx/1000, @TotCashOpEx_QTR = avg3mo.TotCashOpEx/1000, @TotCashOpEx_Avg = avg12mo.TotCashOpEx/1000, @TotCashOpEx_YTD = avgYTD.TotCashOpEx/1000
FROM dbo.SLProfileLiteKPIs(@SubListMonth, @FactorSet, @Scenario, @Currency, @UOM) m
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList3Mo, @FactorSet, @Scenario, @Currency, @UOM) avg3mo ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubListYTD, @FactorSet, @Scenario, @Currency, @UOM) avgYTD ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList12Mo, @FactorSet, @Scenario, @Currency, @UOM) avg12mo ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList24Mo, @FactorSet, @Scenario, @Currency, @UOM) avg24mo ON 1=1
LEFT JOIN dbo.SLMaintIndex(@SubList24Mo, 24, @FactorSet, @Currency) mi24 ON 1=1

