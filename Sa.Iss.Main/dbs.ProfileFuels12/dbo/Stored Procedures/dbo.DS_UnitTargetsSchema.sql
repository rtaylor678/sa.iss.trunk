﻿CREATE PROCEDURE [dbo].[DS_UnitTargetsSchema]
		@RefineryID nvarchar(10)
AS
BEGIN
		SELECT u.UnitID, u.MechAvail, u.OpAvail,u.OnStream, u.UtilPcnt, u.RoutCost, u.TACost, RTRIM(u.CurrencyCode) AS CurrencyCode,c.SortKey,RTRIM(c.ProcessID) AS ProcessID, RTRIM(c.UnitName) AS UnitName FROM UnitTargets u ,Config c 
        WHERE c.UnitID=u.UnitID  AND c.SubmissionID=u.SubmissionID 
        AND u.SubmissionID=(SELECT TOP 1 SubmissionID From dbo.Submissions WHERE RefineryID = @RefineryID ORDER BY PeriodStart DESC)
END

