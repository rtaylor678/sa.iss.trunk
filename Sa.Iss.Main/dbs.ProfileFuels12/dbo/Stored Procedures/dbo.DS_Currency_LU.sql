﻿CREATE PROC [dbo].[DS_Currency_LU]
	
AS

SELECT RTRIM(Country) + ' - ' + RTRIM(CurrencyCode) AS Description, 
                                 RTRIM(Country) AS Country, RTRIM(Currency) AS Currency, 
                                 RTRIM(KCurrency) AS KCurrency,RTRIM(CurrencyCode) as CurrencyCode 
                                 FROM Currency_LU 
                                 WHERE LastYear Is NULL
                                 ORDER BY Country
