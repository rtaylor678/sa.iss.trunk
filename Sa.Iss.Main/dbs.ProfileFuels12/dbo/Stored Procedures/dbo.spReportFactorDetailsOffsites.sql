﻿



CREATE    PROC [dbo].[spReportFactorDetailsOffsites] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod, Currency = @Currency, UOM = @UOM,
f.EDC/1000 as TotEDC, f.UEDC/1000 as TotUEDC, f.UtilPcnt AS RefUtilPcnt, TotRSEDC/1000 as kEDC, TotRSUEDC/1000 as kUEDC, SensHeatStdEnergy,AspStdEnergy, 
OffsitesStdEnergy,TotStdEnergy,EstGain/s.NumDays as EstGain, 
TotRSMaintEffDiv/1000 as TotRSMaintEffDiv, f.MaintEffDiv/1000 as MaintEffDiv
,TotRSPersEffDiv/1000 as TotRSPersEffDiv, TotRSMaintPersEffDiv/1000 as TotRSMaintPersEffDiv,TotRSNonMaintPersEffDiv/1000 as TotRSNonMaintPersEffDiv
, f.MaintPersEffDiv/1000 as MaintPersEffDiv, f.NonMaintPersEffDiv/1000 as NonMaintPersEffDiv, f.PersEffDiv/1000 as PersEffDiv
,TotRSNEOpExEffDiv/1000 as TotRSNEOpExEffDiv,  f.NEOpExEffDiv/1000 as NEOpExEffDiv, 
TnkStdCap , TnkStdEDC/1000 AS TnkStdEDC,((PurElecUEDC+PurStmUEDC)/1000) AS PurchasedUtilityUEDC 
, ProcessEDC = pt.EDC/1000, ProcessUEDC = pt.UEDC/1000, ProcessUtilPcnt = pt.UtilPcnt
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
LEFT JOIN FactorProcessCalc pt ON pt.SubmissionID = f.SubmissionID AND pt.FactorSet = f.FactorSet AND pt.ProcessID = 'OperProc'
WHERE f.FactorSet=@FactorSet 
AND s.RefineryID=@RefineryID AND s.DataSet=@DataSet AND s.PeriodYear=ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth=ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1
ORDER BY s.PeriodStart DESC




