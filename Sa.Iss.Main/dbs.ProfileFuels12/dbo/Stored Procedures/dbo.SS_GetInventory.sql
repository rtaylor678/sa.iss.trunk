﻿CREATE PROC [dbo].[SS_GetInventory]

	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@DataSet nvarchar(20)='ACTUAL'
AS

	SELECT TankType,NumTank,FuelsStorage,AvgLevel 
            FROM dbo.Inventory WHERE  SubmissionID IN 
            (SELECT DISTINCT SubmissionID FROM dbo.Submissions 
            WHERE RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1
            AND (PeriodStart BETWEEN @PeriodStart AND 
            DateAdd(Day, -1, @PeriodEnd)))

