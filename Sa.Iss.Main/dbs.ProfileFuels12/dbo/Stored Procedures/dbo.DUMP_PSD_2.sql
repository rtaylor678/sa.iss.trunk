﻿CREATE PROCEDURE [dbo].[DUMP_PSD_2]

	@Refnum nvarchar(10),
	@DataSetID nvarchar(10),
	@CapColName nvarchar(10)
	
	AS
	DECLARE @SQL as nvarchar(2000)
	
	SET @SQL = 'SELECT s.Location,s.PeriodStart,s.PeriodEnd,s.NumDays as DaysInPeriod,s.RptCurrency as Currency,s.UOM, 
                                    c.UnitID,c.UnitName, c.ProcessID, c.ProcessType, ISNULL(c.' + @CapColName + ' ,0) AS ' + @CapColName + '  , ISNULL(c.UtilPcnt,0) AS UtilPcnt,  
                                    ISNULL(EDCNoMult,0) AS EDC, ISNULL(UEDCNoMult,0) AS UEDC, d.DisplayTextUS, d.DisplayTextMet  
                                    FROM config c,FactorCalc fc, ProcessID_LU p ,DisplayUnits_LU d,Submissions s  
                                     WHERE c.ProcessID = p.ProcessID AND p.DisplayUnits = d.DisplayUnits AND  
                                    c.UnitID = fc.UnitID AND c.SubmissionID = fc.SubmissionID AND  
                                    c.ProcessID IN (''STEAMGEN'', ''ELECGEN'', ''FCCPOWER'')  
                                     AND fc.FactorSet= + studyYear.ToString +  AND s.SubmissionID=c.SubmissionID AND c.SubmissionID IN  
                                    (SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet=' + @DataSetID + ' AND  
                                     RefineryID=' + @Refnum + ') ORDER BY PeriodStart DESC'
                                     
                                     EXEC @SQL

