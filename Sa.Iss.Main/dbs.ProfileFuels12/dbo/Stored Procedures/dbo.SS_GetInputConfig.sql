﻿CREATE PROC [dbo].[SS_GetInputConfig]
	@RefineryID nvarchar(10),
	@DataSet nvarchar(20)='ACTUAL'
AS

SELECT 
           s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
           c.UnitID,RTRIM(c.ProcessID) as ProcessID,c.SortKey,RTRIM(c.UnitName) AS UnitName,RTRIM(c.ProcessType) AS ProcessType,c.RptCap, RTRIM(p.ProcessGroup) AS ProcessGroup, 
           c.UtilPcnt,c.RptStmCap,c.StmUtilPcnt,c.InServicePcnt,c.DesignFeedSulfur 
           FROM dbo.Config c,ProcessID_LU p 
           ,dbo.Submissions s  
           WHERE   
           c.SubmissionID = s.SubmissionID AND 
           c.ProcessID=p.ProcessID AND   
           p.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND  
           c.SubmissionID IN  
           (SELECT DISTINCT SubmissionID  
           FROM dbo.Submissions 
           WHERE s.RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1)

