﻿CREATE PROC [dbo].[DS_Chart_LU]
	@CompanyID nvarchar(20)
AS

SELECT RTRIM(ChartTitle) as ChartTitle,RTRIM(SectionHeader) as SectionHeader,
                  SortKey,RTRIM(ChartType) as ChartType,RTRIM(AxisLabelUS) as AxisLabelUS,
                  RTRIM(AxisLabelMetric) as AxisLabelMetric,RTRIM(DataTable) as DataTable,RTRIM(ValueField1) as ValueField1,
                  RTRIM(ValueField2) as ValueField2,RTRIM(ValueField3) as ValueField3,RTRIM(ValueField4) as ValueField4,
                  RTRIM(ValueField5) as ValueField5,RTRIM(TargetField) as TargetField,RTRIM(YTDField) as YTDField,
                  RTRIM(AvgField) as AvgField,RTRIM(Legend1) As Legend1,RTRIM(Legend2) As Legend2,RTRIM(Legend3) As Legend3,
                  RTRIM(Legend4) As Legend4,RTRIM(Legend5) As Legend5,DecPlaces FROM Chart_LU WHERE
                  CustomGroup=0 OR CustomGroup IN (SELECT CustomGroup FROM CoCustom WHERE CompanyID=@CompanyID AND CustomType='C')
                  ORDER BY SortKey
