﻿CREATE PROCEDURE [dbo].[DS_YieldRMBSchema]
		@RefineryID nvarchar(10)
AS
BEGIN
		SELECT m.SortKey, RTRIM(y.Category) AS Category, RTRIM(y.MaterialID) as MaterialID,
                   RTRIM(y.MaterialName) as MaterialName, CAST(0.0 AS FLOAT) AS Bbl, CAST(0.0 AS FLOAT) AS PriceLocal FROM Yield y , Material_LU m WHERE  m.MaterialID=y.MaterialID AND 
                   y.Category ='RMB' AND y.SubmissionID=(SELECT TOP 1 SubmissionID From dbo.Submissions WHERE RefineryID = @RefineryID ORDER BY PeriodStart DESC)
END

