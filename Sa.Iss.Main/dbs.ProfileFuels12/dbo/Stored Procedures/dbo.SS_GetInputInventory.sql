﻿CREATE PROC [dbo].[SS_GetInputInventory]
	@RefineryID nvarchar(10),
	@DataSet nvarchar(20)='ACTUAL'
AS

SELECT  
            s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            TankType,NumTank,FuelsStorage,AvgLevel  
            FROM dbo.Inventory i 
            ,dbo.Submissions s  
            WHERE   
            i.SubmissionID = s.SubmissionID AND 
            i.SubmissionID IN  
            (SELECT DISTINCT SubmissionID FROM dbo.Submissions
            WHERE RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1)

