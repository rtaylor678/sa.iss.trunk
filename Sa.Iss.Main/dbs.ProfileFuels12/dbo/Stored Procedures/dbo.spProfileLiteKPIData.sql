﻿

CREATE   PROC [dbo].[spProfileLiteKPIData] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON

DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime, @StartQTR smalldatetime, @EndQuarter smalldatetime
SELECT @Start3Mo = p.Start3Mo, @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo, @StartYTD = p.StartYTD, @StartQTR = p.StartQTR, @EndQuarter = p.EndQTR
FROM dbo.GetPeriods(@SubmissionID) p

DECLARE @SubListMonth dbo.SubmissionIDList, @SubListQTR dbo.SubmissionIDList, @SubListYTD dbo.SubmissionIDList, @SubList12Mo dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
INSERT @SubListMonth SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @PeriodStart, @PeriodEnd)
INSERT @SubListYTD SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartYTD, @PeriodEnd)
INSERT @SubList12Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd)
INSERT @SubList24Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start24Mo, @PeriodEnd)


DECLARE	
	@EII real, @EII_YTD real, @EII_Avg real, 
	@EnergyUseDay real, @EnergyUseDay_YTD real, @EnergyUseDay_Avg real, 
	@TotStdEnergy real, @TotStdEnergy_YTD real, @TotStdEnergy_Avg real, 

	@RefUtilPcnt real, @RefUtilPcnt_YTD real, @RefUtilPcnt_Avg real, 
	@EDC real, @EDC_YTD real, @EDC_Avg real, 

	@ProcessEffIndex real, @ProcessEffIndex_YTD real, @ProcessEffIndex_Avg real, 

	@VEI real, @VEI_YTD real, @VEI_Avg real, 
	@ReportLossGain real, @ReportLossGain_YTD real, @ReportLossGain_Avg real, 
	@EstGain real, @EstGain_YTD real, @EstGain_Avg real, 
	@NetInputBPD real, @NetInputBPD_YTD real, @NetInputBPD_Avg real,
	
	@OpAvail real, @OpAvail_YTD real, @OpAvail_Avg real, 
	@MechUnavailTA real, @MechUnavailTA_YTD real, @MechUnavailTA_Avg real, 
	@NonTAUnavail real, @NonTAUnavail_YTD real, @NonTAUnavail_Avg real, 

	@PersIndex real, @PersIndex_YTD real, @PersIndex_Avg real, 
	@AnnTAWHr real, @AnnTAWHr_YTD real, @AnnTAWHr_Avg real,
	@NonTAWHr real, @NonTAWHr_YTD real, @NonTAWHr_Avg real,
	@TAWHrEDC real,
	
	@MaintIndex real, @MaintIndex_YTD real, @MaintIndex_Avg real, 
	@RoutCost real, @RoutCost_YTD real, @RoutCost_Avg real, 
	@AnnTACost real, @AnnTACost_YTD real, @AnnTACost_Avg real,

	@NEOpExEDC real, @NEOpExEDC_YTD real, @NEOpExEDC_Avg real, 
	@NEOpEx real, @NEOpEx_YTD real, @NEOpEx_Avg real, 

	@OpExUEDC real, @OpExUEDC_YTD real, @OpExUEDC_Avg real,
	@EnergyCost real, @EnergyCost_YTD real, @EnergyCost_Avg real, 
	@TAAdj real, @TAAdj_YTD real, @TAAdj_Avg real,
	@TotCashOpEx real, @TotCashOpEx_YTD real, @TotCashOpEx_Avg real,
	@UEDC real, @UEDC_YTD real, @UEDC_Avg real
	
SELECT @EII = NULL, @EII_YTD = NULL, @EII_Avg = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_YTD = NULL, @EnergyUseDay_Avg = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_YTD = NULL, @TotStdEnergy_Avg = NULL, 
	@RefUtilPcnt = NULL, @RefUtilPcnt_YTD = NULL, @RefUtilPcnt_Avg = NULL, 
	@EDC = NULL, @EDC_YTD = NULL, @EDC_Avg = NULL, 
	@UEDC = NULL, @UEDC_YTD = NULL, @UEDC_Avg = NULL, 
	@VEI = NULL, @VEI_YTD = NULL, @VEI_Avg = NULL, 
	@ReportLossGain = NULL, @ReportLossGain_YTD = NULL, @ReportLossGain_Avg = NULL, 
	@EstGain = NULL, @EstGain_YTD = NULL, @EstGain_Avg = NULL, 
	@OpAvail = NULL, @OpAvail_YTD = NULL, @OpAvail_Avg = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_YTD = NULL, @MechUnavailTA_Avg = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_YTD = NULL, @NonTAUnavail_Avg = NULL, 
	@MaintIndex = NULL, @MaintIndex_YTD = NULL, @MaintIndex_Avg = NULL,
	@RoutCost = NULL, @RoutCost_YTD = NULL, @RoutCost_Avg = NULL, 
	@PersIndex = NULL, @PersIndex_YTD = NULL, @PersIndex_Avg = NULL, 
	@AnnTAWHr = NULL, @AnnTAWHr_YTD = NULL, @AnnTAWHr_Avg = NULL,
	@NonTAWHr = NULL, @NonTAWHr_YTD = NULL, @NonTAWHr_Avg = NULL,
	@NEOpExEDC = NULL, @NEOpExEDC_YTD = NULL, @NEOpExEDC_Avg = NULL, 
	@NEOpEx = NULL, @NEOpEx_YTD = NULL, @NEOpEx_Avg = NULL, 
	@OpExUEDC = NULL, @OpExUEDC_YTD = NULL, @OpExUEDC_Avg = NULL, 
	@TAAdj = NULL, @TAAdj_YTD = NULL, @TAAdj_Avg = NULL,
	@EnergyCost = NULL, @EnergyCost_YTD = NULL, @EnergyCost_Avg = NULL, 
	@TotCashOpEx = NULL, @TotCashOpEx_YTD = NULL, @TotCashOpEx_Avg = NULL

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

SELECT @EII = EII, @EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy
	, @RefUtilPcnt = RefUtilPcnt, @EDC = EDC, @UEDC = UEDC
	, @VEI = VEI, @ReportLossGain = ReportLossGain, @EstGain = EstGain
	, @ProcessEffIndex = ProcessEffIndex, @NetInputBPD = NetInputBPD
	, @OpAvail = OpAvail, @MechUnavailTA = MechUnavailTA, @NonTAUnavail = MechUnavailRout + RegUnavail
	, @MaintIndex = MaintIndex, @RoutCost = RoutCost, @AnnTACost = AnnTACost
	, @PersIndex = PersIndex, @AnnTAWHr = NULL, @NonTAWHr = NULL
	, @NEOpExEDC = NEOpExEDC, @NEOpEx = NEOpEx
	, @OpExUEDC = OpExUEDC, @TAAdj = AnnTACost, @EnergyCost = EnergyCost, @TotCashOpEx = TotCashOpEx
FROM dbo.SLProfileLiteKPIs(@SubListMonth, @FactorSet, @Scenario, @Currency, @UOM)

SELECT @NonTAWHr = TotNonTAWHr/1000 FROM dbo.SLSumPersTot(@SubListMonth)
SELECT @TAWHrEDC = (ISNULL(p.OCCTAWHrEDC,0) + ISNULL(p.MPSTAWHrEDC,0)) FROM dbo.SLPersTAAdj(@SubListMonth, @FactorSet) p
SELECT @AnnTAWHr = @TAWHrEDC*@EDC*100

SELECT @EII_YTD = EII, @EnergyUseDay_YTD = EnergyUseDay, @TotStdEnergy_YTD = TotStdEnergy
	, @RefUtilPcnt_YTD = RefUtilPcnt, @EDC_YTD = EDC, @UEDC_YTD = UEDC
	, @VEI_YTD = VEI, @ReportLossGain_YTD = ReportLossGain, @EstGain_YTD = EstGain
	, @ProcessEffIndex_YTD = ProcessEffIndex, @NetInputBPD_YTD = NetInputBPD
	, @OpAvail_YTD = OpAvail, @MechUnavailTA_YTD = MechUnavailTA, @NonTAUnavail_YTD = MechUnavailRout + RegUnavail
	, @MaintIndex_YTD = MaintIndex, @RoutCost_YTD = RoutCost, @AnnTACost_YTD = AnnTACost
	, @PersIndex_YTD = PersIndex, @AnnTAWHr_YTD = NULL, @NonTAWHr_YTD = NULL
	, @NEOpExEDC_YTD = NEOpExEDC, @NEOpEx_YTD = NEOpEx
	, @OpExUEDC_YTD = OpExUEDC, @TAAdj_YTD = AnnTACost, @EnergyCost_YTD = EnergyCost, @TotCashOpEx_YTD = TotCashOpEx
FROM dbo.SLProfileLiteKPIs(@SubListYTD, @FactorSet, @Scenario, @Currency, @UOM)

SELECT @NonTAWHr_YTD = TotNonTAWHr/1000 FROM dbo.SLSumPersTot(@SubListYTD)
SELECT @AnnTAWHr_YTD = @TAWHrEDC*@EDC_YTD*100

SELECT @EII_Avg = EII, @EnergyUseDay_Avg = EnergyUseDay, @TotStdEnergy_Avg = TotStdEnergy
	, @RefUtilPcnt_Avg = RefUtilPcnt, @EDC_Avg = EDC, @UEDC_Avg = UEDC
	, @VEI_Avg = VEI, @ReportLossGain_Avg = ReportLossGain, @EstGain_Avg = EstGain
	, @ProcessEffIndex_Avg = ProcessEffIndex, @NetInputBPD_Avg = NetInputBPD
	, @PersIndex_Avg = PersIndex, @AnnTAWHr_Avg = NULL, @NonTAWHr_Avg = NULL
	, @NEOpExEDC_Avg = NEOpExEDC, @NEOpEx_Avg = NEOpEx
	, @OpExUEDC_Avg = OpExUEDC, @TAAdj_Avg = AnnTACost, @EnergyCost_Avg = EnergyCost, @TotCashOpEx_Avg = TotCashOpEx
FROM dbo.SLProfileLiteKPIs(@SubList12Mo, @FactorSet, @Scenario, @Currency, @UOM)

SELECT @NonTAWHr_YTD = TotNonTAWHr/1000 FROM dbo.SLSumPersTot(@SubList12Mo)
SELECT @AnnTAWHr_Avg = @TAWHrEDC*@EDC_Avg*100

SELECT @OpAvail_Avg = OpAvail, @MechUnavailTA_Avg = MechUnavailTA, @NonTAUnavail_Avg = MechUnavailRout + RegUnavail
	, @MaintIndex_Avg = MaintIndex, @RoutCost_Avg = RoutCost, @AnnTACost_Avg = AnnTACost
FROM dbo.SLProfileLiteKPIs(@SubList24Mo, @FactorSet, @Scenario, @Currency, @UOM)


SELECT 
	EII = @EII, EII_YTD = @EII_YTD, EII_Avg = @EII_Avg, 
	EnergyUseDay = @EnergyUseDay, EnergyUseDay_YTD = @EnergyUseDay_YTD, EnergyUseDay_Avg = @EnergyUseDay_Avg, 
	TotStdEnergy = @TotStdEnergy, TotStdEnergy_YTD = @TotStdEnergy_YTD, TotStdEnergy_Avg = @TotStdEnergy_Avg, 

	UtilPcnt = @RefUtilPcnt, UtilPcnt_YTD = @RefUtilPcnt_YTD, UtilPcnt_Avg = @RefUtilPcnt_Avg, 
	EDC = @EDC, EDC_YTD = @EDC_YTD, EDC_Avg = @EDC_Avg, 
	UtilUEDC = @EDC*@RefUtilPcnt/100, UtilUEDC_YTD = @EDC_YTD*@RefUtilPcnt_YTD/100, UtilUEDC_Avg = @EDC_Avg*@RefUtilPcnt_Avg/100, 
	
	ProcessEffIndex = @ProcessEffIndex, ProcessEffIndex_YTD = @ProcessEffIndex_YTD, ProcessEffIndex_Avg = @ProcessEffIndex_Avg, 
	VEI = @VEI, VEI_YTD = @VEI_YTD, VEI_Avg = @VEI_Avg, 
	NetInputBPD = @NetInputBPD, NetInputBPD_YTD = @NetInputBPD_YTD, NetInputBPD_Avg = @NetInputBPD_Avg,
	ReportLossGain = @ReportLossGain, ReportLossGain_YTD = @ReportLossGain_YTD, ReportLossGain_Avg = @ReportLossGain_Avg, 
	EstGain = @EstGain, EstGain_YTD = @EstGain_YTD, EstGain_Avg = @EstGain_Avg, 

	OpAvail = @OpAvail, OpAvail_YTD = @OpAvail_YTD, OpAvail_Avg = @OpAvail_Avg, 
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_YTD = @MechUnavailTA_YTD, MechUnavailTA_Avg = @MechUnavailTA_Avg, 
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_YTD = @NonTAUnavail_YTD, NonTAUnavail_Avg = @NonTAUnavail_Avg, 

	TotWHrEDC = @PersIndex, TotWHrEDC_YTD = @PersIndex_YTD, TotWHrEDC_Avg = @PersIndex_Avg, 
	AnnTAWHr = @AnnTAWHr, AnnTAWHr_YTD = @AnnTAWHr_YTD, AnnTAWHr_Avg = @AnnTAWHr_Avg,
	NonTAWHr = @NonTAWHr, NonTAWHr_YTD = @NonTAWHr_YTD, NonTAWHr_Avg = @NonTAWHr_Avg, 

	MaintIndex = @MaintIndex, MaintIndex_YTD = @MaintIndex_YTD, MaintIndex_Avg = @MaintIndex_Avg, 
	RoutCost = @RoutCost, RoutCost_YTD = @RoutCost_YTD, RoutCost_Avg = @RoutCost_Avg, 
	AnnTACost = @AnnTACost, AnnTACost_YTD = @AnnTACost_YTD, AnnTACost_Avg = @AnnTACost_Avg,

	NEOpExEDC = @NEOpExEDC, NEOpExEDC_YTD = @NEOpExEDC_YTD, NEOpExEDC_Avg = @NEOpExEDC_Avg, 
	NEOpEx = @NEOpEx, NEOpEx_YTD = @NEOpEx_YTD, NEOpEx_Avg = @NEOpEx_Avg, 

	TotCashOpExUEDC = @OpExUEDC, TotCashOpExUEDC_YTD = @OpExUEDC_YTD, TotCashOpExUEDC_Avg = @OpExUEDC_Avg, 
	TAAdj = @TAAdj, TAAdj_YTD = @TAAdj_YTD, TAAdj_Avg = @TAAdj_Avg,
	EnergyCost = @EnergyCost, EnergyCost_YTD = @EnergyCost_YTD, EnergyCost_Avg = @EnergyCost_Avg, 
	TotCashOpEx = @TotCashOpEx, TotCashOpEx_YTD = @TotCashOpEx_YTD, TotCashOpEx_Avg = @TotCashOpEx_Avg, 
	UEDC = @UEDC, UEDC_YTD = @UEDC_YTD, UEDC_Avg = @UEDC_Avg






