﻿CREATE PROC [dbo].[DS_OpEx_LU]
	
AS

SELECT OpExID,RTRIM(Description) AS Description,Indent, RTRIM(ParentID) AS ParentID, RTRIM(DetailStudy) AS DetailStudy, RTRIM(DetailProfile) AS DetailProfile,SortKey FROM OpEx_LU ORDER BY SortKey
