﻿CREATE PROC [dbo].[SS_GetInputOpEx]
	@RefineryID nvarchar(10),
	@DataSet nvarchar(20)='ACTUAL'
AS

SELECT  
s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            ThirdPartyTerminalProd,ThirdPartyTerminalRM,OthRevenue, 
            OthNonVol,OthVol,PurOth,Catalysts,Chemicals,GAPers,Envir,OthCont, 
            ContMaintLabor,MaintMatl,MPSBen,OCCBen,MPSSal,OCCSal,POXO2 
            FROM  
            dbo.OpEx o 
            ,dbo.Submissions s  
            WHERE  
            o.SubmissionID = s.SubmissionID AND 
            DataType='RPT' AND (o.SubmissionID IN  
            (SELECT DISTINCT SubmissionID FROM dbo.Submissions 
            WHERE RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1))

