﻿CREATE PROC [dbo].[SS_GetYieldProd]

	@RefineryID nvarchar(10),
	@PeriodStart datetime,
	@PeriodEnd datetime,
	@DataSet nvarchar(20)='ACTUAL'
	
AS

SELECT y.Category,y.MaterialID,y.MaterialName,y.Bbl,y.PriceLocal,m.SortKey
             FROM dbo.Yield y, Material_LU m WHERE m.MaterialID=y.MaterialID AND  y.SubmissionID IN 
             (SELECT SubmissionID FROM dbo.Submissions 
             WHERE RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1
             AND Category NOT IN ('OTHRM','RCHEM','RLUBE','RMI','RPF') AND y.MaterialID <> 'GAIN' AND (PeriodStart BETWEEN @PeriodStart AND 
            DateAdd(Day, -1, @PeriodEnd)))

