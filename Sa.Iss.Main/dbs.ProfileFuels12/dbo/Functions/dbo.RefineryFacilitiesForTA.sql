﻿CREATE FUNCTION [dbo].[RefineryFacilitiesForTA](@RefineryID varchar(6), @DataSet varchar(15))
RETURNS TABLE 
AS
RETURN (
	SELECT cl.UnitID, cl.ProcessID, cl.UnitName, ul.LastPeriodStart, pid.SortKey
	FROM dbo.Submissions sl INNER JOIN Config cl ON cl.SubmissionID = sl.SubmissionID
	INNER JOIN (
		SELECT c.UnitID, MAX(s.PeriodStart) AS LastPeriodStart
		FROM Config c INNER JOIN dbo.Submissions s ON c.SubmissionID = s.SubmissionID
		WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
		AND c.ProcessID IN (SELECT ProcessID FROM ProcessID_LU WHERE MaintDetails = 'Y')
		GROUP BY c.UnitID) ul ON ul.UnitID = cl.UnitID AND sl.PeriodStart = ul.LastPeriodStart
	INNER JOIN ProcessID_LU pid ON pid.ProcessID = cl.ProcessID
	WHERE sl.RefineryID = @RefineryID AND sl.UseSubmission = 1
	UNION
	SELECT DISTINCT ta.UnitID, ta.ProcessID, pid.Description, (SELECT MAX(PeriodStart) FROM Submissions WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UseSubmission = 1), pid.SortKey
	FROM MaintTA ta INNER JOIN ProcessID_LU pid ON pid.ProcessID = ta.ProcessID
	WHERE ta.RefineryID = @RefineryID AND ta.DataSet = @DataSet
	AND pid.MaintDetails = 'Y' AND pid.ProfileProcFacility = 'N'
)


