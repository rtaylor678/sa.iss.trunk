﻿

CREATE FUNCTION [dbo].[SLProcessAvailability](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT s.RefineryID, s.DataSet, g.FactorSet, g.ProcessGrouping AS ProcessID, 
	MechAvail_Ann = [$(dbsGlobal)].dbo.WtAvg(m.MechAvail_Ann,m.PeriodEDC), MechAvailSlow_Ann = [$(dbsGlobal)].dbo.WtAvg(m.MechAvailSlow_Ann,m.PeriodEDC), MechAvailOSTA = [$(dbsGlobal)].dbo.WtAvg(m.MechAvailOSTA, m.PeriodEDC*CASE WHEN m.PeriodHrs > 0 THEN m.PeriodHrsOSTA/m.PeriodHrs END),
	OpAvail_Ann = [$(dbsGlobal)].dbo.WtAvg(m.OpAvail_Ann,m.PeriodEDC), OpAvailSlow_Ann = [$(dbsGlobal)].dbo.WtAvg(m.OpAvailSlow_Ann,m.PeriodEDC),
	OnStream_Ann = [$(dbsGlobal)].dbo.WtAvg(m.OnStream_Ann,m.PeriodEDC), OnStreamSlow_Ann = [$(dbsGlobal)].dbo.WtAvg(m.OnStreamSlow_Ann,m.PeriodEDC), 
	MechAvail_Act = [$(dbsGlobal)].dbo.WtAvg(m.MechAvail_Act,m.PeriodEDC), MechAvailSlow_Act = [$(dbsGlobal)].dbo.WtAvg(m.MechAvailSlow_Act,m.PeriodEDC),
	OpAvail_Act = [$(dbsGlobal)].dbo.WtAvg(m.OpAvail_Act,m.PeriodEDC), OpAvailSlow_Act = [$(dbsGlobal)].dbo.WtAvg(m.OpAvailSlow_Act,m.PeriodEDC),
	OnStream_Act = [$(dbsGlobal)].dbo.WtAvg(m.OnStream_Act,m.PeriodEDC), OnStreamSlow_Act = [$(dbsGlobal)].dbo.WtAvg(m.OnStreamSlow_Act,m.PeriodEDC),
	MechUnavailTA_Ann = [$(dbsGlobal)].dbo.WtAvg(m.MechUnavailTA_Ann,m.PeriodEDC), MechUnavailTA_Act = [$(dbsGlobal)].dbo.WtAvg(m.MechUnavailTA_Act,m.PeriodEDC), 
	MechUnavailPlan = [$(dbsGlobal)].dbo.WtAvg(m.MechUnavailPlan,m.PeriodEDC), MechUnavailUnp = [$(dbsGlobal)].dbo.WtAvg(m.MechUnavailUnp,m.PeriodEDC), 
	MechUnavail_Ann = [$(dbsGlobal)].dbo.WtAvg(m.MechUnavail_Ann,m.PeriodEDC), MechUnavail_Act = [$(dbsGlobal)].dbo.WtAvg(m.MechUnavail_Act,m.PeriodEDC), 
	RegUnavail = [$(dbsGlobal)].dbo.WtAvg(m.RegUnavail,m.PeriodEDC), RegUnavailPlan = [$(dbsGlobal)].dbo.WtAvg(m.RegUnavailPlan,m.PeriodEDC), RegUnavailUnp = [$(dbsGlobal)].dbo.WtAvg(m.RegUnavailUnp,m.PeriodEDC), 
	OpUnavail_Ann = [$(dbsGlobal)].dbo.WtAvg(m.OpUnavail_Ann,m.PeriodEDC), OpUnavail_Act = [$(dbsGlobal)].dbo.WtAvg(m.OpUnavail_Act,m.PeriodEDC), 
	OthUnavailEconomic = [$(dbsGlobal)].dbo.WtAvg(m.OthUnavailEconomic,m.PeriodEDC), OthUnavailExternal = [$(dbsGlobal)].dbo.WtAvg(m.OthUnavailExternal,m.PeriodEDC), OthUnavailUnitUpsets = [$(dbsGlobal)].dbo.WtAvg(m.OthUnavailUnitUpsets,m.PeriodEDC), OthUnavailOffsiteUpsets = [$(dbsGlobal)].dbo.WtAvg(m.OthUnavailOffsiteUpsets,m.PeriodEDC), OthUnavailOther = [$(dbsGlobal)].dbo.WtAvg(m.OthUnavailOther,m.PeriodEDC), 
	OthUnavail = [$(dbsGlobal)].dbo.WtAvg(m.OthUnavail,m.PeriodEDC), OthUnavailPlan = [$(dbsGlobal)].dbo.WtAvg(m.OthUnavailPlan,m.PeriodEDC), OthUnavailUnp = [$(dbsGlobal)].dbo.WtAvg(m.OthUnavailUnp,m.PeriodEDC), 
	TotUnavail_Ann = [$(dbsGlobal)].dbo.WtAvg(m.TotUnavail_Ann,m.PeriodEDC), TotUnavail_Act = [$(dbsGlobal)].dbo.WtAvg(m.TotUnavail_Act,m.PeriodEDC), TotUnavailUnp = [$(dbsGlobal)].dbo.WtAvg(m.TotUnavailUnp,m.PeriodEDC)
	FROM [dbo].[SLUnitAvailability](@SubmissionList) m 
	INNER JOIN (dbo.SLLastPeriods(@SubmissionList) s INNER JOIN RefProcessGroupings g ON g.SubmissionID = s.SubmissionID)  --This filters the results to only those units reported in last month of period*/
		ON g.UnitID = m.UnitID AND g.FactorSet = m.FactorSet
	GROUP BY s.RefineryID, s.DataSet, g.FactorSet, g.ProcessGrouping
)




