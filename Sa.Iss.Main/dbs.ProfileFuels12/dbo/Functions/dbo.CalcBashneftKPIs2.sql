﻿CREATE FUNCTION [dbo].[CalcBashneftKPIs2] (@SubmissionList dbo.SubmissionIDList READONLY, 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @UOM varchar(5))
RETURNS @KPIData TABLE (
	ProcessUtilPcnt real NULL, ProcessEDC real NULL, ProcessUEDC real NULL, OffsitesEDC real NULL, Complexity real NULL,
	EII real NULL, EnergyUseDay real NULL, TotStdEnergy real NULL, FuelUseDay real NULL,
	OpAvail real NULL, MechUnavailTA real NULL, NonTAOpUnavail real NULL, TADD real NULL, NTAMDD real NULL, RPDD real NULL, NumDays real NULL,
	MechAvail real NULL, NonTAMechUnavail real NULL,
	MaintEffIndex real NULL, AnnTACost real NULL, RoutCost real NULL, MaintEffDiv real NULL, TAIndex real NULL,
	mPersEffIndex real NULL, MaintWHr real NULL, OCCMaintWHr real NULL, MPSMaintWHr real NULL, mPersEffDiv real NULL, 
	NEOpExEDC real NULL, NEOpEx real NULL, EDC real NULL,
	TotCashOpExUEDC real NULL, TAAdj real NULL, EnergyCost real NULL, TotCashOpEx real NULL, UEDC real NULL, ExchRate real NULL
)
AS
BEGIN

DECLARE	
	@ProcessUtilPcnt real, @TotProcessEDC real, @TotProcessUEDC real, @OffsitesEDC real, @Complexity real,
	@EII real, @EnergyUseDay real, @FuelUseDay real, @TotStdEnergy real,
	@OpAvail real, @MechAvail real, @MechUnavailTA real, @NonTAMechUnavail real, @NonTAOpUnavail real, @RegUnavail real,
	@TADD real, @NTAMDD real, @RPDD real, @NumDays real,
	@MaintEffIndex real, @TAAdj real, @AnnTACost real, @RoutCost real, @MaintEffDiv real, @EDC real, @TAIndex real, @TAEffIndex real,
	@mPersEffIndex real, @MaintWHr real, @OCCMaintWHr real, @MPSMaintWHr real, @mPersEffDiv real,
	@OpExUEDC real, @EnergyCost real, @TotCashOpEx real, @UEDC real, @NEOpExEDC real, @NEOpEx real, @ExchRate real

SELECT @ProcessUtilPcnt = ProcessUtilPcnt, @TotProcessEDC = TotProcessEDC/1000, @TotProcessUEDC = TotProcessUEDC/1000, @Complexity = Complexity 
	, @EII = EII, @EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy
	, @OpAvail = OpAvail, @MechAvail = MechAvail, @MechUnavailTA = MechUnavailTA, @NonTAMechUnavail = MechUnavailRout, @NonTAOpUnavail = RegUnavail + MechUnavailRout, @RegUnavail = RegUnavail
	, @NumDays = NumDays
	, @MaintEffIndex = MaintEffIndex, @TAAdj = AnnTACost, @AnnTACost = AnnTACost, @RoutCost = RoutCost, @MaintEffDiv = MaintEffDiv, @EDC = EDC/1000, @TAIndex = TAIndex, @TAEffIndex = TAEffIndex
	, @mPersEffIndex = mPersEffIndex, @MaintWHr = MaintWHr_k, @OCCMaintWHr = OCCMaintWHr_k, @MPSMaintWHr = MPSMaintWHr_k, @mPersEffDiv = mPersEffDiv
	, @OpExUEDC = OpExUEDC, @EnergyCost = EnergyCost, @TotCashOpEx = TotCashOpEx, @UEDC = UEDC/1000, @NEOpExEDC = NEOpExEDC, @NEOpEx = NEOpEx
FROM dbo.SLProfileLiteKPIs(@SubmissionList, @FactorSet, @Scenario, @Currency, @UOM)

SELECT @OffsitesEDC = @EDC - @TotProcessEDC

SELECT @FuelUseDay = SUM(PurFGMBTU + PurLiquidMBTU + ProdOthMBTU + ProdFGMBTU)/(SUM(s.NumDays*1.0)/COUNT(DISTINCT s.RefineryID+s.DataSet))*CASE WHEN LEFT(@UOM,3)='MET' THEN 1.055 ELSE 1.0 END
FROM EnergyTot e INNER JOIN Submissions s ON s.SubmissionID = e.SubmissionID
INNER JOIN @SubmissionList sl ON sl.SubmissionID = s.SubmissionID

SELECT @TADD = @MechUnavailTA/100*@NumDays, @NTAMDD = @NonTAMechUnavail/100*@NumDays, @RPDD = @RegUnavail/100*@NumDays

SELECT @MaintWHr = @mPersEffIndex*@mPersEffDiv/100
	, @OCCMaintWHr = @mPersEffIndex*@mPersEffDiv/100*(OCCWHrEDC/TotWHrEDC)
	, @MPSMaintWHr = @mPersEffIndex*@mPersEffDiv/100*(MPSWHrEDC/TotWHrEDC)
FROM dbo.SLAveragePersKPIs(@SubmissionList, @FactorSet)

SELECT @ExchRate = dbo.AvgExchangeRate('RUB', MIN(PeriodStart), MAX(PeriodEnd))
FROM Submissions WHERE SubmissionID IN (SELECT SubmissionID FROM @SubmissionList)

INSERT @KPIData (ProcessUtilPcnt, ProcessEDC, ProcessUEDC, OffsitesEDC, Complexity,
	EII, EnergyUseDay, TotStdEnergy, FuelUseDay, 
	OpAvail, MechUnavailTA, NonTAOpUnavail, TADD, NTAMDD, RPDD, NumDays,
	MechAvail, NonTAMechUnavail,
	MaintEffIndex, AnnTACost, RoutCost, MaintEffDiv, TAIndex,
	mPersEffIndex, MaintWHr, OCCMaintWHr, MPSMaintWHr, mPersEffDiv, 
	NEOpExEDC, NEOpEx, EDC,
	TotCashOpExUEDC, TAAdj, EnergyCost, TotCashOpEx, UEDC, ExchRate)
SELECT @ProcessUtilPcnt, @TotProcessEDC, @TotProcessUEDC, @OffsitesEDC, @Complexity, 	
	@EII, @EnergyUseDay, @TotStdEnergy, @FuelUseDay,
	@OpAvail, @MechUnavailTA, @NonTAOpUnavail, TADD = @MechUnavailTA/100*@NumDays, NTAMDD = @NonTAMechUnavail/100*@NumDays, RPDD = @RegUnavail/100*@NumDays, @NumDays,
	@MechAvail, @NonTAMechUnavail,
	@MaintEffIndex, @TAAdj/1000, @RoutCost/1000, @MaintEffDiv/1000000, @TAIndex,
	@mPersEffIndex, @MaintWHr/1000, @OCCMaintWHr/1000, @MPSMaintWHr/1000, @mPersEffDiv/1000,
	@NEOpExEDC, @NEOpEx/1000, @EDC,
	@OpExUEDC, @TAAdj/1000, @EnergyCost/1000, @TotCashOpEx/1000, @UEDC, @ExchRate

RETURN

END

