﻿CREATE function [dbo].[UnitsConv](@ValToConv float, @FromUnits varchar(255), @ToUnits varchar(255))
RETURNS float
BEGIN
	DECLARE @DivLocTo int, @DivLocFrom int
	DECLARE @UOMGroupFrom varchar(20), @FactorFrom float
	DECLARE @UOMGroupTo varchar(20), @FactorTo float
	DECLARE @Result float

	IF @ToUnits = @FromUnits 
		SET @Result = @ValToConv
	ELSE IF CHARINDEX('/', @ToUnits) > 0 
	BEGIN
		SELECT 	@DivLocTo = CHARINDEX('/', @ToUnits),
			@DivLocFrom = CHARINDEX('/', @FromUnits)
		SELECT @Result = dbo.UnitsConv(@ValToConv, SUBSTRING(@FromUnits, 1, @DivLocFrom-1), SUBSTRING(@ToUnits, 1, @DivLocTo-1))/
                    		dbo.UnitsConv(1, SUBSTRING(@FromUnits, @DivLocFrom+1, LEN(@FromUnits)), SUBSTRING(@ToUnits, @DivLocTo+1, LEN(@ToUnits)))
	END
	ELSE IF dbo.CaseSensitiveSearch(@ToUnits, 'p', 1) > 0
	BEGIN
		SELECT 	@DivLocTo = dbo.CaseSensitiveSearch(@ToUnits, 'p', 1),
			@DivLocFrom = dbo.CaseSensitiveSearch(@FromUnits, 'p', 1)
		SELECT @Result = dbo.UnitsConv(@ValToConv, SUBSTRING(@FromUnits, 1, @DivLocFrom-1), SUBSTRING(@ToUnits, 1, @DivLocTo-1))
	END
	ELSE IF CHARINDEX('*', @ToUnits) > 0
	BEGIN
		SELECT 	@DivLocTo = CHARINDEX('*', @ToUnits),
			@DivLocFrom = CHARINDEX('*', @FromUnits)
		SELECT @Result = dbo.UnitsConv(@ValToConv, SUBSTRING(@FromUnits, 1, @DivLocFrom-1), SUBSTRING(@ToUnits, 1, @DivLocTo-1))*
				dbo.UnitsConv(1, SUBSTRING(@FromUnits,@DivLocFrom+1, LEN(@FromUnits)), SUBSTRING(@ToUnits, @DivLocTo+1, LEN(@ToUnits)))
	END
	ELSE BEGIN
		SELECT @UOMGroupFrom = UOMGroup, @FactorFrom = Factor
		FROM UOM WHERE UOMCode = @FromUnits
		SELECT @UOMGroupTo = UOMGroup, @FactorTo = Factor
		FROM UOM WHERE UOMCode = @ToUnits

		SELECT @Result = CASE ISNULL(@UOMGroupFrom, '')
			WHEN '' THEN @ValToConv
			WHEN 'Gravity' THEN dbo.ConvGravity(@ValToConv, @FromUnits, @ToUnits)
			WHEN 'Temperature' THEN dbo.ConvTemp(@ValToConv, @FromUnits, @ToUnits)
			WHEN 'Viscosity' THEN dbo.ConvViscosity(@ValToConv, @FromUnits, @ToUnits)
			ELSE @ValToConv * (@FactorTo/@FactorFrom) END
	END
	RETURN @Result
END
