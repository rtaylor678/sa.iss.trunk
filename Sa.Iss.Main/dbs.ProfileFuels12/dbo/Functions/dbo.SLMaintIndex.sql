﻿
CREATE FUNCTION [dbo].[SLMaintIndex](@SubmissionList dbo.SubmissionIDList READONLY, @NumMonths tinyint, @FactorSet varchar(8), @Currency char(4))
RETURNS TABLE
AS
RETURN (

	SELECT 	r.FactorSet, r.Currency, r.RoutIndex, r.RoutMatlIndex, r.RoutEffIndex
		, ta.TAIndex, ta.TAMatlIndex, ta.TAEffIndex
		, MaintIndex = ISNULL(r.RoutIndex,0) + ISNULL(ta.TAIndex,0), MaintMatlIndex = ISNULL(r.RoutMatlIndex,0) + ISNULL(ta.TAMatlIndex,0)
		, MaintEffIndex = CASE WHEN r.RoutEffIndex IS NULL AND ta.TAEffIndex IS NULL THEN NULL ELSE ISNULL(r.RoutEffIndex, 0) + ISNULL(ta.TAEffIndex, 0) END
	FROM dbo.SLRoutIndex(@SubmissionList, @NumMonths, @FactorSet, @Currency) r
	INNER JOIN dbo.SLTAIndex(@SubmissionList, @FactorSet, @Currency) ta ON ta.FactorSet = r.FactorSet AND ta.Currency = r.Currency

)

