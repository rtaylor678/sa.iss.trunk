﻿
CREATE FUNCTION [dbo].[SLAverageGrossMargin](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT m.Currency, m.Scenario, GPV = [$(dbsGlobal)].dbo.WtAvg(m.GPV,m.Divisor)
		, RMC = [$(dbsGlobal)].dbo.WtAvg(m.RMC, m.Divisor)
		, GrossMargin = [$(dbsGlobal)].dbo.WtAvg(m.GrossMargin, m.Divisor)
		, OthRev = [$(dbsGlobal)].dbo.WtAvg(m.OthRev, m.Divisor)
--		, CashOpEx = [$(dbsGlobal)].dbo.WtAvg(m.CashOpEx, m.Divisor)
--		, CashMargin = [$(dbsGlobal)].dbo.WtAvg(m.CashMargin, m.Divisor)
	FROM MarginCalc m INNER JOIN @SubmissionList s ON s.SubmissionID = m.SubmissionID
	WHERE m.DataType = 'Bbl'
	GROUP BY m.Currency, m.Scenario
	)

