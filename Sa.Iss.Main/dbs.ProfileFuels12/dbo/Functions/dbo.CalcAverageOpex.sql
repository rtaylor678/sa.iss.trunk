﻿CREATE  FUNCTION [dbo].[CalcAverageOpEx](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode)
RETURNS @OpEx TABLE (FactorSet varchar(8), Scenario dbo.Scenario, Currency dbo.CurrencyCode, TotCashOpExUEDC real NULL, VolOpExUEDC real NULL, NonVolOpExUEDC real NULL, 
	NEOpExUEDC real NULL, NEOpExEDC real NULL, NEI real NULL)
AS
BEGIN

DECLARE @OpExScenario dbo.Scenario; SET @OpExScenario = 'CLIENT' -- Using Client Energy prices for all pricing scenarios

INSERT INTO @OpEx (FactorSet, Scenario, Currency, NEOpExEDC, TotCashOpExUEDC, VolOpExUEDC, NonVolOpExUEDC, NEOpExUEDC, NEI)
SELECT e.FactorSet, e.Scenario, e.Currency, e.NEOpEx, u.TotCashOpEx, u.STVol, u.STNonVol, u.NEOpEx, nei.NEOpEx
FROM dbo.CalcAverageOpExByDataType(@RefineryID, @DataSet, @StartDate, @EndDate, @FactorSet, @OpExScenario, @Currency, 'EDC') e
LEFT JOIN dbo.CalcAverageOpExByDataType(@RefineryID, @DataSet, @StartDate, @EndDate, @FactorSet, @OpExScenario, @Currency, 'UEDC') u ON u.FactorSet = e.FactorSet AND u.Scenario = e.Scenario AND u.Currency = e.Currency
LEFT JOIN dbo.CalcAverageOpExByDataType(@RefineryID, @DataSet, @StartDate, @EndDate, @FactorSet, @OpExScenario, @Currency, 'NEI') nei ON nei.FactorSet = e.FactorSet AND nei.Scenario = e.Scenario AND nei.Currency = e.Currency

RETURN
END
