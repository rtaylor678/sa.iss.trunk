﻿CREATE FUNCTION [dbo].[DaysInPeriod] (@SubmissionID int)  
RETURNS int AS  
BEGIN 
	DECLARE @NumDays int
	SELECT @NumDays = DATEDIFF(d, PeriodStart, PeriodEnd)
	FROM dbo.SubmissionsAll WHERE SubmissionID = @SubmissionID
	RETURN @NumDays
END
