﻿
CREATE VIEW [dbo].[CustomUnitReportData]
AS
SELECT c.SubmissionID, c.ProcessID, c.UnitID, c.UnitName, cur.Property
, FactorSet = ISNULL(cud.FactorSet, 'N/A'), Currency = ISNULL(cud.Currency, 'N/A')
, cur.USDescription, cur.USDecPlaces, cud.USValue, cud.USTarget
, cur.MetDescription, cur.MetDecPlaces, cud.MetValue, cud.MetTarget
, cur.SortKey
FROM Submissions s INNER JOIN TSort t ON t.RefineryID = s.RefineryID
INNER JOIN CustomUnitReport cur ON cur.CompanyID = t.CompanyID
INNER JOIN Config c ON c.SubmissionID = s.SubmissionID AND c.ProcessID = cur.ProcessID
LEFT JOIN CustomUnitData cud ON cud.SubmissionID = c.SubmissionID AND cud.UnitID = c.UnitID AND cud.Property = cur.Property


