﻿

CREATE       VIEW [dbo].[TAForSubmission] AS
SELECT s.SubmissionID, ta.*, 
HrsDownInPeriod = CASE WHEN TAExceptions IN (1,3) THEN 0 
	WHEN ta.RestartDate > s.PeriodStart AND ta.TADate < s.PeriodEnd THEN 
		DATEDIFF(hh, CASE WHEN ta.TADate < s.PeriodStart THEN s.PeriodStart ELSE ta.TADate END, CASE WHEN ta.RestartDate > s.PeriodEnd THEN s.PeriodEnd ELSE ta.RestartDate END) 
	ELSE 0 END
FROM dbo.SubmissionsAll s INNER JOIN MaintTA ta ON s.RefineryID = ta.RefineryID AND s.DataSet = ta.DataSet
WHERE (s.PeriodEnd < ta.NextTADate OR ta.NextTADate IS NULL) AND s.PeriodEnd > ta.TADate
AND (EXISTS (SELECT * FROM Config WHERE config.SubmissionID = s.SubmissionID AND config.UnitID = ta.UnitID AND Config.Cap > 0)
OR ta.ProcessID IN (SELECT ProcessID FROM ProcessID_LU WHERE MaintDetails = 'Y' AND ProcessGroup = 'U' AND ProfileProcFacility = 'N'))
AND ta.TAID > 0


