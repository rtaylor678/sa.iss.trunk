﻿




CREATE    VIEW [dbo].[PersSTCalc] AS 
SELECT e.SubmissionID, e.FactorSet, p.SectionID, Description = (SELECT SectionDesc FROM PersSectionID_LU LU WHERE LU.SectionID = p.SectionID), p.NumPers, p.STH, p.OVTHours, p.OVTPcnt, p.Contract, p.GA, p.AbsHrs,
p.CompEqP, p.CompEqP*100000/PlantEDC AS CompEqPEDC,
p.ContEqP, p.ContEqP*100000/PlantEDC AS ContEqPEDC, 
p.GAEqP, p.GAEqP*100000/PlantEDC AS GAEqPEDC,
p.TotEqP, p.TotEqP*100000/PlantEDC AS TotEqPEDC, 
p.CompWHr, p.CompWHr*100/(PlantEDC*s.FractionOfYear) AS CompWHrEDC, 
p.ContWHr, p.ContWHr*100/(PlantEDC*s.FractionOfYear) AS ContWHrEDC,
p.GAWHr, p.GAWHr*100/(PlantEDC*s.FractionOfYear) AS GAWHrEDC, 
p.TotWHr, p.TotWHr*100/(PlantEDC*s.FractionOfYear) AS TotWHrEDC,
p.TotWHr*100/CASE WHEN e.PersEffDiv > 0 THEN e.PersEffDiv*s.FractionOfYear END AS TotWHrEffIndex,
e.PlantEDC/100000 AS EqPEDCDivisor,
e.PlantEDC*s.FractionOfYear/100 AS WHrEDCDivisor,
e.PersEffDiv*s.FractionOfYear/100 AS EffDivisor
FROM PersST p 
INNER JOIN FactorTotCalc e ON e.SubmissionID = p.SubmissionID 
INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = p.SubmissionID 
WHERE e.PlantEDC>0




