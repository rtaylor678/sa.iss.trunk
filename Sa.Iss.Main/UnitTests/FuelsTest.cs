﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Sa.Iss;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using Sa.Core;
using System.Reflection;

namespace UnitTests
{
    [TestClass]
    public class FuelsTest
    {
        Excel.Application xla;
        Excel.Workbook wkb;
        Excel.Worksheet wks;

        [TestInitialize]
        public void Setup()
        {
            bool showApp = true;
            xla = XL.NewExcelApplication(showApp);
            String xlsPath = CutPath(Assembly.GetExecutingAssembly().Location, 3) + @"IssReport.xlsx";
            wkb = XL.OpenWorkbook_ReadOnly(xla, xlsPath);
        }

        [TestMethod]
        public void TestReport_AddData_ForADatasource()
        {
            TestReport_AddData(56,"Energy") ; //energy
            TestReport_AddData(57,"Opex"); //opex
        }

        public void TestReport_AddData(int dataSourceId, string contentName)
        {
            try
            {
                Excel.Sheets wksColl = wkb.Worksheets;
                foreach (Excel.Worksheet thisSheet in wksColl)
                {
                    if (thisSheet.Name.ToUpper() == contentName.ToUpper())
                    {
                        wks = thisSheet;
                        break;
                    }
                }
                string test = wks.Name; //err if not find sheet
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["BafcoConnection"].ToString()))
                {
                    int rowOffset = 1;
                    conn.Open();
                    //SqlTransaction trans = conn.BeginTransaction("Test");
                    Submission submission = new Submission { deliverableId = 1002, Id = 1030 };
                    Deliverable deliverable=new Deliverable { CurrencyId = 1, Id = 1002, MethodologyId = 1, Name = "OBSOLETE"};
                    object[] constructorArgs = new object[2];
                    constructorArgs[0]=deliverable;
                    constructorArgs[1]=submission;
                    PrivateObject reportObject = new PrivateObject(typeof(Sa.Iss.Report), constructorArgs); //Sa.Iss.Fuel.Calcs.Upload
                    object[] args = new object[6];
                    args[0] = conn;
                    args[1] = dataSourceId; 
                    args[2] = wks;
                    args[3] = rowOffset;
                    args[4] = deliverable;
                    args[5] = submission;
                    //int AddData(SqlConnection cn, int dataSourceId, Excel.Worksheet wks, int rowOffset, Deliverable deliverable, Submission submission)
                    var result = (int)reportObject.Invoke("AddData", args);
                    //trans.Rollback();
                    Assert.IsTrue(result != rowOffset);                    
                }
            }
            catch (Exception ex)
            {
                String msg = ex.Message;
                //Assert.Fail();
            }
        }



        private String CutPath(String path, int LevelsToCut)
        {
            String returnValue = String.Empty;
            String[] pathParts = path.Split('\\');
            for (int count = 0; count < (pathParts.Length - LevelsToCut); count++)
            {
                returnValue += pathParts[count] + @"\";
            }
            return returnValue;
        }

        [TestCleanup]
        public void Shutdown()
        {
            XL.CloseWorkbook(ref wkb);
            XL.CloseApplication(ref xla);
        }
       
    }
}
