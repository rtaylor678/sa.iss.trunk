﻿CREATE FUNCTION [fact].[Select_SubmissionsYTD]
(
	@fctSubmissionId	INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS RETURN
SELECT ALL
		[BaseSubmissionId]	= [s].[SubmissionId],
	[p].[SubmissionId],
	[p].[GroupId],
	[p].[DataSpan],
	[p].[DataYear],
	[p].[DataMonth],
	[p].[PeriodBeg],
	[p].[PeriodEnd],

	[a].[AttributeId],
	[a].[AttributeTag],
	[a].[AttributeName],
	[a].[AttributeDetail],
		[AttributeSortKey]	=	[a].[SortKey],

	[e].[EntityId],
	[e].[EntityTag],
	[e].[EntityName],
	[e].[EntityDetail],
		[EntitySortKey]		=	[e].[SortKey]
FROM
	[fact].[Submissions]	[s]
INNER JOIN
	[fact].[Submissions]	[p]
		ON	[p].[GroupId]	=	[s].[GroupId]
		AND	[p].[DataSpan]	=	[s].[DataSpan]
		AND	([p].[DataYear]	=	[s].[DataYear] - 1)
INNER JOIN
	[rpt].[AttributesMonths]	[a]
		ON	[a].[DataMonth]	=	[p].[DataMonth]
INNER JOIN
	[rpt].[EntitiesChart]		[e]
		ON	[e].[EntityTag]	=	'YearPrev'
WHERE	[s].[DataSpan]		=	'M'
	AND	[s].[SubmissionId]	=	@fctSubmissionId
UNION ALL
SELECT ALL
		[BaseSubmissionId]	= [s].[SubmissionId],
	[p].[SubmissionId],
	[p].[GroupId],
	[p].[DataSpan],
	[p].[DataYear],
	[p].[DataMonth],
	[p].[PeriodBeg],
	[p].[PeriodEnd],

	[a].[AttributeId],
	[a].[AttributeTag],
	[a].[AttributeName],
	[a].[AttributeDetail],
		[AttributeSortKey]	=	[a].[SortKey],

	[e].[EntityId],
	[e].[EntityTag],
	[e].[EntityName],
	[e].[EntityDetail],
		[EntitySortKey]		=	[e].[SortKey]
FROM
	[fact].[Submissions]	[s]
INNER JOIN
	[fact].[Submissions]	[p]
		ON	[p].[GroupId]	=	[s].[GroupId]
		AND	[p].[DataYear]	=	[s].[DataYear]
		AND	[p].[DataSpan]	=	[s].[DataSpan]
		AND	[p].[PeriodBeg]	<=	[s].[PeriodEnd]
INNER JOIN
	[rpt].[AttributesMonths]	[a]
		ON	[a].[DataMonth]	=	[p].[DataMonth]
INNER JOIN
	[rpt].[EntitiesChart]		[e]
		ON	[e].[EntityTag]	<>	'YearPrev'
WHERE	[s].[DataSpan]		=	'M'
	AND	[s].[SubmissionId]	=	@fctSubmissionId;