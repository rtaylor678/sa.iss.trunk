﻿CREATE FUNCTION [fact].[Select_SubmissionsLast12Mos]
(
	@fctSubmissionId	INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS RETURN
SELECT
		[BaseSubmissionId]	= [s].[SubmissionId],
	[p].[SubmissionId],
	[p].[GroupId],
	[p].[DataSpan],
	[p].[DataYear],
	[p].[DataMonth],
	[p].[PeriodBeg],
	[p].[PeriodEnd],

	[a].[AttributeId],
	[a].[AttributeTag],
	[a].[AttributeName],
	[a].[AttributeDetail],
		[AttributeSortKey]	= RANK() OVER(PARTITION BY [s].[SubmissionId] ORDER BY [p].[PeriodBeg]),

	[e].[EntityId],
	[e].[EntityTag],
	[e].[EntityName],
	[e].[EntityDetail],
		[EntitySortKey]		= [e].[SortKey]

FROM
	[fact].[Submissions]	[s]
INNER JOIN
	[fact].[Submissions]	[p]
		ON	[p].[GroupId]	=	[s].[GroupId]
		AND	[p].[DataSpan]	=	[s].[DataSpan]
		AND	(
			([p].[DataYear] =	[s].[DataYear] - 1 AND [p].[DataMonth] > [s].[DataMonth])
			OR
			([p].[DataYear] =	[s].[DataYear]	 AND [p].[DataMonth] <= [s].[DataMonth])
			)
INNER JOIN
	[rpt].[AttributesMonths]	[a]
		ON	[a].[DataMonth]	=	[p].[DataMonth]
INNER JOIN
	[rpt].[EntitiesChart]		[e]
		ON	[e].EntityTag	IN ('tgt', 'YTD')
WHERE	[s].[DataSpan]		=	'M'
	AND	[s].[SubmissionId]	=	@fctSubmissionId;