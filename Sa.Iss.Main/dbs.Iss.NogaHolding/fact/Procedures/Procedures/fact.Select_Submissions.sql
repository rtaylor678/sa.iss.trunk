﻿CREATE PROCEDURE [fact].[Select_Submissions]
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		SELECT
			[g].[GroupName],
			[f].[SubmissionId],
			[f].[DataYear],
			[f].[DataMonth]
		FROM
			[fact].[Submissions]	[f]
		INNER JOIN
			[org].[Groups]			[g]
				ON	[g].[GroupId]	= [f].[GroupId]

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, N'[fact].[Select_Submissions]';
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;