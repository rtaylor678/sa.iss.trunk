﻿CREATE PROCEDURE [fact].[Inject_Chem_Submission]
(
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @SubmissionId	INT = NULL;

		DECLARE @Submissions	TABLE
		(
			[SubmissionId]		INT		NOT	NULL,
			PRIMARY KEY CLUSTERED([SubmissionId] ASC)
		);

		INSERT INTO [fact].[Submissions]
		(
			[GroupId],
			[DataSpan],
			[DataYear],
			[DataMonth],
			[PeriodBeg],
			[PeriodEnd]
		)
		OUTPUT
			INSERTED.[SubmissionId]
		INTO
			@Submissions
			(
				[SubmissionId]
			)
		SELECT
			[f].[GroupId],
				[DataSpan]		= 'M',
				[DataYear]		= YEAR([s].[PeriodBeg]),
				[DataMonth]		= MONTH([s].[PeriodBeg]),
			[s].[PeriodBeg],
				[DateEnd]		= EOMONTH([s].[PeriodBeg])
		FROM
			[stg].[Submissions]			[s]
		INNER JOIN
			[org].[GroupFacilities]		[f]
				ON	[f].[FacilityId]	= [s].[FacilityId]
		INNER JOIN
			[org].[Groups]				[g]
				ON	[g].[GroupId]		= [f].[GroupId]
				AND	[g].[GroupName]		= [s].[FacilityName]
		LEFT OUTER JOIN
			[fact].[Submissions]		[x]
				ON	[x].[GroupId]		= [f].[GroupId]
				AND	[x].[DataSpan]		= 'M'
				AND	[x].[DataYear]		= YEAR([s].[PeriodBeg])
				AND	[x].[DataMonth]		= MONTH([s].[PeriodBeg])
		WHERE	[x].[SubmissionId]	IS NULL
			AND	[s].[SubmissionId]	= @stgSubmissionId;

		SET	@SubmissionId = 
		(
			SELECT TOP 1
				[s].[SubmissionId]
			FROM
				@Submissions	[s]
		);

		RETURN @SubmissionId;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;