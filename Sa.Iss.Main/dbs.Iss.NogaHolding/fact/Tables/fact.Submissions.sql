﻿CREATE TABLE [fact].[Submissions]
(
	[SubmissionId]						INT					NOT	NULL	IDENTITY(1, 1),

	[GroupId]							INT					NOT	NULL	CONSTRAINT [FK_Submissions_Groups]					REFERENCES [org].[Groups]([GroupId]),

	[DataSpan]							CHAR(1)				NOT	NULL,	CONSTRAINT [CR_Submissions_DataSpan]				CHECK([DataSpan] IN ('Y', 'M')),
	[DataYear]							SMALLINT			NOT	NULL,	CONSTRAINT [CR_Submissions_DataYear]				CHECK([DataYear]  >= 2010 AND [DataYear]  <= YEAR(SYSDATETIME())),
	[DataMonth]							TINYINT					NULL,	CONSTRAINT [CR_Submissions_DataMonth]				CHECK([DataMonth] >=	1 AND [DataMonth] <= 12),
	[DaysInYear]						AS CONVERT(INT, DATEPART(DAYOFYEAR, DATEFROMPARTS(YEAR([PeriodBeg]), 12, 31))),

	[PeriodBeg]							DATE					NULL,
	[PeriodEnd]							DATE					NULL,	CONSTRAINT [CR_Submissions_PeriodBegEnd]			CHECK([PeriodBeg] <= [PeriodEnd]),
	[Duration_Days]						AS CONVERT(INT, DATEDIFF(DAY, [PeriodBeg], [PeriodEnd]) + 1)
										PERSISTED					,
	[Duration_Hours]					AS CONVERT(INT, (DATEDIFF(DAY, [PeriodBeg], [PeriodEnd]) + 1) * 24),

	[Duration_Fraction]					AS CONVERT(FLOAT, DATEDIFF(DAY, [PeriodBeg], [PeriodEnd]) + 1.0) / CONVERT(FLOAT, DATEPART(DAYOFYEAR, DATEFROMPARTS(YEAR([PeriodBeg]), 12, 31))),
	[Duration_Multiplier]				AS CONVERT(FLOAT, DATEPART(DAYOFYEAR, DATEFROMPARTS(YEAR([PeriodBeg]), 12, 31))) / CONVERT(FLOAT, DATEDIFF(DAY, [PeriodBeg], [PeriodEnd]) + 1.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Submissions_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Submissions_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Submissions_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Submissions_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Submissions_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Submissions]			PRIMARY KEY NONCLUSTERED([SubmissionId] ASC),
	CONSTRAINT [UK_Submissions]			UNIQUE CLUSTERED([GroupId] ASC, [DataSpan] ASC, [DataYear] ASC, [DataMonth] ASC)
);
GO

CREATE TRIGGER [fact].[t_Submissions_u]
ON	[fact].[Submissions]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[fact].[Submissions]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[fact].[Submissions].[SubmissionId]	= [i].[SubmissionId];

END;