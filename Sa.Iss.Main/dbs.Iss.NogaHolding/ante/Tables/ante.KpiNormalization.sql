﻿CREATE TABLE [ante].[KpiNormalization]
(
	[KpiNormalizationId]				INT					NOT	NULL	IDENTITY(1, 1),

	[MethodologyId]						INT					NOT	NULL	CONSTRAINT [FK_KpiNormalization_Methodology_LookUp]		REFERENCES [ante].[Methodology]([MethodologyId]),
	[CurrencyId]						INT					NOT	NULL	CONSTRAINT [FK_KpiNormalization_Currency_LookUp]		REFERENCES [dim].[Currency_LookUp]([CurrencyId]),
	[EntityId]							INT					NOT	NULL	CONSTRAINT [FK_KpiNormalization_Entity_LookUp]			REFERENCES [rpt].[Entities]([EntityId]),
	[AttributeId]						INT					NOT	NULL	CONSTRAINT [FK_KpiNormalization_Attribute_LookUp]		REFERENCES [rpt].[Attributes]([AttributeId]),

	[Value_Q0]							FLOAT				NOT	NULL,
	[Value_Q1]							FLOAT				NOT	NULL,
	[Value_Q2]							FLOAT				NOT	NULL,
	[Value_Q3]							FLOAT				NOT	NULL,
	[Value_Q4]							FLOAT				NOT	NULL,
	[Pcnt_Max]							FLOAT				NOT	NULL,

	[Pcnt0]								AS ([Value_Q4] - [Value_Q0]) / ([Value_Q4] - [Value_Q0]) * 100.0
										PERSISTED			NOT NULL,
	[Pcnt1]								AS ([Value_Q3] - [Value_Q0]) / ([Value_Q4] - [Value_Q0]) * 100.0
										PERSISTED			NOT NULL,
	[Pcnt2]								AS ([Value_Q2] - [Value_Q0]) / ([Value_Q4] - [Value_Q0]) * 100.0
										PERSISTED			NOT NULL,
	[Pcnt3]								AS ([Value_Q1] - [Value_Q0]) / ([Value_Q4] - [Value_Q0]) * 100.0
										PERSISTED			NOT NULL,
	[Pcnt4]								AS ([Value_Q0] - [Value_Q0]) / ([Value_Q4] - [Value_Q0]) * 100.0
										PERSISTED			NOT NULL,

	[Bar1]								AS ([Value_Q4] - [Value_Q3]) / ([Value_Q4] - [Value_Q0]) * 100.0
										PERSISTED			NOT NULL,
	[Bar2]								AS ([Value_Q3] - [Value_Q2]) / ([Value_Q4] - [Value_Q0]) * 100.0
										PERSISTED			NOT NULL,
	[Bar3]								AS ([Value_Q2] - [Value_Q1]) / ([Value_Q4] - [Value_Q0]) * 100.0
										PERSISTED			NOT NULL,
	[Bar4]								AS ([Value_Q1] - [Value_Q0]) / ([Value_Q4] - [Value_Q0]) * 100.0
										PERSISTED			NOT NULL,
	
	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_KpiNormalization_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_KpiNormalization_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_KpiNormalization_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_KpiNormalization_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_KpiNormalization_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_KpiNormalization]			PRIMARY KEY NONCLUSTERED([KpiNormalizationId] ASC),
	CONSTRAINT [UK_KpiNormalization]			UNIQUE CLUSTERED([MethodologyId] ASC, [CurrencyId] ASC, [EntityId] ASC, [AttributeId] ASC),
	CONSTRAINT [CR_KpiNormalization_values]		CHECK
			(
				([Value_Q0] <= [Value_Q1] AND [Value_Q1] <= [Value_Q2] AND [Value_Q2] <= [Value_Q3] AND [Value_Q3] <= [Value_Q4])
			OR
				([Value_Q0] >= [Value_Q1] AND [Value_Q1] >= [Value_Q2] AND [Value_Q2] >= [Value_Q3] AND [Value_Q3] >= [Value_Q4])
			)
);
GO

CREATE TRIGGER [ante].[t_KpiNormalization_u]
ON	[ante].[KpiNormalization]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[ante].[KpiNormalization]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[ante].[KpiNormalization].[KpiNormalizationId]	= [i].[KpiNormalizationId];

END;