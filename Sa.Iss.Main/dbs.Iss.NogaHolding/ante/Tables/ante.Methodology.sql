﻿CREATE TABLE [ante].[Methodology]
(
	[MethodologyId]						INT					NOT	NULL	IDENTITY(1, 1),

	[MethodologyTag]					VARCHAR(4)			NOT	NULL,	CONSTRAINT [CL_Methodology_MethodologyTag]		CHECK([MethodologyTag] <> ''),
																		CONSTRAINT [UK_Methodology_MethodologyTag]		UNIQUE NONCLUSTERED([MethodologyTag] ASC),	
	[MethodologyName]					NVARCHAR(24)		NOT	NULL,	CONSTRAINT [CL_Methodology_MethodologyName]		CHECK([MethodologyName] <> N''),
																		CONSTRAINT [UK_Methodology_MethodologyName]		UNIQUE NONCLUSTERED([MethodologyName] ASC),	
	[MethodologyDetail]					NVARCHAR(24)		NOT	NULL,	CONSTRAINT [CL_Methodology_MethodologyDetail]	CHECK([MethodologyDetail] <> N''),
																		CONSTRAINT [UK_Methodology_MethodologyDetail]	UNIQUE NONCLUSTERED([MethodologyDetail] ASC),	

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Methodology_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Methodology_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Methodology_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Methodology_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Methodology_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Methodology]			PRIMARY KEY NONCLUSTERED([MethodologyId] ASC)
);
GO

CREATE TRIGGER [ante].[t_Methodology_u]
ON	[ante].[Methodology]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[ante].[Methodology]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED [i]
	WHERE
		[ante].[Methodology].[MethodologyId]	= [i].[MethodologyId];

END;