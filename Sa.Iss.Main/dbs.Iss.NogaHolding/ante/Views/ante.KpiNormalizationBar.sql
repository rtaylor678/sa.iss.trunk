﻿CREATE VIEW [ante].[KpiNormalizationBar]
WITH SCHEMABINDING
AS
SELECT
	[k].[KpiNormalizationId],
	[k].[MethodologyId],
	[k].[CurrencyId],
	[k].[EntityId],
	[k].[AttributeId],
	[k].[Value_Q0],
	[k].[Value_Q1],
	[k].[Value_Q2],
	[k].[Value_Q3],
	[k].[Value_Q4],
	[k].[Pcnt_Max],
	[k].[Pcnt0],
	[k].[Pcnt1],
	[k].[Pcnt2],
	[k].[Pcnt3],
	[k].[Pcnt4],
		[Bar0] = (MAX([k].[Bar1] + [k].[Bar2]) OVER()) - ([k].[Bar1] + [k].[Bar2]),
	[k].[Bar1],
	[k].[Bar2],
	[k].[Bar3],
	[k].[Bar4]
FROM
	[ante].[KpiNormalization]	[k];
