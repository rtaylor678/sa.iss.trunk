﻿CREATE FUNCTION [ante].[Return_MethodologyId]
(
	@Methodology		NVARCHAR(96)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @MethodologyId	INT;

	SET @Methodology	= RTRIM(LTRIM(@Methodology));

	SELECT TOP 1
		@MethodologyId	= [c].[MethodologyId]
	FROM
		[ante].[Methodology]	[c]
	WHERE	[c].[MethodologyTag]	= @Methodology
		OR	[c].[MethodologyName]	= @Methodology
		OR	[c].[MethodologyDetail]	= @Methodology;

	RETURN	@MethodologyId;

END;