﻿CREATE FUNCTION [dim].[Return_TankTypeId]
(
	@TankType		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @TankTypeId	INT;

	SET @TankType = RTRIM(LTRIM(@TankType));

	SELECT TOP 1
		@TankTypeId = [c].[TankTypeId]
	FROM
		[dim].[TankType_LookUp]	[c]
	WHERE	[c].[TankTypeTag]	= @TankType
		OR	[c].[TankTypeName]	= @TankType;

	RETURN	@TankTypeId;

END;