﻿CREATE FUNCTION [dim].[Return_PumpServiceId]
(
	@PumpService		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @PumpServiceId	INT;

	SET @PumpService = RTRIM(LTRIM(@PumpService));

	SELECT TOP 1
		@PumpServiceId = [c].[PumpServiceId]
	FROM
		[dim].[PumpService_LookUp]	[c]
	WHERE	[c].[PumpServiceTag]	= @PumpService
		OR	[c].[PumpServiceName]	= @PumpService;

	RETURN	@PumpServiceId;

END;