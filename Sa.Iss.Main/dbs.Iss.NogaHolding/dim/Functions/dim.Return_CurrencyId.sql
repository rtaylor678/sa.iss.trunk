﻿CREATE FUNCTION [dim].[Return_CurrencyId]
(
	@Currency		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @CurrencyId	INT;

	SET @Currency = RTRIM(LTRIM(@Currency));

	SELECT TOP 1
		@CurrencyId = [c].[CurrencyId]
	FROM
		[dim].[Currency_LookUp]		[c]
	WHERE	[c].[CurrencyTag]		= @Currency
		OR	[c].[CurrencyName]		= @Currency
		OR	[c].[CurrencyDetail]	= @Currency;

	RETURN	@CurrencyId;

END;