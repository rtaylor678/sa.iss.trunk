﻿CREATE FUNCTION [dim].[Return_DivisorId]
(
	@Divisor		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @DivisorId	INT;

	SET @Divisor = RTRIM(LTRIM(@Divisor));

	SELECT TOP 1
		@DivisorId = [c].[DivisorId]
	FROM
		[dim].[Divisor_LookUp]	[c]
	WHERE	[c].[DivisorTag]	= @Divisor
		OR	[c].[DivisorName]	= @Divisor;

	RETURN	@DivisorId;

END;