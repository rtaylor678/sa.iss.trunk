﻿CREATE FUNCTION [dim].[Return_EnergyId]
(
	@Energy		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @EnergyId	INT;

	SET @Energy = RTRIM(LTRIM(@Energy));

	SELECT TOP 1
		@EnergyId = [c].[EnergyId]
	FROM
		[dim].[Energy_LookUp]	[c]
	WHERE	[c].[EnergyTag]		= @Energy
		OR	[c].[EnergyName]	= @Energy;

	RETURN	@EnergyId;

END;