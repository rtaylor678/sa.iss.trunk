﻿CREATE FUNCTION [dim].[Return_TransportationMethodId]
(
	@TransportationMethod		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @TransportationMethodId	INT;

	SET @TransportationMethod = RTRIM(LTRIM(@TransportationMethod));

	SELECT TOP 1
		@TransportationMethodId = [c].[TransportationMethodId]
	FROM
		[dim].[TransportationMethod_LookUp]	[c]
	WHERE	[c].[TransportationMethodTag]	= @TransportationMethod
		OR	[c].[TransportationMethodName]	= @TransportationMethod;

	RETURN	@TransportationMethodId;

END;