﻿CREATE FUNCTION [dim].[Return_CompressorId]
(
	@Compressor		NVARCHAR(24)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @CompressorId	INT;

	SET @Compressor = RTRIM(LTRIM(@Compressor));

	SELECT TOP 1
		@CompressorId = [c].[CompressorId]
	FROM
		[dim].[Compressor_LookUp]	[c]
	WHERE	[c].[CompressorTag]		= @Compressor
		OR	[c].[CompressorName]	= @Compressor;

	RETURN	@CompressorId;

END;