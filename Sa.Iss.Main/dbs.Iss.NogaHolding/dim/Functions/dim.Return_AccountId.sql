﻿CREATE FUNCTION [dim].[Return_AccountId]
(
	@Account		NVARCHAR(96)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @AccountId	INT;

	SET @Account = RTRIM(LTRIM(@Account));

	SELECT TOP 1
		@AccountId = [c].[AccountId]
	FROM
		[dim].[Account_LookUp]	[c]
	WHERE	[c].[AccountTag]	= @Account
		OR	[c].[AccountName]	= @Account
		OR	[c].[AccountDetail]	= @Account;

	RETURN	@AccountId;

END;