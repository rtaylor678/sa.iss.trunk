﻿CREATE TABLE [dim].[Material_LookUp]
(
	[MaterialId]						INT					NOT	NULL	IDENTITY(1, 1),

	[MaterialTag]						VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_Material_LookUp_MaterialTag]			UNIQUE CLUSTERED([MaterialTag] ASC),
																		CONSTRAINT [CL_Material_LookUp_MaterialTag]			CHECK([MaterialTag] <> ''),
	[MaterialName]						NVARCHAR(48)		NOT	NULL	CONSTRAINT [UK_Material_LookUp_MaterialName]		UNIQUE NONCLUSTERED([MaterialName] ASC),
																		CONSTRAINT [CL_Material_LookUp_MaterialName]		CHECK([MaterialName] <> N''),
	[MaterialDetail]					NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_Material_LookUp_MaterialDetail]		UNIQUE NONCLUSTERED([MaterialDetail] ASC),
																		CONSTRAINT [CL_Material_LookUp_MaterialDetail]		CHECK([MaterialDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Material_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Material_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Material_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Material_LookUp_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Material_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Material_LookUp]		PRIMARY KEY NONCLUSTERED([MaterialId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Material_LookUp_u]
ON	[dim].[Material_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[Material_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[Material_LookUp].[MaterialId]	= [i].[MaterialId];

END;