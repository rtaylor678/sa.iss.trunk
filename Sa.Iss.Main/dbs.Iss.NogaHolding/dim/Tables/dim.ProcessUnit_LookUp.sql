﻿CREATE TABLE [dim].[ProcessUnit_LookUp]
(
	[ProcessUnitId]						INT					NOT	NULL	IDENTITY(1, 1),

	[ProcessUnitTag]					VARCHAR(48)			NOT	NULL	CONSTRAINT [UK_ProcessUnit_LookUp_AircraftTag]			UNIQUE CLUSTERED([ProcessUnitTag] ASC),
																		CONSTRAINT [CL_ProcessUnit_LookUp_AircraftTag]			CHECK([ProcessUnitTag] <> ''),
	[ProcessUnitName]					NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_ProcessUnit_LookUp_AircraftName]			UNIQUE NONCLUSTERED([ProcessUnitName] ASC),
																		CONSTRAINT [CL_ProcessUnit_LookUp_AircraftName]			CHECK([ProcessUnitName] <> N''),
	[ProcessUnitDetail]					NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_ProcessUnit_LookUp_AircraftDetail]		UNIQUE NONCLUSTERED([ProcessUnitDetail] ASC),
																		CONSTRAINT [CL_ProcessUnit_LookUp_AircraftDetail]		CHECK([ProcessUnitDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ProcessUnit_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnit_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnit_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnit_LookUp_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ProcessUnit_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ProcessUnit_LookUp]	PRIMARY KEY NONCLUSTERED([ProcessUnitId] ASC)
);
GO

CREATE TRIGGER [dim].[t_ProcessUnit_LookUp_u]
ON	[dim].[ProcessUnit_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[ProcessUnit_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[ProcessUnit_LookUp].[ProcessUnitId]	= [i].[ProcessUnitId];

END;