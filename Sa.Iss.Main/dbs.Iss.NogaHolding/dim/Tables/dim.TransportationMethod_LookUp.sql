﻿CREATE TABLE [dim].[TransportationMethod_LookUp]
(
	[TransportationMethodId]			INT					NOT	NULL	IDENTITY(1, 1),

	[TransportationMethodTag]			VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_TransportationMethod_LookUp_TransportationMethodTag]			UNIQUE CLUSTERED([TransportationMethodTag] ASC),
																		CONSTRAINT [CL_TransportationMethod_LookUp_TransportationMethodTag]			CHECK([TransportationMethodTag] <> ''),
	[TransportationMethodName]			NVARCHAR(48)		NOT	NULL	CONSTRAINT [UK_TransportationMethod_LookUp_TransportationMethodName]		UNIQUE NONCLUSTERED([TransportationMethodName] ASC),
																		CONSTRAINT [CL_TransportationMethod_LookUp_TransportationMethodName]		CHECK([TransportationMethodName] <> N''),
	[TransportationMethodDetail]		NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_TransportationMethod_LookUp_TransportationMethodDetail]		UNIQUE NONCLUSTERED([TransportationMethodDetail] ASC),
																		CONSTRAINT [CL_TransportationMethod_LookUp_TransportationMethodDetail]		CHECK([TransportationMethodDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_TransportationMethod_LookUp_tsModified]						DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_TransportationMethod_LookUp_tsModifiedHost]					DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_TransportationMethod_LookUp_tsModifiedUser]					DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_TransportationMethod_LookUp_tsModifiedApp]					DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_TransportationMethod_LookUp_tsModifiedGuid]					DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_TransportationMethod_LookUp]		PRIMARY KEY NONCLUSTERED([TransportationMethodId] ASC)
);
GO

CREATE TRIGGER [dim].[t_TransportationMethod_LookUp_u]
ON	[dim].[TransportationMethod_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[TransportationMethod_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[TransportationMethod_LookUp].[TransportationMethodId]	= [i].[TransportationMethodId];

END;