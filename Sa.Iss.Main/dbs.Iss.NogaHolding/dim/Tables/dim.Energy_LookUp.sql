﻿CREATE TABLE [dim].[Energy_LookUp]
(
	[EnergyId]							INT					NOT	NULL	IDENTITY(1, 1),

	[EnergyTag]							VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_Energy_LookUp_EnergyTag]			UNIQUE CLUSTERED([EnergyTag] ASC),
																		CONSTRAINT [CL_Energy_LookUp_EnergyTag]			CHECK([EnergyTag] <> ''),
	[EnergyName]						NVARCHAR(48)		NOT	NULL	CONSTRAINT [UK_Energy_LookUp_EnergyName]			UNIQUE NONCLUSTERED([EnergyName] ASC),
																		CONSTRAINT [CL_Energy_LookUp_EnergyName]			CHECK([EnergyName] <> N''),
	[EnergyDetail]						NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_Energy_LookUp_EnergyDetail]		UNIQUE NONCLUSTERED([EnergyDetail] ASC),
																		CONSTRAINT [CL_Energy_LookUp_EnergyDetail]		CHECK([EnergyDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Energy_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Energy_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Energy_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Energy_LookUp_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Energy_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Energy_LookUp]		PRIMARY KEY NONCLUSTERED([EnergyId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Energy_LookUp_u]
ON	[dim].[Energy_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[Energy_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[Energy_LookUp].[EnergyId]	= [i].[EnergyId];

END;