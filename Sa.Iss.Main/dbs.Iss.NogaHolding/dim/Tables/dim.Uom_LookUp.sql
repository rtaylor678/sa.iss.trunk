﻿CREATE TABLE [dim].[Uom_LookUp]
(
	[UomId]								INT					NOT	NULL	IDENTITY(1, 1),

	[UomTag]							VARCHAR(12)			NOT	NULL,	CONSTRAINT [CL_Uom_LookUp_UomTag]			CHECK([UomTag] <> ''),
																		CONSTRAINT [UK_Uom_LookUp_UomTag]			UNIQUE CLUSTERED([UomTag] ASC),	
	[UomName]							NVARCHAR(48)		NOT	NULL,	CONSTRAINT [CL_Uom_LookUp_UomName]			CHECK([UomName] <> N''),
																		CONSTRAINT [UX_Uom_LookUp_UomName]			UNIQUE NONCLUSTERED([UomName] ASC),	
	[UomDetail]							NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Uom_LookUp_UomDetail]		CHECK([UomDetail] <> N''),
																		CONSTRAINT [UX_Uom_LookUp_UomDetail]		UNIQUE NONCLUSTERED([UomDetail] ASC),	
	[IsMetric]							INT					NOT	NULL	CONSTRAINT [DF_Uom_LookUp_IsMetric]			DEFAULT(0),
	
	[UomGroup]							INT					NOT	NULL	CONSTRAINT [FK_Uom_LookUp_Uom_LookUp]		REFERENCES [dim].[Uom_LookUp]([UomId]),
	[Factor]							FLOAT				NOT	NULL	CONSTRAINT [DF_Uom_LookUp_Factor]			DEFAULT(1.0),
																			
	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Uom_LookUp_tsModified]		DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Uom_LookUp_tsModifiedHost]	DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Uom_LookUp_tsModifiedUser]	DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Uom_LookUp_tsModifiedApp]	DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Uom_LookUp_tsModifiedGuid]	DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Uom_LookUp]			PRIMARY KEY NONCLUSTERED([UomId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Uom_LookUp_u]
ON	[dim].[Uom_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[Uom_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[Uom_LookUp].[UomId]	= [i].[UomId];

END;