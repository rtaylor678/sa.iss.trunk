﻿CREATE TABLE [dim].[AircraftType_LookUp]
(
	[AircraftTypeId]					INT					NOT	NULL	IDENTITY(1, 1),

	[AircraftTypeTag]					VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_AircraftType_LookUp_AircraftTag]			UNIQUE CLUSTERED([AircraftTypeTag] ASC),
																		CONSTRAINT [CL_AircraftType_LookUp_AircraftTag]			CHECK([AircraftTypeTag] <> ''),
	[AircraftTypeName]					NVARCHAR(48)		NOT	NULL	CONSTRAINT [UK_AircraftType_LookUp_AircraftName]		UNIQUE NONCLUSTERED([AircraftTypeName] ASC),
																		CONSTRAINT [CL_AircraftType_LookUp_AircraftName]		CHECK([AircraftTypeName] <> N''),
	[AircraftTypeDetail]				NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_AircraftType_LookUp_AircraftDetail]		UNIQUE NONCLUSTERED([AircraftTypeDetail] ASC),
																		CONSTRAINT [CL_AircraftType_LookUp_AircraftDetail]		CHECK([AircraftTypeDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AircraftType_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AircraftType_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AircraftType_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AircraftType_LookUp_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AircraftType_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AircraftType_LookUp]		PRIMARY KEY NONCLUSTERED([AircraftTypeId] ASC)
);
GO

CREATE TRIGGER [dim].[t_AircraftType_LookUp_u]
ON	[dim].[AircraftType_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[AircraftType_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[AircraftType_LookUp].[AircraftTypeId]	= [i].[AircraftTypeId];

END;