﻿CREATE TABLE [dim].[DeliverySystem_LookUp]
(
	[DeliverySystemId]					INT					NOT	NULL	IDENTITY(1, 1),

	[DeliverySystemTag]					VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_DeliverySystem_LookUp_AircraftTag]			UNIQUE CLUSTERED([DeliverySystemTag] ASC),
																		CONSTRAINT [CL_DeliverySystem_LookUp_AircraftTag]			CHECK([DeliverySystemTag] <> ''),
	[DeliverySystemName]				NVARCHAR(48)		NOT	NULL	CONSTRAINT [UK_DeliverySystem_LookUp_AircraftName]			UNIQUE NONCLUSTERED([DeliverySystemName] ASC),
																		CONSTRAINT [CL_DeliverySystem_LookUp_AircraftName]			CHECK([DeliverySystemName] <> N''),
	[DeliverySystemDetail]				NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_DeliverySystem_LookUp_AircraftDetail]		UNIQUE NONCLUSTERED([DeliverySystemDetail] ASC),
																		CONSTRAINT [CL_DeliverySystem_LookUp_AircraftDetail]		CHECK([DeliverySystemDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_DeliverySystem_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_DeliverySystem_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_DeliverySystem_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_DeliverySystem_LookUp_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_DeliverySystem_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_DeliverySystem_LookUp]		PRIMARY KEY NONCLUSTERED([DeliverySystemId] ASC)
);
GO

CREATE TRIGGER [dim].[t_DeliverySystem_LookUp_u]
ON	[dim].[DeliverySystem_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[DeliverySystem_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[DeliverySystem_LookUp].[DeliverySystemId]	= [i].[DeliverySystemId];

END;