﻿CREATE TABLE [dim].[Conveyor_LookUp]
(
	[ConveyorId]						INT					NOT	NULL	IDENTITY(1, 1),

	[ConveyorTag]						VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_Conveyor_LookUp_ConveyorTag]			UNIQUE CLUSTERED([ConveyorTag] ASC),
																		CONSTRAINT [CL_Conveyor_LookUp_ConveyorTag]			CHECK([ConveyorTag] <> ''),
	[ConveyorName]						NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_Conveyor_LookUp_ConveyorName]		UNIQUE NONCLUSTERED([ConveyorName] ASC),
																		CONSTRAINT [CL_Conveyor_LookUp_ConveyorName]		CHECK([ConveyorName] <> N''),
	[ConveyorDetail]					NVARCHAR(256)		NOT	NULL	CONSTRAINT [UK_Conveyor_LookUp_ConveyorDetail]		UNIQUE NONCLUSTERED([ConveyorDetail] ASC),
																		CONSTRAINT [CL_Conveyor_LookUp_ConveyorDetail]		CHECK([ConveyorDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Conveyor_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Conveyor_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Conveyor_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Conveyor_LookUp_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Conveyor_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Conveyor_LookUp]		PRIMARY KEY NONCLUSTERED([ConveyorId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Conveyor_LookUp_u]
ON	[dim].[Conveyor_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[Conveyor_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[Conveyor_LookUp].[ConveyorId]	= [i].[ConveyorId];

END;