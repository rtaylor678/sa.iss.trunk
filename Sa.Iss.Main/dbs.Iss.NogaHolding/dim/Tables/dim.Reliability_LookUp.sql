﻿CREATE TABLE [dim].[Reliability_LookUp]
(
	[ReliabilityId]						INT					NOT	NULL	IDENTITY(1, 1),

	[ReliabilityTag]					VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_Reliability_LookUp_AircraftTag]			UNIQUE CLUSTERED([ReliabilityTag] ASC),
																		CONSTRAINT [CL_Reliability_LookUp_AircraftTag]			CHECK([ReliabilityTag] <> ''),
	[ReliabilityName]					NVARCHAR(48)		NOT	NULL	CONSTRAINT [UK_Reliability_LookUp_AircraftName]			UNIQUE NONCLUSTERED([ReliabilityName] ASC),
																		CONSTRAINT [CL_Reliability_LookUp_AircraftName]			CHECK([ReliabilityName] <> N''),
	[ReliabilityDetail]					NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_Reliability_LookUp_AircraftDetail]		UNIQUE NONCLUSTERED([ReliabilityDetail] ASC),
																		CONSTRAINT [CL_Reliability_LookUp_AircraftDetail]		CHECK([ReliabilityDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Reliability_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Reliability_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Reliability_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Reliability_LookUp_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Reliability_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Reliability_LookUp]	PRIMARY KEY NONCLUSTERED([ReliabilityId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Reliability_LookUp_u]
ON	[dim].[Reliability_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[Reliability_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[Reliability_LookUp].[ReliabilityId]	= [i].[ReliabilityId];

END;