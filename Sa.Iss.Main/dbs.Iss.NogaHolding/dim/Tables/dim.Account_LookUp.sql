﻿CREATE TABLE [dim].[Account_LookUp]
(
	[AccountId]							INT					NOT	NULL	IDENTITY(1, 1),

	[AccountTag]						VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_Account_LookUp_AccountTag]			UNIQUE CLUSTERED([AccountTag] ASC),
																		CONSTRAINT [CL_Account_LookUp_AccountTag]			CHECK([AccountTag] <> ''),
	[AccountName]						NVARCHAR(48)		NOT	NULL	CONSTRAINT [UK_Account_LookUp_AccountName]			UNIQUE NONCLUSTERED([AccountName] ASC),
																		CONSTRAINT [CL_Account_LookUp_AccountName]			CHECK([AccountName] <> N''),
	[AccountDetail]						NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_Account_LookUp_AccountDetail]		UNIQUE NONCLUSTERED([AccountDetail] ASC),
																		CONSTRAINT [CL_Account_LookUp_AccountDetail]		CHECK([AccountDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Account_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Account_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Account_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Account_LookUp_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Account_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Account_LookUp]		PRIMARY KEY NONCLUSTERED([AccountId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Account_LookUp_u]
ON	[dim].[Account_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[Account_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[Account_LookUp].[AccountId]	= [i].[AccountId];

END;