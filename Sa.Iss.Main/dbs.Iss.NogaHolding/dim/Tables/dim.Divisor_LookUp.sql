﻿CREATE TABLE [dim].[Divisor_LookUp]
(
	[DivisorId]							INT					NOT	NULL	IDENTITY(1, 1),

	[DivisorTag]						VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_Divisor_LookUp_AircraftTag]			UNIQUE CLUSTERED([DivisorTag] ASC),
																		CONSTRAINT [CL_Divisor_LookUp_AircraftTag]			CHECK([DivisorTag] <> ''),
	[DivisorName]						NVARCHAR(48)		NOT	NULL	CONSTRAINT [UK_Divisor_LookUp_AircraftName]			UNIQUE NONCLUSTERED([DivisorName] ASC),
																		CONSTRAINT [CL_Divisor_LookUp_AircraftName]			CHECK([DivisorName] <> N''),
	[DivisorDetail]						NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_Divisor_LookUp_AircraftDetail]		UNIQUE NONCLUSTERED([DivisorDetail] ASC),
																		CONSTRAINT [CL_Divisor_LookUp_AircraftDetail]		CHECK([DivisorDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Divisor_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Divisor_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Divisor_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Divisor_LookUp_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Divisor_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Divisor_LookUp]		PRIMARY KEY NONCLUSTERED([DivisorId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Divisor_LookUp_u]
ON	[dim].[Divisor_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[Divisor_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[Divisor_LookUp].[DivisorId]	= [i].[DivisorId];

END;