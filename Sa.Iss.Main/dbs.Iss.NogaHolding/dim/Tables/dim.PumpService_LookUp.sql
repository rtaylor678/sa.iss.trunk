﻿CREATE TABLE [dim].[PumpService_LookUp]
(
	[PumpServiceId]						INT					NOT	NULL	IDENTITY(1, 1),

	[PumpServiceTag]					VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_PumpService_LookUp_AircraftTag]			UNIQUE CLUSTERED([PumpServiceTag] ASC),
																		CONSTRAINT [CL_PumpService_LookUp_AircraftTag]			CHECK([PumpServiceTag] <> ''),
	[PumpServiceName]					NVARCHAR(48)		NOT	NULL	CONSTRAINT [UK_PumpService_LookUp_AircraftName]			UNIQUE NONCLUSTERED([PumpServiceName] ASC),
																		CONSTRAINT [CL_PumpService_LookUp_AircraftName]			CHECK([PumpServiceName] <> N''),
	[PumpServiceDetail]					NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_PumpService_LookUp_AircraftDetail]		UNIQUE NONCLUSTERED([PumpServiceDetail] ASC),
																		CONSTRAINT [CL_PumpService_LookUp_AircraftDetail]		CHECK([PumpServiceDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_PumpService_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_PumpService_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_PumpService_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_PumpService_LookUp_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_PumpService_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_PumpService_LookUp]	PRIMARY KEY NONCLUSTERED([PumpServiceId] ASC)
);
GO

CREATE TRIGGER [dim].[t_PumpService_LookUp_u]
ON	[dim].[PumpService_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[PumpService_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[PumpService_LookUp].[PumpServiceId]	= [i].[PumpServiceId];

END;