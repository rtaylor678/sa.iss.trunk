﻿CREATE TABLE [dim].[Compressor_LookUp]
(
	[CompressorId]						INT					NOT	NULL	IDENTITY(1, 1),

	[CompressorTag]						VARCHAR(12)			NOT	NULL	CONSTRAINT [UK_Compressor_LookUp_CompressorTag]			UNIQUE CLUSTERED([CompressorTag] ASC),
																		CONSTRAINT [CL_Compressor_LookUp_CompressorTag]			CHECK([CompressorTag] <> ''),
	[CompressorName]					NVARCHAR(96)		NOT	NULL	CONSTRAINT [UK_Compressor_LookUp_CompressorName]		UNIQUE NONCLUSTERED([CompressorName] ASC),
																		CONSTRAINT [CL_Compressor_LookUp_CompressorName]		CHECK([CompressorName] <> N''),
	[CompressorDetail]					NVARCHAR(256)		NOT	NULL	CONSTRAINT [UK_Compressor_LookUp_CompressorDetail]		UNIQUE NONCLUSTERED([CompressorDetail] ASC),
																		CONSTRAINT [CL_Compressor_LookUp_CompressorDetail]		CHECK([CompressorDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Compressor_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Compressor_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Compressor_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Compressor_LookUp_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Compressor_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Compressor_LookUp]	PRIMARY KEY NONCLUSTERED([CompressorId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Compressor_LookUp_u]
ON	[dim].[Compressor_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[Compressor_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[Compressor_LookUp].[CompressorId]	= [i].[CompressorId];

END;