﻿CREATE TABLE [dim].[Currency_LookUp]
(
	[CurrencyId]						INT					NOT	NULL	IDENTITY(1, 1),

	[CurrencyTag]						VARCHAR(4)			NOT	NULL,	CONSTRAINT [CL_Currency_LookUp_CurrencyTag]			CHECK([CurrencyTag] <> ''),
																		CONSTRAINT [UK_Currency_LookUp_CurrencyTag]			UNIQUE CLUSTERED([CurrencyTag] ASC),	
	[CurrencyName]						NVARCHAR(24)		NOT	NULL,	CONSTRAINT [CL_Currency_LookUp_CurrencyName]		CHECK([CurrencyName] <> N''),
																		CONSTRAINT [UX_Currency_LookUp_CurrencyName]		UNIQUE NONCLUSTERED([CurrencyName] ASC),	
	[CurrencyDetail]					NVARCHAR(24)		NOT	NULL,	CONSTRAINT [CL_Currency_LookUp_CurrencyDetail]		CHECK([CurrencyDetail] <> N''),
																		CONSTRAINT [UX_Currency_LookUp_CurrencyDetail]		UNIQUE NONCLUSTERED([CurrencyDetail] ASC),	

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Currency_LookUp_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Currency_LookUp_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Currency_LookUp_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Currency_LookUp_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Currency_LookUp_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Currency_LookUp]		PRIMARY KEY NONCLUSTERED([CurrencyId] ASC)
);
GO

CREATE TRIGGER [dim].[t_Currency_LookUp_u]
ON	[dim].[Currency_LookUp]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[dim].[Currency_LookUp]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[dim].[Currency_LookUp].[CurrencyId]	= [i].[CurrencyId];

END;