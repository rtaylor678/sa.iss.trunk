﻿CREATE PROCEDURE [dim].[Update_Parent]
(
	@SchemaName				VARCHAR(48)	= 'dim',
	@TableName				VARCHAR(48),

	@Prefix					VARCHAR(2),
	@NamePrime				VARCHAR(48),

	@NameParent				VARCHAR(48)	= 'ParentId',
	@NameSortKey			VARCHAR(48)	= 'SortKey',
	@NameHierarchy			VARCHAR(48)	= 'Hierarchy'
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	SET @NamePrime			= @Prefix + '_' + @NamePrime;
	SET @NameParent			= @Prefix + '_' + @NameParent;
	SET @NameSortKey		= @Prefix + '_' + @NameSortKey;
	SET @NameHierarchy		= @Prefix + '_' + @NameHierarchy;

	DECLARE @SQL	VARCHAR(MAX) =
	'WITH cte([PrimeId], [ParentId], [Hierarchy]) AS
	(
		SELECT
			[a].[' + @NamePrime + '],
			[a].[' + @NameParent + '],
			SYS.HIERARCHYID::Parse(''/'' + CAST(ROW_NUMBER() OVER(ORDER BY [a].[' + @NameSortKey + ']) AS VARCHAR) + ''/'')
		FROM [' + @SchemaName + '].[' + @TableName + '] [a]
		WHERE [a].[' + @NamePrime + '] = [a].[' + @NameParent + ']
		UNION ALL
		SELECT
			[a].[' + @NamePrime + '],
			[a].[' + @NameParent + '],
			SYS.HIERARCHYID::Parse([c].[Hierarchy].ToString() + CAST(ROW_NUMBER() OVER(ORDER BY [a].[' + @NameSortKey + ']) AS VARCHAR) + ''/'')
		FROM [' + @SchemaName + '].[' + @TableName + '] [a]
		INNER JOIN cte [c]
			ON	[c].[PrimeId] = [a].[' + @NameParent + ']
		WHERE [a].[' + @NamePrime + '] <> [a].[' + @NameParent + ']
	)
	UPDATE [a]
	SET [a].[' + @NameHierarchy + '] = [c].[Hierarchy]
	FROM [' + @SchemaName + '].[' + @TableName + '] [a]
	INNER JOIN cte [c]
		ON	[c].[PrimeId] = [a].[' + @NamePrime + ']
	WHERE [a].[' + @NameHierarchy + '] <> [c].[Hierarchy];';

	EXECUTE(@SQL);

END;

/*
EXECUTE dim.Update_Parents 'dim', 'LO_Location_Parent', 'LO', 'MethodologyId', 'StreamId', 'ParentId', 'SortKey', 'Hierarchy';
*/
