﻿CREATE PROCEDURE [dim].[Merge_Bridge]
(
	@SourceSchemaName		VARCHAR(48)	= 'dim',
	@SourceTableName		VARCHAR(48),

	@TargetSchemaName		VARCHAR(48)	= 'dim',
	@TargetTableName		VARCHAR(48),

	@Prefix					VARCHAR(2),
	@NamePrime				VARCHAR(48),

	@NameParent				VARCHAR(48)	= 'ParentId',
	@NameDescendant			VARCHAR(48)	= 'DescendantId',
	@NameHierarchy			VARCHAR(48)	= 'Hierarchy'
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	SET @NamePrime			= @Prefix + '_' + @NamePrime;
	SET @NameParent			= @Prefix + '_' + @NameParent;
	SET @NameDescendant		= @Prefix + '_' + @NameDescendant;
	SET @NameHierarchy		= @Prefix + '_' + @NameHierarchy;

	DECLARE @SQL	VARCHAR(MAX) = 
	'MERGE INTO [' + @TargetSchemaName + '].[' + @TargetTableName + '] AS Target
	USING
	(
		SELECT
			[a].[' + @NamePrime + '],
			[' + @NameDescendant + '] = [d].[' + @NamePrime + ']		
		FROM [' + @SourceSchemaName + '].[' + @SourceTableName + ']			[a]
		INNER JOIN [' + @SourceSchemaName + '].[' + @SourceTableName + ']	[d]
			ON	[d].[' + @NameHierarchy + '].IsDescendantOf([a].[' + @NameHierarchy + ']) = 1
	)
	AS Source([' + @NamePrime + '], [' + @Prefix + '_DescendantId])
	ON	Target.[' + @NamePrime + '] = Source.[' + @NamePrime + ']
	AND	Target.[' + @NameDescendant + '] = Source.[' + @NameDescendant + ']
	WHEN NOT MATCHED BY TARGET THEN
		INSERT([' + @NamePrime + '], [' + @NameDescendant + '])
		VALUES([' + @NamePrime + '], [' + @NameDescendant + '])
	WHEN NOT MATCHED BY SOURCE THEN DELETE;'

	EXECUTE(@SQL);

END;

/*
EXECUTE dim.Merge_Bridge 'dim', 'Stream_Parent', 'dim', 'Stream_Bridge', 'MethodologyId', 'StreamId', 'SortKey', 'Hierarchy', 'Operator';
*/