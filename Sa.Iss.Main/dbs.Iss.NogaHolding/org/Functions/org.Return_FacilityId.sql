﻿CREATE FUNCTION [org].[Return_FacilityId]
(
	@Facility		NVARCHAR(96)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @FacilityId	INT;

	SELECT TOP 1
		@FacilityId = [c].[FacilityId]
	FROM
		[org].[Facilities]	[c]
	WHERE	[c].[FacilityTag]		= @Facility
		OR	[c].[FacilityName]		= @Facility
		OR	[c].[FacilityDetail]	= @Facility;

	RETURN	@FacilityId;

END;