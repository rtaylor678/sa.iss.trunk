﻿CREATE FUNCTION [org].[Return_CompanyId]
(
	@Company		NVARCHAR(96)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @CompanyId	INT;

	SELECT TOP 1
		@CompanyId = [c].[CompanyId]
	FROM
		[org].[Companies]	[c]
	WHERE	[c].[CompanyTag]	= @Company
		OR	[c].[CompanyName]	= @Company
		OR	[c].[CompanyDetail]	= @Company;

	RETURN	@CompanyId;

END;