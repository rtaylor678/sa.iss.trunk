﻿CREATE TABLE [org].[GroupFacilities]
(
	[GroupFacilityId]					INT					NOT	NULL	IDENTITY(1, 1),

	[GroupId]							INT					NOT	NULL	CONSTRAINT [FK_GroupFacilities_Groups]				REFERENCES [org].[Groups]([GroupId]),
	[FacilityId]						INT					NOT	NULL	CONSTRAINT [FK_GroupFacilities_Facilities]			REFERENCES [org].[Facilities]([FacilityId]),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_GroupFacilities_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_GroupFacilities_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_GroupFacilities_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_GroupFacilities_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_GroupFacilities_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_GroupFacilities]		PRIMARY KEY NONCLUSTERED([GroupFacilityId] ASC),
	CONSTRAINT [UK_GroupFacilities]		UNIQUE CLUSTERED([GroupId] ASC, [FacilityId] ASC)
);
GO

CREATE TRIGGER [org].[t_GroupFacilities_u]
ON	[org].[GroupFacilities]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[org].[GroupFacilities]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[org].[GroupFacilities].[GroupFacilityId]	= [i].[GroupFacilityId];

END;