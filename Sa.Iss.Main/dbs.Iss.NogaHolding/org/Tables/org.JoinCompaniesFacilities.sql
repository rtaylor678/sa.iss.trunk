﻿CREATE TABLE [org].[JoinCompaniesFacilities]
(
	[JoinId]							INT					NOT	NULL	IDENTITY(1, 1),

	[CompanyId]							INT					NOT	NULL	CONSTRAINT [FK_JoinCompaniesFacilities_Companies]			REFERENCES [org].[Companies]([CompanyId]),
	[FacilityId]						INT					NOT	NULL	CONSTRAINT [FK_JoinCompaniesFacilities_Facilities]			REFERENCES [org].[Facilities]([FacilityId]),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_JoinCompaniesFacilities_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinCompaniesFacilities_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinCompaniesFacilities_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_JoinCompaniesFacilities_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_JoinCompaniesFacilities_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_JoinCompaniesFacilities]					PRIMARY KEY NONCLUSTERED([JoinId] ASC),
	CONSTRAINT [UK_JoinCompaniesFacilities]					UNIQUE CLUSTERED([FacilityId] ASC)
);
GO

CREATE TRIGGER [org].[t_JoinCompaniesFacilities_u]
ON	[org].[JoinCompaniesFacilities]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[org].[JoinCompaniesFacilities]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[org].[JoinCompaniesFacilities].[JoinId]	= [i].[JoinId];

END;