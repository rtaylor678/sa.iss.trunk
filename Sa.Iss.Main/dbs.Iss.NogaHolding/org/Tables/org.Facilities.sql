﻿CREATE TABLE [org].[Facilities]
(
	[FacilityId]						INT					NOT	NULL	IDENTITY(1, 1),

	[FacilityTag]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_Facilitys_FacilityTag]			CHECK([FacilityTag] <> ''),
																		CONSTRAINT [UK_Facilitys_FacilityTag]			UNIQUE CLUSTERED([FacilityTag] ASC),	
	[FacilityName]						NVARCHAR(48)		NOT	NULL,	CONSTRAINT [CL_Facilitys_FacilityName]			CHECK([FacilityName] <> N''),
																		CONSTRAINT [UX_Facilitys_FacilityName]			UNIQUE NONCLUSTERED([FacilityName] ASC),	
	[FacilityDetail]					NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Facilitys_FacilityDetail]		CHECK([FacilityDetail] <> N''),
																		CONSTRAINT [UX_Facilitys_FacilityDetail]		UNIQUE NONCLUSTERED([FacilityDetail] ASC),	

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Facilities_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facilities_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facilities_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Facilities_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Facilities_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Facilities]			PRIMARY KEY NONCLUSTERED([FacilityId] ASC)
);
GO

CREATE TRIGGER [org].[t_Facilities_u]
ON	[org].[Facilities]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[org].[Facilities]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[org].[Facilities].[FacilityId]	= [i].[FacilityId];

END;