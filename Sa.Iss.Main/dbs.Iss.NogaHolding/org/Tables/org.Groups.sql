﻿CREATE TABLE [org].[Groups]
(
	[GroupId]							INT					NOT	NULL	IDENTITY(1, 1),

	[GroupTag]							VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_Groups_GroupTag]				CHECK([GroupTag] <> ''),
																		CONSTRAINT [UK_Groups_GroupTag]				UNIQUE CLUSTERED([GroupTag] ASC),	
	[GroupName]							NVARCHAR(48)		NOT	NULL,	CONSTRAINT [CL_Groups_GroupName]			CHECK([GroupName] <> N''),
																		CONSTRAINT [UX_Groups_GroupName]			UNIQUE NONCLUSTERED([GroupName] ASC),	
	[GroupDetail]						NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Groups_GroupDetail]			CHECK([GroupDetail] <> N''),
																		CONSTRAINT [UX_Groups_GroupDetail]			UNIQUE NONCLUSTERED([GroupDetail] ASC),	

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Groups_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Groups_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Groups_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Groups_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Groups_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Groups]				PRIMARY KEY NONCLUSTERED([GroupId] ASC)
);
GO

CREATE TRIGGER [org].[t_Groups_u]
ON	[org].[Groups]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[org].[Groups]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[org].[Groups].[GroupId]	= [i].[GroupId];

END;