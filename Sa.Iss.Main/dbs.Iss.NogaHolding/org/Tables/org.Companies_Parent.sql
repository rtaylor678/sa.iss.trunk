﻿CREATE TABLE [org].[Companies_Parent]
(
	[Companies_ParentId]				INT					NOT	NULL	IDENTITY(1, 1),


	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Companies_Parent_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Companies_Parent_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Companies_Parent_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Companies_Parent_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Companies_Parent_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Companies_Parent]	PRIMARY KEY NONCLUSTERED([Companies_ParentId] ASC)
);
GO

CREATE TRIGGER [org].[t_Companies_Parent_u]
ON	[org].[Companies_Parent]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[org].[Companies_Parent]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[org].[Companies_Parent].[Companies_ParentId]	= [i].[Companies_ParentId];

END;