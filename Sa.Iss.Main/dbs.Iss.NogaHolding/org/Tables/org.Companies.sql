﻿CREATE TABLE [org].[Companies]
(
	[CompanyId]							INT					NOT	NULL	IDENTITY(1, 1),

	[CompanyTag]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_Company_LookUp_CompanyTag]		CHECK([CompanyTag] <> ''),
																		CONSTRAINT [UK_Company_LookUp_CompanyTag]		UNIQUE CLUSTERED([CompanyTag] ASC),	
	[CompanyName]						NVARCHAR(48)		NOT	NULL,	CONSTRAINT [CL_Company_LookUp_CompanyName]		CHECK([CompanyName] <> N''),
																		CONSTRAINT [UX_Company_LookUp_CompanyName]		UNIQUE NONCLUSTERED([CompanyName] ASC),	
	[CompanyDetail]						NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Company_LookUp_CompanyDetail]	CHECK([CompanyDetail] <> N''),
																		CONSTRAINT [UX_Company_LookUp_CompanyDetail]	UNIQUE NONCLUSTERED([CompanyDetail] ASC),	

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Companies_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Companies_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Companies_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Companies_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Companies_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Companies]			PRIMARY KEY NONCLUSTERED([CompanyId] ASC)
);
GO

CREATE TRIGGER [org].[t_Companies_u]
ON	[org].[Companies]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[org].[Companies]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[org].[Companies].[CompanyId]	= [i].[CompanyId];

END;