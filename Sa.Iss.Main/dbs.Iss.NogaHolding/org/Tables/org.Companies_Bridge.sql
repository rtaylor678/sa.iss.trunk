﻿CREATE TABLE [org].[Companies_Bridge]
(
	[Companies_BridgeId]				INT					NOT	NULL	IDENTITY(1, 1),


	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Companies_Bridge_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Companies_Bridge_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Companies_Bridge_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Companies_Bridge_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Companies_Bridge_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Companies_Bridge]	PRIMARY KEY NONCLUSTERED([Companies_BridgeId] ASC)
);
GO

CREATE TRIGGER [org].[t_Companies_Bridge_u]
ON	[org].[Companies_Bridge]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[org].[Companies_Bridge]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[org].[Companies_Bridge].[Companies_BridgeId]	= [i].[Companies_BridgeId];

END;