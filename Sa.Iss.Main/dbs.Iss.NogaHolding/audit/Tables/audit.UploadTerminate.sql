﻿CREATE TABLE [audit].[UploadTerminate]
(
	[UploadTerminateIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_UploadTerminate_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[UploadTerminateName]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_UploadTerminate_UploadTerminateName]			CHECK([UploadTerminateName] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_UploadTerminate_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UploadTerminate_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UploadTerminate_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UploadTerminate_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_UploadTerminate_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_UploadTerminate]	PRIMARY KEY NONCLUSTERED([UploadTerminateIdx] ASC),
	CONSTRAINT [UK_UploadTerminate]	UNIQUE CLUSTERED([SubmissionId] ASC, [UploadTerminateName] ASC)
);
GO

CREATE TRIGGER [audit].[t_UploadTerminate_u]
ON	[audit].[UploadTerminate]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[audit].[UploadTerminate]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[audit].[UploadTerminate].[UploadTerminateIdx]	= [i].[UploadTerminateIdx];

END;