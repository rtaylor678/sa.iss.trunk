﻿CREATE TABLE [audit].[UploadInitiate]
(
	[UploadInitiateIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_UploadInitiate_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[UploadInitiateName]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_UploadInitiate_UploadInitiateName]			CHECK([UploadInitiateName] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_UploadInitiate_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UploadInitiate_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UploadInitiate_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UploadInitiate_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_UploadInitiate_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_UploadInitiate]	PRIMARY KEY NONCLUSTERED([UploadInitiateIdx] ASC),
	CONSTRAINT [UK_UploadInitiate]	UNIQUE CLUSTERED([SubmissionId] ASC, [UploadInitiateName] ASC)
);
GO

CREATE TRIGGER [audit].[t_UploadInitiate_u]
ON	[audit].[UploadInitiate]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[audit].[UploadInitiate]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[audit].[UploadInitiate].[UploadInitiateIdx]	= [i].[UploadInitiateIdx];

END;