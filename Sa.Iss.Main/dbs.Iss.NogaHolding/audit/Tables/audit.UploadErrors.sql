﻿CREATE TABLE [audit].[UploadErrors]
(
	[UploadErrorsId]					INT					NOT	NULL	IDENTITY(1, 1),
	
	[UploadQueueId]						INT					NOT	NULL,--	CONSTRAINT [FK_UploadErrors_UploadQueue]			REFERENCES [audit].[UploadQueue]([UploadQueueIdx]),
	[FileType]							NVARCHAR(32)		NOT	NULL,
	[DataSetIdentifier]					NVARCHAR(48)		NOT	NULL,	CONSTRAINT [CL_UploadErrors_DataSetIdentifier]		CHECK([DataSetIdentifier] <> ''),
	[FilePath]							NVARCHAR(512)		NOT	NULL,

	[Sheet]								VARCHAR(128)			NULL,
	[Range]								VARCHAR(128)			NULL,
	[CellText]							NVARCHAR(128)			NULL,
	[ProcedureName]						VARCHAR(128)			NULL,
	[TableName]							VARCHAR(128)			NULL,
	[ParameterName]						VARCHAR(128)			NULL,
	[ErrorNotes]						VARCHAR(MAX)			NULL,
	[SystemErrorMessage]				VARCHAR(MAX)			NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_UploadErrors_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UploadErrors_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UploadErrors_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_UploadErrors_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_UploadErrors_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_UploadErrors] PRIMARY KEY NONCLUSTERED ([UploadErrorsId] ASC),
	CONSTRAINT [UK_UploadErrors] UNIQUE CLUSTERED([DataSetIdentifier] ASC, [FileType] ASC, [tsModified] ASC)
);