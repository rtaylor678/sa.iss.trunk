﻿CREATE TABLE [audit].[LogError]
(
	[LogErrorId]						INT					NOT	NULL	IDENTITY(1, 1),

	[ErrorNumber]						INT					NOT	NULL,
	[XActState]							SMALLINT			NOT	NULL,

	[DataSetIdentifier]					NVARCHAR(48)			NULL,	CONSTRAINT [CL_LogError_DataSetIdentifier]		CHECK([DataSetIdentifier] <> ''),

	[ProcedureSchema]					NVARCHAR(128)		NOT	NULL,	CONSTRAINT [CL_LogError_ProcedureSchema]		CHECK([ProcedureSchema] <> ''),
	[ProcedureName]						NVARCHAR(128)		NOT	NULL,	CONSTRAINT [CL_LogError_ProcedureName]			CHECK([ProcedureName] <> ''),
	[ProcedureLine]						INT					NOT	NULL,
	[ProcedureDesc]						NVARCHAR(MAX)			NULL,	CONSTRAINT [CL_LogError_ProcedureDesc]			CHECK([ProcedureDesc] <> ''),
	[ErrorMessage]						NVARCHAR(MAX)		NOT	NULL,	CONSTRAINT [CL_LogError_ErrorMessage]			CHECK([ErrorMessage] <> ''),
	[ErrorSeverity]						INT					NOT	NULL,
	[ErrorState]						INT					NOT	NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_LogError_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_LogError_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_LogError_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_LogError_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_LogError_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_LogError]			PRIMARY KEY CLUSTERED([LogErrorId] DESC),
);
GO