﻿CREATE PROCEDURE [audit].[Insert_UploadError]
(
	@UploadQueueId			INT,
	@FileType				NVARCHAR(32),
	@DataSetIdentifier		NVARCHAR(48),
	@FilePath				NVARCHAR(512),

	@Sheet					VARCHAR(128)	= NULL,
	@Range					VARCHAR(128)	= NULL,
	@CellText				NVARCHAR(128)	= NULL,

	@ProcedureName			VARCHAR(128)	= NULL,
	@TableName				VARCHAR(128)	= NULL,
	@ParameterName			VARCHAR(128)	= NULL,

	@ErrorNotes				VARCHAR(MAX)	= NULL,
	@SystemErrorMessage		VARCHAR(MAX)	= NULL
)
AS
BEGIN

	SET NOCOUNT ON;

	INSERT INTO [audit].[UploadErrors]
	(
		[UploadQueueId],
		[FileType],
		[DataSetIdentifier],
		[FilePath],

		[Sheet],
		[Range],
		[CellText],
		[ProcedureName],
		[TableName],
		[ParameterName],
		[ErrorNotes],
		[SystemErrorMessage]
	)
	VALUES
	(
		@UploadQueueId,
		@FileType,
		@DataSetIdentifier,
		@FilePath,

		@Sheet,
		@Range,
		@CellText,
		@ProcedureName,
		@TableName,
		@ParameterName,
		@ErrorNotes,
		@SystemErrorMessage
	);

END;