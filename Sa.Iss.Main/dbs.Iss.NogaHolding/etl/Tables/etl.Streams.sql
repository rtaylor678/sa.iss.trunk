﻿CREATE TABLE [etl].[Streams]
(
	[StreamIdx]							INT					NOT	NULL	IDENTITY(1, 1),

	[XlsStreamDetail]					VARCHAR(96)			NOT	NULL,	CONSTRAINT [CL_Stream_XlsStreamDetail]		CHECK([XlsStreamDetail] <> ''),
	[StreamTag]							VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_Stream_StreamTag]			CHECK([StreamTag] <> ''),
	[StreamId]							AS CONVERT(INT, [dim].[Return_AccountId]([StreamTag])),
	[EntityId]							AS CONVERT(INT, [rpt].[Return_EntityId]([StreamTag])),
	[AttributeId]						AS CONVERT(INT, [rpt].[Return_AttributeId]([StreamTag])),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Stream_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Stream_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Stream_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Stream_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Stream_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Streams]				PRIMARY KEY NONCLUSTERED([StreamIdx] ASC),
	CONSTRAINT [UK_Streams]				UNIQUE CLUSTERED([XlsStreamDetail] ASC)
);
GO

CREATE TRIGGER [etl].[t_Streams_u]
ON	[etl].[Streams]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[etl].[Streams]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[etl].[Streams].[StreamIdx]	= [i].[StreamIdx];

END;