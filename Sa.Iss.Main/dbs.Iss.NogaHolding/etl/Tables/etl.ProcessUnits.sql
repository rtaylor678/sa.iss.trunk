﻿CREATE TABLE [etl].[ProcessUnits]
(
	[ProcessUnitIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[XlsProcessUnitDetail]				VARCHAR(96)			NOT	NULL,	CONSTRAINT [CL_ProcessUnits_XlsProcessUnitDetail]	CHECK([XlsProcessUnitDetail] <> ''),
	[ProcessUnitTag]					VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_ProcessUnits_ProcessUnitTag]			CHECK([ProcessUnitTag] <> ''),
	[ProcessUnitId]						AS CONVERT(INT, [dim].[Return_ProcessUnitId]([ProcessUnitTag])),
	[EntityId]							AS CONVERT(INT, [rpt].[Return_EntityId]([ProcessUnitTag])),
	[AttributeId]						AS CONVERT(INT, [rpt].[Return_AttributeId]([ProcessUnitTag])),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ProcessUnits_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnits_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnits_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ProcessUnits_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ProcessUnits_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ProcessUnits]		PRIMARY KEY NONCLUSTERED([ProcessUnitIdx] ASC),
	CONSTRAINT [UK_ProcessUnits]		UNIQUE CLUSTERED([XlsProcessUnitDetail] ASC)
);
GO

CREATE TRIGGER [etl].[t_ProcessUnits_u]
ON	[etl].[ProcessUnits]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[etl].[ProcessUnits]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[etl].[ProcessUnits].[ProcessUnitIdx]	= [i].[ProcessUnitIdx];

END;