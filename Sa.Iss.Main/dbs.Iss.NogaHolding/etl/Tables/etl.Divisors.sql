﻿CREATE TABLE [etl].[Divisors]
(
	[DivisorIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[XlsDivisorDetail]					VARCHAR(96)			NOT	NULL,	CONSTRAINT [CL_Divisors_XlsDivisorDetail]	CHECK([XlsDivisorDetail] <> ''),
	[DivisorTag]						VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_Divisors_DivisorTag]			CHECK([DivisorTag] <> ''),
	[DivisorId]							AS CONVERT(INT, [dim].[Return_DivisorId]([DivisorTag])),
	[EntityId]							AS CONVERT(INT, [rpt].[Return_EntityId]([DivisorTag])),
	[AttributeId]						AS CONVERT(INT, [rpt].[Return_AttributeId]([DivisorTag])),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Divisors_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Divisors_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Divisors_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Divisors_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Divisors_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Divisors]			PRIMARY KEY NONCLUSTERED([DivisorIdx] ASC),
	CONSTRAINT [UK_Divisors]			UNIQUE CLUSTERED([XlsDivisorDetail] ASC)
);
GO

CREATE TRIGGER [etl].[t_Divisors_u]
ON	[etl].[Divisors]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[etl].[Divisors]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[etl].[Divisors].[DivisorIdx]	= [i].[DivisorIdx];

END;