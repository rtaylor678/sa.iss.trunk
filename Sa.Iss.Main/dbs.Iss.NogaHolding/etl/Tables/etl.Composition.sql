﻿CREATE TABLE [etl].[Composition]
(
	[CompositionIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[XlsComponentDetail]				VARCHAR(96)			NOT	NULL,	CONSTRAINT [CL_Composition_XlsAccountDetail]	CHECK([XlsComponentDetail] <> ''),
	[ComponentTag]						VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_Composition_ComponentTag]		CHECK([ComponentTag] <> ''),
	[ComponentId]						AS CONVERT(INT, [dim].[Return_ComponentId]([ComponentTag])),
	[EntityId]							AS CONVERT(INT, [rpt].[Return_EntityId]([ComponentTag])),
	[AttributeId]						AS CONVERT(INT, [rpt].[Return_AttributeId]([ComponentTag])),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Composition_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Composition_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Composition_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Composition_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Composition_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Composition]			PRIMARY KEY NONCLUSTERED([CompositionIdx] ASC),
	CONSTRAINT [UK_Composition]			UNIQUE CLUSTERED([XlsComponentDetail] ASC)
);
GO

CREATE TRIGGER [etl].[t_Composition_u]
ON	[etl].[Composition]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[etl].[Composition]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[etl].[Composition].[CompositionIdx]	= [i].[CompositionIdx];

END;