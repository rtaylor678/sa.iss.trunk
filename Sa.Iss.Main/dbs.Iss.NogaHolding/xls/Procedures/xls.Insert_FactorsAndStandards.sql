﻿CREATE PROCEDURE [xls].[Insert_FactorsAndStandards]
(
	@SubmissionId						INT,
	@ProcessUnitDetail					NVARCHAR(96),
	
	@Capacity							FLOAT			= NULL,
	@Capacity_UomName					NVARCHAR(24)	= NULL,
	@kEdc								FLOAT,
	@Eii_MBtuDay						FLOAT			= NULL,
	@Pes_kHours							FLOAT,
	@Mes_kUsd							FLOAT,
	@NeCes_kUsd							FLOAT,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [xls].[FactorsAndStandards]
		(
			[SubmissionId],
			[ProcessUnitDetail],

			[Capacity],
			[Capacity_UomName],
			[kEdc],
			[Eii_MBtuDay],
			[Pes_kHours],
			[Mes_kUsd],
			[NeCes_kUsd],

			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@ProcessUnitDetail,

			@Capacity,
			@Capacity_UomName,
			@kEdc,
			@Eii_MBtuDay,
			@Pes_kHours,
			@Mes_kUsd,
			@NeCes_kUsd,

			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
GO