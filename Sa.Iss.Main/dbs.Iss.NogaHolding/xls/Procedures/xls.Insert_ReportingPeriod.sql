﻿CREATE PROCEDURE [xls].[Insert_ReportingPeriod]
(
	@SubmissionId						INT,
	@PeriodName							NVARCHAR(24),
	@PeriodBeg_Date						DATE			= NULL,
	@Duration_Days						INT				= NULL,
	@Duration_PcntYear					FLOAT			= NULL,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [xls].[ReportingPeriod]
		(
			[SubmissionId],
			[PeriodName],
			[PeriodBeg_Date],
			[Duration_Days],
			[Duration_Fraction],

			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@PeriodName,
			@PeriodBeg_Date,
			@Duration_Days,
			@Duration_PcntYear,

			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
GO