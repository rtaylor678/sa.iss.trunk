﻿CREATE PROCEDURE [xls].[Insert_Personnel]
(
	@SubmissionId						INT,
	@AccountDetail					NVARCHAR(96),

	@Work_Hours							FLOAT,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [xls].[Personnel]
		(
			[SubmissionId],
			[AccountDetail],
			
			[Work_Hours],

			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@AccountDetail,

			@Work_Hours,

			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
GO