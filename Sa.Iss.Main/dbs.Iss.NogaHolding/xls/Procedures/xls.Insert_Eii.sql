﻿CREATE PROCEDURE [xls].[Insert_Eii]
(
	@SubmissionId						INT,
	@ProcessUnitDetail					NVARCHAR(96),

	@UtilizedCapacity					FLOAT			= NULL,
	@UtilizedCapacity_UomName			NVARCHAR(48)	= NULL,
	@EiiStandardEnergy_MBtuDay			FLOAT,
	@StandardEnergy_Pcnt				FLOAT,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [xls].[Eii]
		(
			[SubmissionId],
			[ProcessUnitDetail],

			[UtilizedCapacity],
			[UtilizedCapacity_UomName],
			[EiiStandardEnergy_MBtuDay],
			[StandardEnergy_Pcnt],

			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@ProcessUnitDetail,

			@UtilizedCapacity,
			@UtilizedCapacity_UomName,
			@EiiStandardEnergy_MBtuDay,
			@StandardEnergy_Pcnt,

			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
GO