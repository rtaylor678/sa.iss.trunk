﻿CREATE PROCEDURE [xls].[Insert_Edc]
(
	@SubmissionId						INT,
	@ProcessUnitDetail					NVARCHAR(96),

	@Unit_Count							INT				= NULL,
	@Capacity							FLOAT			= NULL,
	@Capacity_UomName					NVARCHAR(24)	= NULL,
	@kEdc								FLOAT,
	@ukEdc								FLOAT,
	@UtilizedCapacity_Pcnt				FLOAT			= NULL,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [xls].[Edc]
		(
			[SubmissionId],
			[ProcessUnitDetail],

			[Unit_Count],
			[Capacity],
			[Capacity_UomName],
			[kEdc],
			[ukEdc],
			[UtilizedCapacity_Pcnt],

			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@ProcessUnitDetail,

			@Unit_Count,
			@Capacity,
			@Capacity_UomName,
			@kEdc,
			@ukEdc,
			@UtilizedCapacity_Pcnt,

			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
GO