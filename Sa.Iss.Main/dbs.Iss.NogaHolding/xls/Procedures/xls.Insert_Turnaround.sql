﻿CREATE PROCEDURE [xls].[Insert_Turnaround]
(
	@SubmissionId						INT,
	@ProcessUnitDetail					NVARCHAR(96),

	@Interval_Years						FLOAT			= NULL,
	@Duration_Type						VARCHAR(12),

	@Downtime_Hours						FLOAT			= NULL,
	@Cost_kUsd							FLOAT,
	@Work_Hours							FLOAT,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [xls].[Turnaround]
		(
			[SubmissionId],
			[ProcessUnitDetail],

			[Interval_Years],
			[Duration_Type],
			[Downtime_Hours],
			[Cost_kUsd]	,
			[Work_Hours],

			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@ProcessUnitDetail,

			@Interval_Years,
			@Duration_Type,
			@Downtime_Hours,
			@Cost_kUsd,
			@Work_Hours,

			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
GO