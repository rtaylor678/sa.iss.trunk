﻿CREATE VIEW [xls].[PersonnelAllocation]
WITH SCHEMABINDING
AS
SELECT
	[t].[PersonnelIdx],
	[t].[SubmissionId],
	[t].[AccountDetail],
	[t].[AccountId],
	[t].[Work_Hours],
	[p].[Duration_Days],
	[Work_HoursAnnualized]	= [t].[Work_Hours] * [p].[Duration_Multiplier]
FROM
	[xls].[ReportingPeriod]	[p]
INNER JOIN
	[xls].[Personnel]		[t]
		ON	[t].[SubmissionId]	= [p].[SubmissionId];
GO