﻿CREATE VIEW [xls].[ReliabilityAllocation_Total]
WITH SCHEMABINDING
AS
SELECT
	[t].[SubmissionId],

	[ProcessUnitTag]			= 'ProcUnits',

	[EdcRegulatory_Hours]		= SUM([t].[Regulatory_Hours]	* [e].[kEdc]) / SUM([e].[kEdc]),
	[EdcMechanical_Hours]		= SUM([t].[Mechanical_Hours]	* [e].[kEdc]) / SUM([e].[kEdc]),
	[EdcTotal_Hours]			= SUM([t].[Total_Hours]			* [e].[kEdc]) / SUM([e].[kEdc]),
	[p].[Duration_Days],
	[EdcRegulatory_HoursAlloc]	= SUM([t].[Regulatory_Hours]	* [e].[kEdc]) / SUM([e].[kEdc])	* [p].[Duration_Fraction],
	[EdcMechanical_HoursAlloc]	= SUM([t].[Mechanical_Hours]	* [e].[kEdc]) / SUM([e].[kEdc])	* [p].[Duration_Fraction],
	[EdcTotal_HoursAlloc]		= SUM([t].[Total_Hours]			* [e].[kEdc]) / SUM([e].[kEdc])	* [p].[Duration_Fraction]
FROM
	[xls].[ReportingPeriod]		[p]
INNER JOIN
	[xls].[Reliability]			[t]
		ON	[t].[SubmissionId]	= [p].[SubmissionId]
INNER JOIN
	[xls].[Edc]					[e]
		ON	[e].[SubmissionId]	= [p].[SubmissionId]
		AND	[e].[ProcessUnitId]	= [t].[ProcessUnitId]
GROUP BY
	[t].[SubmissionId],
	[p].[Duration_Days],
	[p].[Duration_Fraction];
GO