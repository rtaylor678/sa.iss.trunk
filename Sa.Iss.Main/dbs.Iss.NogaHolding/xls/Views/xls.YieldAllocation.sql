﻿CREATE VIEW [xls].[YieldAllocation]
WITH SCHEMABINDING
AS
SELECT
	[y].[SubmissionId],
	[y].[ComponentDetail],
	[y].[Mole_Pcnt],
	[y].[Mass_MolecularWeight],
	[y].[Weight_Pcnt],
	[y].[LowerHeatingValue_kJNm3],

		[Mass_Tonnes]	= [calc].[Maximum]([OSPreDeSulfur], [OSAmmoniaSyngas] + [OSMethanolSyngas]) * [y].[Mole_Pcnt] * [Mass_MolecularWeight] * [p].[Duration_Days] / 22.414 / 100.0,

		[LowerHeatingValue_GJDay]
						= CASE WHEN [y].[ComponentDetail] = 'Average Molecular Weight and Average LHV'
							THEN
								([calc].[Maximum]([OSPreDeSulfur], [OSAmmoniaSyngas] + [OSMethanolSyngas]) * [LowerHeatingValue_kJNm3] * [p].[Duration_Days] / 1000.0 + [e].[Quantity_GJMonth]) / [p].[Duration_Days]
							END
FROM
	[xls].[Yield]			[y]
INNER JOIN
	[xls].[ReportingPeriod]	[p]
		ON	[p].[SubmissionId]	= [y].[SubmissionId]
INNER JOIN(
	SELECT
		[p].[SubmissionId],
		[e].[ProcessUnitTag],
		[p].[Quantity_kNm3Day]
	FROM
		[stg].[ChemProcessUnitsQuantity]	[p]
	INNER JOIN
		[etl].[ProcessUnits]				[e]
			ON	[e].[XlsProcessUnitDetail]	= [p].[ProcessUnitName]
			AND	[e].[ProcessUnitTag] IN ('OSPreDeSulfur', 'OSAmmoniaSyngas', 'OSMethanolSyngas')
	) [t]
	PIVOT (
		MAX([t].[Quantity_kNm3Day]) FOR [t].[ProcessUnitTag] IN ([OSPreDeSulfur], [OSAmmoniaSyngas], [OSMethanolSyngas])
	) [u]
		ON	[u].[SubmissionId]	= [y].[SubmissionId]
INNER JOIN(
	SELECT
		[t].[SubmissionId],
			[Quantity_GJMonth] = SUM([t].[Quantity_GJMonth])
	FROM
		[stg].[ChemEnergyThermal]		[t]
	INNER JOIN
		[etl].[Accounts]				[e]
			ON	[e].[XlsAccountDetail]	= [t].[ThermalName]
			AND	[e].[AccountTag]		IN ('EnergyPurchFuelGas', 'EnergyPurchOther')
	GROUP BY
		[t].[SubmissionId]
	) [e]
		ON	[e].[SubmissionId]	= [y].[SubmissionId];