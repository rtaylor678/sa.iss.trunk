﻿CREATE VIEW [xls].[TurnaroundEdc_Total]
WITH SCHEMABINDING
AS
SELECT
	[t].[SubmissionId],
	[ProcessUnitTag]			= 'ProcUnits',
	[t].[Duration_Type],
	[EdcDowntime_Hours]			= SUM([t].[Downtime_Hours]	* [e].[kEdc]) / SUM([e].[kEdc]),
	[TotalCost_kUsd]			= SUM([t].[Cost_kUsd]),
	[TotalWork_Hours]			= SUM([t].[Work_Hours]),
	[p].[Duration_Days],
	[EdcDowntime_HoursAlloc]	= SUM([t].[Downtime_Hours]	* [e].[kEdc]) / SUM([e].[kEdc])	* [p].[Duration_Fraction],
	[TotalCost_kUsdAlloc]		= SUM([t].[Cost_kUsd])										* [p].[Duration_Fraction],
	[TotalWork_HoursAlloc]		= SUM([t].[Work_Hours])										* [p].[Duration_Fraction]
FROM
	[xls].[ReportingPeriod]		[p]
INNER JOIN
	[xls].[Turnaround]			[t]
		ON	[t].[SubmissionId]	= [p].[SubmissionId]
INNER JOIN
	[xls].[Edc]					[e]
		ON	[e].[SubmissionId]	= [p].[SubmissionId]
		AND	[e].[ProcessUnitId]	= [t].[ProcessUnitId]
GROUP BY
	[t].[SubmissionId],
	[t].[Duration_Type],
	[p].[Duration_Days],
	[p].[Duration_Fraction];
GO