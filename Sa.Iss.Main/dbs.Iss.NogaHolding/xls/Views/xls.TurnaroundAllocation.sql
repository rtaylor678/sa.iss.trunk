﻿CREATE VIEW [xls].[TurnaroundAllocation]
WITH SCHEMABINDING
AS
SELECT
	[t].[SubmissionId],

	[t].[ProcessUnitDetail],
	[t].[ProcessUnitId],

	[t].[Duration_Type],
	[t].[Downtime_Hours],
	[t].[Cost_kUsd],
	[t].[Work_Hours],
	[p].[Duration_Days],

	[Downtime_HoursAlloc]	= [t].[Downtime_Hours]	* [p].[Duration_Fraction],
	[Cost_kUsdAlloc]		= [t].[Cost_kUsd]		* [p].[Duration_Fraction],
	[Work_HoursAlloc]		= [t].[Work_Hours]		* [p].[Duration_Fraction]
FROM
	[xls].[ReportingPeriod]		[p]
INNER JOIN
	[xls].[Turnaround]			[t]
		ON	[t].[SubmissionId]	= [p].[SubmissionId]
		AND	[t].[Duration_Type]	= 'Annualized'
GO