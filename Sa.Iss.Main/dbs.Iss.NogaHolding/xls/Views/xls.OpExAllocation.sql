﻿CREATE VIEW [xls].[OpExAllocation]
WITH SCHEMABINDING
AS
SELECT
	[t].[OpExIdx],
	[t].[SubmissionId],
	[t].[AccountDetail],
	[t].[AccountId],
	[t].[Amount_kUsd],
	[t].[Amount_CentsUEdc],
	[t].[Amount_kUsdHvc],
	[p].[Duration_Days],
		[Amount_kUsdAnnualized]			= [t].[Amount_kUsd]			* [p].[Duration_Multiplier],
		[Amount_CentsUEdcAnnualized]	= [t].[Amount_CentsUEdc]	* [p].[Duration_Multiplier],
		[Amount_kUsdEdcAnnualized]		= [t].[Amount_kUsd]			* [p].[Duration_Multiplier]	/ [e].[kEdc],
		[Amount_CentsUEdcEDCAnnualized]	= [t].[Amount_CentsUEdc]	* [p].[Duration_Multiplier]	/ [e].[kEdc]
FROM
	[xls].[ReportingPeriod]	[p]
INNER JOIN
	[xls].[OpEx]			[t]
		ON	[t].[SubmissionId]		= [p].[SubmissionId]
LEFT OUTER JOIN
	[xls].[Edc]				[e]
		ON	[e].[SubmissionId]		= [p].[SubmissionId]
		AND	[e].[ProcessUnitDetail]	= 'Total'
GO