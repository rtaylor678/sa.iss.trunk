﻿CREATE TABLE [xls].[FactorsAndStandards]
(
	[FactorsAndStandardsIdx]			INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_FactorsAndStandards_Submissions]				REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitDetail]					NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_FactorsAndStandards_ProcessUnitDetail]		CHECK([ProcessUnitDetail] <> ''),
	[ProcessUnitId]						AS CONVERT(INT, [dim].[Return_ProcessUnitId]([ProcessUnitDetail])),

	[Capacity]							FLOAT					NULL,	CONSTRAINT [CR_FactorsAndStandards_Capacity]				CHECK([Capacity] >= 0.0),
	[Capacity_UomName]					NVARCHAR(48)			NULL,	CONSTRAINT [CL_FactorsAndStandards_Capacity_UomName]		CHECK([Capacity_UomName] <> ''),
	[UomId]								AS CONVERT(INT, [dim].[Return_UomId]([Capacity_UomName])),
	[kEdc]								FLOAT				NOT	NULL,	CONSTRAINT [CR_FactorsAndStandards_kEdc]					CHECK([kEdc] >= 0.0),
	[Eii_MBtuDay]						FLOAT					NULL,	CONSTRAINT [CR_FactorsAndStandards_Eii_MBtuDay]				CHECK([Eii_MBtuDay] >= 0.0),
	[Pes_kHours]						FLOAT				NOT	NULL,	CONSTRAINT [CR_FactorsAndStandards_Pes_kHours]				CHECK([Pes_kHours] >= 0.0),
	[Mes_kUsd]							FLOAT				NOT	NULL,	CONSTRAINT [CR_FactorsAndStandards_Mes_kUsd]				CHECK([Mes_kUsd] >= 0.0),
	[NeCes_kUsd]						FLOAT				NOT	NULL,	CONSTRAINT [CR_FactorsAndStandards_NeCes_kUsd]				CHECK([NeCes_kUsd] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_FactorsAndStandards_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FactorsAndStandards_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FactorsAndStandards_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_FactorsAndStandards_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_FactorsAndStandards_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_FactorsAndStandards]	PRIMARY KEY NONCLUSTERED([FactorsAndStandardsIdx] ASC),
	CONSTRAINT [UK_FactorsAndStandards]	UNIQUE CLUSTERED([SubmissionId] ASC, [ProcessUnitDetail] ASC)
);
GO

CREATE TRIGGER [xls].[t_FactorsAndStandards_u]
ON	[xls].[FactorsAndStandards]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[xls].[FactorsAndStandards]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[xls].[FactorsAndStandards].[FactorsAndStandardsIdx]	= [i].[FactorsAndStandardsIdx];

END;