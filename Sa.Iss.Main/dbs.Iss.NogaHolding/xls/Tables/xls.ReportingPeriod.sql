﻿CREATE TABLE [xls].[ReportingPeriod]
(
	[ReportingPeriodIdx]				INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ReportingPeriod_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[PeriodName]						NVARCHAR(24)		NOT	NULL,	CONSTRAINT [CR_ReportingPeropd_PeriodName]			CHECK([PeriodName] <> ''),
	[PeriodBeg_Date]					DATE					NULL,

	[DaysInYear]						AS CONVERT(INT, DATEPART(DAYOFYEAR, DATEFROMPARTS(YEAR([PeriodBeg_Date]), 12, 31))),
	[Duration_Hours]					AS CONVERT(INT, [Duration_Days] * 24),
	[Duration_Days]						INT					NOT	NULL,	CONSTRAINT [CR_ReportingPeropd_Duration_Days]		CHECK([Duration_Days] >= 0),
	[Duration_Fraction]					FLOAT				NOT	NULL,	CONSTRAINT [CR_ReportingPeropd_Duration_Fraction]	CHECK([Duration_Fraction] >= 0),
	[Duration_Multiplier]				AS CONVERT(FLOAT, 1.0 / [Duration_Fraction]),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ReportingPeriod_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ReportingPeriod_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ReportingPeriod_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ReportingPeriod_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ReportingPeriod_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ReportingPeriod]		PRIMARY KEY NONCLUSTERED([ReportingPeriodIdx] ASC),
	CONSTRAINT [UK_ReportingPeriod]		UNIQUE CLUSTERED([SubmissionId] ASC)
);
GO

CREATE TRIGGER [xls].[t_ReportingPeriod_u]
ON	[xls].[ReportingPeriod]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[xls].[ReportingPeriod]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[xls].[ReportingPeriod].[ReportingPeriodIdx]	= [i].[ReportingPeriodIdx];

END;