﻿CREATE TABLE [xls].[InputRaw]
(
	[InputRawIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_InputRaw_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[InputRawDetail]					NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_InputRaw_InputRawDetail]			CHECK([InputRawDetail] <> ''),
 
	[Quantity_TonnesDay]					FLOAT				NOT	NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_InputRaw_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_InputRaw_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_InputRaw_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_InputRaw_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_InputRaw_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_InputRaw]			PRIMARY KEY NONCLUSTERED([InputRawIdx] ASC),
	CONSTRAINT [UK_InputRaw]			UNIQUE CLUSTERED([SubmissionId] ASC, [InputRawDetail] ASC)
);
GO

CREATE TRIGGER [xls].[t_InputRaw_u]
ON	[xls].[InputRaw]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[xls].[InputRaw]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[xls].[InputRaw].[InputRawIdx]	= [i].[InputRawIdx];

END;