﻿CREATE TABLE [xls].[Yield]
(
	[YieldIdx]							INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_Yield_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[ComponentDetail]					NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Yield_ComponentDetail]		CHECK([ComponentDetail] <> ''),

	[Mole_Pcnt]							FLOAT				NOT	NULL,
	[Mass_MolecularWeight]				FLOAT				NOT	NULL,
	[Weight_Pcnt]						FLOAT				NOT	NULL,
	[LowerHeatingValue_kJNm3]			FLOAT				NOT	NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Yield_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Yield_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Yield_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Yield_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Yield_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Yield]				PRIMARY KEY NONCLUSTERED([YieldIdx] ASC),
	CONSTRAINT [UK_Yield]				UNIQUE CLUSTERED([SubmissionId] ASC, [ComponentDetail] ASC)
);
GO

CREATE TRIGGER [xls].[t_Yield_u]
ON	[xls].[Yield]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[xls].[Yield]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[xls].[Yield].[YieldIdx]	= [i].[YieldIdx];

END;