﻿CREATE TABLE [xls].[Reliability]
(
	[ReliabilityIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_Reliability_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitDetail]					NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Reliability_ProcessUnitDetail]	CHECK([ProcessUnitDetail] <> ''),
	[ProcessUnitId]						AS CONVERT(INT, [dim].[Return_ProcessUnitId]([ProcessUnitDetail])),

	[Regulatory_Hours]					FLOAT					NULL,	CONSTRAINT [CR_Reliability_Regulatory_Hours]	CHECK([Regulatory_Hours] >= 0.0),
	[Mechanical_Hours]					FLOAT				NOT	NULL,	CONSTRAINT [CR_Reliability_Mechanical_Hours]	CHECK([Mechanical_Hours] >= 0.0),
	[Total_Hours]						FLOAT				NOT	NULL,	CONSTRAINT [CR_Reliability_Total_Hours]			CHECK([Total_Hours] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Reliability_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Reliability_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Reliability_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Reliability_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Reliability_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Reliability]			PRIMARY KEY NONCLUSTERED([ReliabilityIdx] ASC),
	CONSTRAINT [UK_Reliability]			UNIQUE CLUSTERED([SubmissionId] ASC, [ProcessUnitDetail] ASC)
);
GO

CREATE TRIGGER [xls].[t_Reliability_u]
ON	[xls].[Reliability]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[xls].[Reliability]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[xls].[Reliability].[ReliabilityIdx]	= [i].[ReliabilityIdx];

END;