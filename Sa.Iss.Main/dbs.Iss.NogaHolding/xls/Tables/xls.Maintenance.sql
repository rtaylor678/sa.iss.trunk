﻿CREATE TABLE [xls].[Maintenance]
(
	[MaintenanceIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_Maintenance_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[AccountDetail]						NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Maintenance_AccountName]			CHECK([AccountDetail] <> ''),
	[AccountId]							AS CONVERT(INT, [dim].[Return_AccountId]([AccountDetail])),

	[Cost_kUsd]							FLOAT				NOT	NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Maintenance_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Maintenance_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Maintenance_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Maintenance_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Maintenance_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Maintenance]			PRIMARY KEY NONCLUSTERED([MaintenanceIdx] ASC),
	CONSTRAINT [UK_Maintenance]			UNIQUE CLUSTERED([SubmissionId] ASC, [AccountDetail] ASC)
);
GO

CREATE TRIGGER [xls].[t_Maintenance_u]
ON	[xls].[Maintenance]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[xls].[Maintenance]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[xls].[Maintenance].[MaintenanceIdx]	= [i].[MaintenanceIdx];

END;