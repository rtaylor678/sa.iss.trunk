﻿CREATE TABLE [xls].[OpEx]
(
	[OpExIdx]							INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_OpEx_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[AccountDetail]						NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_OpEx_AccountDetail]			CHECK([AccountDetail] <> ''),
	[AccountId]							AS CONVERT(INT, [dim].[Return_AccountId]([AccountDetail])),

	[Amount_kUsd]						FLOAT				NOT	NULL,
	[Amount_CentsUEdc]					FLOAT				NOT	NULL,

	[Amount_kUsdHvc]					FLOAT					NULL,
	
	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_OpEx_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_OpEx_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_OpEx_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_OpEx_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_OpEx_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_OpEx]				PRIMARY KEY NONCLUSTERED([OpExIdx] ASC),
	CONSTRAINT [UK_OpEx]				UNIQUE CLUSTERED([SubmissionId] ASC, [AccountDetail] ASC)
);
GO

CREATE TRIGGER [xls].[t_OpEx_u]
ON	[xls].[OpEx]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[xls].[OpEx]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[xls].[OpEx].[OpExIdx]	= [i].[OpExIdx];

END;