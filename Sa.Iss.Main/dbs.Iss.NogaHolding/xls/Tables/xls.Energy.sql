﻿CREATE TABLE [xls].[Energy]
(
	[EnergyIdx]							INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_Energy_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[AccountDetail]						NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_Energy_AccountDetail]		CHECK([AccountDetail] <> ''),
	[AccountId]							AS CONVERT(INT, [dim].[Return_AccountId]([AccountDetail])),

	[Consumption_GJ]					FLOAT				NOT	NULL,

	[Consumption_GJHvc]					FLOAT				NOT	NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Energy_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Energy_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Energy_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Energy_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Energy_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Energy]				PRIMARY KEY NONCLUSTERED([EnergyIdx] ASC),
	CONSTRAINT [UK_Energy]				UNIQUE CLUSTERED([SubmissionId] ASC, [AccountDetail] ASC)
);
GO

CREATE TRIGGER [xls].[t_Energy_u]
ON	[xls].[Energy]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[xls].[Energy]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[xls].[Energy].[EnergyIdx]	= [i].[EnergyIdx];

END;