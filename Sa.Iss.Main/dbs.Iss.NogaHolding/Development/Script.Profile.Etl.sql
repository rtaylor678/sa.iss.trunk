﻿:setvar dbProfile "ProfileFuels12"
:setvar DatabaseName "NogaHolding"
:setvar dbGlobal "GlobalDB"

--DECLARE @profileSubmissionId	INT			= 1221;
DECLARE @profileSubmissionId	INT			= 4373;

DECLARE @FactorSetID			CHAR(8)		= '2015';
DECLARE @CurrencyId				CHAR(4)		= 'USD';

DECLARE @profileFactorSetID		CHAR(8)		= '2012';
DECLARE @profileScenarioId		CHAR(8)		= 'CLIENT';
DECLARE @profileCurrencyId		CHAR(4)		= 'USD';
DECLARE @profileDataTypeId		CHAR(6)		= 'ADJ';
DECLARE @profileUomId			CHAR(3)		= 'MET';

--		SELECT * FROM [$(dbProfile)].[dbo].[ProcessID_LU]

SELECT * FROm rpt.OutputFacilities


SELECT
	--[s].[SubmissionID],
	--[s].[RefineryID],
	--[s].[DataSet],
	--[m].[UnitId],
	--[m].[ProcessID],
	----interval(years)

	--[m].[TAHrsDown],
	--[m].[TACostUS],
	--	[WorkHours]	= COALESCE([m].[TACompWhr], 0.0) + COALESCE([m].[TAContWHr], 0.0),
--Actual TA Hours (TAWHr) should be [TAOCCSTH] + [TAOCCOVT] +[TAMPSSTH] + [TAMPSOVT] + [TAContOCC] +[TAContMPS]:

	--[m].[AnnTADownDays],
	--[m].[AnnTACost]
		--[AnnWorkHours]	= COALESCE([m].[AnnTACompWhr], 0.0) + COALESCE([m].[AnnTAContWHr], 0.0)
--Annualized TA workhours (AnnTAWHr) should be: [AnnTAOCCSTH] + [AnnTAOCCOVT] + [AnnTAMPSSTH] + [AnnTAMPSOVT] + [AnnTAContOCCWHr] + [AnnTAContMPSWHr]


[s].[SubmissionID],
[m].*
FROM
	[$(dbProfile)].[dbo].[Submissions]	[s]
INNER JOIN
	[$(dbProfile)].[dbo].[MaintTA]		[m]
		ON	[m].[RefineryID]	= [s].[RefineryID]
		AND	[m].[DataSet]		= [s].[DataSet]
WHERE	[s].[SubmissionID]			= @ProfileSubmissionId
	AND	[m].[TADate] IS NOT NULL









--SELECT DISTINCT '(N''' + RTRIM(LTRIM([ProcessID])) + ''', N''''),'
--FROM [$(dbProfile)].[dbo].[FactorProcessCalc]
--ORDER BY '(N''' + RTRIM(LTRIM([ProcessID])) + ''', N''''),';
	
--

DECLARE @Etl	TABLE
(
	[EaTag]				VARCHAR(16)	NOT	NULL	CHECK([EaTag] <> ''),
	[EntityTag]			VARCHAR(16)	NOT	NULL	CHECK([EntityTag] <> ''),
	[AttributeTag]		VARCHAR(24)	NOT	NULL	CHECK([AttributeTag] <> ''),
	PrIMARY KEY CLUSTERED([EaTag] ASC),
	UNIQUE NONCLUSTERED([EntityTag] ASC, [AttributeTag] ASC)
);

INSERT INTO @Etl
(
	[EaTag],
	[EntityTag],
	[AttributeTag]
)
SELECT
	[t].[EaTag],
	[t].[EntityTag],
	[t].[AttributeTag]
FROM(VALUES
	('EDC', '-', 'kEdc'),
	('UEDC', '-', 'ukEdc'),
	('UtilPcnt', '-', 'uCap_Pcnt'),
	('StdEnergy', '-', 'Eii_MBtu'),
	('StdEnergy_GJDay', '-', 'Eii_GJ'),
	('StdEnergy_Pcnt', '-', 'Eii_Pcnt'),
	('PlantPersEffDiv', '-', 'Pes'),
	('PlantMaintEffDiv', '-', 'Mes'),
	('NEOpexEffDiv', '-', 'NeCes'),
	('ItemCount', '-', 'Unit'),
	('Cap', '-', 'Cap'),
	('UtilCap', '-', 'uCap')
	) [t]([EaTag], [EntityTag], [AttributeTag]);

SELECT
		--[SubmissionID]		= @fctSubmissionId,
		[MethodologyId]		= [$(DatabaseName)].[ante].[Return_MethodologyId](@FactorSetID),
		[CurrencyId]		= [$(DatabaseName)].[dim].[Return_CurrencyId](@CurrencyId),
	[u].[ProcessID],
		[EntityId]			= [$(DatabaseName)].[rpt].[Return_EntityId]([u].[ProcessID]),
		[AttributeId]		= [$(DatabaseName)].[rpt].[Return_AttributeId]([e].[AttributeTag]),
	[u].[Value_Float]
FROM (
	SELECT
		[c].[SubmissionID],
			--[EntityId]			= [$(DatabaseName)].[rpt].[Return_EntityId]([c].[ProcessID]),
		[c].[ProcessID],
		[c].[EDC],
		[c].[UEDC],
		[c].[UtilPcnt],
		[c].[StdEnergy],
			[StdEnergy_GJDay]	= CONVERT(REAL, [c].[StdEnergy] * 1.055),
			[StdEnergy_Pcnt]	= CONVERT(REAL, [c].[StdEnergy] / SUM([c].[StdEnergy]) OVER() * 100.0),
		[c].[PlantPersEffDiv],
		[c].[PlantMaintEffDiv],
		[c].[NEOpexEffDiv]
	FROM
		[$(dbProfile)].[dbo].[FactorProcessCalc]		[c]
	WHERE	[c].[FactorSet]		= @profileFactorSetID
		AND	[c].[SubmissionID]	= @profileSubmissionId
	) [p]
	UNPIVOT(
		[Value_Float] FOR [EaTag] IN
		(
			[EDC],
			[UEDC],
			[UtilPcnt],
			[StdEnergy],
			[StdEnergy_GJDay],
			[StdEnergy_Pcnt],
			[PlantPersEffDiv],
			[PlantMaintEffDiv],
			[NEOpexEffDiv]
		)
	) [u]
LEFT OUTER JOIN	@Etl	[e]
	ON	[e].[EaTag]	= [u].[EaTag];
	

SELECT
		[MethodologyId]		= [$(DatabaseName)].[ante].[Return_MethodologyId](@FactorSetID),
		[CurrencyId]		= [$(DatabaseName)].[dim].[Return_CurrencyId](@CurrencyId),
	[u].[ProcessID],
		[EntityId]			= [$(DatabaseName)].[rpt].[Return_EntityId]([u].[ProcessID]),
		[AttributeId]		= [$(DatabaseName)].[rpt].[Return_AttributeId]([e].[AttributeTag]),
	[u].[Value_Float]
FROM (
	SELECT
		[c].[SubmissionID],
			[EntityId]			= [$(DatabaseName)].[rpt].[Return_EntityId]([c].[ProcessID]),
		[c].[ProcessID],
			[ItemCount]			= CONVERT(REAL, COUNT(1)),
			[Cap]				= CONVERT(REAL,SUM([c].[Cap])),
			[UtilCap]			= CONVERT(REAL,SUM([c].[UtilCap]))
	FROM
		[$(dbProfile)].[dbo].[Config]	[c]
	WHERE
		[SubmissionID]	= @ProfileSubmissionId
	GROUP BY
		[c].[SubmissionID],
		[c].[ProcessID]
	) [p]
	UNPIVOT(
		[Value_Float] FOR [EaTag] IN
		(
			[ItemCount],
			[Cap],
			[UtilCap]
		)
	) [u]
LEFT OUTER JOIN	@Etl	[e]
	ON	[e].[EaTag]	= [u].[EaTag];








-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--	Submission

DECLARE @Submissions			TABLE
(
	[SubmissionId]		INT		NOT	NULL,
	PRIMARY KEY CLUSTERED([SubmissionId] ASC)
);

INSERT INTO [$(dbIss)].[fact].[Submissions]
(
	[GroupId],
	[DataSpan],
	[DataYear],
	[DataMonth],
	[PeriodBeg],
	[PeriodEnd]
)
OUTPUT
	INSERTED.[SubmissionId]
INTO
	@Submissions
	(
		[SubmissionId]
	)
SELECT
	[GroupId]		= NULL,
	[DataSpan]		= 'M',
	[DataYear]		= YEAR([p].[PeriodStart]),
	[DataMonth]		= MONTH([p].[PeriodStart]),
	[PeriodBeg]		= [p].[PeriodStart],
	[PeriodEnd]		= [p].[PeriodEnd]
FROM
	[$(dbProfile)].[dbo].[GenSum]	[p]
WHERE	[p].[FactorSet]		= @FactorSetId
	AND	[p].[Scenario]		= @ScenarioId
	AND	[p].[Currency]		= @CurrencyId
	AND	[p].[UOM]			= @UomId
	AND	[p].[SubmissionID]	= @ProfileSubmissionId;

SET	@SubmissionId = 
(
	SELECT TOP 1
		[s].[SubmissionId]
	FROM
		@Submissions	[s]
);





-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--	Turnaround






-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--	Personnel

SELECT
	[u].[SubmissionID],
		[EntityId]			= [$(dbIss)].[rpt].[Return_EntityId]([u].[EntityTag]),
		[AttributeId]		= [$(dbIss)].[rpt].[Return_AttributeId]('WorkHours'),
--		[AttributeId]		= [$(dbIss)].[rpt].[Return_AttributeId]('WorkHoursAnnualized'),
	[u].[WorkHours]
FROM (
	SELECT
		[p].[SubmissionID],
		[GeneralMaintCompanyLabor]	= SUM(CASE WHEN [p].[PersID]	NOT	IN ('OCCTAADJ', 'MPSTAADJ') THEN [p].[CompWHr]	END),
		[GeneralMaintContractLabor]	= SUM(CASE WHEN [p].[PersID]	NOT	IN ('OCCTAADJ', 'MPSTAADJ') THEN [p].[ContWHr]	END),
		[AllocGAPers]				= SUM(CASE WHEN [p].[PersID]	NOT	IN ('OCCTAADJ', 'MPSTAADJ') THEN [p].[GAWhr]	END),
		[GeneralMaint]				= SUM(CASE WHEN [p].[PersID]	NOT	IN ('OCCTAADJ', 'MPSTAADJ') THEN [p].[TotWhr]	END),
		[MaintTaLabor]				= SUM(CASE WHEN [p].[PersID]		IN ('OCCTAADJ', 'MPSTAADJ') THEN [p].[TotWhr]	END),
		[OpExLaborMaterials]		= SUM([p].[TotWhr])
	FROM
		[$(dbProfile)].[dbo].[Pers]	[p]
	WHERE
		[p].[SubmissionID]		= @ProfileSubmissionId
	GROUP BY
		[p].[SubmissionID]
	) [p]
	UNPIVOT (
		[WorkHours] FOR [EntityTag] IN (
			[GeneralMaintCompanyLabor],
			[GeneralMaintContractLabor],
			[AllocGAPers],
			[GeneralMaint],
			[MaintTaLabor],
			[OpExLaborMaterials]
		)
	) [u]


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--	Energy

SELECT
	[p].[SubmissionID],
	[e].[EntityId],
		--[AttributeId]	= [$(dbIss)].[rpt].[Return_AttributeId]('EnergyConsMBTU'),
		--[Amount_MBTU]	= COALESCE([p].[SourceMBTU], [p].[UsageMBTU]),
		[AttributeId]	= [$(dbIss)].[rpt].[Return_AttributeId]('EnergyConsGJ'),
		[Amount_GJ]		= COALESCE([p].[SourceMBTU], [p].[UsageMBTU]) / 0.94708628903179 
FROM
	[$(dbProfile)].[dbo].[Energy]	[p]
INNER JOIN
	[$(dbIss)].[etl].[Energy]		[e]
		ON	[e].[EnergyType]		= [p].[EnergyType]
		AND	[e].[TransType]			= [p].[TransType]
WHERE	[p].[SubmissionID]			= @ProfileSubmissionId;


-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--	OpEx and Maintenance Costs

--	NeOpEx
--	Personnel Cost (not the index)
--	Production Divisors

SELECT
	[u].[SubmissionID],

		[EntityId]			= [$(dbIss)].[rpt].[Return_EntityId]([u].[EntityTag]),
	[u].[EntityTag],
	
		--[AttributeId]		= [$(dbIss)].[rpt].[Return_AttributeId]('RptExpCentsUedc'),
		[AttributeId]		= [$(dbIss)].[rpt].[Return_AttributeId]('RptExp'),
	[u].[RptExp]
FROM(
	SELECT
		[o].[SubmissionID],
		[PersRoutCmpny]			= [o].[OCCSal],			--	[Emp SWB]
		[MaintRoutContract]		= [o].[ContMaintLabor],	--	[Maint Cont. Labor]
		[PersRoutCntrct]		= [o].[OthCont],		--	[Misc. Contact Svc.]
		[OpEx]					= [o].[STNonVol],		--	[Total OpEx]
		[OpExSGA]				= [o].[GAPers]			--	[Alloc. GA. Prs. Costs]
	FROM
		[$(dbProfile)].[dbo].[OpExAll]	[o]
	--WHERE	[o].[Currency]		= @CurrencyId
	--	AND	[o].[Scenario]		= @ScenarioId
	--	AND	[o].[DataType]		= @DataTypeId
	WHERE	[o].[SubmissionID]	= @ProfileSubmissionId
	) [p]
	UNPIVOT (
		[RptExp] FOR [EntityTag] IN (
			[PersRoutCmpny],
			[MaintRoutContract],
			[PersRoutCntrct],
			[OpEx],
			[OpExSGA]
		)
	) [u];


