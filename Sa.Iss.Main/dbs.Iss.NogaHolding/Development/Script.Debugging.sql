﻿
SELECT * FROM [audit].[LogError]



DECLARE @EntityId		INT = [rpt].[Return_EntityId]('MEI');
DECLARE @AttributeId	INT = [rpt].[Return_AttributeId]('MEI');
DECLARE @fctSubmissionId	INT = -1;
DECLARE @stgSubmissionId	INT =  1;

--SELECT * FROM [xls].[Personnel]


		SELECT
				[SubmissionId]	= @fctSubmissionId,
				[MethodologyId]	= [ante].[Return_MethodologyId]('2015'),
				[CurrencyId]	= [dim].[Return_CurrencyId]('USD'),
				[EntityId]		= [rpt].[Return_EntityId]('Pei'),
				[AttributeId]	= [rpt].[Return_AttributeId]('Pei'),
			[n].[Work_HoursAnnualized] , [d].[Pes_kHours], 10.0,
			[a].[AccountTag]
		FROM
			[xls].[PersonnelAllocation]			[n]
		INNER JOIN
			[etl].[Accounts]					[a]
				ON	[a].[XlsAccountDetail]		= [n].[AccountDetail]
				AND	[a].[AccountTag]			= 'OpExLaborMaterials'
		INNER JOIN
			[xls].[FactorsAndStandards]			[d]
				ON	[d].[SubmissionId]			= [n].[SubmissionId]
		INNER JOIN
			[etl].[ProcessUnits]				[p]
				ON	[p].[XlsProcessUnitDetail]	= [d].[ProcessUnitDetail]
				AND	[p].[ProcessUnitTag]		= 'ProcUnits'
		WHERE
			[n].[SubmissionId]	= @stgSubmissionId;
