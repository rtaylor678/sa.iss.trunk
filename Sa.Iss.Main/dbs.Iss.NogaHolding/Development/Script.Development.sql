﻿
DECLARE @stgSubmissionId		INT = IDENT_CURRENT('[stg].[Submissions]');
DECLARE @fctSubmissionId		INT = IDENT_CURRENT('[fact].[Submissions]') - 1;

--SELECT @stgSubmissionId, @fctSubmissionId
--EXECUTE [rpt].[Inject_OutputFacilities] @stgSubmissionId;

DECLARE @SubmissionId			INT = @fctSubmissionId;
DECLARE @MethodologyId			INT = [ante].[Return_MethodologyId]('2015');
DECLARE @CurrencyId				INT = [dim].[Return_CurrencyId]('USD');

--DECLARE @EntityId				INT	= [rpt].[Return_EntityId]('OpExNe');
--DECLARE @AttributeId			INT	= [rpt].[Return_AttributeId]('RptExpEdcAnnualized');

--DECLARE @DivEntityId			INT	= [rpt].[Return_EntityId]('ProcUnits');
--DECLARE @DivAttributeId			INT	= [rpt].[Return_AttributeId]('ukEdc');

		DECLARE @EntityId				INT	= [rpt].[Return_EntityId]('OpEx');
		DECLARE @AttributeId			INT	= [rpt].[Return_AttributeId]('RptExpCentsUedc');

SELECT
		[SubmissionId]		= [s].[BaseSubmissionId],
	--[r].[MethodologyId],
	--[r].[CurrencyId],

	[s].[EntityId],
		[EntitySortKey]		= CONVERT(INT, [s].[EntitySortKey]),
	[s].[EntityTag],
	[s].[EntityName],
	[s].[EntityDetail],

	[s].[AttributeId],
		[AttributeSortKey]	= CONVERT(INT, [s].[AttributeSortKey]),
	[s].[AttributeTag],
	[s].[AttributeName],
	[s].[AttributeDetail],

		[Value]	 = CASE [s].[EntityTag]
				WHEN 'Tgt'	THEN [b].[Value]
				WHEN 'YTD'	THEN [r].[Value]
				END

FROM
	[fact].[Select_SubmissionsLast12Mos](@SubmissionId)		[s]
LEFT OUTER JOIN
	[rpt].[OutputFacilities]								[r]
		ON	[r].[SubmissionId]	= [s].[SubmissionId]
		AND	[r].[EntityId]		= @EntityId
		AND	[r].[AttributeId]	= @AttributeId
		AND	[r].[MethodologyId]	= @MethodologyId
		AND	[r].[CurrencyId]	= @CurrencyId
LEFT OUTER JOIN
	[ante].[BaselineTarget]									[b]
		ON	[b].[MethodologyId]	= [r].[MethodologyId]
		AND	[b].[CurrencyId]	= [r].[CurrencyId]
		AND	[b].[EntityId]		= [r].[EntityId]
		AND	[b].[AttributeId]	= [r].[AttributeId];