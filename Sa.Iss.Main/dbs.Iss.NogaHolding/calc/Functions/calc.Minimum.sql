﻿CREATE FUNCTION [calc].[Minimum]
(
	@NumberA	FLOAT,
	@NumberB 	FLOAT
)
RETURNS FLOAT
WITH SCHEMABINDING
AS
BEGIN

	RETURN CASE WHEN @NumberA < @NumberB THEN @NumberA ELSE @NumberB END;

END;
