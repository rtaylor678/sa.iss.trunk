﻿CREATE PROCEDURE [stg].[Insert_ChemEnergyElectricity]
(
	@SubmissionId						INT,
	
	@ElectricityName					VARCHAR(40),
	@Quantity_MWhMonth					FLOAT,
	@Efficiency_MJMWh					FLOAT			= NULL,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [stg].[ChemEnergyElectricity]
		(
			[SubmissionId],
			[ElectricityName],
			[Quantity_MWhMonth],
			[Efficiency_MJMWh],
			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@ElectricityName,
			@Quantity_MWhMonth,
			@Efficiency_MJMWh,
			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;