﻿CREATE PROCEDURE [stg].[Insert_ChemProcessUnits]
(
	@SubmissionId						INT,
	@ProcessUnitName					VARCHAR(48),
	
	@ProcessUnitNumber					INT				= NULL,
	@ProcessId							VARCHAR(12)		= NULL,
	@ProcessType						VARCHAR(12)		= NULL,
	@Capacity_Units						VARCHAR(32)		= NULL,
	@Capacity							FLOAT			= NULL,
	@CapacityUtilization_Pcnt			FLOAT			= NULL,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [stg].[ChemProcessUnits]
		(
			[SubmissionId],
			[ProcessUnitName],
			[ProcessUnitNumber],
			[ProcessId],
			[ProcessType],
			[Capacity_Units],
			[Capacity],
			[CapacityUtilization_Pcnt],
			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@ProcessUnitName,

			@ProcessUnitNumber,
			@ProcessId,
			@ProcessType,
			@Capacity_Units,
			@Capacity,
			@CapacityUtilization_Pcnt,

			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;