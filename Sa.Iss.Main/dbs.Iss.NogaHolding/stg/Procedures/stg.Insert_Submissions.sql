﻿CREATE PROCEDURE [stg].[Insert_Submissions]
(
	@CompanyName						NVARCHAR(48),
	@FacilityName						NVARCHAR(48),

	@DataSpan							CHAR(1),
	@DataYear							SMALLINT,
	@DataMonth							TINYINT			= NULL,
	@PeriodBeg							DATE			= NULL,
	@PeriodEnd							DATE			= NULL,
	@Duration_Days						SMALLINT		= NULL,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	DECLARE @SubmissionId	INT = NULL;

	BEGIN TRY

		DECLARE @Submissions	TABLE
		(
			[SubmissionId]		INT		NOT	NULL,
			PRIMARY KEY CLUSTERED([SubmissionId] ASC)
		);

		INSERT INTO [stg].[Submissions]
		(
			[CompanyName],
			[FacilityName],
			[DataSpan],
			[DataYear],
			[DataMonth],
			[PeriodBeg],
			[PeriodEnd],
			[Duration_Days],
			[tsModified]
		)
		OUTPUT
			INSERTED.[SubmissionId]
		INTO
			@Submissions
			(
				[SubmissionId]
			)
		VALUES
		(
			@CompanyName,
			@FacilityName,
			@DataSpan,
			@DataYear,
			@DataMonth,
			@PeriodBeg,
			@PeriodEnd,
			@Duration_Days,
			@tsModified
		);

		SELECT TOP 1
			@SubmissionId = [s].[SubmissionId]
		FROM
			@Submissions	[s];

		RETURN @SubmissionId;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;