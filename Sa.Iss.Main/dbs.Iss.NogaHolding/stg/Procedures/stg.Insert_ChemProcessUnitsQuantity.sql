﻿CREATE PROCEDURE [stg].[Insert_ChemProcessUnitsQuantity]
(
	@SubmissionId						INT,
	@ProcessUnitName					VARCHAR(48),
	
	@Quantity_kNm3Day					FLOAT,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [stg].[ChemProcessUnitsQuantity]
		(
			[SubmissionId],
			[ProcessUnitName],
			[Quantity_kNm3Day],
			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@ProcessUnitName,
			@Quantity_kNm3Day,
			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;