﻿CREATE PROCEDURE [stg].[Insert_ChemLosses]
(
	@SubmissionId						INT,
	
	@LossDetail							NVARCHAR(96),
	@Quantity_TonneMonth				FLOAT,

	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [stg].[ChemLosses]
		(
			[SubmissionId],
			[LossDetail],
			[Quantity_TonnesMonth],
			[tsModified]
		)
		VALUES
		(
			@SubmissionId,
			@LossDetail,
			@Quantity_TonneMonth,
			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;