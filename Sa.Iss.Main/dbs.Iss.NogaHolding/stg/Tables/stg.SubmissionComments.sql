﻿CREATE TABLE [stg].[SubmissionComments]
(
	[CommentId]							INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionIdx]						INT					NOT	NULL	CONSTRAINT [FK_SubmissionComments_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[CommentIdx]						INT					NOT	NULL,
	[CommentNotes]						VARCHAR(MAX)		NOT	NULL,	CONSTRAINT [CL_SubmissionComments_CommentNotes]			CHECK([CommentNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_SubmissionComments_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_SubmissionComments_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_SubmissionComments_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_SubmissionComments_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_SubmissionComments_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_SubmissionComments]	PRIMARY KEY NONCLUSTERED([CommentId] ASC),
	CONSTRAINT [UK_SubmissionComments]	UNIQUE CLUSTERED([SubmissionIdx] ASC),
);
GO

CREATE TRIGGER [stg].[t_SubmissionComments_u]
ON	[stg].[SubmissionComments]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[SubmissionComments]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[SubmissionComments].[CommentId]	= [i].[CommentId];

END;