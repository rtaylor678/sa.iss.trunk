﻿CREATE TABLE [stg].[Submissions]
(
	[SubmissionId]						INT					NOT	NULL	IDENTITY(1, 1),

	/*	[BusinessUnitName]	*/
	[CompanyName]						NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL_Submissions_CompanyName]				CHECK([CompanyName] <> ''),
	[CompanyId]							AS CONVERT(INT, [org].[Return_CompanyId]([CompanyName])),
	
	/*	[Location]			*/
	[FacilityName]						NVARCHAR(48)		NOT	NULL	CONSTRAINT [CL_Submissions_FacilitName]				CHECK([FacilityName] <> ''),
	[FacilityId]						AS CONVERT(INT, [org].[Return_FacilityId]([FacilityName])),

	[DataSpan]							CHAR(1)				NOT	NULL,	CONSTRAINT [CR_Submissions_DataSpan]				CHECK([DataSpan] IN ('Y', 'M')),
	[DataYear]							SMALLINT			NOT	NULL,	CONSTRAINT [CR_Submissions_DataYear]				CHECK([DataYear]  >= 2010 AND [DataYear]  <= YEAR(SYSDATETIME())),
	[DataMonth]							TINYINT				NOT	NULL,	CONSTRAINT [CR_Submissions_DataMonth]				CHECK([DataMonth] >=	1 AND [DataMonth] <= 12),

	[PeriodBeg]							DATE					NULL,
	[PeriodEnd]							DATE					NULL,	CONSTRAINT [CR_Submissions_PeriodBegEnd]			CHECK([PeriodBeg] <= [PeriodEnd]),
	[Duration_Days]						SMALLINT				NULL,	CONSTRAINT [CR_Submissions_Duration_Days]			CHECK([Duration_Days]   >=	1 AND [Duration_Days]   <= 366),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Submissions_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Submissions_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Submissions_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Submissions_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Submissions_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Submissions]	PRIMARY KEY NONCLUSTERED([SubmissionId] ASC),
	CONSTRAINT [UK_Submissions] UNIQUE CLUSTERED([CompanyName] ASC, [FacilityName] ASC, [DataSpan] ASC, [DataYear] ASC, [DataMonth] ASC)
);
GO

CREATE TRIGGER [stg].[t_Submissions_u]
ON	[stg].[Submissions]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[Submissions]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[Submissions].[SubmissionId]	= [i].[SubmissionId];

END;