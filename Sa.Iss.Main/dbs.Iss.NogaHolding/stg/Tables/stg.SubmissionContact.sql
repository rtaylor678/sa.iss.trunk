﻿CREATE TABLE [stg].[SubmissionContact]
(
	[ContactId]							INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_SubmissionContact_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[CoordPrimary]						INT					NOT	NULL,	CONSTRAINT [CL_SubmissionContact_CoordPrimary]			CHECK([CoordPrimary] >= 1),

	[CoordName]							NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_SubmissionContact_CoordName]				CHECK([CoordName] <> ''),
	[CoordTitle]						NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_SubmissionContact_CoordTitle]			CHECK([CoordTitle] <> ''),
	[CoordTelephone]					NVARCHAR(96)		NOT	NULL,	CONSTRAINT [CL_SubmissionContact_CoordTelephone]		CHECK([CoordTelephone] <> ''),
	[CoordEMail]						NVARCHAR(254)		NOT	NULL,	CONSTRAINT [CL_SubmissionContact_CoordEMail]			CHECK([CoordEMail] <> ''),

	[CoordAddress1]						NVARCHAR(96)			NULL,	CONSTRAINT [CL_SubmissionContact_CoordAddress1]			CHECK([CoordAddress1] <> ''),
	[CoordAddress2]						NVARCHAR(96)			NULL,	CONSTRAINT [CL_SubmissionContact_CoordAddress2]			CHECK([CoordAddress2] <> ''),
	[CoordCity]							NVARCHAR(96)			NULL,	CONSTRAINT [CL_SubmissionContact_CoordCity]				CHECK([CoordCity] <> ''),
	[CoordProvince]						NVARCHAR(96)			NULL,	CONSTRAINT [CL_SubmissionContact_CoordProvince]			CHECK([CoordProvince] <> ''),
	[CoordCountryTag]					CHAR(3)					NULL,	CONSTRAINT [CL_SubmissionContact_CoordCountryTag]		CHECK([CoordCountryTag] <> ''),
	[CoordCountryId]					AS CONVERT(INT, [dim].[Return_CountryId]([CoordCountryTag])),
	[CoordPostalCode]					NVARCHAR(24)			NULL,	CONSTRAINT [CL_SubmissionContact_CoordPostalCode]		CHECK([CoordPostalCode] <> ''),
	[CoordFax]							NVARCHAR(96)			NULL,	CONSTRAINT [CL_SubmissionContact_CoordFax]				CHECK([CoordFax] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_SubmissionContact_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_SubmissionContact_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_SubmissionContact_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_SubmissionContact_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_SubmissionContact_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_SubmissionContact]	PRIMARY KEY NONCLUSTERED([ContactId] ASC),
	CONSTRAINT [UK_SubmissionContact]	UNIQUE CLUSTERED([SubmissionId] ASC, [CoordPrimary] ASC)
);
GO

CREATE TRIGGER [stg].[t_SubmissionContact_u]
ON	[stg].[SubmissionContact]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[SubmissionContact]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[SubmissionContact].[ContactId]	= [i].[ContactId];

END;