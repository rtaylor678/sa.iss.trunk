﻿CREATE TABLE [stg].[ChemExpenses]
(
	[ExpenseIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemExpenses_Submissions]							REFERENCES [stg].[Submissions]([SubmissionId]),
	[AccountDetail]						VARCHAR(96)			NOT	NULL,	CONSTRAINT [CL_ChemExpenses_AccountDetail]							CHECK([AccountDetail] <> ''),
	[AccountId]							AS CONVERT(INT, [dim].[Return_AccountId]([AccountDetail])),

	[Expense_kUsd]						FLOAT				NOT	NULL,
	[ExpenseNotes]						VARCHAR(MAX)			NULL,	CONSTRAINT [CL_ChemExpenses_ExpenseNotes]							CHECK([ExpenseNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemExpenses_tsModified]								DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemExpenses_tsModifiedHost]							DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemExpenses_tsModifiedUser]							DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemExpenses_tsModifiedApp]							DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemExpenses_tsModifiedGuid]							DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemExpenses]		PRIMARY KEY NONCLUSTERED([ExpenseIdx] ASC),
	CONSTRAINT [UK_ChemExpenses]		UNIQUE CLUSTERED([SubmissionId] ASC, [AccountDetail] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemExpenses_u]
ON	[stg].[ChemExpenses]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemExpenses]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemExpenses].[ExpenseIdx]	= [i].[ExpenseIdx];

END;