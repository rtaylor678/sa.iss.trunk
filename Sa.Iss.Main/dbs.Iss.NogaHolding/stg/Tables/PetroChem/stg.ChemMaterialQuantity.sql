﻿CREATE TABLE [stg].[ChemMaterialQuantity]
(
	[MaterialQuantityIdx]				INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemMaterialQuantity_Submissions]					REFERENCES [stg].[Submissions]([SubmissionId]),
	[MaterialName]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_ChemMaterialQuantity_MaterialName]					CHECK([MaterialName] <> ''),
	[MaterialId]						AS CONVERT(INT, [dim].[Return_MaterialId]([MaterialName])),
	[MaterialType]						CHAR(1)				NOT	NULL,	CONSTRAINT [CR_ChemMaterialQuantity_MaterialType]					CHECK([MaterialType] IN ('R', 'Y', 'L')),

	[Quantity_TonneMonth]				FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemMaterialQuantity_Quantity_TonneMonth]			CHECK([Quantity_TonneMonth] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemMaterialQuantity_tsModified]						DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemMaterialQuantity_tsModifiedHost]					DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemMaterialQuantity_tsModifiedUser]					DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemMaterialQuantity_tsModifiedApp]					DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemMaterialQuantity_tsModifiedGuid]					DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemMaterialQuantity]	PRIMARY KEY NONCLUSTERED([MaterialQuantityIdx] ASC),
	CONSTRAINT [UK_ChemMaterialQuantity]	UNIQUE CLUSTERED([SubmissionId] ASC, [MaterialName] ASC, [MaterialType] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemMaterialQuantity_u]
ON	[stg].[ChemMaterialQuantity]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemMaterialQuantity]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemMaterialQuantity].[MaterialQuantityIdx]	= [i].[MaterialQuantityIdx];

END;