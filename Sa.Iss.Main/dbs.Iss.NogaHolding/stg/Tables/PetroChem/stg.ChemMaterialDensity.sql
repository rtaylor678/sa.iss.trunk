﻿CREATE TABLE [stg].[ChemMaterialDensity]
(
	[MaterialDensityIdx]				INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemMaterialDensity_Submissions]						REFERENCES [stg].[Submissions]([SubmissionId]),
	
	[MaterialName]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_ChemMaterialDensity_MaterialName]					CHECK([MaterialName] <> ''),
	[MaterialId]						AS CONVERT(INT, [dim].[Return_MaterialId]([MaterialName])),

	[Density_knm3Month]					FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemMaterialDensity_Density_knm3Month]				CHECK([Density_knm3Month] >= 0.0),
	[Density_knm3]						FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemMaterialDensity_Density_knm3]					CHECK([Density_knm3] >= 0.0),

	[Quantity_TonneMonth]				AS CONVERT(REAL, [Density_knm3Month] * [Density_knm3] / 6.2899)
										PERSISTED			NOT	NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemMaterialDensity_tsModified]						DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemMaterialDensity_tsModifiedHost]					DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemMaterialDensity_tsModifiedUser]					DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemMaterialDensity_tsModifiedApp]					DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemMaterialDensity_tsModifiedGuid]					DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemMaterialDensity]	PRIMARY KEY NONCLUSTERED([MaterialDensityIdx] ASC),
	CONSTRAINT [UK_ChemMaterialDensity]	UNIQUE CLUSTERED([SubmissionId] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemMaterialDensity_u]
ON	[stg].[ChemMaterialDensity]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemMaterialDensity]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemMaterialDensity].[MaterialDensityIdx]	= [i].[MaterialDensityIdx];

END;