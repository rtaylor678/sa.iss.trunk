﻿CREATE TABLE [stg].[ChemEnergyThermal]
(
	[ThermalIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemEnergyThermal_Submissions]						REFERENCES [stg].[Submissions]([SubmissionId]),
	[ThermalName]						VARCHAR(40)			NOT	NULL,	CONSTRAINT [CL_ChemEnergyThermal_ThermalName]						CHECK([ThermalName] <> ''),
	[EnergyId]							AS CONVERT(INT, [dim].[Return_EnergyId]([ThermalName])),

	[Quantity_GJMonth]					FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemEnergyThermal_Quantity_GJMonth]					CHECK([Quantity_GJMonth] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemEnergyThermal_tsModified]						DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemEnergyThermal_tsModifiedHost]					DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemEnergyThermal_tsModifiedUser]					DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemEnergyThermal_tsModifiedApp]						DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemEnergyThermal_tsModifiedGuid]					DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemEnergyThermal]	PRIMARY KEY NONCLUSTERED([ThermalIdx] ASC),
	CONSTRAINT [UK_ChemEnergyThermal]	UNIQUE CLUSTERED([SubmissionId] ASC, [ThermalName] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemEnergyThermal_u]
ON	[stg].[ChemEnergyThermal]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemEnergyThermal]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemEnergyThermal].[ThermalIdx]	= [i].[ThermalIdx];

END;