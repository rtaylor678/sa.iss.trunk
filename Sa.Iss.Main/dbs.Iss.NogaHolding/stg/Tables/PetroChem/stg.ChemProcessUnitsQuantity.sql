﻿CREATE TABLE [stg].[ChemProcessUnitsQuantity]
(
	[ProcessUnitsQuantityIdx]			INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsQuantity_Submissions]		REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitName]					VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_ChemProcessUnitsQuantity_ProcessUnitName]	CHECK([ProcessUnitName] <> ''),
	[ProcessUnitIdx]					AS [stg].[Return_ChemProcessUnitIdx]([SubmissionId], [ProcessUnitName]),

	[Quantity_kNm3Day]					FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsQuantity_Quantity_kNm3Day]	CHECK([Quantity_kNm3Day] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsQuantity_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsQuantity_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsQuantity_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsQuantity_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsQuantity_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemProcessUnitsQuantity]	PRIMARY KEY NONCLUSTERED([ProcessUnitsQuantityIdx] ASC),
	CONSTRAINT [UK_ChemProcessUnitsQuantity]	UNIQUE CLUSTERED([SubmissionId] ASC, [ProcessUnitName] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemProcessUnitsQuantity_u]
ON	[stg].[ChemProcessUnitsQuantity]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemProcessUnitsQuantity]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemProcessUnitsQuantity].[ProcessUnitsQuantityIdx]	= [i].[ProcessUnitsQuantityIdx];

END;