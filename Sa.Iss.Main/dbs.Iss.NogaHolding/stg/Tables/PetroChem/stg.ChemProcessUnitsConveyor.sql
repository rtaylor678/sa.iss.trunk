﻿CREATE TABLE [stg].[ChemProcessUnitsConveyor]
(
	[ConveyorIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsConveyor_Submissions]				REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsConveyor_ChemProcessUnits]		REFERENCES [stg].[ChemProcessUnits]([ProcessUnitIdx]),

	[ConveyorType]						VARCHAR(12)			NOT	NULL,	CONSTRAINT [CL_ChemProcessUnitsConveyor_ConveyorType]			CHECK([ConveyorType] <> ''),
	[ConveyorTypeId]					AS CONVERT(INT, [dim].[Return_ConveyorId]([ConveyorType])),
	[ParallelLines_Count]				INT					NOT	NULL,	CONSTRAINT [CL_ChemProcessUnitsConveyor_ParallelLines_Count]		CHECK([ParallelLines_Count] >= 0),
	[ConveyorLength_m]					FLOAT				NOT	NULL,	CONSTRAINT [CL_ChemProcessUnitsConveyor_ConveyorLength_m]		CHECK([ConveyorLength_m] >= 0.0),
	[ConveyorBedWidth_m]				FLOAT				NOT	NULL,	CONSTRAINT [CL_ChemProcessUnitsConveyor_ConveyorBedWidth_m]		CHECK([ConveyorBedWidth_m] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsConveyor_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsConveyor_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsConveyor_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsConveyor_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsConveyor_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemProcessUnitsConveyor]	PRIMARY KEY NONCLUSTERED([ConveyorIdx] ASC),
	CONSTRAINT [UK_ChemProcessUnitsConveyor]	UNIQUE CLUSTERED([SubmissionId] ASC, [ProcessUnitId] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemProcessUnitsConveyor_u]
ON	[stg].[ChemProcessUnitsConveyor]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemProcessUnitsConveyor]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemProcessUnitsConveyor].[ConveyorIdx]	= [i].[ConveyorIdx];

END;