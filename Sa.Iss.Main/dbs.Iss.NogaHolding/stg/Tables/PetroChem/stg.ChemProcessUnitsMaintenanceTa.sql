﻿CREATE TABLE [stg].[ChemProcessUnitsMaintenanceTa]
(
	[MaintenanceTaIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsMaintenanceTa_Submissions]				REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsMaintenanceTa_ChemProcessUnits]			REFERENCES [stg].[ChemProcessUnits]([ProcessUnitIdx]),
	
	[RecentTurnaround_Date]				DATE				NOT	NULL,
	[PreviousTurnaround_Date]			DATE				NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsMaintenanceTa_TurnaroundSequence]			CHECK([PreviousTurnaround_Date] <= [RecentTurnaround_Date]),
	[TotalDownTime_Hours]				FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsMaintenanceTa_TotalDownTime_Hours]		CHECK([TotalDownTime_Hours] >= 0.0),
	[TotalCost_kUsd]					FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsMaintenanceTa_TotalCost_kUsd]				CHECK([TotalCost_kUsd] >= 0.0),
	[TotalCompanyLabor_Hours]			FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsMaintenanceTa_TotalCompanyLabor_Hours]	CHECK([TotalCompanyLabor_Hours] >= 0.0),
	[TotalContractorLabor_Hours]		FLOAT				NOT	NULL,	CONSTRAINT [CR_ChemProcessUnitsMaintenanceTa_TotalContractorLabor_Hours]	CHECK([TotalContractorLabor_Hours] >= 0.0),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsMaintenanceTa_tsModified]					DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsMaintenanceTa_tsModifiedHost]				DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsMaintenanceTa_tsModifiedUser]				DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsMaintenanceTa_tsModifiedApp]				DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsMaintenanceTa_tsModifiedGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemProcessUnitsMaintenanceTa]	PRIMARY KEY NONCLUSTERED([MaintenanceTaIdx] ASC),
	CONSTRAINT [UK_ChemProcessUnitsMaintenanceTa]	UNIQUE CLUSTERED([SubmissionId] ASC, [ProcessUnitId] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemProcessUnitsMaintenanceTa_u]
ON	[stg].[ChemProcessUnitsMaintenanceTa]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemProcessUnitsMaintenanceTa]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemProcessUnitsMaintenanceTa].[MaintenanceTaIdx]	= [i].[MaintenanceTaIdx];

END;