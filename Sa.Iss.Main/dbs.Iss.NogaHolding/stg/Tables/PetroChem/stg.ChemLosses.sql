﻿CREATE TABLE [stg].[ChemLosses]
(
	[LossIdx]							INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemLosses_Submissions]				REFERENCES [stg].[Submissions]([SubmissionId]),
	[LossDetail]						VARCHAR(96)			NOT	NULL,	CONSTRAINT [CL_ChemLosses_LossDetail]				CHECK([LossDetail] <> ''),

	[Quantity_TonnesMonth]				FLOAT				NOT	NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemLosses_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemLosses_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemLosses_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemLosses_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemLosses_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemLosses]			PRIMARY KEY NONCLUSTERED([LossIdx] ASC),
	CONSTRAINT [UK_ChemLosses]			UNIQUE CLUSTERED([SubmissionId] ASC, [LossDetail] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemLosses_u]
ON	[stg].[ChemLosses]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemLosses]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemLosses].[LossIdx]	= [i].[LossIdx];

END;