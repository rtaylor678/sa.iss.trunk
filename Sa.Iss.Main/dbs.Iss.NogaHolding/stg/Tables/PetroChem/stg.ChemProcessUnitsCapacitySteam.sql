﻿CREATE TABLE [stg].[ChemProcessUnitsCapacitySteam]
(
	[CapacitySteamIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsCapacitySteam_Submissions]				REFERENCES [stg].[Submissions]([SubmissionId]),
	[ProcessUnitId]						INT					NOT	NULL	CONSTRAINT [FK_ChemProcessUnitsCapacitySteam_ChemProcessUnits]			REFERENCES [stg].[ChemProcessUnits]([ProcessUnitIdx]),

	[Capacity]							FLOAT					NULL,	CONSTRAINT [CL_ChemProcessUnitsCapacitySteam_Capacity]					CHECK([Capacity] >= 0.0),
	[CapacityUtilization_Pcnt]			FLOAT					NULL,	CONSTRAINT [CL_ChemProcessUnitsCapacitySteam_CapacityUtilization_Pcnt]	CHECK([CapacityUtilization_Pcnt] >= 0.0),
	[Capacity_Units]					VARCHAR(16)				NULL,	CONSTRAINT [CL_ChemProcessUnitsCapacitySteam_Capacity_Units]				CHECK([Capacity_Units] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsCapacitySteam_tsModified]					DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsCapacitySteam_tsModifiedHost]				DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsCapacitySteam_tsModifiedUser]				DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsCapacitySteam_tsModifiedApp]				DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ChemProcessUnitsCapacitySteam_tsModifiedGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ChemProcessUnitsCapacitySteam]	PRIMARY KEY NONCLUSTERED([CapacitySteamIdx] ASC),
	CONSTRAINT [UK_ChemProcessUnitsCapacitySteam]	UNIQUE CLUSTERED([SubmissionId] ASC, [ProcessUnitId] ASC)
);
GO

CREATE TRIGGER [stg].[t_ChemProcessUnitsCapacitySteam_u]
ON	[stg].[ChemProcessUnitsCapacitySteam]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[ChemProcessUnitsCapacitySteam]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[ChemProcessUnitsCapacitySteam].[CapacitySteamIdx]	= [i].[CapacitySteamIdx];

END;