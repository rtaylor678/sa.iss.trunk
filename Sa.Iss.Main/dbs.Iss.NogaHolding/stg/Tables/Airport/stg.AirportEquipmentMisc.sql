﻿CREATE TABLE [stg].[AirportEquipmentMisc]
(
	[EquipmentIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportEquipmentMisc_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[EquipmentType]						VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_AirportEquipmentMisc_EquipmentType]			CHECK([EquipmentType] <> ''),

	[EquipmentItems_Count]				SMALLINT				NULL,	CONSTRAINT [CL_AirportEquipmentMisc_EquipmentItems_Count]	CHECK([EquipmentItems_Count] >= 1),
	[EquipmentCapacity]					FLOAT					NULL,	CONSTRAINT [CL_AirportEquipmentMisc_EquipmentCapacity]		CHECK([EquipmentCapacity] >= 0.0),
	[EquipmentService]					VARCHAR(48)				NULL,	CONSTRAINT [CL_AirportEquipmentMisc_EquipmentService]		CHECK([EquipmentService] <> ''),

	[EquipmentNotes]					VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportEquipmentMisc_EquipmentNotes]			CHECK([EquipmentNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportEquipmentMisc_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportEquipmentMisc_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportEquipmentMisc_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportEquipmentMisc_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportEquipmentMisc_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportMisc]			PRIMARY KEY NONCLUSTERED([EquipmentIdx] ASC),
	CONSTRAINT [UK_AirportMisc]			UNIQUE CLUSTERED([SubmissionId] ASC, [EquipmentType] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportEquipmentMisc_u]
ON	[stg].[AirportEquipmentMisc]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportEquipmentMisc]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportEquipmentMisc].[EquipmentIdx]	= [i].[EquipmentIdx];

END;