﻿CREATE TABLE [stg].[AirportOpsMassBalanceBasis]
(
	[MassBalanceBasisIdx]				INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportOpsMassBalanceBasis_Submissions]						REFERENCES [stg].[Submissions]([SubmissionId]),

	[MassBalanceBasis_Current]			CHAR(1)				NOT	NULL,	CONSTRAINT [CR_AirportOpsMassBalanceBasis_MassBalanceBasis_Current]			CHECK([MassBalanceBasis_Current] IN ('Y', 'N')),
	[MassBalanceBasis_Frequency]		CHAR(1)				NOT	NULL,	CONSTRAINT [CR_AirportOpsMassBalanceBasis_MassBalanceBasis_Frequency]		CHECK([MassBalanceBasis_Frequency] IN ('S', 'D', 'M', 'Y', 'O')),
	[MassBalanceBasis_Base]				CHAR(1)				NOT	NULL,	CONSTRAINT [CR_AirportOpsMassBalanceBasis_MassBalanceBasis_Base]			CHECK([MassBalanceBasis_Base] IN ('V', 'M', 'O')),
	[MassBalanceBasis_Wet]				CHAR(1)				NOT	NULL,	CONSTRAINT [CR_AirportOpsMassBalanceBasis_MassBalanceBasis_Wet]				CHECK([MassBalanceBasis_Wet] IN ('D', 'W', 'O')),

	[MassBalanceBasis_CurrentNotes]		VARCHAR(MAX)		NOT	NULL,	CONSTRAINT [CL_AirportOpsMassBalanceBasis_MassBalanceBasis_CurrentNotes]	CHECK([MassBalanceBasis_CurrentNotes] <> ''),
	[MassBalanceBasis_FrequencyNotes]	VARCHAR(MAX)		NOT	NULL,	CONSTRAINT [CL_AirportOpsMassBalanceBasis_MassBalanceBasis_FrequencyNotes]	CHECK([MassBalanceBasis_FrequencyNotes] <> ''),
	[MassBalanceBasis_BaseNotes]		VARCHAR(MAX)		NOT	NULL,	CONSTRAINT [CL_AirportOpsMassBalanceBasis_MassBalanceBasis_BaseNotes]		CHECK([MassBalanceBasis_BaseNotes] <> ''),
	[MassBalanceBasis_WetNotes]			VARCHAR(MAX)		NOT	NULL,	CONSTRAINT [CL_AirportOpsMassBalanceBasis_MassBalanceBasis_WetNotes]		CHECK([MassBalanceBasis_WetNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportOpsMassBalanceBasis_tsModified]						DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsMassBalanceBasis_tsModifiedHost]					DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsMassBalanceBasis_tsModifiedUser]					DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsMassBalanceBasis_tsModifiedApp]					DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportOpsMassBalanceBasis_tsModifiedGuid]					DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportOpsMassBalanceBasis]	PRIMARY KEY NONCLUSTERED([MassBalanceBasisIdx] ASC),
	CONSTRAINT [UK_AirportOpsMassBalanceBasis]	UNIQUE CLUSTERED([SubmissionId] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportOpsMassBalanceBasis_u]
ON	[stg].[AirportOpsMassBalanceBasis]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportOpsMassBalanceBasis]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportOpsMassBalanceBasis].[MassBalanceBasisIdx]	= [i].[MassBalanceBasisIdx];

END;