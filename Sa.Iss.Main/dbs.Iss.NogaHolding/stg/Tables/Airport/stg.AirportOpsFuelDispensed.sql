﻿CREATE TABLE [stg].[AirportOpsFuelDispensed]
(
	[FuelDispensedIdx]					INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportOpsFuelDispensed_Submissions]						REFERENCES [stg].[Submissions]([SubmissionId]),
	[FuelDispensedName]					VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportOpsFuelDispensed_FuelDispensedName]				CHECK([FuelDispensedName] <> ''),

	[FuelDispensedVolume_m3]			FLOAT				NOT	NULL,	CONSTRAINT [CR_AirportOpsFuelDispensed_FuelDispensedVolume_m3]			CHECK([FuelDispensedVolume_m3] >= 0.0),
	[FuelDispensedVolumeMax_m3]			FLOAT				NOT	NULL,	CONSTRAINT [CR_AirportOpsFuelDispensed_FuelDispensedVolumeMax_m3]		CHECK([FuelDispensedVolumeMax_m3] >= 0.0),
	[FuelDispensedNotes]				VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportOpsFuelDispensed_FuelDispensedNotes]				CHECK([FuelDispensedNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportOpsFuelDispensed_tsModified]						DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsFuelDispensed_tsModifiedHost]					DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsFuelDispensed_tsModifiedUser]					DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportOpsFuelDispensed_tsModifiedApp]					DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportOpsFuelDispensed_tsModifiedGuid]					DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportOpsFuelDispensed]	PRIMARY KEY NONCLUSTERED([FuelDispensedIdx] ASC),
	CONSTRAINT [UK_AirportOpsFuelDispensed]	UNIQUE CLUSTERED([SubmissionId] ASC, [FuelDispensedName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportOpsFuelDispensed_u]
ON	[stg].[AirportOpsFuelDispensed]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportOpsFuelDispensed]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportOpsFuelDispensed].[FuelDispensedIdx]	= [i].[FuelDispensedIdx];

END;