﻿CREATE TABLE [stg].[AirportAprons]
(
	[ApronIdx]							INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportAprons_Submissions]					REFERENCES [stg].[Submissions]([SubmissionId]),
	[ApronInformation]					VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportAprons_ApronInformation]				CHECK([ApronInformation] <> ''),
	[ApronName]							VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_AirportAprons_ApronName]						CHECK([ApronName] <> ''),

	[ApronDeliverySystem]				VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_AirportAprons_ApronDeliverySystem]			CHECK([ApronDeliverySystem] <> ''),
	[DeliverySystemId]					AS CONVERT(INT, [dim].[Return_DeliverySystemId]([ApronDeliverySystem])),
	[ApronFuelingServices]				VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_AirportAprons_ApronFuelingServices]			CHECK([ApronFuelingServices] <> ''),
	[FuelingServicesId]					AS CONVERT(INT, [dim].[Return_FuelingServiceId]([ApronFuelingServices])),
	[ApronAircraftTypes_Commercial]		VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_AirportAprons_ApronAircraftTypes_Commercial]	CHECK([ApronAircraftTypes_Commercial] <> ''),
	[AircraftTypeId_Commercial]			AS CONVERT(INT, [dim].[Return_AircraftTypeId]([ApronAircraftTypes_Commercial])),
	[ApronAircraftTypes_Military]		VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_AirportAprons_ApronAircraftTypes_Military]	CHECK([ApronAircraftTypes_Military] <> ''),
	[AircraftTypeId_Military]			AS CONVERT(INT, [dim].[Return_AircraftTypeId]([ApronAircraftTypes_Military])),

	[ApronJetFuelClasses]				VARCHAR(12)			NOT	NULL,	CONSTRAINT [CL_AirportAprons_ApronJetFuelClasses]			CHECK([ApronJetFuelClasses] <> ''),
	[ApronAviationGasoline]				VARCHAR(12)			NOT	NULL,	CONSTRAINT [CL_AirportAprons_ApronAviationGasoline]			CHECK([ApronAviationGasoline] <> ''),
	[ApronDefuelingServices]			VARCHAR(3)			NOT	NULL,	CONSTRAINT [CL_AirportAprons_ApronDefuelingServices]		CHECK([ApronDefuelingServices] <> ''),
	[ApronNotes]						VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportAprons_ApronNotes]					CHECK([ApronNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportAprons_tsModified]					DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportAprons_tsModifiedHost]				DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportAprons_tsModifiedUser]				DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportAprons_tsModifiedApp]					DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportAprons_tsModifiedGuid]				DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportAprons]		PRIMARY KEY NONCLUSTERED([ApronIdx] ASC),
	CONSTRAINT [UK_AirportAprons]		UNIQUE CLUSTERED([SubmissionId] ASC, [ApronInformation] ASC),
	CONSTRAINT [UX_AirportAprons]		UNIQUE NONCLUSTERED([SubmissionId] ASC, [ApronName] ASC),
);
GO

CREATE TRIGGER [stg].[t_AirportAprons_u]
ON	[stg].[AirportAprons]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportAprons]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportAprons].[ApronIdx]	= [i].[ApronIdx];

END;