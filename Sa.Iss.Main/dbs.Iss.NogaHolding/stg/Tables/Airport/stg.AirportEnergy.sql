﻿CREATE TABLE [stg].[AirportEnergy]
(
	[EnergyIdx]							INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportEnergy_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[EnergyName]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportEnergy_EnergyName]			CHECK([EnergyName] <> ''),
	[EnergyId]							AS CONVERT(INT, [dim].[Return_EnergyId]([EnergyName])),

	[EnergyValue]						FLOAT				NOT	NULL,
	[EnergyUnits]						VARCHAR(12)			NOT	NULL,
	[EnergyCost]						FLOAT				NOT	NULL,
	[EnergyNotes]						VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportEnergy_EnergyNotes]			CHECK([EnergyNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportEnergy_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportEnergy_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportEnergy_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportEnergy_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportEnergy_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportEnergy]		PRIMARY KEY NONCLUSTERED([EnergyIdx] ASC),
	CONSTRAINT [UK_AirportEnergy]		UNIQUE CLUSTERED([SubmissionId] ASC, [EnergyName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportEnergy_u]
ON	[stg].[AirportEnergy]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportEnergy]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportEnergy].[EnergyIdx]	= [i].[EnergyIdx];

END;