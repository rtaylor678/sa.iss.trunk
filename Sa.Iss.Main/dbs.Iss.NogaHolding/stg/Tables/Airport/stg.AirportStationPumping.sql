﻿CREATE TABLE [stg].[AirportStationsPumping]
(
	[PumpingIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportStationsPumping_Submissions]				REFERENCES [stg].[Submissions]([SubmissionId]),
	[PumpingInformation]				VARCHAR(12)			NOT	NULL,	CONSTRAINT [CL_AirportStationsPumping_PumpingInformation]		CHECK([PumpingInformation] <> ''),
	[PumpingName]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportStationsPumping_PumpingName]				CHECK([PumpingName] <> ''),

	[PumpingHydrant_Count]				SMALLINT			NOT	NULl,	CONSTRAINT [CL_AirportStationsPumping_PumpingHydrant_Count]		CHECK([PumpingHydrant_Count] >= 0),
	[PumpingTransfer_Count]				SMALLINT			NOT	NULl,	CONSTRAINT [CL_AirportStationsPumping_PumpingTransfer_Count]	CHECK([PumpingTransfer_Count] >= 0),
	[PumpingNotes]						VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportStationsPumpding_PumpingNotes]			CHECK([PumpingNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportStationsPumping_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportStationsPumping_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportStationsPumping_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportStationsPumping_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportStationsPumping_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportStationsPumping]	PRIMARY KEY NONCLUSTERED([PumpingIdx] ASC),
	CONSTRAINT [UK_AirportStationsPumping]	UNIQUE CLUSTERED([SubmissionId] ASC, [PumpingInformation] ASC),
	CONSTRAINT [UX_AirportStationsPumping]	UNIQUE NONCLUSTERED([SubmissionId] ASC, [PumpingName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportStationsPumping_u]
ON	[stg].[AirportStationsPumping]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportStationsPumping]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportStationsPumping].[PumpingIdx]	= [i].[PumpingIdx];

END;