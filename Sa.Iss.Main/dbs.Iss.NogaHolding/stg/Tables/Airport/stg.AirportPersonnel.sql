﻿CREATE TABLE [stg].[AirportPersonnel]
(
	[PersonnelIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportPersonnel_Submissions]				REFERENCES [stg].[Submissions]([SubmissionId]),
	[AccountName]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportPersonnel_AccountName]				CHECK([AccountName] <> ''),
	[AccountId]							AS CONVERT(INT, [dim].[Return_AccountId]([AccountName])),

	[CompanyLabor_Count]				FLOAT					NULL,	CONSTRAINT [CR_AirportPersonnel_CompanyLabor_Count]			CHECK([CompanyLabor_Count] >= 0.0),
	[CompanyLabor_kUsd]					FLOAT					NULL,	CONSTRAINT [CR_AirportPersonnel_CompanyLabor_kUsd]			CHECK([CompanyLabor_kUsd] >= 0.0),
	[CompanyLabor_Hours]				FLOAT					NULL,	CONSTRAINT [CR_AirportPersonnel_CompanyLabor_Hours]			CHECK([CompanyLabor_Hours] >= 0.0),
	[CompanyLabor_kUsdHr]				AS CASE WHEN ([CompanyLabor_Hours] <> 0.0) THEN [CompanyLabor_kUsd] / [CompanyLabor_Hours] END
										PERSISTED					,
	[ContractExpendatures_kUsd]			FLOAT					NULL,	CONSTRAINT [CR_AirportPersonnel_ContractExpendatures_kUsd]	CHECK([ContractExpendatures_kUsd] >= 0.0),

	[ContractLabor_Count]				FLOAT					NULL,	CONSTRAINT [CR_AirportPersonnel_ContractLabor_Count]			CHECK([ContractLabor_Count] >= 0.0),	
	[ContractLabor_kUsd]				FLOAT					NULL,	CONSTRAINT [CR_AirportPersonnel_ContractLabor_kUsd]			CHECK([ContractLabor_kUsd] >= 0.0),
	[ContractLabor_Hours]				FLOAT					NULL,	CONSTRAINT [CR_AirportPersonnel_ContractLabor_Hours]			CHECK([ContractLabor_Hours] >= 0.0),
	[ContractLabor_kUsdHr]				AS CASE WHEN ([ContractLabor_Hours] <> 0.0) THEN [ContractLabor_kUsd] / [ContractLabor_Hours] END
										PERSISTED					,

	[TotalLabor_kUsd]					AS COALESCE([CompanyLabor_kUsd], 0.0) + COALESCE([ContractLabor_kUsd], 0.0)
										PERSISTED			NOT	NULL,
	[TotalWork_Hours]					AS COALESCE([CompanyLabor_Hours], 0.0) + COALESCE(CASE WHEN ([ContractLabor_Hours] <> 0.0) THEN [ContractLabor_kUsd] / [ContractLabor_Hours] END, 0.0)
										PERSISTED			NOT	NULL,

	[PersonnelNotes]					VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportPersonnel_PersonnelNotes]			CHECK([PersonnelNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportPersonnel_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportPersonnel_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportPersonnel_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportPersonnel_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportPersonnel_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportPersonnel]	PRIMARY KEY NONCLUSTERED([PersonnelIdx] ASC),
	CONSTRAINT [UK_AirportPersonnel]	UNIQUE CLUSTERED([SubmissionId] ASC, [AccountName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportPersonnel_u]
ON	[stg].[AirportPersonnel]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportPersonnel]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportPersonnel].[PersonnelIdx]	= [i].[PersonnelIdx];

END;