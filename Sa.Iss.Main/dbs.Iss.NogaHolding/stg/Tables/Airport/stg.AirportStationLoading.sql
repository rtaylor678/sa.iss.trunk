﻿CREATE TABLE [stg].[AirportStationsLoading]
(
	[LoadingIdx]						INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportStationsLoading_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[LoadingInformation]				VARCHAR(12)			NOT	NULL,	CONSTRAINT [CL_AirportStationsLoading_LoadingInformation]	CHECK([LoadingInformation] <> ''),
	[LoadingName]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportStationsLoading_LoadingName]			CHECK([LoadingName] <> ''),

	[LoadingCapacity_m3Min]				FLOAT				NOT	NULL,	CONSTRAINT [CL_AirportStationsLoading_LoadingCapacity_m3]	CHECK([LoadingCapacity_m3Min] >= 0.0),
	[LoadingNotes]						VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportStationsLoading_LoadingNotes]			CHECK([LoadingNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportStationsLoading_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportStationsLoading_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportStationsLoading_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportStationsLoading_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportStationsLoading_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportStationsLoading]	PRIMARY KEY NONCLUSTERED([LoadingIdx] ASC),
	CONSTRAINT [UK_AirportStationsLoading]	UNIQUE CLUSTERED([SubmissionId] ASC, [LoadingInformation] ASC),
	CONSTRAINT [UX_AirportStationsLoading]	UNIQUE NONCLUSTERED([SubmissionId] ASC, [LoadingName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportStationsLoading_u]
ON	[stg].[AirportStationsLoading]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportStationsLoading]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportStationsLoading].[LoadingIdx]	= [i].[LoadingIdx];

END;