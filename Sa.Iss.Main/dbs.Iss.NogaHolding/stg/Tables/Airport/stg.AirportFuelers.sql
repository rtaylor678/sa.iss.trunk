﻿CREATE TABLE [stg].[AirportFuelers]
(
	[FuelerIdx]							INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportFuelers_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[FuelerInformation]					VARCHAR(12)			NOT	NULL,	CONSTRAINT [CL_AirportFuelers_FuelerInformation]	CHECK([FuelerInformation] <> ''),
	[FuelerName]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportFuelers_FuelerName]			CHECK([FuelerName] <> ''),

	[FuelerCapacity_m3]					FLOAT				NOT	NULL,	CONSTRAINT [CL_AirportFuelers_FuelerCapacity_m3]	CHECK([FuelerCapacity_m3] >= 0.0),
	[FuelerNotes]						VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportFuelers_FuelerNotes]			CHECK([FuelerNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportFuelers_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportFuelers_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportFuelers_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportFuelers_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportFuelers_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportFuelers]		PRIMARY KEY NONCLUSTERED([FuelerIdx] ASC),
	CONSTRAINT [UK_AirportFuelers]		UNIQUE CLUSTERED([SubmissionId] ASC, [FuelerInformation] ASC),
	CONSTRAINT [UX_AirportFuelers]		UNIQUE NONCLUSTERED([SubmissionId] ASC, [FuelerName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportFuelers_u]
ON	[stg].[AirportFuelers]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportFuelers]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportFuelers].[FuelerIdx]	= [i].[FuelerIdx];

END;