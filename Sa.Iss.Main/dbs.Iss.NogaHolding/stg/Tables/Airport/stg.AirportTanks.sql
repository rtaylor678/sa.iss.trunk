﻿CREATE TABLE [stg].[AirportTanks]
(
	[TankIdx]							INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_AirportTanks_Submissions]			REFERENCES [stg].[Submissions]([SubmissionId]),
	[TankInformation]					VARCHAR(12)			NOT	NULL,	CONSTRAINT [CL_AirportTanks_TankInformation]		CHECK([TankInformation] <> ''),
	[TankName]							VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportTanks_TankName]				CHECK([TankName] <> ''),

	[TankProduct]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_AirportTanks_TankProduct]			CHECK([TankProduct] <> ''),
	[TankType]							VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_AirportTanks_TankType]				CHECK([TankType] <> ''),
	[TankTypeId]						AS CONVERT(INT, [dim].[Return_TankTypeId]([TankType])),

	[TankVolume_m3]						FLOAT				NOT	NULL,	CONSTRAINT [CR_AirportTanks_TankVolume_m3]			CHECK([TankVolume_m3] >= 0.0),
	[TankBuilt_Year]					SMALLINT			NOT	NULL,	CONSTRAINT [CR_AirportTanks_TankBuilt_Year]			CHECK([TankBuilt_Year] >= 1935),

	[TankTurnaround_Year]				SMALLINT				NULL,	CONSTRAINT [CR_AirportTanks_TankTurnaround_Year]	CHECK([TankTurnaround_Year] >= 1935),
	[TankCost_kUsd]						FLOAT					NULL,	CONSTRAINT [CR_AirportTanks_TankCost_kUsd]			CHECK([TankCost_kUsd] >= 0.0),
	[TankCycleTime_Years]				FLOAT					NULL,	CONSTRAINT [CR_AirportTanks_TankCycleTime_Years]	CHECK([TankCycleTime_Years] >= 0.0),
	[TankNotes]							VARCHAR(MAX)			NULL,	CONSTRAINT [CL_AirportTanks_TankNotes]				CHECK([TankNotes] <> ''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AirportTanks_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportTanks_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportTanks_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AirportTanks_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AirportTanks_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AirportTanks]		PRIMARY KEY NONCLUSTERED([TankIdx] ASC),
	CONSTRAINT [UK_AirportTanks]		UNIQUE CLUSTERED([SubmissionId] ASC, [TankInformation] ASC),
	CONSTRAINT [UX_AirportTanks]		UNIQUE NONCLUSTERED([SubmissionId] ASC, [TankName] ASC)
);
GO

CREATE TRIGGER [stg].[t_AirportTanks_u]
ON	[stg].[AirportTanks]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[stg].[AirportTanks]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[stg].[AirportTanks].[TankIdx]	= [i].[TankIdx];

END;