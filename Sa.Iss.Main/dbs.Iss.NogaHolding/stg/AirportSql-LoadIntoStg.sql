﻿--to insert into [stg] tables only.
-- ~8 hours of work for first time.
--modified 3 tables (AirportEnergy, and 2 more)
---see associated input file:  AirportInputDataMapping.2015-10-21xlsx.xlsx
/*

INSERT INTO [org].[Companies]([CompanyTag],[CompanyName],[CompanyDetail])
VALUES('Bafco','BAFCO','BAFCO');
INSERT INTO [org].[Facilities] ([FacilityTag],[FacilityName],[FacilityDetail])
VALUES ( 'BafcoAirport','BAFCO Airport','BAFCO Airport');
INSERT INTO [org].[groups] (grouptag,groupname,groupdetail)
values ('Noga'	,'National Oil and Gas Authority'	,'National Oil and Gas Authority (All Members)');

--facilityid: (2)  --select FacilityId from [org].[Facilities] where FacilityName = 'BAFCO Airport';
--CompanyId: (1) --select CompanyId  from [org].[Companies] where CompanyName = 'BAFCO';
--groupid (1): --select GroupId from org.groups where GroupName='National Oil and Gas Authority';

insert into [org].[JoinCompaniesFacilities] (CompanyId,FacilityId) 
Values (
(select CompanyId  from [org].[Companies] where CompanyName = 'BAFCO'),
(select FacilityId from [org].[Facilities] where FacilityName = 'BAFCO Airport')
);
insert into [stg].[Submissions]
([CompanyName],[FacilityName],[DataSpan],[DataYear],
[DataMonth],[PeriodBeg],[PeriodEnd],[Duration_Days],[tsModifiedHost],[tsModifiedApp])
values
('BAFCO','BAFCO Airport','M',2015,1,'3/1/2015','3/31/2015',31,'Sa.Iss','Sa.Iss');
--submission id 2003  select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc
insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail],[tsModifiedHost],[tsModifiedApp])
values
('airCapExpTot','airCapitalExpenditures','airport CapitalExpendituresTotal','Sa.Iss','Sa.Iss');
insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail]) 
values
('airCENMDiscr','CapExpNonMaintDiscr','airport CapitalExpenseNonMaintDiscretionary');
insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail]) 
values
('airCENMNoDis','CapExpNonMaintNonDiscr','airport CapitalExpenseNonMaintNonDiscretionary'); 
insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail]) 
values
('airCENMOvrhd','CapExpNonMaintOverhead','airport CapitalExpenseNonMaintOverheadCapital'); 
insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail]) 
values
('airCEMaint','CapExpMaintenance','airport CapitalExpenseMaintenance'); 
insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail]) 
values
('airMaintCost','MaintCosts','airport MaintenanceCosts');

--============DATA STARTS HERE===================================
insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd],[ExpenseNotes]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport CapitalExpenditures',0,'Total'
);
insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd],[ExpenseNotes]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport CapitalExpenseNonMaintDiscretionary',0,'Discretionary'
);
insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd],[ExpenseNotes]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport CapitalExpenseNonMaintNonDiscretionary',0,'NonDiscretionary'
);
insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd],[ExpenseNotes]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport CapitalExpenseNonMaintOverheadCapital',0,'OverheadCapital'
);
insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd],[ExpenseNotes]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport CapitalExpenseMaintenance',0,'CapitalExpenseMaintenance'
);

--BELOW THIS, START THE LOOKUP INSERTS (ONE TIME) NEXT TO THE DATA INSERTS (EACH SUBMISSION)


*/
--==============================
/*
select * from dim.Account_LookUp
insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail]) 
values
('airMCCECCLbr','airMaintenanceCostsCapExpCompanyAndContractLabor','airport MaintenanceCostsCapExpCompanyAndContractLabor');

insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd],[ExpenseNotes]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport MaintenanceCostsCapExpCompanyAndContractLabor',0,'MaintenanceCostsCapExpCompanyAndContractLabor'
);

insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail]) 
values
('airMCCECCMNP','airMaintenanceCostsCapExpCompanyAndContMatsNoPrs','airport MaintenanceCostsCapExpCompanyAndContractMatsNonPersonnel');

insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd],[ExpenseNotes]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport MaintenanceCostsCapExpCompanyAndContractMatsNonPersonnel',0,'MaintenanceCostsCapExpCompanyAndContractMatsNonPersonnel'
);

insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail]) 
values
('airMCECCLbr','airMaintenanceCostsExpCompanyAndContractLabor','airport MaintenanceCostsExpCompanyAndContractLabor');
insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd],[ExpenseNotes]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport MaintenanceCostsExpCompanyAndContractLabor',1990,'MaintenanceCostsExpCompanyAndContractLabor'
);
insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail]) 
values
('airMECCLbr','airMaintenanceCostsExpCompanyAndContMatsNoPrs','airport MaintenanceCostsExpCompanyAndContractMatsNonPersonnel');
insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd],[ExpenseNotes]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport MaintenanceCostsExpCompanyAndContractMatsNonPersonnel',7177,'MaintenanceCostsExpCompanyAndContractMatsNonPersonnel'
);

insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail]) 
values
('airIMCSCExp','airIntegrityMaintenanceContractServicesCapExp','airport IntegrityMaintenanceContractServices(OutOfServiceInspectionAndRepair)CapExp');

insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd],[ExpenseNotes]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport IntegrityMaintenanceContractServices(OutOfServiceInspectionAndRepair)CapExp',0,'IntegrityMaintenanceContractServices(OutOfServiceInspectionAndRepair)CapExp'
);

insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail]) 
values
('airIMCSExp','airIntegrityMaintenanceContractServicesExp','airport IntegrityMaintenanceContractServices(OutOfServiceInspectionAndRepair)Exp');

insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd],[ExpenseNotes]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport IntegrityMaintenanceContractServices(OutOfServiceInspectionAndRepair)Exp',0,'IntegrityMaintenanceContractServices(OutOfServiceInspectionAndRepair)Exp'
);
--=====OPns Data
insert into [stg].[AirportFuelDispensed]
([SubmissionId],[FuelDispensedProduct],[FuelDispensedVolume_m3],[FuelDispensedNotes])
Values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'Jet Fuel, million US gallons',11.116779,'Gallons, not m3');
insert into [stg].[AirportFuelDispensed]
([SubmissionId],[FuelDispensedProduct],[FuelDispensedVolume_m3],[FuelDispensedNotes])
Values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'Aviation Gasoline, US gallons',70,'Gallons, not m3');

insert into [dim].[AircraftType_LookUp]
([AircraftTypeTag],[AircraftTypeName],[AircraftTypeDetail])
values('NonSpecUwng','Non-Specified Underwing','Non-Specified Underwing');

insert into [dim].[AircraftType_LookUp]
([AircraftTypeTag],[AircraftTypeName],[AircraftTypeDetail])
values('NonSpecOwng','Non-Specified Overwing','Non-Specified Overwing');

Insert into [stg].[AirportOpsAircraftServiced]
([SubmissionId],[ServicedName],[Serviced_Count],[ServicedMax_Count],[ServicedNotes])
Values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'NonSpecUwng',3479,0,'Non-specified Underwing');

Insert into [stg].[AirportOpsAircraftServiced]
([SubmissionId],[ServicedName],[Serviced_Count],[ServicedMax_Count],[ServicedNotes])
Values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'NonSpecOwng',2,0,'Non-specified Overwing');

should consider data check/cleanse

insert into [dim].[Reliability_LookUp]
([ReliabilityTag],[ReliabilityName],[ReliabilityDetail])
values
('airUDCCONum','airUnplannedDowntimeImpactRcptDelCoCauseOpnNum','airUnplannedDowntimeImpactingReceiptsOrDeliveriesCompanyCausesOperationNumber');

insert into [dim].[Reliability_LookUp]
([ReliabilityTag],[ReliabilityName],[ReliabilityDetail])
values
('airUDCCOHrs','airUnplannedDowntimeImpactRcptDelCoCauseOpnHrs',
'airUnplannedDowntimeImpactingReceiptsOrDeliveriesCompanyCausesOperationHours');

insert into [dim].[Reliability_LookUp]
([ReliabilityTag],[ReliabilityName],[ReliabilityDetail])
values
('airUDCCMNum','airUnplannedDowntimeImpactRcptDelCoCauseMechNum',
'airUnplannedDowntimeImpactingReceiptsOrDeliveriesCompanyCausesMechanicalNumber');

insert into [dim].[Reliability_LookUp]
([ReliabilityTag],[ReliabilityName],[ReliabilityDetail])
values
('airUDCCMHrs','airUnplannedDowntimeImpactRcptDelCoCauseMechHrs',
'airUnplannedDowntimeImpactingReceiptsOrDeliveriesCompanyCausesMechanicalHours');

insert into [dim].[Reliability_LookUp]
([ReliabilityTag],[ReliabilityName],[ReliabilityDetail])
values
('airUDNCCNum','airUnplannedDowntimeImpactRcptDelNonCoCauseNum',
'airUnplannedDowntimeImpactingReceiptsOrDeliveriesNonCompanyCausesNumber');

insert into [dim].[Reliability_LookUp]
([ReliabilityTag],[ReliabilityName],[ReliabilityDetail])
values
('airUDNCCHrs','airUnplannedDowntimeImpactRcptDelNonCoCauseHrs',
'airUnplannedDowntimeImpactingReceiptsOrDeliveriesNonCompanyCausesHours');

--now the data for Unplanned Downtime
insert into [stg].[AirportOpsReliabilityUnplanned]
([SubmissionId],[UnplannedName],[UnplannedEvent_Count],[UnplannedDuration_Hours],[UnplannedNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airUDCCONum', 
0,
null,
null);

insert into [stg].[AirportOpsReliabilityUnplanned]
([SubmissionId],[UnplannedName],[UnplannedEvent_Count],[UnplannedDuration_Hours],[UnplannedNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airUDCCOHrs', 
null,
0,
null);

insert into [stg].[AirportOpsReliabilityUnplanned]
([SubmissionId],[UnplannedName],[UnplannedEvent_Count],[UnplannedDuration_Hours],[UnplannedNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airUDCCMNum', 
0,
null,
null);

insert into [stg].[AirportOpsReliabilityUnplanned]
([SubmissionId],[UnplannedName],[UnplannedEvent_Count],[UnplannedDuration_Hours],[UnplannedNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airUDCCMHrs', 
null,
0,
null);

insert into [stg].[AirportOpsReliabilityUnplanned]
([SubmissionId],[UnplannedName],[UnplannedEvent_Count],[UnplannedDuration_Hours],[UnplannedNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airUDNCCNum', 
0,
null,
null);

insert into [stg].[AirportOpsReliabilityUnplanned]
([SubmissionId],[UnplannedName],[UnplannedEvent_Count],[UnplannedDuration_Hours],[UnplannedNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airUDNCCHrs', 
null,
0,
null);

insert into [dim].[Reliability_LookUp]
([ReliabilityTag],[ReliabilityName],[ReliabilityDetail])
values
('airUDSoLNum','airUnplannedDowntimeImpactRcptDelSpillOrLeakNum',
'airUnplannedDowntimeImpactingReceiptsOrDeliveriesSpillsOrLeaksNumber');

insert into [dim].[Reliability_LookUp]
([ReliabilityTag],[ReliabilityName],[ReliabilityDetail])
values
('airUDSoLVol','airUnplannedDowntimeImpactRcptDelSpillOrLeakVol',
'airUnplannedDowntimeImpactingReceiptsOrDeliveriesSpillsOrLeaksVolume');

insert into [stg].[AirportOpsReliabilityHydrocarbon]
([SubmissionId],[HydrocarbonName],[HydrocarbonEvent_Count],[HydrocarbonVolume_m3],[HydrocarbonNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airUDSoLNum',
0,null,null);

insert into [stg].[AirportOpsReliabilityHydrocarbon]
([SubmissionId],[HydrocarbonName],[HydrocarbonEvent_Count],[HydrocarbonVolume_m3],[HydrocarbonNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airUDSoLVol',
null,0,null);
--I added these fields to table [stg].[AirportOpsMassBalance] : [MassBalanceMass_Gallons],[MassBalanceMass_Percent]
insert into  [stg].[AirportOpsMassBalance]
([SubmissionId],[MassBalanceName],[MassBalanceMass_Tonne],[MassBalanceMass_Gallons],[MassBalanceMass_Percent],[MassBalanceNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'Total Loss or Gain',
null,13630,null,null);

insert into  [stg].[AirportOpsMassBalance]
([SubmissionId],[MassBalanceName],[MassBalanceMass_Tonne],[MassBalanceMass_Gallons],[MassBalanceMass_Percent],[MassBalanceNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'Loss or Gain as %',
null,null,0.1226,null);

--personnel start hsere
insert into [dim].[Account_LookUp] ([AccountTag],[AccountName],[AccountDetail])
Values ('airPersCOMO','airPersonnelCostsOpnsAndMaintCostsOpnsStaff',
'airPersonnelCostsOperationsAndMaintenanceCostsOperationsStaff');

insert into [dim].[Account_LookUp] ([AccountTag],[AccountName],[AccountDetail])
Values ('airPersCOMM','airPersonnelCostsOpnsAndMaintCostsMaintStaff',
'airPersonnelCostsOperationsAndMaintenanceCostsMaintenanceStaff');

insert into [dim].[Account_LookUp] ([AccountTag],[AccountName],[AccountDetail])
Values ('airPersCGAT','airPersonnelCostsGeneralAndAdminSvcsTechStaff',
'airPersonnelCostsGeneralAndAdminSvcsTechnicalStaff');


insert into [dim].[Account_LookUp] ([AccountTag],[AccountName],[AccountDetail])
Values ('airPersCGAA','airPersonnelCostsGeneralAndAdminSvcsAdminStaff',
'airPersonnelCostsGeneralAndAdminSvcsAdminStaff');
--pers data

insert into [stg].[AirportPersonnel]
([SubmissionId],[AccountName],
[CompanyLabor_Count],[CompanyLabor_Hours],[CompanyLabor_kUsd],
[ContractLabor_Count],[ContractLabor_kUsd],[ContractLabor_Hours],
[PersonnelNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airPersCOMO',
68,11234,95988,
0,0,0,
null);

insert into [stg].[AirportPersonnel]
([SubmissionId],[AccountName],
[CompanyLabor_Count],[CompanyLabor_Hours],[CompanyLabor_kUsd],
[ContractLabor_Count],[ContractLabor_kUsd],[ContractLabor_Hours],
[PersonnelNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airPersCOMM',
22,3635,31055,
5,1101,635,
null);

insert into [stg].[AirportPersonnel]
([SubmissionId],[AccountName],
[CompanyLabor_Count],[CompanyLabor_Hours],[CompanyLabor_kUsd],
[ContractLabor_Count],[ContractLabor_kUsd],[ContractLabor_Hours],
[PersonnelNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airPersCGAT',
1,165,38993,
0,0,0,
null);

insert into [stg].[AirportPersonnel]
([SubmissionId],[AccountName],
[CompanyLabor_Count],[CompanyLabor_Hours],[CompanyLabor_kUsd],
[ContractLabor_Count],[ContractLabor_kUsd],[ContractLabor_Hours],
[PersonnelNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airPersCGAA',
11,1817,35287,
0,0,0,
null);

--energy

insert into [dim].[Energy_LookUp]
([EnergyTag],[EnergyName],[EnergyDetail])
values 
('airElectric','airElectric','airElectric');
insert into [dim].[Energy_LookUp]
([EnergyTag],[EnergyName],[EnergyDetail])
values 
('airDiesel','airDiesel','airDiesel');

insert into [stg].[AirportEnergy]
([SubmissionId],[EnergyName],[EnergyValue],[EnergyUnits],[EnergyCost]) --,[EnergyNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airElectric',100630,'kWh',1610);

insert into [stg].[AirportEnergy]
([SubmissionId],[EnergyName],[EnergyValue],[EnergyUnits],[EnergyCost]) --,[EnergyNotes])
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airDiesel',6834,'L',2242);


insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail])
values
('airMONVEOMV','airMiscOtherNonVolExpOpnsMaintVehicleLeasePurch',
'airport MiscOtherExpensesNonVolRelatedExpOpnsAndMaintVehicleRelatedLeasePurchaseEtc');
insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail])
values
('airMONVEOMM','airMiscOtherNonVolExpOpnsMaintMiscOtherLubesMats',
'airport MiscOtherExpensesNonVolRelatedExpOpnsAndMaintMiscOtherLubesMaterialsConsumables');
insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail])
values
('airMOECNPEx','airMiscOtherExpensesCompanyNonPersonalExpense',
'airport MiscellaneousOtherExpensesCompanyNonPersonalExpense');
insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail])
values
('airMOEONVRE','airMiscellaneousOtherExpOtherNonVolRelatedExp',
'airport MiscellaneousOtherExpensesOtherNonVolumeRelatedExpFeesLeasePropTaxInsEnviro');
insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail])
values
('airMOEOVREx','airMiscOtherExpOtherVolRelatedExpOtherVolRelated',
'airport MiscellaneousOtherExpensesOtherVolumeRelatedExpensesOtherVolumeRelatedExpenses');
insert into [dim].[Account_LookUp]
([AccountTag],[AccountName],[AccountDetail])
values
('airMOEExclu','airMiscellaneousOtherExpensesExclusions',
'airport MiscellaneousOtherExpensesExclusions');
--data
insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport MiscOtherExpensesNonVolRelatedExpOpnsAndMaintVehicleRelatedLeasePurchaseEtc',1164);
insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport MiscOtherExpensesNonVolRelatedExpOpnsAndMaintMiscOtherLubesMaterialsConsumables'  ,0);
insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport MiscellaneousOtherExpensesCompanyNonPersonalExpense'  ,38937);
insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport MiscellaneousOtherExpensesOtherNonVolumeRelatedExpFeesLeasePropTaxInsEnviro'  ,10759);
insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport MiscellaneousOtherExpensesOtherVolumeRelatedExpensesOtherVolumeRelatedExpenses'  ,2895);
insert into [stg].[AirportExpenses] 
([SubmissionId],[AccountDetail],[Expense_kUsd]) 
values
(
(select top 1 SubmissionId from [stg].[Submissions] where CompanyName='BAFCO' order by SubmissionId desc),
'airport MiscellaneousOtherExpensesExclusions' ,0);
*/
