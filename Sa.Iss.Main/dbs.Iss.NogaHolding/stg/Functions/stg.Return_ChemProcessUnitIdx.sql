﻿CREATE FUNCTION [stg].[Return_ChemProcessUnitIdx]
(
	@SubmissionId						INT,
	@ProcessUnitName					VARCHAR(48)
)
RETURNS INT
WITH SCHEMABINDING, RETURNS NULL ON NULL INPUT
AS
BEGIN

	DECLARE @Idx	INT;

	SET @ProcessUnitName = RTRIM(LTRIM(@ProcessUnitName));

	SELECT TOP 1
		@Idx = [t].[ProcessUnitIdx]
	FROM
		[stg].[ChemProcessUnits]	[t]
	WHERE	[t].[SubmissionId]		= @SubmissionId
		AND	[t].[ProcessUnitName]	= @ProcessUnitName;

	RETURN	@Idx;

END;