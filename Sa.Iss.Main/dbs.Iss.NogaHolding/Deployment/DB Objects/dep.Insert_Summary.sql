﻿CREATE PROCEDURE [dep].[Insert_Summary]
(
	@CompanyName						NVARCHAR(48),
	@FacilityName						NVARCHAR(48),

	@EntityDetail						NVARCHAR(96),
	@HistDate							VARCHAR(24),
	@Value								FLOAT,
	@tsModified							DATETIMEOFFSET
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [dep].[Summary]
		(
			[CompanyName],
			[FacilityName],
			[EntityDetail],
			[HistDate],
			[Value],
			[tsModified]
		)
		VALUES
		(
			@CompanyName,
			@FacilityName,
			@EntityDetail,
			@HistDate,
			@Value,
			@tsModified
		);

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT				= XACT_STATE();
		DECLARE @DataId		VARCHAR(108)	= @EntityDetail + @HistDate;
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @DataId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
GO