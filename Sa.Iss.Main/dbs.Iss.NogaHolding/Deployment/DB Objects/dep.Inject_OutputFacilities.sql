﻿CREATE PROCEDURE [dep].[Inject_OutputFacilities]
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [fact].[Submissions]
		(
			[GroupId],
			[DataSpan],
			[DataYear],
			[DataMonth],
			[PeriodBeg],
			[PeriodEnd]
		)
		SELECT DISTINCT
			[g].[GroupId],
				[DataSpan]		= 'M',
				[DataYear]		= YEAR([s].[HistDate]),
				[DataMonth]		= MONTH([s].[HistDate]),
				[PeriodBeg]		= CONVERT(DATE, [s].[HistDate]),
				[PeriodEnd]		= EOMONTH([s].[HistDate])
		FROM
			[dep].[Summary]				[s]
		INNER JOIN
			[org].[Groups]				[g]
				ON	[g].[GroupName]		= [s].[FacilityName]
		LEFT OUTER JOIN
			[fact].[Submissions]		[f]
				ON	[f].[GroupId]		= [g].GroupId
				AND	[f].[DataSpan]		= 'M'
				AND	[f].[PeriodBeg]		=  CONVERT(DATE, [s].[HistDate])
		WHERE
			[f].[SubmissionId]			IS NULL;

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
			[f].[SubmissionId],
				[MethodologyId]	= [ante].[Return_MethodologyId]('2015'),
				[CurrencyId]	= [dim].[Return_CurrencyId]('USD'),
			[e].[EntityId],
			[a].[AttributeId],
			[s].[Value]
		FROM
			[dep].[Summary]		[s]
		INNER JOIN(VALUES
				('Ammonia Capacity, tonne/day', 'OSAmmoniaRefrig', 'Cap'),
				('Ammonia Production, tonne/day', 'OSAmmoniaRefrig', 'Prod'),
				('Methanol Capacity, tonne/day', 'OSMethanolDist', 'Cap'),
				('Methanol Production, tonne/day', 'OSMethanolDist', 'Prod'),
				('Urea Capacity, tonne/day', 'OSUrea', 'Cap'),
				('Urea Production, tonne/day', 'OSUrea', 'Prod'),

				('Process EDC', 'ProcUnitsOnSite', 'kEdc'),
				('EDC', 'ProcUnits', 'kEdc'),
				('Utilized EDC', 'ProcUnits', 'ukEdc'),
				('Utilization Rate, percent', 'ProcUnits', 'uCap_Pcnt'),

				('High-Value Chemical Yield, percent of hydrocarbon in feed', 'Hydrocarbons', 'HVCYield_Pcnt'),
				('Flare Loss, percent of feed', 'LossFlare', 'Loss_PcntFeed'),
				('Feed and Fuel Index', 'FeedFuelIdx', 'FeedFuelIdx'),

				('Cash Operating Expense, US Cents/UEDC', 'OpEx', 'RptExpCentsUedc'),
				('Non-Energy Cash Operating Expense, $/EDC', 'OpExNe', 'RptExpEdcAnnualized'),
				('Non-Energy Efficiency Index (NEI)', 'Nei', 'Nei'),

				('Specific Energy, GJ/tonne High-Value Chemicals', 'OpExEnergy', 'EnergyConsGJHvc'),
				('Energy Intensity Index (EII)', 'Eii', 'Eii'),

				('Maintenance Index, $/EDC', 'OpExMaintRoutTA', 'MaintIdx'),
				('Non-Turnaround Maintenance Index, $/EDC', 'MaintRout', 'MaintIdx'),
				('Turnaround Maintenance Index, $/EDC', 'MaintTa', 'MaintIdx'),

				('Maintenance Efficiency Index (MEI)', 'Mei', 'Mei'),
				('Operational Availability, percent of time', 'AvailOper', 'AvailOper'),
				('Mechanical Availability, percent of time', 'AvailMech', 'AvailMech'),

				('Personnel Index, hours/100 EDC', 'PersIdx', 'PersIdx'),
				('Personnel Cost Index, $/EDC', 'OpExLaborMaterials', 'RptExpEdcAnnualized'),
				('Personnel Efficiency Index (PEI)', 'Pei', 'Pei')

			) [t]([EntityDetail], [EntityTag], [AttributeTag])
				ON	[t].[EntityDetail]	= [s].[EntityDetail]
		INNER JOIN
			[org].[Groups]				[g]
				ON	[g].[GroupName]		= [s].[FacilityName]
		INNER JOIN
			[rpt].[Entities]			[e]
				ON	[e].[EntityTag]		= [t].[EntityTag]
		INNER JOIN
			[rpt].[Attributes]			[a]
				ON	[a].[AttributeTag]	= [t].AttributeTag
		INNER JOIN
			[fact].[Submissions]		[f]
				ON	[f].[GroupId]		= [g].[GroupId]
				AND	[f].[PeriodBeg]		= [s].[HistDate];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT				= XACT_STATE();
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, 'Etl';
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
GO