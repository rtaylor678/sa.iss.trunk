﻿DECLARE @Currency	TABLE
(
	[CurrencyTag]						VARCHAR(48)			NOT	NULL,	UNIQUE CLUSTERED([CurrencyTag] ASC),
																		CHECK([CurrencyTag] <> ''),
	[CurrencyName]						NVARCHAR(96)		NOT	NULL,	UNIQUE NONCLUSTERED([CurrencyName] ASC),
																		CHECK([CurrencyName] <> N''),
	[CurrencyDetail]					NVARCHAR(96)		NOT	NULL,	UNIQUE NONCLUSTERED([CurrencyDetail] ASC),
																		CHECK([CurrencyDetail] <> N'')
);

INSERT INTO @Currency
(
	[CurrencyTag],
	[CurrencyName],
	[CurrencyDetail]
)
SELECT
	[t].[CurrencyTag],
	[t].[CurrencyName],
	[t].[CurrencyDetail]
	FROM (VALUES
	('USD', 'US Dollar', 'United States Dollar')
	) [t]([CurrencyTag], [CurrencyName], [CurrencyDetail]);

INSERT INTO [dim].[Currency_LookUp]
(
	[CurrencyTag],
	[CurrencyName],
	[CurrencyDetail]
)
SELECT
	[e].[CurrencyTag],
	[e].[CurrencyName],
	[e].[CurrencyDetail]
FROM
	@Currency				[e]
LEFT OUTER JOIN
	[dim].[Currency_LookUp]	[x]
		ON	[x].[CurrencyTag]	= [e].[CurrencyTag]
WHERE	[x].[CurrencyId] IS NULL;