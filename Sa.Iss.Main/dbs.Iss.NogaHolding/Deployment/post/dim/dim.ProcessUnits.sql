﻿DECLARE @ProcessUnits	TABLE
(
	[ProcessUnitParent]					VARCHAR(48)			NOT	NULL,	CHECK([ProcessUnitParent] <> ''),

	[Operator]							CHAR(1)				NOT	NULL	CHECK([Operator] IN ('+', '-', '*', '/', '~')),
	[SortKey]							INT					NOT	NULL,
	[ProcessUnitTag]					VARCHAR(48)			NOT	NULL,	UNIQUE CLUSTERED([ProcessUnitTag] ASC),
																		CHECK([ProcessUnitTag] <> ''),
	[ProcessUnitName]					NVARCHAR(96)		NOT	NULL,	UNIQUE NONCLUSTERED([ProcessUnitName] ASC),
																		CHECK([ProcessUnitName] <> N''),
	[ProcessUnitDetail]					NVARCHAR(96)		NOT	NULL,	UNIQUE NONCLUSTERED([ProcessUnitDetail] ASC),
																		CHECK([ProcessUnitDetail] <> N'')
);

INSERT INTO @ProcessUnits
(
	[ProcessUnitParent],
	[Operator],
	[SortKey],
	[ProcessUnitTag],
	[ProcessUnitName],
	[ProcessUnitDetail]
)
SELECT
	[t].[ProcessUnitParent],
	[t].[Operator],
	[t].[SortKey],
	[t].[ProcessUnitTag],
	[t].[ProcessUnitName],
	[t].[ProcessUnitDetail]
	FROM (VALUES
--	Process ProcessUnits
--		EDC
--		Factors and Standards
--		EII
--		Reliability
--		Turnaround

	('ProcProcessUnits', '+', 0, 'ProcProcessUnits', 'Process ProcessUnits', 'Process ProcessUnits'),
		('ProcProcessUnits', '+', 0, 'ProcProcessUnitsOnSite', 'Process ProcessUnits (On Site)', 'Process ProcessUnits (On Site)'),
			('ProcProcessUnitsOnSite', '+', 0, 'OSPreDeSulfur', 'Predesulfurization', 'Predesulfurization'),
			('ProcProcessUnitsOnSite', '+', 0, 'OSAmmoniaSyngas', 'Ammonia Syngas Preparation', 'Ammonia Syngas Preparation'),
			('ProcProcessUnitsOnSite', '+', 0, 'OSAmmoniaRefrig', 'Ammonia Synthesis and Refrigeration', 'Ammonia Synthesis and Refrigeration'),
			('ProcProcessUnitsOnSite', '+', 0, 'OSMethanolSyngas', 'Methanol Syngas Preparation', 'Methanol Syngas Preparation'),
			('ProcProcessUnitsOnSite', '+', 0, 'OSMethanolDist', 'Methanol Synthesis and Distillation', 'Methanol Synthesis and Distillation'),
			('ProcProcessUnitsOnSite', '+', 0, 'OSUrea', 'Urea', 'Urea'),
			('ProcProcessUnitsOnSite', '+', 0, 'OSPsaH2Pur', 'PSA Hydrogen Purification', 'PSA Hydrogen Purification'),
			('ProcProcessUnitsOnSite', '+', 0, 'OSFlueGasCo2Recov', 'Flue Gas CO₂ Recovery', 'Flue Gas CO₂ Recovery'),
			('ProcProcessUnitsOnSite', '+', 0, 'OSDeSal', 'Desalination', 'Desalination'),

		('ProcProcessUnits', '+', 0, 'ProcProcessUnitsUOSL', 'Utility, Offsite, & Logistics', 'Utility, Offsite, & Logistics Facilities'),

			('ProcProcessUnitsUOSL', '+', 0, 'UOSLFiredSteamBoilers', 'Fired Steam Boilers', 'Fired Steam Boilers'),
			('ProcProcessUnitsUOSL', '+', 0, 'UOSLGeneratorFTD', 'Generator with Fired Turbine Driver', 'Generator with Fired Turbine Driver'),

			('ProcProcessUnitsUOSL', '+', 0, 'UOSLCogeneration', 'Cogeneration', 'Cogeneration'),
			('ProcProcessUnitsUOSL', '+', 0, 'UOSLOthSteamElec', 'Other Steam and Electrical', 'Other Steam and Electrical'),
			('ProcProcessUnitsUOSL', '+', 0, 'UOSLTankage', 'Tankage', 'Tankage'),

			('ProcProcessUnitsUOSL', '+', 0, 'ProcProcessUnitsUOSLOther', 'Other Utilities, Offsite, & Logistics', 'Other Utilities, Offsite, & Logistics Facilities'),
				('ProcProcessUnitsOSUOther', '+', 0, 'UOSLOtherUtil', 'Other Utilities', 'Other Utilities'),
				('ProcProcessUnitsOSUOther', '+', 0, 'UOSLUreaWarehouse', 'Urea Warehouse', 'Urea Warehouse'),
				('ProcProcessUnitsOSUOther', '+', 0, 'UOSLUreaPackage', 'Urea Packaging', 'Urea Packaging'),
				('ProcProcessUnitsOSUOther', '+', 0, 'UOSLUreaConveyor', 'Urea Conveyor', 'Urea Conveyor'),
				('ProcProcessUnitsOSUOther', '+', 0, 'UOSLMarine', 'Marine Facilities', 'Marine Facilities'),
				('ProcProcessUnitsOSUOther', '+', 0, 'UOSLEnviro', 'Environmental', 'Environmental'),
				('ProcProcessUnitsOSUOther', '+', 0, 'UOSLOtherOff', 'Other Offsite', 'Other Offsite '),

				('ProcProcessUnitsOSUOther', '+', 0, 'UOSLOtherLog', 'Other Logistics', 'Other Logistics'),

			('ProcProcessUnitsUOSL', '+', 0, 'ProcProcessUnitsUOSLOffSite', 'Offsite Facilities', 'Offsite Facilities'),

			('ProcProcessUnitsUOSL', '+', 0, 'ProcProcessUnitsUOSLUtilities', 'Utility Facilities', 'Utility Facilities'),

			('ProcProcessUnitsUOSL', '+', 0, 'ProcProcessUnitsUOSLLogistis', 'Logistical Facilities', 'Logistical Facilities')

	) [t]([ProcessUnitParent], [Operator], [SortKey], [ProcessUnitTag], [ProcessUnitName], [ProcessUnitDetail]);

INSERT INTO [dim].[ProcessUnit_LookUp]
(
	[ProcessUnitTag],
	[ProcessUnitName],
	[ProcessUnitDetail]
)
SELECT
	[e].[ProcessUnitTag],
	[e].[ProcessUnitName],
	[e].[ProcessUnitDetail]
FROM
	@ProcessUnits				[e]
LEFT OUTER JOIN
	[dim].[ProcessUnit_LookUp]	[x]
		ON	[x].[ProcessUnitTag]	= [e].[ProcessUnitTag]
WHERE	[x].[ProcessUnitId] IS NULL;