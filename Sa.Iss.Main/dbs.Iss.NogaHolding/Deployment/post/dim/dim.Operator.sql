﻿INSERT INTO [dim].[Operator_LookUp]([Operator])
SELECT t.[Operator]
FROM (
	VALUES
		('*'),
		('+'),
		('-'),
		('/'),
		('~')
	) t([Operator])
LEFT OUTER JOIN
	[dim].[Operator_LookUp]	[o]
		ON	[o].[Operator]	= [t].[Operator]
WHERE
	[o].[Operator]	IS NULL;