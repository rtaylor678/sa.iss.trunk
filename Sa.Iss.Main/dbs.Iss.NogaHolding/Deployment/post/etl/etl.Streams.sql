﻿DECLARE @EtlStreams	TABLE
(
	[XlsStreamDetail]					NVARCHAR(96)		NOT	NULL,	UNIQUE NONCLUSTERED([XlsStreamDetail] ASC),
																		CHECK([XlsStreamDetail] <> N''),
	[StreamTag]							VARCHAR(48)			NOT	NULL,	CHECK([StreamTag] <> N'')
);

INSERT INTO @EtlStreams
(
	[XlsStreamDetail],
	[StreamTag]
)
SELECT
	[t].[XlsStreamDetail],
	[t].[StreamTag]
FROM (VALUES
	(N'Total Raw Material Inputs, tonnes/day', N'FeedRaw'),
	
	(N'Total Complex Physical Hydrocarbon Loss¹', N'LossTotal'),
	(N'Estimated Flare Losses Included in Above²', N'LossFlare')

	) [t]([XlsStreamDetail], [StreamTag]);

INSERT INTO [etl].[Streams]
(
	[XlsStreamDetail],
	[StreamTag]
)
SELECT
	[t].[XlsStreamDetail],
	[t].[StreamTag]
FROM
	@EtlStreams		[t]
LEFT OUTER JOIN
	[etl].[Streams]	[e]
		ON	[e].[XlsStreamDetail]	= [t].[XlsStreamDetail]
WHERE
	[e].[StreamIdx]	IS NULL;