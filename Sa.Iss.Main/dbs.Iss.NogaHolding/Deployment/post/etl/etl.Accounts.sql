﻿DECLARE @EtlAccounts	TABLE
(
	[XlsAccountDetail]					NVARCHAR(96)		NOT	NULL,	UNIQUE NONCLUSTERED([XlsAccountDetail] ASC),
																		CHECK([XlsAccountDetail] <> N''),
	[AccountTag]						VARCHAR(48)			NOT	NULL,	CHECK([AccountTag] <> N'')
);

INSERT INTO @EtlAccounts
(
	[XlsAccountDetail],
	[AccountTag]
)
SELECT
	[t].[XlsAccountDetail],
	[t].[AccountTag]
FROM (VALUES
	--	OpEx
	(N'Purchased Natural Gas for Fuel', N'EnergyPurchFuelGas'),
	(N'Other Purchased Fuels', N'EnergyPurchOther'),

	(N'Purchased Electricity', N'EnergyPurchElec'),
	(N'Credit for Electricity Sold', N'EnergySoldElec'),
	(N'Sales of Electricity', N'EnergySoldElec'),

	(N'Purchased Steam', N'EnergyPurchSteam'),
	(N'Credit for Steam Sold', N'EnergySoldSteam'),

	(N'Chemicals and Catalysts', N'OpExChemCat'),

	(N'Company Maintenance Salaries, Wages and Benefits', N'MaintRoutCompanyLabor'),
	(N'Maintenance Materials', N'MaintRoutMaterial'),
	(N'Maintenance Equipment Rental', N'MaintRoutEquipment'),

	(N'Company Non-Maintenance Salaries,  Wages & Benefits', N'GeneralCompanyLabor'),
	(N'Non-Maintenance Contract Services', N'GeneralContractLabor'),
	(N'Allocated G&A Personnel Cost', N'AllocGAPers'),

	(N'Other Non-Turnaround Operating Expense', N'OpExOther'),
	(N'Other Allocated G&A Cost', N'AllocGAOther'),

	(N'Total Cash Operating Expense as Reported', N'OpExRpt'),

	(N'Annualized Turnaround Expense', N'MaintTa'),

	(N'Total Cash Operating Expense', N'OpEx'),

	(N'NEOPEX', N'OpExNe'),
	(N'Personnel Cost', N'OpExLaborMaterials'),

	--	Energy

	(N'Fuel Gas', N'EnergyPurchFuelGas'),
	(N'Sales of Steam', N'EnergySoldSteam'),
	(N'Net Energy Consumption', N'OpExEnergy'),


	--	Personnel

	(N'Company Employees', N'GeneralMaintCompanyLabor'),
	(N'Contract Personnel', N'GeneralMaintContractLabor'),
	(N'Allocated G&A', N'AllocGAPers'),
	(N'Work Hours Expended', N'GeneralMaint'),
	(N'Annualized Turnaround Work Hours', N'MaintTaLabor'),
	(N'Turnaround-Adjusted Work Hours', N'OpExLaborMaterials'),

	--	Maintenance

	(N'Company Maintenance Salaries, Wages, and Benefits', N'MaintRoutCompanyLabor'),
	(N'Contract Maintenance Services', N'MaintRoutContractLabor'),
	(N'Maintenance Material', N'MaintRoutMaterial'),
	(N'Maintenance Equipment', N'MaintRoutEquipment'),
	(N'Maintenance Costs', N'MaintRout'),
	(N'Annualized Turnaround Cost', N'MaintTA'),
	(N'Turnaround-Adjusted Maintenance Cost', N'OpExMaintRoutTA'),

	--	Complex Level Input

	(N'Purchased Natural Gas for Fuel 5', N'EnergyPurchFuelGas'),
	(N'Other Purchased Fuels 5', N'EnergyPurchOther'),
	(N'Sales of Steam1', N'EnergySoldSteam')

	) [t]([XlsAccountDetail], [AccountTag]);

INSERT INTO [etl].[Accounts]
(
	[XlsAccountDetail],
	[AccountTag]
)
SELECT
	[t].[XlsAccountDetail],
	[t].[AccountTag]
FROM
	@EtlAccounts		[t]
LEFT OUTER JOIN
	[etl].[Accounts]	[e]
		ON	[e].[XlsAccountDetail]	= [t].[XlsAccountDetail]
WHERE
	[e].[AccountIdx]	IS NULL;