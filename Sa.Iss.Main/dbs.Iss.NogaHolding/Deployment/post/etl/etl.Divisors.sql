﻿DECLARE @EtlDivisors	TABLE
(
	[XlsDivisorDetail]					NVARCHAR(96)		NOT	NULL,	UNIQUE NONCLUSTERED([XlsDivisorDetail] ASC),
																		CHECK([XlsDivisorDetail] <> N''),
	[DivisorTag]						VARCHAR(48)			NOT	NULL,	CHECK([DivisorTag] <> N'')
);

INSERT INTO @EtlDivisors
(
	[XlsDivisorDetail],
	[DivisorTag]
)
SELECT
	[t].[XlsDivisorDetail],
	[t].[DivisorTag]
FROM (VALUES
	(N'Total Production', N'DivProd'),
		(N'Ammonia', N'Ammonia'),
		(N'Bulk Urea', N'DivUreaBulk'),
		(N'Packaged Urea', N'DivUreaPackaged'),
		(N'Methanol', N'Methanol')
	) [t]([XlsDivisorDetail], [DivisorTag]);

INSERT INTO [etl].[Divisors]
(
	[XlsDivisorDetail],
	[DivisorTag]
)
SELECT
	[t].[XlsDivisorDetail],
	[t].[DivisorTag]
FROM
	@EtlDivisors		[t]
LEFT OUTER JOIN
	[etl].[Divisors]	[e]
		ON	[e].[XlsDivisorDetail]	= [t].[XlsDivisorDetail]
WHERE
	[e].[DivisorIdx]	IS NULL;


