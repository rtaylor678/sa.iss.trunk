﻿DECLARE @EtlComposition	TABLE
(
	[XlsComponentDetail]				NVARCHAR(96)		NOT	NULL,	UNIQUE NONCLUSTERED([XlsComponentDetail] ASC),
																		CHECK([XlsComponentDetail] <> N''),
	[ComponentTag]						VARCHAR(48)			NOT	NULL,	CHECK([ComponentTag] <> N'')
);

INSERT INTO @EtlComposition
(
	[XlsComponentDetail],
	[ComponentTag]
)
SELECT
	[t].[XlsComponentDetail],
	[t].[ComponentTag]
FROM (VALUES
	(N'Hydrogen', 'H2'),
	(N'Methane', 'CH4'),
	(N'Ethane', 'C2H6'),
	(N'Propane', 'C3H8'),
	(N'Butane and C5+', 'C4H10+C5s'),
	(N'Carbon Monoxide', 'CO'),
	(N'Carbon Dioxide', 'CO2'),
	(N'Hydrogen Sulfide', 'H2S'),
	(N'Nitrogen', 'N2'),
	(N'Argon and Other', 'Ar+Other'),
	(N'Average Molecular Weight and Average LHV', 'CompTotal')

	) [t]([XlsComponentDetail], [ComponentTag]);

INSERT INTO [etl].[Composition]
(
	[XlsComponentDetail],
	[ComponentTag]
)
SELECT
	[t].[XlsComponentDetail],
	[t].[ComponentTag]
FROM
	@EtlComposition		[t]
LEFT OUTER JOIN
	[etl].[Composition]	[e]
		ON	[e].[XlsComponentDetail]	= [t].[XlsComponentDetail]
WHERE
	[e].[CompositionIdx]	IS NULL;