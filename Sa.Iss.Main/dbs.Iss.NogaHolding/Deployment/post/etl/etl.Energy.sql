﻿DECLARE @EtlEnergy		TABLE
(
	[EnergyType]		VARCHAR(3)			NOT	NULL	CHECK([EnergyType] <> ''),
	[TransType]			CHAR(3)				NOT	NULL	CHECK([TransType] <> ''),
	[AccountTag]		VARCHAR(48)			NOT	NULL	CHECK([AccountTag] <> ''),
	PRIMARY KEY CLUSTERED ([EnergyType] ASC, [TransType] ASC)
);

INSERT INTO @EtlEnergy
(
	[EnergyType],
	[TransType],
	[AccountTag]
)
SELECT
	[t].[EnergyType],
	[t].[TransType],
	[t].[AccountTag]
FROM (VALUES
	('FG',	'PRO', 'EnergyProdFuelGas'),
	('FG',	'PUR', 'EnergyPurchFuelGas'),
	('NG',	'PUR', 'EnergyPurchFuelGas'),
	('GE',	'DST', 'EnergySoldElec'),
	('GE',	'SOL', 'EnergySoldElec'),
	('PT',	'PUR', 'EnergyPurchElec'),
	('PT',	'SOL', 'EnergySoldElec'),
	('PT',	'TXO', 'EnergySoldElec'),
	('STM',	'TXO', 'EnergySoldSteam')
	) [t]([EnergyType], [TransType], [AccountTag]);

INSERT INTO [etl].[Energy]
(
	[EnergyType],
	[TransType],
	[AccountTag]
)
SELECT
	[t].[EnergyType],
	[t].[TransType],
	[t].[AccountTag]
FROM
	@EtlEnergy		[t]
LEFT OUTER JOIN
	[etl].[Energy]	[e]
		ON	[e].[EnergyType]	= [t].[EnergyType]
		AND	[e].[TransType]		= [t].[TransType]
WHERE
	[e].[EnergyIdx]	IS NULL;