﻿DECLARE @EtlProcessUnits	TABLE
(
	[XlsProcessUnitDetail]				NVARCHAR(96)		NOT	NULL,	UNIQUE NONCLUSTERED([XlsProcessUnitDetail] ASC),
																		CHECK([XlsProcessUnitDetail] <> N''),
	[ProcessUnitTag]					VARCHAR(48)			NOT	NULL,	CHECK([ProcessUnitTag] <> N'')
);

INSERT INTO @EtlProcessUnits
(
	[XlsProcessUnitDetail],
	[ProcessUnitTag]
)
SELECT
	[t].[XlsProcessUnitDetail],
	[t].[ProcessUnitTag]
FROM (VALUES
	(N'Subtotal - Process Units', N'ProcUnitsOnSite'),
	(N'Total Process Units', N'ProcUnitsOnSite'),
		(N'Ammonia Syngas Preparation', N'OSAmmoniaSyngas'),
		(N'Ammonia Synthesis and Refrigeration', N'OSAmmoniaRefrig'),
		(N'Ammonia Synthesis', N'OSAmmoniaRefrig'),

		(N'Methanol Syngas Preparation', N'OSMethanolSyngas'),
		(N'Methanol Synthesis and Distillation', N'OSMethanolDist'),
		(N'Methanol Synthesis', N'OSMethanolDist'),

		(N'Urea', N'OSUrea'),

		(N'PSA Hydrogen Purification', N'OSPsaH2Pur'),
		(N'Flue Gas CO₂ Recovery', N'OSFlueGasCo2Recov'),
		(N'Desalination', N'OSDeSal'),

		(N'Predesulfurization', N'OSPreDeSulfur'),

		(N'Fired Steam Boilers', N'UOSLFiredSteamBoilers'),
		(N'Generator with Fired Turbine Driver', N'UOSLGeneratorFTD'),
		(N'Tankage', N'UOSLTankage'),

		(N'Other Steam and Electrical', N'UOSLOthSteamElec'),

	(N'Other Utilities, Offsite, & Logistics Facilities', N'ProcUnitsUOSL'),
		(N'All Other Utilities', N'UOSLOtherUtil'),
		(N'Urea Warehouse', N'UOSLUreaWarehouse'),
		(N'Urea Packaging', N'UOSLUreaPackage'),
		(N'Urea Conveyor', N'UOSLUreaConveyor'),
		(N'Marine Facilities', N'UOSLMarine'),
		(N'Environmental', N'UOSLEnviro'),
		(N'Other Offsite Facilities', N'UOSLOtherOff'),

	(N'Total', N'ProcUnits'),
	(N'Total Site Energy Standard, MBtu/day', N'ProcUnits'),
	(N'Total Annualized Cost and Workhours, MBtu/day', N'ProcUnits'),
	(N'Total Annualized Cost and Workhours', N'ProcUnits'),
	(N'EDC-Weighted Average Downtime', N'ProcUnits'),

	--	From Process-Level Input
	(N'Pre-desulphurization', N'OSPreDeSulfur'),
	(N'Ammonia Steam Reforming', N'OSAmmoniaSyngas'),
	(N'Methanol Steam Reforming', N'OSMethanolSyngas'),

	--	From ProfileFuels12
	--	SELECT DISTINCT [ProcessID] FROM [$(dbProfile)].[dbo].[FactorProcessCalc];
	(N'TotRef', N'ProcUnits'),
	(N'TotProc', N'ProcUnitsOnSite'),
	(N'TotUtly', N'ProcUnitsUOSL'),

	(N'CDU', N'OSCdu'),
	(N'DESAL', N'OSDeSal'),
	(N'DHYT', N'OSDesulTreatDist'),
	(N'FCC', N'OSCatCrack'),
	(N'H2PURE', N'OSPsaH2Pur'),
	(N'HYC', N'OSH2Crack'),
	(N'HYG', N'OSH2Gen'),
	(N'NHYT', N'OSDesulTreatGasNap'),
	(N'POLY', N'OSPoly'),
	(N'REF', N'OSCatReform'),
	(N'SRU', N'OSRecoveryS2'),
	(N'TRU', N'OSRecoveryTailGas'),
	(N'U45', N'OSU45'),
	(N'VAC', N'OSVacDist'),
	(N'VBR', N'OSVbr'),
	(N'TNK+BLND', N'UOSLTankBlend')

	--(N'TotRS', N''),		--	Received Shipment
	--(N'RSCRUDE', N''),		--	Crude oil and condensate receipt
	--(N'RSOTHRM', N''),		--	Other raw material receipt
	--(N'RSPROD', N''),		--	Product shipment
	--(N'AncUnits', N''),
	--(N'CDWAX', N''),
	--(N'ELECDIST', N''),
	--(N'ELECGEN', N''),
	--(N'FTCOGEN', N''),
	--(N'KHYT', N''),
	--(N'MajConv', N''),
	--(N'OperProc', N''),
	--(N'OthProc', N''),
	--(N'RERUN', N''),
	--(N'STEAMGEN', N''),
	--(N'U31', N''),
	--(N'U9', N''),
	--(N'VHYT', N'')

	) [t]([XlsProcessUnitDetail], [ProcessUnitTag]);

INSERT INTO [etl].[ProcessUnits]
(
	[XlsProcessUnitDetail],
	[ProcessUnitTag]
)
SELECT
	[t].[XlsProcessUnitDetail],
	[t].[ProcessUnitTag]
FROM
	@EtlProcessUnits		[t]
LEFT OUTER JOIN
	[etl].[ProcessUnits]	[e]
		ON	[e].[XlsProcessUnitDetail]	= [t].[XlsProcessUnitDetail]
WHERE
	[e].[ProcessUnitIdx]	IS NULL;