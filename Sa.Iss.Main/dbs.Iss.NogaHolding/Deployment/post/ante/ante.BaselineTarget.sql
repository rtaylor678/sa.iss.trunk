﻿INSERT INTO [ante].[BaselineTarget]
(
	[MethodologyId],
	[CurrencyId],
	[EntityId],
	[AttributeId],
	[Value_Float],
	[Value_Text],
	[Value_Date]
)
SELECT
	[ante].[Return_MethodologyId]('2015'),
	[dim].[Return_CurrencyId]('USD'),
	[t].[EntityId],
	[t].[AttributeId],
	[t].[Value_Float],
	[t].[Value_Text],
	[t].[Value_Date]
FROM (VALUES
	([rpt].[Return_EntityId]('ProcUnits'), [rpt].[Return_AttributeId]('uCap_Pcnt'),							 98.1, NULL, NULL),
	
	([rpt].[Return_EntityId]('OpEx'), [rpt].[Return_AttributeId]('RptExpCentsUedc'),						 33.9, NULL, NULL),
	([rpt].[Return_EntityId]('Nei'), [rpt].[Return_AttributeId]('Nei'),										135.0, NULL, NULL),
	([rpt].[Return_EntityId]('Eii'), [rpt].[Return_AttributeId]('Eii'),										109.0, NULL, NULL),
	([rpt].[Return_EntityId]('OpExMaintRoutTA'), [rpt].[Return_AttributeId]('MaintIdx'),					 24.4, NULL, NULL),
	([rpt].[Return_EntityId]('Mei'), [rpt].[Return_AttributeId]('Mei'),										116.0, NULL, NULL),
	([rpt].[Return_EntityId]('AvailMech'), [rpt].[Return_AttributeId]('AvailMech'),							 96.9, NULL, NULL),
	([rpt].[Return_EntityId]('PersIdx'), [rpt].[Return_AttributeId]('PersIdx'),								228.0, NULL, NULL),
	([rpt].[Return_EntityId]('OpExLaborMaterials'), [rpt].[Return_AttributeId]('RptExpEdcAnnualized'),		 60.0, NULL, NULL),
	([rpt].[Return_EntityId]('Pei'), [rpt].[Return_AttributeId]('Pei'),										240.0, NULL, NULL),
	([rpt].[Return_EntityId]('AvailOper'), [rpt].[Return_AttributeId]('AvailOper'),							 96.7, NULL, NULL)

	--	Non Energy OpEx ([rpt].[Return_EntityId]('OpExNe'), [rpt].[Return_AttributeId]('RptExpEdcAnnualized'),		 NULL, NULL, NULL)
	-- Feed and Fuel Index	([rpt].[Return_EntityId](''), [rpt].[Return_AttributeId](''),		 NULL, NULL, NULL)

	) [t]([EntityId], [AttributeId], [Value_Float], [Value_Text], [Value_Date])
LEFT OUTER JOIN
	[ante].[BaselineTarget]	[b]
		ON	[b].[EntityId]		= [t].[EntityId]
		AND	[b].[AttributeId]	= [t].[AttributeId]
WHERE
	[b].[BaselineTargetId] IS NULL;
	

