﻿INSERT INTO [ante].[KpiNormalization]
(
	[MethodologyId],
	[CurrencyId],
	[EntityId],
	[AttributeId],

	[Value_Q0],
	[Value_Q1],
	[Value_Q2],
	[Value_Q3],
	[Value_Q4],
	[Pcnt_Max]
)
SELECT
		[MethodologyId]	= [ante].[Return_MethodologyId]('2015'),
		[CurrencyId]	= [dim].[Return_CurrencyId]('USD'),
	[t].[EntityId],
	[t].[AttributeId],

	[t].[Value_Q0],
	[t].[Value_Q1],
	[t].[Value_Q2],
	[t].[Value_Q3],
	[t].[Value_Q4],
	[t].[Value_Max]

FROM (VALUES
	([rpt].[Return_EntityId]('Eii'), [rpt].[Return_AttributeId]('Eii'),									 53.0,  79.4,  98.2, 120.4, 180.0,  97.0),
	([rpt].[Return_EntityId]('ProcUnits'), [rpt].[Return_AttributeId]('uCap_Pcnt'),						 96.5,  85.4,  76.5,  67.4,  40.0,  97.0),
	([rpt].[Return_EntityId]('AvailOper'), [rpt].[Return_AttributeId]('AvailOper'),						 99.3,  97.5,  96.3,  94.1,  85.0,  97.0),
	([rpt].[Return_EntityId]('OpExLaborMaterials'), [rpt].[Return_AttributeId]('RptExpEdcAnnualized'),	 13.5,  23.4,  42.5,  63.3, 150.0,  97.0),
	([rpt].[Return_EntityId]('Pei'), [rpt].[Return_AttributeId]('Pei'),									 41.5,  68.1,  93.3, 137.5, 500.0,  97.0),
	([rpt].[Return_EntityId]('Mei'), [rpt].[Return_AttributeId]('Mei'),									 30.0,  85.3, 141.3, 219.5, 500.0,  97.0),
	([rpt].[Return_EntityId]('Nei'), [rpt].[Return_AttributeId]('Nei'),									 82.0, 124.8, 135.4, 180.1, 310.0,  97.0),
	([rpt].[Return_EntityId]('OpEx'), [rpt].[Return_AttributeId]('RptExpCentsUedc'),					 21.0,  42.9,  55.2,  74.3, 142.0,  97.0)
	) [t]([EntityId], [AttributeId], [Value_Q4], [Value_Q3], [Value_Q2], [Value_Q1], [Value_Q0], [Value_Max])
LEFT OUTER JOIN
	[ante].[KpiNormalization]	[o]
		ON	[o].[EntityId]		= [t].[EntityId]
		AND	[o].[AttributeId]	= [t].[AttributeId]
WHERE
	[o].[KpiNormalizationId]	IS NULL;