﻿INSERT INTO [ante].[Methodology]
(
	[MethodologyTag],
	[MethodologyName],
	[MethodologyDetail]
)
SELECT
	[t].[MethodologyTag],
	[t].[MethodologyName],
	[t].[MethodologyDetail]
FROM (VALUES
	('2015', '2015', '2015')
	) [t]([MethodologyTag], [MethodologyName], [MethodologyDetail])
LEFT OUTER JOIN
	[ante].[Methodology]	[m]
		ON	[m].[MethodologyTag]	= [t].[MethodologyTag]
WHERE
	[m].[MethodologyId] IS NULL;
	
