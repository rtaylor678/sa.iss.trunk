﻿SELECT
	[Entity]	 = '(''' + [t].[Table] + ''', '''', '''', ''' + [t].[Detail] + ''', ''''),'
FROM (
	SELECT DISTINCT
		[Table]		= REPLACE(REPLACE([t].[Table], '[xls].[', ''), ']', ''),
		[Detail]	= REPLACE([t].[EntityDetail], 'May, 2015', '<MONTH>, <YEAR>')
	FROM (
		SELECT
			[Table]			= '[xls].[Divisors]',
			[EntityDetail]	= [e].[DivisorsDetail]
		FROM
			[xls].[Divisors]				[e]

		UNION
		SELECT
			[Table]			= '[xls].[Edc]',
			[EntityDetail]	= [e].[ProcessUnitDetail]
		FROM
			[xls].[Edc]						[e]

		UNION
		SELECT
			[Table]			= '[xls].[Eii]',
			[EntityDetail]	= [e].[ProcessUnitDetail]
		FROM
			[xls].[Eii]						[e]

		UNION
		SELECT
			[Table]			= '[xls].[Energy]',
			[EntityDetail]	= [e].[AccountDetail]
		FROM
			[xls].[Energy]					[e]

		UNION
		SELECT
			[Table]			= '[xls].[FactorsAndStandards]',
			[EntityDetail]	= [e].[ProcessUnitDetail]
		FROM
			[xls].[FactorsAndStandards]		[e]

		UNION
		SELECT
			[Table]			= '[xls].[Maintenance]',
			[EntityDetail]	= [e].[MaintenanceDetail]
		FROM
			[xls].[Maintenance]				[e]

		UNION
		SELECT
			[Table]			= '[xls].[OpEx]',
			[EntityDetail]	= [e].[AccountDetail]
		FROM
			[xls].[OpEx]					[e]

		UNION
		SELECT
			[Table]			= '[xls].[Personnel]',
			[EntityDetail]	= [e].[PersonnelDetail]
		FROM
			[xls].[Personnel]				[e]

		UNION
		SELECT
			[Table]			= '[xls].[Reliability]',
			[EntityDetail]	= [e].[ProcessUnitDetail]
		FROM
			[xls].[Reliability]				[e]

		UNION
		SELECT
			[Table]			= '[xls].[Turnaround]',
			[EntityDetail]	= [e].[ProcessUnitDetail]
		FROM
			[xls].[Turnaround]				[e]
		) [t]
	) [t];