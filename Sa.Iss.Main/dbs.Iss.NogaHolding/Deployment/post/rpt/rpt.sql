﻿SET NOCOUNT ON;


--			Configuration
INSERT INTO [rpt].[Style]
(
	[StyleTag],
	[StyleName],
	[StyleDetail]
)
SELECT
	[t].[StyleTag],
	[t].[StyleName],
	[t].[StyleDetail]
FROM (VALUES
	('H0', 'Table Title', 'Table Title (Heading 0)'),
	('H1', 'Title', 'Title (Heading 1)'),
	('H2', 'Sub-title', 'Sub-title (Heading 2)'),
	
	('Ent', 'Entity', 'Entity'),
	('Att', 'Attribute', 'Attribute'),

	('F0', 'Footer 0', 'Footer 0'),
	('F1', 'Footer 1', 'Footer 1'),
	('F2', 'Footer 2', 'Footer 3')
	) [t]([StyleTag], [StyleName], [StyleDetail])
LEFT OUTER JOIN
	[rpt].[Style]	[x]
		ON	[x].[StyleTag]	= [t].[StyleTag]
WHERE	[x].[StyleId] IS NULL;

INSERT INTO [rpt].[StyleFont]
(
	[StyleId],
	[FontSize],
	[FontBold]
)
SELECT
	[s].[StyleId],
	[t].[FontSize],
	[t].[FontBold]
FROM (VALUES
	('H0', 14, 1),
	('H1', 14, 0),
	('H2', 14, null),
	('Ent', 12, 1),
	('Att', 12, 1)
	) [t]([StyleTag], [FontSize], [FontBold])
INNER JOIN
	[rpt].[Style]		[s]
		ON	[s].[StyleTag]	= [t].[StyleTag]
LEFT OUTER JOIN
	[rpt].[StyleFont]	[x]
		ON	[x].[StyleId]	= [s].[StyleId]
WHERE	[x].[StyleId] IS NULL;


INSERT INTO [rpt].[AnnotationsLevel]
(
	[AnnotationsLevelTag],
	[AnnotationsLevelName],
	[AnnotationsLevelDetail],
	[AnnotationLevelStyleId]
)
SELECT
	[t].[AnnotationsLevelTag],
	[t].[AnnotationsLevelName],
	[t].[AnnotationsLevelDetail],
		[StyleId]	= [rpt].[Return_StyleId]([t].[AnnotationsLevelTag])
FROM (VALUES
	('H0', 'Table Title', 'Table Title (Heading 0)'),
	('H1', 'Title', 'Title (Heading 1)'),
	('H2', 'Sub-title', 'Sub-title (Heading 2)'),

	('F0', 'Footer 0', 'Footer 0'),
	('F1', 'Footer 1', 'Footer 1'),
	('F2', 'Footer 2', 'Footer 3')
	) [t]([AnnotationsLevelTag], [AnnotationsLevelName], [AnnotationsLevelDetail])
LEFT OUTER JOIN
	[rpt].[AnnotationsLevel]	[x]
		ON	[x].[AnnotationsLevelTag]	= [t].[AnnotationsLevelTag]
WHERE	[x].[AnnotationsLevelId] IS NULL;

INSERT INTO [rpt].[AnnotationsText]
(
	[AnnotationsTextTag],
	[AnnotationsTextName],
	[AnnotationsTextDetail]
)
SELECT
	[t].[AnnotationsTextTag],
	[t].[AnnotationsTextName],
	[t].[AnnotationsTextDetail]
FROM (VALUES
	('Table 1', 'Table 1', 'Table 1'),
	('Table 2', 'Table 2', 'Table 2'),
	('Table 3', 'Table 3', 'Table 3'),
	('Table 4', 'Table 4', 'Table 4'),
	('Table 5', 'Table 5', 'Table 5'),
	('Table 6', 'Table 6', 'Table 6'),
	('Table 7', 'Table 7', 'Table 7'),
	('Table 8', 'Table 8', 'Table 8'),
	('Table 9', 'Table 9', 'Table 9'),
	('Table 10', 'Table 10', 'Table 10'),
	('Table 11', 'Table 11', 'Table 11'),

	('Summary of Indicators', 'Summary of Indicators', 'Summary of Indicators'),
	('EDC and UEDC Calculation', 'EDC and UEDC Calculation', 'EDC and UEDC Calculation'),
	('Factors and Standards', 'Factors and Standards', 'Factors and Standards'),
	('Hydrocarbon Yield', 'Hydrocarbon Yield', 'Hydrocarbon Yield'),
	('Cash Operating Expense', 'Cash Operating Expense', 'Cash Operating Expense'),
	('Energy Consumption and Exports', 'Energy Consumption and Exports', 'Energy Consumption and Exports'),
	('Personnel', 'Personnel', 'Personnel'),
	('Maintenance', 'Maintenance', 'Maintenance'),
	('Reliability', 'Reliability', 'Reliability'),
	('Annualized Turnaround Costs', 'Annualized Turnaround Costs', 'Annualized Turnaround Costs'),

	('<CompanyName>', '<CompanyName>', '<CompanyName>')

	) [t]([AnnotationsTextTag], [AnnotationsTextName], [AnnotationsTextDetail])
LEFT OUTER JOIN
	[rpt].[AnnotationsText]		[x]
		ON	[x].[AnnotationsTextTag]	= [t].[AnnotationsTextTag]
WHERE	[x].[AnnotationsTextId] IS NULL;


--	TODO:	Data Source
INSERT INTO [rpt].[DataSource]
(
	[TvfSchema],
	[TvfName]
)
SELECT
	[t].[TvfSchema],
	[t].[TvfName]
FROM (VALUES
	(N'rpt', N'Select_OutputFacilitiesSummary'),

	(N'rpt', N'Select_OutputFacilitiesEdc'),
	(N'rpt', N'Select_OutputFacilitiesFS'),
	(N'rpt', N'Select_OutputFacilitiesEii'),
	(N'rpt', N'Select_OutputFacilitiesDivisors'),
	(N'rpt', N'Select_OutputFacilitiesOpEx'),
	(N'rpt', N'Select_OutputFacilitiesOpExCat'),
	(N'rpt', N'Select_OutputFacilitiesPeriod'),
	(N'rpt', N'Select_OutputFacilitiesEnergy'),
	(N'rpt', N'Select_OutputFacilitiesPersonnel'),
	(N'rpt', N'Select_OutputFacilitiesMaintenance'),
	(N'rpt', N'Select_OutputFacilitiesReliability'),
	(N'rpt', N'Select_OutputFacilitiesReliabilityEdc'),
	(N'rpt', N'Select_OutputFacilitiesReliabilityAvailability'),
	(N'rpt', N'Select_OutputFacilitiesTurnaround'),
	(N'rpt', N'Select_OutputFacilitiesTurnaroundAlloc'),

	(N'rpt', N'Select_OutputFacilitiesYield'),
	(N'rpt', N'Select_OutputFacilitiesYieldHydrocarbons'),
	(N'rpt', N'Select_OutputFacilitiesYieldLosses'),
	(N'rpt', N'Select_OutputFacilitiesYieldFuelStandard'),

	(N'rpt', N'Select_OutputFacilities_Eii'),
	(N'rpt', N'Select_OutputFacilities_Mei'),
	(N'rpt', N'Select_OutputFacilities_Nei'),
	(N'rpt', N'Select_OutputFacilities_Pei'),
	(N'rpt', N'Select_OutputFacilities_MaintIdx'),
	(N'rpt', N'Select_OutputFacilities_PersIdx'),
	(N'rpt', N'Select_OutputFeedFuelIndex'),

	(N'rpt', N'Select_OutputFacilities_ChartEiiTrend'),
	(N'rpt', N'Select_OutputFacilities_ChartEiiYtd'),
	(N'rpt', N'Select_OutputFacilities_ChartMaintIdxTrend'),
	(N'rpt', N'Select_OutputFacilities_ChartMaintIdxYtd'),
	(N'rpt', N'Select_OutputFacilities_ChartMechAvailTrend'),
	(N'rpt', N'Select_OutputFacilities_ChartMechAvailYtd'),
	(N'rpt', N'Select_OutputFacilities_ChartMeiTrend'),
	(N'rpt', N'Select_OutputFacilities_ChartMeiYtd'),
	(N'rpt', N'Select_OutputFacilities_ChartNeiTrend'),
	(N'rpt', N'Select_OutputFacilities_ChartNeiYtd'),
	(N'rpt', N'Select_OutputFacilities_ChartOperAvailTrend'),
	(N'rpt', N'Select_OutputFacilities_ChartOperAvailYtd'),
	(N'rpt', N'Select_OutputFacilities_ChartOpExTrend'),
	(N'rpt', N'Select_OutputFacilities_ChartOpExYtd'),
	(N'rpt', N'Select_OutputFacilities_ChartOpExNeTrend'),
	(N'rpt', N'Select_OutputFacilities_ChartOpExNeYtd'),
	(N'rpt', N'Select_OutputFacilities_ChartPeiTrend'),
	(N'rpt', N'Select_OutputFacilities_ChartPeiYtd'),
	(N'rpt', N'Select_OutputFacilities_ChartPersCostIdxTrend'),
	(N'rpt', N'Select_OutputFacilities_ChartPersCostIdxYtd'),
	(N'rpt', N'Select_OutputFacilities_ChartPersIdxTrend'),
	(N'rpt', N'Select_OutputFacilities_ChartPersIdxYtd'),
	(N'rpt', N'Select_OutputFacilities_ChartUtilizationTrend'),
	(N'rpt', N'Select_OutputFacilities_ChartUtilizationYtd'),

	(N'rpt', N'Select_OutputFacilities_ChartRadarCurrent'),
	(N'rpt', N'Select_OutputFacilities_ChartRadarTrend'),

	(N'rpt', N'Select_OutputFacilities_ChartBarCurrent'),
	(N'rpt', N'Select_OutputFacilities_ChartBarTrend')

	) [t]([TvfSchema], [TvfName])
LEFT OUTER JOIN
	[rpt].[DataSource]	[x]
		ON	[x].[TvfSchema]	= [t].[TvfSchema]
		AND	[x].[TvfName]	= [t].[TvfName]
WHERE	[x].[DataSourceId]	IS NULL;

--	TODO:	Content (Sheets, Slides, Pages)
INSERT INTO [rpt].[Content]
(
	[ContentTag],
	[ContentName],
	[ContentDetail]
)
SELECT
	[t].[ContentTag],
	[t].[ContentName],
	[t].[ContentDetail]
FROM (VALUES
	('Summary', 'Summary', 'Summary'),
	('EDC', 'EDC', 'EDC'),
	('FS', 'Factors&Standards', 'Factors and Standards'),
	('EII', 'EII', 'EII'),
	('Yield', 'Yield', 'Yield'),
	('OpEx', 'OpEx', 'OpEx'),
	('Energy', 'Energy', 'Energy'),
	('Personnel', 'Personnel', 'Personnel'),
	('Maintenance', 'Maintenance', 'Maintenance'),
	('Reliability', 'Reliability', 'Reliability'),
	('Turnaround', 'Turnaround', 'Turnaround'),

	('ChartEii',			'Chart Eii', 'Chart Eii'),
	('ChartMaintIdx',		'Chart MaintIdx', 'Chart MaintIdx'),
	('ChartMechAvail',		'Chart MechAvail', 'Chart MechAvail'),
	('ChartMei',			'Chart Mei', 'Chart Mei'),
	('ChartNei',			'Chart Nei', 'Chart Nei'),
	('ChartOperAvail',		'Chart OperAvail', 'Chart OperAvail'),
	('ChartOpEx',			'Chart OpEx', 'Chart OpEx'),
	('ChartOpExNe',			'Chart OpExNe', 'Chart OpExNe'),
	('ChartPei',			'Chart Pei', 'Chart Pei'),
	('ChartPersCostIdx',	'Chart PersCostIdx', 'Chart PersCostIdx'),
	('ChartPersIdx',		'Chart PersIdx', 'Chart PersIdx'),
	('ChartUtilization',	'Chart Utilization', 'Chart Utilization'),

	('ChartRadar',			'Chart Radar', 'Chart Radar'),
	('ChartQuartile',		'Chart Quartile', 'Chart Quartile')

	) [t]([ContentTag], [ContentName], [ContentDetail])
LEFT OUTER JOIN
	[rpt].[Content]	[x]
		ON	[x].[ContentTag] = [t].[ContentTag]
WHERE	[x].[ContentId]		IS NULL;

--			Headers and Footers
INSERT INTO [rpt].[ContentAnnotations]
(
	[ContentId],
	[AnnotationLevelId],
	[AnnotationTextId]
)
SELECT
	[c].[ContentId],
	[l].[AnnotationsLevelId],
	[a].[AnnotationsTextId]
FROM (VALUES
	('Summary', 'H0', 'Table 1'),
	('Summary', 'H1', 'Summary of Indicators'),
	('Summary', 'H2', '<CompanyName>'),

	('EDC', 'H0', 'Table 2'),
	('EDC', 'H1', 'EDC and UEDC Calculation'),
	('EDC', 'H2', '<CompanyName>'),

	('FS', 'H0', 'Table 3'),
	('FS', 'H1', 'Factors and Standards'),
	('FS', 'H2', '<CompanyName>'),
	
	('EII', 'H0', 'Table 4'),
	('EII', 'H1', 'Factors and Standards'),
	('EII', 'H2', '<CompanyName>'),
	
	('Yield', 'H0', 'Table 5'),
	('Yield', 'H1', 'Hydrocarbon Yield'),
	('Yield', 'H2', '<CompanyName>'),
	
	('OpEx', 'H0', 'Table 6'),
	('OpEx', 'H1', 'Cash Operating Expense'),
	('OpEx', 'H2', '<CompanyName>'),
	
	('Energy', 'H0', 'Table 7'),
	('Energy', 'H1', 'Energy Consumption and Exports'),
	('Energy', 'H2', '<CompanyName>'),
	
	('Personnel', 'H0', 'Table 8'),
	('Personnel', 'H1', 'Personnel'),
	('Personnel', 'H2', '<CompanyName>'),
	
	('Maintenance', 'H0', 'Table 9'),
	('Maintenance', 'H1', 'Maintenance'),
	('Maintenance', 'H2', '<CompanyName>'),
	
	('Reliability', 'H0', 'Table 10'),
	('Reliability', 'H1', 'Reliability'),
	('Reliability', 'H2', '<CompanyName>'),

	('Turnaround', 'H0', 'Table 11'),
	('Turnaround', 'H1', 'Annualized Turnaround Costs'),
	('Turnaround', 'H2', '<CompanyName>')
	) [t]([ContentTag], [AnnotationsLevelTag], [AnnotationTextTag])
INNER JOIN
	[rpt].[Content]				[c]
		ON	[c].[ContentTag]	= [t].[ContentTag]
INNER JOIN
	[rpt].[AnnotationsLevel]	[l]
		ON	[l].[AnnotationsLevelTag]	= [t].[AnnotationsLevelTag]
INNER JOIN
	[rpt].[AnnotationsText]		[a]
		ON	[a].[AnnotationsTextTag]	= [t].[AnnotationTextTag]
LEFT OUTER JOIN
	[rpt].[ContentAnnotations]	[x]
		ON	[x].[ContentId]				= [c].[ContentId]
		AND	[x].[AnnotationLevelId]		= [l].[AnnotationsLevelId]
		AND	[x].[AnnotationTextId]		= [a].[AnnotationsTextId]
WHERE	[x].[ContentAnnotationId]	 IS NULL;

--	TODO:	TVF for content sections
INSERT INTO [rpt].[ContentSection]
(
	[ContentId],
	[SectionNumber],
	[DataSourceId]
)
SELECT
	[c].[ContentId],
	[t].[SectionNumber],
	[d].[DataSourceId]
FROM (VALUES
	('Summary',		1, N'rpt', N'Select_OutputFacilitiesSummary'),

	('EDC',			1, N'rpt', N'Select_OutputFacilitiesEdc'),

	('FS',			1, N'rpt', N'Select_OutputFacilitiesFS'),

	('EII',			1, N'rpt', N'Select_OutputFacilitiesEii'),
	('EII',			2, N'rpt', N'Select_OutputFacilities_Eii'),

	('Yield',		1, N'rpt', N'Select_OutputFacilitiesYield'),
	('Yield',		2, N'rpt', N'Select_OutputFacilitiesYieldHydrocarbons'),
	('Yield',		3, N'rpt', N'Select_OutputFacilitiesDivisors'),
	('Yield',		4, N'rpt', N'Select_OutputFacilitiesYieldLosses'),
	('Yield',		5, N'rpt', N'Select_OutputFacilitiesYieldFuelStandard'),
	('Yield',		6, N'rpt', N'Select_OutputFeedFuelIndex'),

	('OpEx',		1, N'rpt', N'Select_OutputFacilitiesOpEx'),
	('OpEx',		2, N'rpt', N'Select_OutputFacilitiesOpExCat'),
	('OpEx',		3, N'rpt', N'Select_OutputFacilities_Nei'),
	('OpEx',		4, N'rpt', N'Select_OutputFacilitiesDivisors'),
	('OpEx',		5, N'rpt', N'Select_OutputFacilitiesPeriod'),

	('Energy',		1, N'rpt', N'Select_OutputFacilitiesEnergy'),
	('Energy',		2, N'rpt', N'Select_OutputFacilitiesDivisors'),

	('Personnel',	1, N'rpt', N'Select_OutputFacilitiesPersonnel'),
	('Personnel',	2, N'rpt', N'Select_OutputFacilities_PersIdx'),
	('Personnel',	3, N'rpt', N'Select_OutputFacilities_Pei'),
	('Personnel',	4, N'rpt', N'Select_OutputFacilitiesPeriod'),

	('Maintenance',	1, N'rpt', N'Select_OutputFacilitiesMaintenance'),
	('Maintenance',	2, N'rpt', N'Select_OutputFacilities_MaintIdx'),
	('Maintenance',	3, N'rpt', N'Select_OutputFacilities_Mei'),
	('Maintenance',	4, N'rpt', N'Select_OutputFacilitiesPeriod'),

	('Reliability',	1, N'rpt', N'Select_OutputFacilitiesReliability'),
	('Reliability',	2, N'rpt', N'Select_OutputFacilitiesReliabilityEdc'),
	('Reliability',	3, N'rpt', N'Select_OutputFacilitiesReliabilityAvailability'),

	('Turnaround',	1, N'rpt', N'Select_OutputFacilitiesTurnaround'),
	('Turnaround',	2, N'rpt', N'Select_OutputFacilitiesTurnaroundAlloc'),
	('Turnaround',	3, N'rpt', N'Select_OutputFacilitiesPeriod'),

	('ChartEii',			1, N'rpt', N'Select_OutputFacilities_ChartEiiTrend'),
	('ChartEii',			2, N'rpt', N'Select_OutputFacilities_ChartEiiYtd'),
	('ChartMaintIdx',		1, N'rpt', N'Select_OutputFacilities_ChartMaintIdxTrend'),
	('ChartMaintIdx',		2, N'rpt', N'Select_OutputFacilities_ChartMaintIdxYtd'),
	('ChartMechAvail',		1, N'rpt', N'Select_OutputFacilities_ChartMechAvailTrend'),
	('ChartMechAvail',		2, N'rpt', N'Select_OutputFacilities_ChartMechAvailYtd'),
	('ChartMei',			1, N'rpt', N'Select_OutputFacilities_ChartMeiTrend'),
	('ChartMei',			2, N'rpt', N'Select_OutputFacilities_ChartMeiYtd'),
	('ChartNei',			1, N'rpt', N'Select_OutputFacilities_ChartNeiTrend'),
	('ChartNei',			2, N'rpt', N'Select_OutputFacilities_ChartNeiYtd'),
	('ChartOperAvail',		1, N'rpt', N'Select_OutputFacilities_ChartOperAvailTrend'),
	('ChartOperAvail',		2, N'rpt', N'Select_OutputFacilities_ChartOperAvailYtd'),
	('ChartOpEx',			1, N'rpt', N'Select_OutputFacilities_ChartOpExTrend'),
	('ChartOpEx',			2, N'rpt', N'Select_OutputFacilities_ChartOpExYtd'),
	('ChartOpExNe',			1, N'rpt', N'Select_OutputFacilities_ChartOpExNeTrend'),
	('ChartOpExNe',			2, N'rpt', N'Select_OutputFacilities_ChartOpExNeYtd'),
	('ChartPei',			1, N'rpt', N'Select_OutputFacilities_ChartPeiTrend'),
	('ChartPei',			2, N'rpt', N'Select_OutputFacilities_ChartPeiYtd'),
	('ChartPersCostIdx',	1, N'rpt', N'Select_OutputFacilities_ChartPersCostIdxTrend'),
	('ChartPersCostIdx',	2, N'rpt', N'Select_OutputFacilities_ChartPersCostIdxYtd'),
	('ChartPersIdx',		1, N'rpt', N'Select_OutputFacilities_ChartPersIdxTrend'),
	('ChartPersIdx',		2, N'rpt', N'Select_OutputFacilities_ChartPersIdxYtd'),
	('ChartUtilization',	1, N'rpt', N'Select_OutputFacilities_ChartUtilizationTrend'),
	('ChartUtilization',	2, N'rpt', N'Select_OutputFacilities_ChartUtilizationYtd'),

	('ChartRadar',			1, N'rpt', N'Select_OutputFacilities_ChartRadarTrend'),
	('ChartRadar',			2, N'rpt', N'Select_OutputFacilities_ChartRadarCurrent'),

	('ChartQuartile',		1, N'rpt', N'Select_OutputFacilities_ChartBarTrend'),
	('ChartQuartile',		2, N'rpt', N'Select_OutputFacilities_ChartBarCurrent')

	) [t]([ContentTag], [SectionNumber], [TvfSchema], [TvfName])
INNER JOIN
	[rpt].[Content]			[c]
		ON	[c].[ContentTag]	= [t].[ContentTag]
INNER JOIN
	[rpt].[DataSource]		[d]
		ON	[d].[TvfSchema]		= [t].[TvfSchema]
		AND	[d].[TvfName]		= [t].[TvfName]
LEFT OUTER JOIN
	[rpt].[ContentSection]	[x]
		ON	[x].[ContentId]		= [c].[ContentId]
		AND	[x].[SectionNumber]	= [t].[SectionNumber]
		AND	[x].[DataSourceId]	= [d].[DataSourceId]
WHERE	[x].[DataSourceId]	IS  NULL;

--			Deliverable (Document, Presentation, Workbook)
INSERT INTO [rpt].[Deliverables]
(
	[DeliverableTag],
	[DeliverableName],
	[DeliverableDetail]
)
SELECT
	[t].[DeliverableTag],
	[t].[DeliverableName],
	[t].[DeliverableDetail]
FROM (VALUES
	('NOGA ISS', 'NOGA Holding ISS', 'NOGA Holding ISS')
	) [t]([DeliverableTag], [DeliverableName], [DeliverableDetail])
LEFT OUTER JOIN
	[rpt].[Deliverables]	[x]
		ON	[x].[DeliverableTag] = [t].[DeliverableTag]
WHERE	[x].[DeliverableId]		IS NULL;

--			Items in a deliverable
INSERT INTO [rpt].[DeliverablesContent]
(
	[DeliverableId],
	[ItemNumber],
	[ContentId]
)
SELECT
	[d].[DeliverableId],
	[c].[ContentId],
	[c].[ContentId]
FROM
	[rpt].[Deliverables]		[d]
CROSS JOIN
	[rpt].[Content]				[c]
LEFT OUTER JOIN
	[rpt].[DeliverablesContent]	[x]
		ON	[x].[DeliverableId]	= [d].[DeliverableId]
		AND	[x].[ItemNumber]	= [c].[ContentId]
		AND	[x].[ContentId]		= [c].[ContentId]
WHERE	[x].[DeliverablesContentId]	IS NULL;


/*
SELECT * FROM [rpt].[AnnotationsLevel];
SELECT * FROM [rpt].[AnnotationsText];
SELECT * FROM [rpt].[DataSource];
SELECT * FROM [rpt].[Content];
SELECT * FROM [rpt].[ContentAnnotations];
SELECT * FROM [rpt].[ContentSection];
SELECT * FROM [rpt].[Deliverables];
SELECT * FROM [rpt].[DeliverablesContent];

SELECT * FROM [rpt].[DataSourceParameters];
*/

----DELETE FROM [rpt].[AnnotationsLevel];
----DELETE FROM [rpt].[AnnotationsText];
----DELETE FROM [rpt].[DataSource];
----DELETE FROM [rpt].[Content];
----DELETE FROM [rpt].[ContentAnnotations];
----DELETE FROM [rpt].[ContentSection];
----DELETE FROM [rpt].[Deliverables];
----DELETE FROM [rpt].[DeliverablesContent];