﻿DECLARE @Entities	TABLE
(
	[EntityParent]						VARCHAR(24)			NOT	NULL,	CHECK([EntityParent] <> ''),

	[Operator]							CHAR(1)				NOT	NULL	CHECK([Operator] IN ('+', '-', '*', '/', '~')),
	[SortKey]							INT					NOT	NULL,
	[EntityTag]							VARCHAR(48)			NOT	NULL,	UNIQUE CLUSTERED([EntityTag] ASC),
																		CHECK([EntityTag] <> ''),
	[EntityName]						NVARCHAR(96)		NOT	NULL,	UNIQUE NONCLUSTERED([EntityName] ASC),
																		CHECK([EntityName] <> N''),
	[EntityDetail]						NVARCHAR(96)		NOT	NULL,	UNIQUE NONCLUSTERED([EntityDetail] ASC),
																		CHECK([EntityDetail] <> N'')
);

INSERT INTO @Entities
(
	[EntityParent],
	[Operator],
	[SortKey],
	[EntityTag],
	[EntityName],
	[EntityDetail]
)
SELECT
	[t].[EntityParent],
	[t].[Operator],
	[t].[SortKey],
	[t].[EntityTag],
	[t].[EntityName],
	[t].[EntityDetail]
	FROM (VALUES

	('Chart', '+', 0, 'Chart', 'Chart', 'Chart'),
		('Chart', '+', 0, 'YearCurr', 'Current Year', 'Current Year'),
		('Chart', '+', 0, 'YearPrev', 'Previous Year', 'Previous Year'),
		('Chart', '+', 0, 'Tgt', 'Target', 'Target'),
		('Chart', '+', 0, 'YTD', 'Year to Date', 'Year to Date'),

--	Process Units
--		EDC
--		Factors and Standards
--		EII
--		Reliability
--		Turnaround

	('ProcUnits', '+', 0, 'ProcUnits', 'Process Units', 'Process Units'),
		('ProcUnits', '+', 0, 'ProcUnitsOnSiteTA', 'Process Units (Turnaround, On Site)', 'Process Units (Turnaround, On Site)'),
		('ProcUnits', '+', 0, 'ProcUnitsOnSite', 'Process Units (On Site)', 'Process Units (On Site)'),
			('ProcUnitsOnSite', '+', 0, 'OSPreDeSulfur', 'Predesulfurization', 'Predesulfurization'),
			
			('ProcUnitsOnSite', '+', 0, 'OSAmmonia', 'Ammonia Unit', 'Ammonia Unit'),
				('OSAmmonia', '+', 0, 'OSAmmoniaSyngas', 'Ammonia Syngas Preparation', 'Ammonia Syngas Preparation'),
				('OSAmmonia', '+', 0, 'OSAmmoniaRefrig', 'Ammonia Synthesis and Refrigeration', 'Ammonia Synthesis and Refrigeration'),
			('ProcUnitsOnSite', '+', 0, 'OSMethanol', 'Methanol Unit', 'Methanol Unit'),
				('OSMethanol', '+', 0, 'OSMethanolSyngas', 'Methanol Syngas Preparation', 'Methanol Syngas Preparation'),
				('OSMethanol', '+', 0, 'OSMethanolDist', 'Methanol Synthesis and Distillation', 'Methanol Synthesis and Distillation'),
			
			('ProcUnitsOnSite', '+', 0, 'OSUrea', 'Urea Unit', 'Urea Unit'),
			('ProcUnitsOnSite', '+', 0, 'OSPsaH2Pur', 'PSA Hydrogen Purification', 'PSA Hydrogen Purification'),
			('ProcUnitsOnSite', '+', 0, 'OSDeSal', 'Desalination', 'Desalination'),

			('ProcUnitsOnSite', '+', 0, 'OSCdu', 'Atmospheric Crude Distillation', 'Atmospheric Crude Distillation'),
			
			('ProcUnitsOnSite', '+', 0, 'OSDesulTreatDist', 'Distillate Desulfurization and Treating', 'Distillate Desulfurization and Treating'),
			('ProcUnitsOnSite', '+', 0, 'OSDesulTreatGasNap', 'Gasoline/Naphtha Desulfurization and Treating', 'Gasoline/Naphtha Desulfurization and Treating'),
			
			('ProcUnitsOnSite', '+', 0, 'OSCatCrack', 'Catalytic Cracking', 'Catalytic Cracking'),
			('ProcUnitsOnSite', '+', 0, 'OSCatReform', 'Catalytic Reforming', 'Catalytic Reforming'),
			('ProcUnitsOnSite', '+', 0, 'OSH2Crack', 'Hydrocracking', 'Hydrocracking'),
			('ProcUnitsOnSite', '+', 0, 'OSH2Gen', 'Hydrogen Generation', 'Hydrogen Generation'),
			('ProcUnitsOnSite', '+', 0, 'OSPoly', 'Polymerization', 'Polymerization'),

			('ProcUnitsOnSite', '+', 0, 'OSFlueGasCo2Recov', 'Flue Gas CO₂ Recovery Unit', 'Flue Gas CO₂ Recovery Unit'),
			('ProcUnitsOnSite', '+', 0, 'OSRecoveryS2', 'Sulfur Recovery Unit', 'Sulfur Recovery Unit'),
			('ProcUnitsOnSite', '+', 0, 'OSRecoveryTailGas', 'Tail Gas Recovery Unit', 'Tail Gas Recovery Unit'),
			('ProcUnitsOnSite', '+', 0, 'OSU45', 'U45 - Amine Regeneration', 'U45 - Amine Regeneration'),
			('ProcUnitsOnSite', '+', 0, 'OSVacDist', 'Vacuum Distillation', 'Vacuum Distillation'),
			('ProcUnitsOnSite', '+', 0, 'OSVbr', 'Visbreaking', 'Visbreaking'),


		('ProcUnits', '+', 0, 'ProcUnitsUOSL', 'Utility, Offsite, & Logistics', 'Utility, Offsite, & Logistics Facilities'),

			('ProcUnitsUOSL', '+', 0, 'UOSLFiredSteamBoilers', 'Fired Steam Boilers', 'Fired Steam Boilers'),
			('ProcUnitsUOSL', '+', 0, 'UOSLGeneratorFTD', 'Generator with Fired Turbine Driver', 'Generator with Fired Turbine Driver'),

			('ProcUnitsUOSL', '+', 0, 'UOSLCogeneration', 'Cogeneration', 'Cogeneration'),
			('ProcUnitsUOSL', '+', 0, 'UOSLOthSteamElec', 'Other Steam and Electrical', 'Other Steam and Electrical'),
			('ProcUnitsUOSL', '+', 0, 'UOSLTankBlend', 'Tankage and Blending', 'Tankage and Blending'),
				('UOSLTankBlend', '+', 0, 'UOSLTankage', 'Tankage', 'Tankage'),
				('UOSLTankBlend', '+', 0, 'UOSLBlending', 'Blending', 'Blending'),

			('ProcUnitsUOSL', '+', 0, 'ProcUnitsUOSLOther', 'Other Utilities, Offsite, & Logistics', 'Other Utilities, Offsite, & Logistics Facilities'),
				('ProcUnitsOSUOther', '+', 0, 'UOSLOtherUtil', 'Other Utilities', 'Other Utilities'),
				('ProcUnitsOSUOther', '+', 0, 'UOSLUreaWarehouse', 'Urea Warehouse', 'Urea Warehouse'),
				('ProcUnitsOSUOther', '+', 0, 'UOSLUreaPackage', 'Urea Packaging', 'Urea Packaging'),
				('ProcUnitsOSUOther', '+', 0, 'UOSLUreaConveyor', 'Urea Conveyor', 'Urea Conveyor'),
				('ProcUnitsOSUOther', '+', 0, 'UOSLMarine', 'Marine Facilities', 'Marine Facilities'),
				('ProcUnitsOSUOther', '+', 0, 'UOSLEnviro', 'Environmental', 'Environmental'),
				('ProcUnitsOSUOther', '+', 0, 'UOSLOtherOff', 'Other Offsite', 'Other Offsite '),

				('ProcUnitsOSUOther', '+', 0, 'UOSLOtherLog', 'Other Logistics', 'Other Logistics'),

			('ProcUnitsUOSL', '+', 0, 'ProcUnitsUOSLOffSite', 'Offsite Facilities', 'Offsite Facilities'),

			('ProcUnitsUOSL', '+', 0, 'ProcUnitsUOSLUtilities', 'Utility Facilities', 'Utility Facilities'),

			('ProcUnitsUOSL', '+', 0, 'ProcUnitsUOSLLogistis', 'Logistical Facilities', 'Logistical Facilities'),

	--	AccountId
	--		OpEx
	--		Energy
	--		Personnel
	--		Maintenance

	('OpEx', '+', 0, 'OpEx', 'Total Cash Operating Expenses', 'Total Cash Operating Expenses'),
		('OpEx', '+', 0, 'OpExRpt', 'Cash Operating Expenses (Reported)', 'Cash Operating Expenses (Reported)'),

			('OpExRpt', '+', 0, 'OpExEnergy', 'Net-Energy', 'Net-Energy'),

				('OpExEnergy', '+', 0, 'EnergyProduced', 'Produced Energy', 'Produced Energy'),

					('EnergyProduced', '+', 0, 'EnergyProdFuel', 'Produced Fuel', 'Produced Fuel'),
						('EnergyProdFuel', '+', 0, 'EnergyProdFuelGas', 'Fuel Gas (Produced)', 'Fuel Gas (Produced)'),
						('EnergyProdFuel', '+', 0, 'EnergyProdOther', 'Other Fuels (Produced)', 'Other Fuels (Produced)'),

				('OpExEnergy', '+', 0, 'EnergyPurchased', 'Purchased Energy', 'Purchased Energy'),

					('EnergyPurchased', '+', 0, 'EnergyPurchFuel', 'Purchased Fuel', 'Purchased Fuel'),
						('EnergyPurchFuel', '+', 0, 'EnergyPurchFuelGas', 'Fuel Gas (Purchased)', 'Fuel Gas (Purchased)'),
						('EnergyPurchFuel', '+', 0, 'EnergyPurchOther', 'Other Purchased Fuels', 'Other Purchased Fuels'),

					('EnergyPurchased', '+', 0, 'EnergyPurchUtil', 'Purchased Utilities', 'Purchased Utilities'),
						('EnergyPurchUtil', '+', 0, 'EnergyPurchElec', 'Electricity (Purchased)', 'Electricity (Purchased)'),
						('EnergyPurchUtil', '+', 0, 'EnergyPurchSteam', 'Steam (Purchased)', 'Steam (Purchased)'),

				('OpExEnergy', '+', 0, 'EnergyExported', 'Sold Energy', 'Sold Energy'),

					('EnergyExported', '+', 0, 'EnergySoldUtil', 'Sold Utilities', 'Sold Utilities'),
						('EnergySoldUtil', '+', 0, 'EnergySoldElec', 'Electricity (Sold)', 'Electricity (Sold)'),
						('EnergySoldUtil', '+', 0, 'EnergySoldSteam', 'Steam (Sold)', 'Steam (Sold)'),

			('OpExRpt', '+', 0, 'OpExChemCat', 'Chemicals and Catalysts', 'Chemicals and Catalysts'),

			('OpExRpt', '+', 0, 'OpExLaborMaterials', 'Labor and/or Materials', 'Labor and/or Materials'),

				('OpExLaborMaterials', '+', 0, 'GeneralMaint', 'General and Maintenance', 'General and Maintenance'),
					('GeneralMaint', '+', 0, 'GeneralMaintCompanyLabor', 'Company General and Maintenance Labor', 'Company General and Maintenance Labor'),
					('GeneralMaint', '+', 0, 'GeneralMaintContractLabor', 'Contract General and Maintenance Labor', 'Contract General and Maintenance Labor'),
					('GeneralMaint', '+', 0, 'SBARD', 'sbard short desc', 'sbard long desc'),

				('OpExLaborMaterials', '+', 0, 'AllocGA', 'Corporate Allocated G&A', 'Corporate Allocated G&A'),
					('AllocGA', '+', 0, 'AllocGAPers', 'Corporate Allocated Personnel', 'Corporate Allocated Personnel'),
					('AllocGA', '+', 0, 'AllocGAOther', 'Corporate Allocated (Other)', 'Corporate Allocated (Other)'),

				('OpExLaborMaterials', '+', 0, 'General', 'Other General', 'Other General'),
					('General', '+', 0, 'GeneralCompanyLabor', 'General Company Labor', 'General Company Labor'),
						--OCC_Sal
						--MPS_Sal
						--OCC_Ben
						--MPS_Ben
					('General', '+', 0, 'GeneralCompanyEquipment', 'General Equipment', 'General Equipment'),
					('General', '+', 0, 'GeneralCompanyMaterial', 'General Material', 'General Material'),

					('General', '+', 0, 'GeneralContractLabor', 'General Contract Labor', 'General Contract Labor'),
						--Cont_ProcOp
						--Cont_TransOp
						--Cont_Fire

				('OpExLaborMaterials', '+', 0, 'OpExMaintRoutTA', 'Routine Maintenance and Turnaround', 'Routine Maintenance and Turnaround'),

				('OpExLaborMaterials', '+', 0, 'MaintRout', 'Routine Maintenance', 'Routine Maintenance'),
					('MaintRout', '+', 0, 'MaintRoutCompanyLabor', 'Company Maintenance Labor', 'Company Maintenance Labor'),
					('MaintRout', '+', 0, 'MaintRoutCompanyEquipment', 'Company Maintenance Equipment', 'Company Maintenance Equipment'),
					('MaintRout', '+', 0, 'MaintRoutCompanyMaterial', 'Company Maintenance Material', 'Company Maintenance Material'),

					('MaintRout', '+', 0, 'MaintRoutContractLabor', 'Contract Maintenance Labor', 'Contract Maintenance Labor'),
					('MaintRout', '+', 0, 'MaintRoutContractEquipment', 'Contract Maintenance Equipment', 'Contract Maintenance Equipment'),
					('MaintRout', '+', 0, 'MaintRoutContractMaterial', 'Contract Maintenance Material', 'Contract Maintenance Material'),
					
					('MaintRout', '+', 0, 'MaintRoutEquipment', 'Maintenance Equipment', 'Maintenance Equipment'),
					('MaintRout', '+', 0, 'MaintRoutMaterial', 'Maintenance Material', 'Maintenance Material'),

				('OpExLaborMaterials', '+', 0, 'MaintTa', 'Turnaround', 'Turnaround'),
					('MaintTa', '+', 0, 'MaintTaCompanyLabor', 'Company Turnaround Labor', 'Company Turnaround Labor'),
					('MaintTa', '+', 0, 'MaintTaCompanyEquipment', 'Company Turnaround Equipment', 'Company Turnaround Equipment'),
					('MaintTa', '+', 0, 'MaintTaCompanyMaterial', 'Company Turnaround Material', 'Company Turnaround Material'),

					('MaintTa', '+', 0, 'MaintTaContractLabor', 'Contract Turnaround Labor', 'Contract Turnaround Labor'),
					('MaintTa', '+', 0, 'MaintTaContractEquipment', 'Contract Turnaround Equipment', 'Contract Turnaround Equipment'),
					('MaintTa', '+', 0, 'MaintTaContractMaterial', 'Contract Turnaround Material', 'Contract Turnaround Material'),

					('MaintTa', '+', 0, 'MaintTaLabor', 'Turnaround Labor', 'Turnaround Labor'),

			('OpExRpt', '+', 0, 'OpExOther', 'Other Operating Expense', 'Other Non-Turnaround Operating Expense'),

		--('OpEx', '+', 0, 'OpExTa', 'Cash Operating Expenses (Turnaround)', 'Cash Operating Expenses (Turnaround)'),

	('OpExNe', '+', 0, 'OpExNe', 'Non-Energy Operating Expenses', 'Non-Energy Operating Expenses'),

	('Divisors', '+', 0, 'Divisors', 'Divisors', 'Divisors'),
		('Divisors', '+', 0, 'DivProd', 'Production Divisors', 'Production  Divisors'),
			('DivProd', '+', 0, 'Ammonia', 'Ammonia', 'Ammonia'),
			('DivProd', '+', 0, 'DivUrea', 'Urea Bulk & Packaged', 'Urea Bulk & Packaged'),
				('DivProdUrea', '+', 0, 'DivUreaBulk', 'Bulk Urea', 'Bulk Urea'),
				('DivProdUrea', '+', 0, 'DivUreaPackaged', 'Packaged Urea', 'Packaged Urea'),
			('DivProd', '+', 0, 'Methanol', 'Methanol', 'Methanol'),

		('Divisors', '+', 0, 'DivPeriod', 'Reporting Period', 'Reporting Period'),

	('Indexes', '+', 0, 'Indexes', 'Indexes', 'Indexes'),
		('Indexes', '+', 0, 'Eii', 'EII', 'EII = (Actual Energy) / (EII Standard Energy) x 100'),
		('Indexes', '+', 0, 'Nei', 'Non-Energy Efficiency Index (NEI)', 'NEI = (Actual Annualized Non-Energy Cost) / (Standard Non-Energy Cost) x 100'),
		('Indexes', '+', 0, 'Pei', 'Personnel Efficiency Index (PEI)', 'PEI = (Actual Annualized Work Hours) / (Standard Work Hours) x 100'),
		('Indexes', '+', 0, 'Mei', 'Maintenance Efficiency Index (MEI)', 'MEI = (Actual Annualized Maintenance Cost) / (Standard Maintenance Cost) x 100'),
		('Indexes', '+', 0, 'AvailMech', 'Mechanical Availability', 'Mechanical Availability'),
		('Indexes', '+', 0, 'AvailOper', 'Operational Availability', 'Operational Availability'),
		('Indexes', '+', 0, 'PersIdx', 'Personnel Index', 'Personnel Index = (Annualized Work Hours) / (EDC/100)'),
		('Indexes', '+', 0, 'MaintIdx', 'Maintenance Index', 'Maintenance Index = (Actual TA-Adjusted Annual Maintenance Cost) / EDC'),

		('Indexes', '+', 0, 'uEdc', 'Utilized EDC', 'Utilized EDC'),
		('Indexes', '+', 0, 'kEdc', 'kEDC', 'kEDC'),
		('Indexes', '+', 0, 'kEdcProcess', 'Process EDC', 'Process EDC'),
		('Indexes', '+', 0, 'FeedFuelIdx', 'Feed and Fuel Index', 'Feed and Fuel Index'),

	('Composition', '+', 0, 'Composition', 'Composition', 'Composition'),
		('Composition', '+', 0, 'H2', 'Hydrogen', 'Hydrogen'),
		('Composition', '+', 0, 'CH4', 'Methane', 'Methane'),
		('Composition', '+', 0, 'C2H6', 'Ethane', 'Ethane'),
		('Composition', '+', 0, 'C3H8', 'Propane', 'Propane'),
		('Composition', '+', 0, 'C4H10+C5s', 'Butane and C5+', 'Butane and C5+'),
		('Composition', '+', 0, 'CO', 'Carbon Monoxide', 'Carbon Monoxide'),
		('Composition', '+', 0, 'CO2', 'Carbon Dioxide', 'Carbon Dioxide'),
		('Composition', '+', 0, 'H2S', 'Hydrogen Sulfide', 'Hydrogen Sulfide'),
		('Composition', '+', 0, 'N2', 'Nitrogen', 'Nitrogen'),
		('Composition', '+', 0, 'Ar+Other', 'Argon and Other', 'Argon and Other'),
		('Composition', '+', 0, 'CompTotal', 'Composition Total', 'Composition Total'),
		('Composition', '+', 0, 'Hydrocarbons', 'Hydrogen and Hydrocarbons', 'Hydrogen and Hydrocarbons'),

	('Streams', '+', 0, 'Streams', 'Streams', 'Streams'),
		('Streams', '+', 0, 'Feed', 'Feed', 'Feed'),
			('Feed', '+', 0, 'FeedRaw', 'Raw Feed', 'Raw Feed'),
		('Streams', '+', 0, 'ProdLoss', 'Production and Losses', 'Production and Losses'),
			('ProdLoss', '+', 0, 'Prod', 'Production', 'Production'),
			('ProdLoss', '+', 0, 'Losses', 'Losses', 'Losses'),
				('Losses', '+', 0, 'LossTotal', 'Total Complex Physical Hydrocarbon Loss', 'Total Complex Physical Hydrocarbon Loss'),
				('Losses', '+', 0, 'LossFlare', 'Estimated Flare Losses', 'Estimated Flare Losses')

	) [t]([EntityParent], [Operator], [SortKey], [EntityTag], [EntityName], [EntityDetail]);

INSERT INTO [rpt].[Entities]
(
	[EntityTag],
	[EntityName],
	[EntityDetail]
)
SELECT
	[e].[EntityTag],
	[e].[EntityName],
	[e].[EntityDetail]
FROM
	@Entities			[e]
LEFT OUTER JOIN
	[rpt].[Entities]	[x]
		ON	[x].[EntityTag]	= [e].[EntityTag]
WHERE	[x].[EntityId] IS NULL;