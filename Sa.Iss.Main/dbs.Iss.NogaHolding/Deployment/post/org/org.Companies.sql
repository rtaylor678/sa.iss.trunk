﻿SET NOCOUNT ON;

DECLARE @Companies TABLE
(
	[CompanyTag]						VARCHAR(24)			NOT	NULL,	CHECK([CompanyTag] <> ''),
	[CompanyName]						NVARCHAR(48)		NOT	NULL,	CHECK([CompanyName] <> N''),
	[CompanyDetail]						NVARCHAR(96)		NOT	NULL,	CHECK([CompanyDetail] <> N''),

	[FacilityTag]						VARCHAR(24)			NOT	NULL,	CHECK([FacilityTag] <> ''),
																		UNIQUE CLUSTERED([FacilityTag] ASC),
	[FacilityName]						NVARCHAR(48)		NOT	NULL,	CHECK([FacilityName] <> N''),
																		UNIQUE NONCLUSTERED([FacilityName] ASC),
	[FacilityDetail]					NVARCHAR(96)		NOT	NULL,	CHECK([FacilityDetail] <> N''),
																		UNIQUE NONCLUSTERED([FacilityDetail] ASC),
	[FacilityParent]					VARCHAR(12)			NOT	NULL,	CHECK([FacilityParent] <> ''),
	[SortKey]							INT					NOT	NULL
)

INSERT INTO @Companies
(
	[CompanyTag],
	[CompanyName],
	[CompanyDetail],
	[FacilityTag],
	[FacilityName],
	[FacilityDetail],
	[FacilityParent],
	[SortKey]
)
SELECT
	[t].[CompanyTag],
	[t].[CompanyName],
	[t].[CompanyDetail],
	[t].[FacilityTag],
	[t].[FacilityName],
	[t].[FacilityDetail],
	[t].[FacilityParent],
	[t].[SortKey]
FROM (VALUES
	('Noga', 'National Oil and Gas Authority', 'National Oil and Gas Authority', 'Noga', 'National Oil and Gas Authority', 'National Oil and Gas Authority (All Members)', 'Noga', 1),
	('Gpic', 'Gulf Petrochemicals Industries Company', 'Gulf Petrochemicals Industries Company', 'GpicSitraIsland', 'Sitra Island, Bahrain', 'GPIC Sitra Island, Bahrain', 'Noga', 2),
	('Bafco', 'BAFCO', 'BAFCO', 'BafcoAirport', 'BAFCO Airport', 'BAFCO Airport', 'Noga', 3),
	('BapcoFuel', 'BAPCOF', 'BAPCOF', '161EUR', 'BAPCO Sitra Refinery', 'BAPCO Sitra Refinery', 'Noga', 4),
	('BapcoLube', 'BAPCOL', 'BAPCOL', '160LEV', 'BAPCO Sitra Lube Plant', 'BAPCO Sitra Lube Plant', 'Noga', 5)
	) [t]([CompanyTag], [CompanyName], [CompanyDetail], [FacilityTag], [FacilityName], [FacilityDetail], [FacilityParent], [SortKey]);

INSERT INTO [org].[Companies]
(
	[CompanyTag],
	[CompanyName],
	[CompanyDetail]
)
SELECT DISTINCT
	[t].[CompanyTag],
	[t].[CompanyName],
	[t].[CompanyDetail]
FROM
	@Companies			[t]
LEFT OUTER JOIN
	[org].[Companies]	[c]
		ON	[c].[CompanyTag]	= [t].[CompanyTag]
WHERE
	[c].[CompanyId]	IS NULL;

INSERT INTO [org].[Facilities]
(
	[FacilityTag],
	[FacilityName],
	[FacilityDetail]
)
SELECT
	[t].[FacilityTag],
	[t].[FacilityName],
	[t].[FacilityDetail]
FROM
	@Companies			[t]
LEFT OUTER JOIN
	[org].[Facilities]	[c]
		ON	[c].[FacilityTag]	= [t].[FacilityTag]
WHERE	[t].[FacilityTag]	<> 'Noga'
	AND	[c].[FacilityId]	IS NULL
ORDER BY
	[t].[SortKey];

INSERT INTO [org].[JoinCompaniesFacilities]
(
	[CompanyId],
	[FacilityId]
)
SELECT
	[c].[CompanyId],
	[f].[FacilityId]
FROM
	@Companies						[t]
INNER JOIN
	[org].[Companies]				[c]
		ON	[c].[CompanyTag]		= [t].[CompanyTag]
INNER JOIN
	[org].[Facilities]				[f]
		ON	[f].[FacilityTag]		= [t].[FacilityTag]
		AND	[f].[FacilityTag]		<> 'Noga'
LEFT OUTER JOIN
	[org].[JoinCompaniesFacilities]	[j]
		ON	[j].[CompanyId]			= [c].[CompanyId]
		AND	[j].[FacilityId]		= [f].[FacilityId]
WHERE
	[j].[JoinId]	IS NULL;

INSERT INTO [org].[Groups]
(
	[GroupTag],
	[GroupName],
	[GroupDetail]
)
SELECT
	[t].[FacilityTag],
	[t].[FacilityName],
	[t].[FacilityDetail]
FROM
	@Companies			[t]
LEFT OUTER JOIN
	[org].[Groups]		[c]
		ON	[c].[GroupTag]	= [t].[FacilityTag]
WHERE
	[c].[GroupId]	IS NULL
ORDER BY
	[t].[SortKey];

WITH CTE([FacilityTag], [FacilityParent], [Hierarchy])
AS
(
	SELECT
		[a].[FacilityTag],
		[a].[FacilityParent],
		SYS.HIERARCHYID::Parse('/' + CAST(ROW_NUMBER() OVER(ORDER BY [a].[SortKey]) AS VARCHAR) + '/')
	FROM
		@Companies	[a]
	WHERE
		[a].[FacilityTag] = [a].[FacilityParent]
	UNION ALL
	SELECT
		[d].[FacilityTag],
		[d].[FacilityParent],
		SYS.HIERARCHYID::Parse([t].[Hierarchy].ToString() + CAST(ROW_NUMBER() OVER(ORDER BY [d].[SortKey]) AS VARCHAR) + '/')
	FROM
		@Companies	[d]
	INNER JOIN
		CTE			[t]
			ON	[t].[FacilityTag] = [d].[FacilityParent]
	WHERE
		[d].[FacilityTag]	<> [d].[FacilityParent]
)
INSERT INTO [org].[GroupFacilities]
(
	[GroupId],
	[FacilityId]
)
SELECT
	--[Parent]	= [p].[FacilityTag],
	--[Child]		= [c].[FacilityTag],
	[Group]		= [g].[GroupId],
	[Facility]	= [f].[FacilityId]
FROM CTE	[p]
INNER JOIN
	CTE		[c]
		ON	[c].[Hierarchy].IsDescendantOf([p].[Hierarchy]) = 1
INNER JOIN
	[org].[Groups]				[g]
		ON	[g].[GroupTag]		= [p].[FacilityTag]
INNER JOIN
	[org].[Facilities]			[f]
		ON	[f].[FacilityTag]	= [c].[FacilityTag]
LEFT OUTER JOIN
	[org].[GroupFacilities]		[j]
		ON	[j].[GroupId]		= [g].[GroupId]
		AND	[j].[FacilityId]	= [f].[FacilityId]
WHERE
	[j].[GroupFacilityId]	IS NULL;