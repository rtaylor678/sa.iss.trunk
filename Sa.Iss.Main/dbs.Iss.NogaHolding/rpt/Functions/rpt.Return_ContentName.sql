﻿CREATE FUNCTION [rpt].[Return_ContentName]
(
	@ContentId	INT
)
RETURNS VARCHAR(24)
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @ContentName	NVARCHAR(24);

	SELECT TOP 1
		@ContentName = [c].[ContentName]
	FROM
		[rpt].[Content]	[c]
	WHERE
		[c].[ContentId]	= @ContentId;

	RETURN @ContentName;

END;