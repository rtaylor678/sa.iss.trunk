﻿CREATE FUNCTION [rpt].[Select_DataSourceParameters]
(
	@Schema		sysname	= NULL,
	@Object		sysname	= NULL
)
RETURNS TABLE
AS RETURN
SELECT
	[SchemaName]				= SCHEMA_NAME([o].[schema_id]),
	[ObjectName]				= [o].[name],
	[ObjectType]				= [o].[type],
	[ParameterId]				= [p].[parameter_id],
	[ParameterName]				= [p].[name],
	[ParameterType]				= TYPE_NAME([p].[user_type_id]),
	[ParameterLength]			= [p].[max_length],
	[ParameterPrecision]		= [p].[precision],
	[ParameterScale]			= [p].[scale],
	[ParameterHasDefault]		= [p].[has_default_value],
	[ParameterDefaultValue]		= [p].[default_value],
	[ParameterIsOut]			= [p].[is_output],
	[ParameterIsReadOnly]		= [p].[is_readonly],
	[ParameterIsXml]			= [p].[is_xml_document],
	[ParemeterXmlCollection]	= [p].[xml_collection_id],
	[ParameterIsCursorRef]		= [p].[is_cursor_ref]
FROM
	sys.objects		[o]
INNER JOIN
	sys.parameters	[p]
		ON	[p].[object_id]	= [o].[object_id]
WHERE	[o].[type]	IN 
		(	--	https://msdn.microsoft.com/en-us/library/ms190324.aspx
			'AF', 'FN', 'FS', 'FT', 'IF', 'P', 'PC',	-- SQL 
			'TF', 'X'									-- SQL Server 2012 - 2016
		)
	AND	SCHEMA_NAME([o].[schema_id])	= @Schema
	AND	[o].[name]						= @Object;