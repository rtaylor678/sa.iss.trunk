﻿CREATE FUNCTION [rpt].[Return_EntityId]
(
	@EntityTag	VARCHAR(48)
)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN

	DECLARE @EntityId	INT;

	SELECT TOP 1
		@EntityId = [a].[EntityId]
	FROM
		[rpt].[Entities]	[a]
	WHERE
		[a].[EntityTag]	= @EntityTag;

	RETURN @EntityId;

END;