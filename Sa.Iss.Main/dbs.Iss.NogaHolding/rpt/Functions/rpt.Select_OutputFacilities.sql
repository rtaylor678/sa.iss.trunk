﻿CREATE FUNCTION [rpt].[Select_OutputFacilities]
(
	@Entities			[rpt].[TypeEntities]	READONLY,
	@Attributes			[rpt].[TypeAttributes]	READONLY,

	@SubmissionId		INT,
	@MethodologyId		INT,
	@CurrencyId			INT
)
RETURNS TABLE
WITH SCHEMABINDING
AS RETURN
SELECT
		[SubmissionId]		= @SubmissionId,
		[MethodologyId]		= @MethodologyId,
		[CurrencyId]		= @CurrencyId,
	[e].[EntityId],
	[x].[EntitySortKey],
	[e].[EntityTag],
	[e].[EntityName],
	[e].[EntityDetail],
	[a].[AttributeId],
	[y].[AttributeSortKey],
	[a].[AttributeTag],
	[a].[AttributeName],
	[a].[AttributeDetail],
	[o].[UnitId],
	[o].[Value]
FROM
	[rpt].[Entities]				[e]
INNER JOIN
	@Entities						[x]
		ON	[x].[EntityId]			= [e].[EntityId]
CROSS APPLY 
	[rpt].[Attributes]				[a]
INNER JOIN
	@Attributes						[y]
		ON	[y].[AttributeId]		= [a].[AttributeId]
LEFT OUTER JOIN
	[rpt].[OutputFacilities]		[o]
		ON	[o].[EntityId]			= [e].[EntityId]
		AND	[o].[AttributeId]		= [a].[AttributeId]
		AND	[o].[SubmissionId]		= @SubmissionId
		AND	[o].[MethodologyId]		= @MethodologyId
		AND	[o].[CurrencyId]		= @CurrencyId;