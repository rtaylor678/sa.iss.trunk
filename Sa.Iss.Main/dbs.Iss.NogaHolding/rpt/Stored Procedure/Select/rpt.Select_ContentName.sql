﻿CREATE PROCEDURE [rpt].[Select_ContentName]
(
	@ContentId	INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @ContentName	NVARCHAR(24)	= [rpt].[Return_ContentName](@ContentId);

		SELECT [ContentName] = @ContentName;
		RETURN @ContentName;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @ContentId = COALESCE(@ContentId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @ContentId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;