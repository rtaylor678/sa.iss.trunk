﻿CREATE PROCEDURE [rpt].[Select_ContentAnnotations]
(
	@ContentId	INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		SELECT
			[a].[ContentAnnotationId],
			[a].[ContentId],
			[l].[AnnotationsLevelId],
			[l].[AnnotationsLevelTag],
			--[l].[AnnotationsLevelName],
			--[l].[AnnotationsLevelDetail],
			[l].[AnnotationLevelStyleId],
			[t].[AnnotationsTextId],
			[t].[AnnotationsTextTag]
			--[t].[AnnotationsTextName],
			--[t].[AnnotationsTextDetail]
		FROM
			[rpt].[ContentAnnotations]		[a]
		INNER JOIN
			[rpt].[AnnotationsLevel]		[l]
				ON	[l].[AnnotationsLevelId]	= [a].[AnnotationLevelId]
		INNER JOIN
			[rpt].[AnnotationsText]			[t]
				ON	[t].[AnnotationsTextId]		= [a].[AnnotationTextId]
		WHERE
			[a].[ContentId]	= @ContentId;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @ContentId = COALESCE(@ContentId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @ContentId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;