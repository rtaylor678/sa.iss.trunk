﻿CREATE PROCEDURE [rpt].[Select_DeliverablesContent]
(
	@DeliverableId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		SELECT
			[c].[DeliverablesContentId],
			[c].[DeliverableId],
			[c].[ItemNumber],
			[c].[ContentId]
		FROM
			[rpt].[DeliverablesContent]	[c]
		WHERE
			[c].[DeliverableId]	= @DeliverableId;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @DeliverableId = COALESCE(@DeliverableId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @DeliverableId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;