﻿CREATE PROCEDURE [rpt].[Select_ContentSection]
(
	@ContentId	INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		SELECT
			[s].[ContentSectionId],
			[s].[ContentId],
			[s].[SectionNumber],
			[s].[DataSourceId]
		FROM
			[rpt].[ContentSection]	[s]
		WHERE
			[s].[ContentId]	= @ContentId;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @ContentId = COALESCE(@ContentId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @ContentId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;