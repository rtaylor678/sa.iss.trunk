﻿CREATE PROCEDURE [rpt].[Select_OutputFacilitiesTurnaround]
(
	@SubmissionId		INT,
	@MethodologyId		INT,
	@CurrencyId			INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Entities	[rpt].[TypeEntities];
		DECLARE @Attributes	[rpt].[TypeAttributes];

		INSERT INTO @Entities
		(
			[EntityId],
			[EntitySortKey]
		)
		VALUES
			([rpt].[Return_EntityId]('OSPreDeSulfur'),			10),
			([rpt].[Return_EntityId]('OSAmmoniaSyngas'),		20),
			([rpt].[Return_EntityId]('OSAmmoniaRefrig'),		30),
			([rpt].[Return_EntityId]('OSMethanolSyngas'),		40),
			([rpt].[Return_EntityId]('OSMethanolDist'),			50),
			([rpt].[Return_EntityId]('OSUrea'),					60),
			([rpt].[Return_EntityId]('OSPsaH2Pur'),				70),
			([rpt].[Return_EntityId]('OSFlueGasCo2Recov'),		80),
			([rpt].[Return_EntityId]('OSDeSal'),				90),
			([rpt].[Return_EntityId]('ProcUnitsOnSite'),		100),

			([rpt].[Return_EntityId]('ProcUnitsUOSL'),			110),
			([rpt].[Return_EntityId]('UOSLCogeneration'),		120),
			([rpt].[Return_EntityId]('UOSLOthSteamElec'),		130),
			([rpt].[Return_EntityId]('UOSLTankage'),			140),

			([rpt].[Return_EntityId]('ProcUnitsUOSLOther'),		150),
			([rpt].[Return_EntityId]('UOSLOtherUtil'),			160),
			([rpt].[Return_EntityId]('UOSLUreaWarehouse'),		170),
			([rpt].[Return_EntityId]('UOSLUreaPackage'),		180),
			([rpt].[Return_EntityId]('UOSLUreaConveyor'),		190),
			([rpt].[Return_EntityId]('UOSLMarine'),				200),
			([rpt].[Return_EntityId]('UOSLEnviro'),				210),
			([rpt].[Return_EntityId]('UOSLOtherOff'),			220),

			([rpt].[Return_EntityId]('ProcUnits'),				230);

		INSERT INTO @Attributes
		(
			[AttributeId],
			[AttributeSortKey]
		)
		VALUES
			([rpt].[Return_AttributeId]('TaIntervalYear'),		10),

			([rpt].[Return_AttributeId]('TaDowntimeDurRpt'),	20),
			([rpt].[Return_AttributeId]('TaDowntimeCostRpt'),	30),
			([rpt].[Return_AttributeId]('TaDowntimeWorkRpt'),	40),

			([rpt].[Return_AttributeId]('TaDowntimeDurAnn'),	50),
			([rpt].[Return_AttributeId]('TaDowntimeCostAnn'),	60),
			([rpt].[Return_AttributeId]('TaDowntimeWorkAnn'),	70);

		SELECT
			[t].[SubmissionId],
			[t].[MethodologyId],
			[t].[CurrencyId],
			[t].[EntityId],
			[t].[EntitySortKey],
			[t].[EntityTag],
			[t].[EntityName],
			[t].[EntityDetail],
			[t].[AttributeId],
			[t].[AttributeSortKey],
			[t].[AttributeTag],
			[t].[AttributeName],
			[t].[AttributeDetail],
			[t].[UnitId],
			[t].[Value]
		FROM
			[rpt].[Select_OutputFacilities](@Entities, @Attributes, @SubmissionId, @MethodologyId, @CurrencyId) [t];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;