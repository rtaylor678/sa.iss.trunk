﻿CREATE PROCEDURE [rpt].[Select_OutputFacilitiesYield]
(
	@SubmissionId		INT,
	@MethodologyId		INT,
	@CurrencyId			INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Entities	[rpt].[TypeEntities];
		DECLARE @Attributes	[rpt].[TypeAttributes];

		INSERT INTO @Entities
		(
			[EntityId],
			[EntitySortKey]
		)
		VALUES
			([rpt].[Return_EntityId]('H2'),				 10),
			([rpt].[Return_EntityId]('CH4'),			 20),
			([rpt].[Return_EntityId]('C2H6'),			 30),
			([rpt].[Return_EntityId]('C3H8'),			 40),
			([rpt].[Return_EntityId]('C4H10+C5s'),		 50),
			([rpt].[Return_EntityId]('CO'),				 60),
			([rpt].[Return_EntityId]('CO2'),			 70),
			([rpt].[Return_EntityId]('H2S'),			 80),
			([rpt].[Return_EntityId]('N2'),				 90),
			([rpt].[Return_EntityId]('Ar+Other'),		100),
			([rpt].[Return_EntityId]('CompTotal'),		110);

		INSERT INTO @Attributes
		(
			[AttributeId],
			[AttributeSortKey]
		)
		VALUES
			([rpt].[Return_AttributeId]('Mole_Pcnt'),		10),
			([rpt].[Return_AttributeId]('Mass_MW'),			20),
			([rpt].[Return_AttributeId]('Mass_Tonnes'),		30),
			([rpt].[Return_AttributeId]('Weight_Pcnt'),		40),
			([rpt].[Return_AttributeId]('LHV_kJNm3'),		50),
			([rpt].[Return_AttributeId]('LHV_GJd'),			60);

		SELECT
			[t].[SubmissionId],
			[t].[MethodologyId],
			[t].[CurrencyId],
			[t].[EntityId],
			[t].[EntitySortKey],
			[t].[EntityTag],
			[t].[EntityName],
			[t].[EntityDetail],
			[t].[AttributeId],
			[t].[AttributeSortKey],
			[t].[AttributeTag],
			[t].[AttributeName],
			[t].[AttributeDetail],
			[t].[UnitId],
			[t].[Value]
		FROM
			[rpt].[Select_OutputFacilities](@Entities, @Attributes, @SubmissionId, @MethodologyId, @CurrencyId) [t];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;