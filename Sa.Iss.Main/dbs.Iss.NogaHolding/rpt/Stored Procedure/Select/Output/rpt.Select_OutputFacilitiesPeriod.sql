﻿CREATE PROCEDURE [rpt].[Select_OutputFacilitiesPeriod]
(
	@SubmissionId		INT,
	@MethodologyId		INT,
	@CurrencyId			INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Entities	[rpt].[TypeEntities];
		DECLARE @Attributes	[rpt].[TypeAttributes];

		INSERT INTO @Entities
		(
			[EntityId],
			[EntitySortKey]
		)
		VALUES
			([rpt].[Return_EntityId]('DivPeriod'),		10);

		INSERT INTO @Attributes
		(
			[AttributeId],
			[AttributeSortKey]
		)
		VALUES
			([rpt].[Return_AttributeId]('RptPerName'),					10),
			([rpt].[Return_AttributeId]('RptPerBeg_Date'),				20),
			([rpt].[Return_AttributeId]('RptPerDuration_Days'),			30),
			([rpt].[Return_AttributeId]('RptPerDuration_PcntYear'),		40),
			([rpt].[Return_AttributeId]('RptPerDuration_Multiplier'),	50),
			([rpt].[Return_AttributeId]('RptPerDaysInYear'),			60);

		SELECT
			[t].[SubmissionId],
			[t].[MethodologyId],
			[t].[CurrencyId],
			[t].[EntityId],
			[t].[EntitySortKey],
			[t].[EntityTag],
			[t].[EntityName],
			[t].[EntityDetail],
			[t].[AttributeId],
			[t].[AttributeSortKey],
			[t].[AttributeTag],
			[t].[AttributeName],
			[t].[AttributeDetail],
			[t].[UnitId],
			[t].[Value]
		FROM
			[rpt].[Select_OutputFacilities](@Entities, @Attributes, @SubmissionId, @MethodologyId, @CurrencyId) [t];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;