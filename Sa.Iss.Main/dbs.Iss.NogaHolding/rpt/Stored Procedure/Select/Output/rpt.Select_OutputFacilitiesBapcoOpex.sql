﻿CREATE PROCEDURE [rpt].[Select_OutputFacilitiesBapcoOpex]
(
	@SubmissionId		INT,
	@MethodologyId		INT,
	@CurrencyId			INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @Entities	[rpt].[TypeEntities];
		DECLARE @Attributes	[rpt].[TypeAttributes];

		INSERT INTO @Entities
		(
			[EntityId],
			[EntitySortKey]
		)
		VALUES
			([rpt].[Return_EntityId]('OpexEmployeeSWB'),			 20),
			([rpt].[Return_EntityId]('OpexContMaintLaborExp'),			 30),
			([rpt].[Return_EntityId]('OpexOthContSvcs'),			 40),
			([rpt].[Return_EntityId]('OpexTAAdjustment'),				 50),
			([rpt].[Return_EntityId]('OpexOtherNonVolExp'),			 60),
			([rpt].[Return_EntityId]('OpexUtilG&APersCost'),			 70),
			([rpt].[Return_EntityId]('OpexPurchasedEnergy'),				 80),
			([rpt].[Return_EntityId]('OpexProducedEnergy'),		 90),	--	
			([rpt].[Return_EntityId]('OpexPurchEnergyNonElecSteam'),		100);

		INSERT INTO @Attributes
		(
			[AttributeId],
			[AttributeSortKey]
		)
		VALUES
			([rpt].[Return_AttributeId]('OpexkUSD'),		10),
			([rpt].[Return_AttributeId]('Opex100USDperbbl'),	20),
			([rpt].[Return_AttributeId]('Opex100USD/UEDC'),	30);

		SELECT
			[t].[SubmissionId],
			[t].[MethodologyId],
			[t].[CurrencyId],
			[t].[EntityId],
			[t].[EntitySortKey],
			[t].[EntityTag],
			[t].[EntityName],
			[t].[EntityDetail],
			[t].[AttributeId],
			[t].[AttributeSortKey],
			[t].[AttributeTag],
			[t].[AttributeName],
			[t].[AttributeDetail],
			--[t].[UnitId],
			[t].[Value]
		FROM
			[rpt].[Select_OutputFacilities](@Entities, @Attributes, @SubmissionId, @MethodologyId, @CurrencyId) [t];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @SubmissionId = COALESCE(@SubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @SubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;