﻿CREATE PROCEDURE [rpt].[Inject_Fuels_OutputFacilitiesOpEx]
(
	@fctSubmissionId		INT,  
	@profileSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],  --referntial integrity with fact table
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionId]		= @fctSubmissionId,
				[MethodologyId],
				[CurrencyId],
				[EntityId]			= [rpt].[Return_EntityId]([u].[EntityTag]),
			--[u].[EntityTag],
				[AttributeId]		= [rpt].[Return_AttributeId]('RptExp'),
			[u].[RptExp]
		FROM(
			SELECT
				[o].[SubmissionID],
				[MethodologyId]				= [ante].[Return_MethodologyId]('2015'),
				[CurrencyId]				= [dim].[Return_CurrencyId]('USD'),
				[GeneralMaintCompanyLabor]	= [o].[OCCSal],			--	[Emp SWB]
				[MaintRoutContractLabor]	= [o].[ContMaintLabor],	--	[Maint Cont. Labor]
				[GeneralContractLabor]		= [o].[OthCont],		--	[Misc. Contact Svc.]
				[OpEx]						= [o].[STNonVol],		--	[Total OpEx]
				[AllocGAPers]				= [o].[GAPers]			--	[Alloc. GA. Prs. Costs]
			FROM
				[$(dbProfile)].[dbo].[OpExAll]	[o]
			WHERE	[o].[SubmissionID]	= @profileSubmissionId
			) [p]
			UNPIVOT (
				[RptExp] FOR [EntityTag] IN (
					[GeneralMaintCompanyLabor],
					[MaintRoutContractLabor], 
					[GeneralContractLabor],
					[OpEx],
					[AllocGAPers]
				)
			) [u];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @profileSubmissionId = COALESCE(@profileSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @profileSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;