﻿CREATE PROCEDURE [rpt].[Inject_Fuels_OutputFacilitiesTurnaround]
	@fctSubmissionId		INT,
	@profileSubmissionId		INT
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY
	SET NOCOUNT ON;

		--INSERT INTO [rpt].[OutputFacilities]
		--(
		--	[SubmissionId],
		--	[MethodologyId],
		--	[CurrencyId],
		--	[EntityId],
		--	[UnitId],
		--	[AttributeId],
		--	[Value_Float]
		--)


	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @profileSubmissionId = COALESCE(@profileSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @profileSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
