﻿CREATE PROCEDURE [rpt].[Inject_Fuels_OutputFacilitiesSummary_Pci]
(
	@fctSubmissionId			INT,
	@profileSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @FactorSetID			CHAR(8)		= '2015';
		DECLARE @CurrencyId				CHAR(4)		= 'USD';

		DECLARE @profileFactorSetID		CHAR(8)		= '2012';
		DECLARE @profileScenarioId		CHAR(8)		= 'CLIENT';
		DECLARE @profileCurrencyId		CHAR(4)		= 'USD';
		DECLARE @profileDataTypeId		CHAR(6)		= 'ADJ';
		DECLARE @profileUomId			CHAR(3)		= 'MET';

		--	43	Personnel Cost Index, $/EDC
		INSERT INTO [$(DatabaseName)].[rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionID]		= @fctSubmissionId,
				[MethodologyId]		= [$(DatabaseName)].[ante].[Return_MethodologyId](@FactorSetID),
				[CurrencyId]		= [$(DatabaseName)].[dim].[Return_CurrencyId](@CurrencyId),
				[EntityId]			= [$(DatabaseName)].[rpt].[Return_EntityId]('PersIdx'),
				[AttributeId]		= [$(DatabaseName)].[rpt].[Return_AttributeId]('PersIdx'),
			[p].[PersCost]
		FROM
			[$(dbProfile)].[dbo].[OpExCalc]	[p]
		WHERE	[p].[FactorSet]		= @profileFactorSetID
			AND	[p].[Scenario]		= @profileScenarioId
			AND	[p].[Currency]		= @profileCurrencyId
			AND	[p].[DataType]		= 'EDC'
			AND	[p].[SubmissionID]	= @profileSubmissionId;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @profileSubmissionId = COALESCE(@profileSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @profileSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;