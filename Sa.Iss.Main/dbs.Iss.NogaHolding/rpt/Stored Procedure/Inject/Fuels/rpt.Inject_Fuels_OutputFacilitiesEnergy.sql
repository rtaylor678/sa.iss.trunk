﻿
CREATE PROCEDURE [rpt].[Inject_Fuels_OutputFacilitiesEnergy]
	@fctSubmissionId		INT,
	@profileSubmissionId		INT
AS
BEGIN
	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY
		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],   --referntial integrity with fact table
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
			[SubmissionId]	= @fctSubmissionId,
			[MethodologyId]	= [ante].[Return_MethodologyId]('2015'),
			[CurrencyId]	= [dim].[Return_CurrencyId]('USD'),
		[e].[EntityId],
			[AttributeId]	= [rpt].[Return_AttributeId]('EnergyConsGJ'),
			[Amount_GJ]		= COALESCE([p].[SourceMBTU], [p].[UsageMBTU]) / 0.94708628903179 
FROM
	[$(dbProfile)].[dbo].[Energy]	[p]
INNER JOIN
		[etl].[Energy]		[e]
		ON	[e].[EnergyType]		= [p].[EnergyType]
		AND	[e].[TransType]			= [p].[TransType]
WHERE	[p].[SubmissionID]			= @profileSubmissionId;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @profileSubmissionId = COALESCE(@profileSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @profileSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;
