﻿CREATE PROCEDURE [rpt].[Inject_Chem_OutputFacilitiesReliabilityAvailability]
(
	@fctSubmissionId		INT,
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[e].[EntityId],
			[u].[AttributeId],
			[u].[Value]
		FROM(
			SELECT
				[r].[SubmissionId],
					[MethodologyId]			= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]			= [dim].[Return_CurrencyId]('USD'),
					[CurrencyName]			= 'USD',
					[AttributeId]			= [rpt].[Return_AttributeId]('AvailMech'),
					[AttributeName]			= 'AvailMech',

					[AvailMech]				= (1.0 - ([r].[EdcMechanical_Hours]	+ [t].[EdcDowntime_HoursAlloc]) / [p].[Duration_Hours]) * 100.0
					--[AvailOper]				= (1.0 - ([r].[EdcTotal_Hours]		+ [t].[EdcDowntime_HoursAlloc]) / [p].[Duration_Hours]) * 100.0
			FROM
				[xls].[ReliabilityAllocation_Total]		[r]
			INNER JOIN
				[xls].[TurnaroundEdc_Total]				[t]
					ON	[t].[SubmissionId]				= [r].[SubmissionId]
					AND	[t].[Duration_Type]				= 'Annualized'
			INNER JOIN
				[xls].[ReportingPeriod]					[p]
					ON	[p].[SubmissionId]				= [r].[SubmissionId]
			WHERE
				[t].[SubmissionId]		= @stgSubmissionId
			) [t]
			UNPIVOT (
				[Value] FOR [EntityTag] IN ([AvailMech])
			) [u]
		INNER JOIN
			[rpt].[Entities]	[e]
				ON	[e].[EntityTag]	= [u].[EntityTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [u].[AttributeId]
				AND	[r].[EntityId]		= [e].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[e].[EntityId],
			[u].[AttributeId],
			[u].[Value]
		FROM(
			SELECT
				[r].[SubmissionId],
					[MethodologyId]			= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]			= [dim].[Return_CurrencyId]('USD'),
					[CurrencyName]			= 'USD',
					[AttributeId]			= [rpt].[Return_AttributeId]('AvailOper'),
					[AttributeName]			= 'AvailOper',

					--[AvailMech]				= (1.0 - ([r].[EdcMechanical_Hours]	+ [t].[EdcDowntime_HoursAlloc]) / [p].[Duration_Hours]) * 100.0,
					[AvailOper]				= (1.0 - ([r].[EdcTotal_Hours]		+ [t].[EdcDowntime_HoursAlloc]) / [p].[Duration_Hours]) * 100.0
			FROM
				[xls].[ReliabilityAllocation_Total]		[r]
			INNER JOIN
				[xls].[TurnaroundEdc_Total]				[t]
					ON	[t].[SubmissionId]				= [r].[SubmissionId]
					AND	[t].[Duration_Type]				= 'Annualized'
			INNER JOIN
				[xls].[ReportingPeriod]					[p]
					ON	[p].[SubmissionId]				= [r].[SubmissionId]
			WHERE
				[t].[SubmissionId]		= @stgSubmissionId
			) [t]
			UNPIVOT (
				[Value] FOR [EntityTag] IN ([AvailOper])
			) [u]
		INNER JOIN
			[rpt].[Entities]	[e]
				ON	[e].[EntityTag]	= [u].[EntityTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [u].[AttributeId]
				AND	[r].[EntityId]		= [e].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;