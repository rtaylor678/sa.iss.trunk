﻿CREATE PROCEDURE [rpt].[Inject_Chem_OutputFacilitiesTurnaroundAlloc]
(
	@fctSubmissionId		INT,
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[u].[EntityId],
			[a].[AttributeId],
			[u].[Value_Float]
		FROM(
			SELECT
				[t].[SubmissionId],
					[MethodologyId]				= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]				= [dim].[Return_CurrencyId]('USD'),
					[CurrencyName]				= 'USD',
				[r].[EntityId],
				[r].[EntityName],

					[TaDowntimeCostAlloc]		= [t].[Cost_kUsdAlloc],
					[TaDowntimeWorkAlloc]		= [t].[Work_HoursAlloc]

			FROM
				[xls].[TurnaroundAllocation]		[t]
			INNER JOIN
				[etl].[ProcessUnits]				[e]
					ON	[e].[XlsProcessUnitDetail]	= [t].[ProcessUnitDetail]
			INNER JOIN
				[rpt].[Entities]					[r]
					ON	[r].[EntityTag]				= [e].[ProcessUnitTag]
			WHERE	[t].[Duration_Type]				= 'Annualized'
				AND	[t].[SubmissionId]				= @stgSubmissionId
			) [t]
			UNPIVOT (
				[Value_Float] FOR [AttributeTag] IN ([TaDowntimeCostAlloc], [TaDowntimeWorkAlloc])
			) [u]
		INNER JOIN
			[rpt].[Attributes]	[a]
				ON	[a].[AttributeTag]	= [u].[AttributeTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [a].[AttributeId]
				AND	[r].[EntityId]		= [u].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;