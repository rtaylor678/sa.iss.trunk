﻿CREATE PROCEDURE [rpt].[Inject_Chem_OutputFacilitiesTurnaround]
(
	@fctSubmissionId		INT,
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[u].[EntityId],
			[a].[AttributeId],
			[u].[Value_Float]
		FROM(
			SELECT
				[t].[SubmissionId],
					[MethodologyId]			= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]			= [dim].[Return_CurrencyId]('USD'),
					[CurrencyName]			= 'USD',
				[r].[EntityId],
				[r].[EntityName],

					[TaIntervalYear]		= [t].[Interval_Years],
					[TaDowntimeDurRpt]		= [t].[Downtime_Hours],
					[TaDowntimeCostRpt]		= [t].[Cost_kUsd],
					[TaDowntimeWorkRpt]		= [t].[Work_Hours]

			FROM
				[xls].[Turnaround]					[t]
			INNER JOIN
				[etl].[ProcessUnits]				[e]
					ON	[e].[XlsProcessUnitDetail]	= [t].[ProcessUnitDetail]
			INNER JOIN
				[rpt].[Entities]					[r]
					ON	[r].[EntityTag]				= [e].[ProcessUnitTag]
			WHERE	[t].[Duration_Type]		= 'Reported'
				AND	[t].[SubmissionId]		= @stgSubmissionId
			) [t]
			UNPIVOT (
				[Value_Float] FOR [AttributeTag] IN ([TaIntervalYear], [TaDowntimeDurRpt], [TaDowntimeCostRpt], [TaDowntimeWorkRpt])
			) [u]
		INNER JOIN
			[rpt].[Attributes]	[a]
				ON	[a].[AttributeTag]	= [u].[AttributeTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [a].[AttributeId]
				AND	[r].[EntityId]		= [u].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Text]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[u].[EntityId],
			[a].[AttributeId],
			[u].[Value]
		FROM(
			SELECT
				[t].[SubmissionId],
					[MethodologyId]		= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]		= [dim].[Return_CurrencyId]('USD'),
					[CurrencyName]		= 'USD',
				[r].[EntityId],
				[r].[EntityName],

				[TaDowntimeDurAnn]		= [t].[Downtime_Hours],
				[TaDowntimeCostAnn]		= [t].[Cost_kUsd],
				[TaDowntimeWorkAnn]		= [t].[Work_Hours]

			FROM
				[xls].[Turnaround]					[t]
			INNER JOIN
				[etl].[ProcessUnits]				[e]
					ON	[e].[XlsProcessUnitDetail]	= [t].[ProcessUnitDetail]
			INNER JOIN
				[rpt].[Entities]					[r]
					ON	[r].[EntityTag]				= [e].[ProcessUnitTag]
			WHERE	[t].[Duration_Type]		= 'Annualized'
				AND	[t].[SubmissionId]		= @stgSubmissionId
			) [t]
			UNPIVOT (
				[Value] FOR [AttributeTag] IN ([TaDowntimeDurAnn], [TaDowntimeCostAnn], [TaDowntimeWorkAnn])
			) [u]
		INNER JOIN
			[rpt].[Attributes]	[a]
				ON	[a].[AttributeTag]	= [u].[AttributeTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [a].[AttributeId]
				AND	[r].[EntityId]		= [u].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;