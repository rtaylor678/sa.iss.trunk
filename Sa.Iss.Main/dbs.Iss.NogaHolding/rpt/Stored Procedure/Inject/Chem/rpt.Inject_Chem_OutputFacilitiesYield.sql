﻿CREATE PROCEDURE [rpt].[Inject_Chem_OutputFacilitiesYield]
(
	@fctSubmissionId		INT,
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[u].[EntityId],
			[a].[AttributeId],
			[u].[Value_Float]
		FROM (
			SELECT
				[t].[SubmissionId],
					[MethodologyId]		= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]		= [dim].[Return_CurrencyId]('USD'),
				[t].[ComponentDetail],
				[e].[EntityId],
				[t].[Mole_Pcnt],
					[Mass_MW]			= [t].[Mass_MolecularWeight],
				[t].[Weight_Pcnt],
					[LHV_kJNm3]			= [t].[LowerHeatingValue_kJNm3],
				[t].[Mass_Tonnes],
					[LHV_GJd]			= [t].[LowerHeatingValue_GJDay]
			FROM
				[xls].[YieldAllocation]				[t]
			INNER JOIN
				[etl].[Composition]					[e]
					ON	[e].[XlsComponentDetail]	= [t].[ComponentDetail]
			WHERE	[t].[SubmissionId]	= @stgSubmissionId
			) [t]
			UNPIVOT (
				[Value_Float] FOR [AttributeTag] IN ([Mole_Pcnt], [Mass_MW], [Weight_Pcnt], [LHV_kJNm3], [Mass_Tonnes], [LHV_GJd])
			) [u]
		INNER JOIN
			[rpt].[Attributes]		[a]
				ON	[a].[AttributeTag] = [u].[AttributeTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [a].[AttributeId]
				AND	[r].[EntityId]		= [u].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[u].[EntityId],
			[a].[AttributeId],
			[u].[Value_Float]
		FROM (
			SELECT
				[y].[SubmissionId],
					[MethodologyId]		= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]		= [dim].[Return_CurrencyId]('USD'),
					[EntityId]			= [rpt].[Return_EntityId]('Hydrocarbons'),
					[Mole_Pcnt]			= SUM([y].[Mole_Pcnt]),
					[Mass_MW]			= SUM([y].[Mole_Pcnt] * [y].[Mass_MolecularWeight])		/ SUM([y].[Mole_Pcnt]),
					[Weight_Pcnt]		= SUM([y].[Weight_Pcnt]),
					[LHV_kJNm3]			= SUM([y].[Mole_Pcnt] * [y].[LowerHeatingValue_kJNm3])	/ SUM([y].[Mole_Pcnt]),
					[Mass_Tonnes]		= SUM([y].[Mass_Tonnes]),
					[HVCYield_Pcnt]		= [d].[Value] / SUM([y].[Mass_Tonnes]) * 100.0
			FROM
				[xls].[YieldAllocation]				[y]
			INNER JOIN
				[etl].[Composition]					[e]
					ON	[e].[XlsComponentDetail]	= [y].[ComponentDetail]
			INNER JOIN
				[xls].[Divisors]					[d]
					ON	[d].[SubmissionId]			= [y].[SubmissionId]
			INNER JOIN
				[etl].[Divisors]					[z]
					ON	[z].[XlsDivisorDetail]		= [d].[DivisorDetail]
					AND	[z].DivisorTag				= 'DivProd'
			WHERE	[y].[SubmissionId]	= @stgSubmissionId
				AND	[e].[ComponentTag] IN ('H2', 'CH4', 'C2H6', 'C3H8', 'C4H10+C5s')
			GROUP BY
				[y].[SubmissionId],
				[d].[Value]
			) [t]
			UNPIVOT (
				[Value_Float] FOR [AttributeTag] IN ([Mole_Pcnt], [Mass_MW], [Weight_Pcnt], [LHV_kJNm3], [Mass_Tonnes], [HVCYield_Pcnt])
			) [u]
		INNER JOIN
			[rpt].[Attributes]		[a]
				ON	[a].[AttributeTag] = [u].[AttributeTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [a].[AttributeId]
				AND	[r].[EntityId]		= [u].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[u].[EntityId],
			[a].[AttributeId],
			[u].[Value_Float]
		FROM (
			SELECT
				[l].[SubmissionId],
					[MethodologyId]		= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]		= [dim].[Return_CurrencyId]('USD'),
				[e].[EntityId],
				[l].[Quantity_TonnesMonth],
				[y].[Mass_Tonnes],
					[Loss_True]			= [l].[Quantity_TonnesMonth] - COALESCE([y].[Mass_Tonnes], 0.0),
				[i].[Quantity_TonnesDay],
					[Loss_PcntFeed]		= ([l].[Quantity_TonnesMonth] - COALESCE([y].[Mass_Tonnes], 0.0)) / CONVERT(FLOAT, [p].[Duration_Days]) / [i].[Quantity_TonnesDay] * 100.0
			FROM
				[stg].[ChemLosses]					[l]
			INNER JOIN
				[etl].[Streams]						[e]
					ON	[e].[XlsStreamDetail]		= [l].[LossDetail]
					AND	[e].[StreamTag]				= 'LossTotal'
			INNER JOIN
				[xls].[YieldAllocation]				[y]
					ON	[y].[SubmissionId]			= [l].[SubmissionId]
			INNER JOIN
				[etl].[Composition]					[c]
					ON	[c].[XlsComponentDetail]	= [y].[ComponentDetail]
					AND	[c].[ComponentTag]			= 'H2S'
			INNER JOIN
				[xls].[InputRaw]					[i]
					ON	[i].[SubmissionId]			= [l].[SubmissionId]
			INNER JOIN
				[etl].[Streams]						[s]
					ON	[s].[XlsStreamDetail]		= [i].[InputRawDetail]
					AND	[s].[StreamTag]				= 'FeedRaw'
			INNER JOIN
				[xls].[ReportingPeriod]	[p]
					ON	[p].[SubmissionId]			= [l].[SubmissionId]
			WHERE
				[l].[SubmissionId]					= @stgSubmissionId
				) [t]
			UNPIVOT (
				[Value_Float] FOR [AttributeTag] IN ([Quantity_TonnesMonth], [Mass_Tonnes], [Loss_True], [Quantity_TonnesDay], [Loss_PcntFeed])
			) [u]
		LEFT OUTER JOIN
			[rpt].[Attributes]		[a]
				ON	[a].[AttributeTag] = [u].[AttributeTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [a].[AttributeId]
				AND	[r].[EntityId]		= [u].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[u].[EntityId],
			[a].[AttributeId],
			[u].[Value_Float]
		FROM (
			SELECT
				[t].[SubmissionId],
					[MethodologyId]			= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]			= [dim].[Return_CurrencyId]('USD'),
					[EntityId]				= COALESCE([r].[EntityId], [rpt].[Return_EntityId]('ProcUnitsOnSite')),
					[Quantity_TonnesDay]	= SUM([t].[UtilizedCapacity]),
					[EnergyConsGJTonne]		= CASE WHEN [r].[EntityId] IS NOT NULL
												THEN
													SUM([s].[EnergyStandard_GJTonne])
												ELSE
													SUM([t].[UtilizedCapacity] * [s].[EnergyStandard_GJTonne])
														/ SUM([t].[UtilizedCapacity])
												END,
					[EnergyConsGJDay]		= SUM([t].[UtilizedCapacity] * [s].[EnergyStandard_GJTonne])

			FROM
				[xls].[Eii]							[t]
			INNER JOIN
				[etl].[ProcessUnits]				[e]
					ON	[e].[XlsProcessUnitDetail]	= [t].[ProcessUnitDetail]
			INNER JOIN
				[rpt].[Entities]					[r]
					ON	[r].[EntityTag]				= [e].[ProcessUnitTag]
					AND	[r].[EntityTag]				IN ('OSAmmoniaRefrig', 'OSMethanolDist', 'OSUrea')
			INNER JOIN
				[ante].[EnergyStandard]				[s]
					ON	[s].[EntityId]				= [r].[EntityId]
					AND	[s].AttributeId				= [rpt].[Return_AttributeId]('EnergyConsGJDay')
			WHERE
				[t].[SubmissionId]					= @stgSubmissionId
			GROUP BY
				[t].[SubmissionId],
				ROLLUP([r].[EntityId])
			) [t]
			UNPIVOT (
				[Value_Float] FOR [AttributeTag] IN ([Quantity_TonnesDay], [EnergyConsGJTonne], [EnergyConsGJDay])
			) [u]
		INNER JOIN
			[rpt].[Attributes]	[a]
				ON	[a].[AttributeTag]	= [u].[AttributeTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [a].[AttributeId]
				AND	[r].[EntityId]		= [u].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;