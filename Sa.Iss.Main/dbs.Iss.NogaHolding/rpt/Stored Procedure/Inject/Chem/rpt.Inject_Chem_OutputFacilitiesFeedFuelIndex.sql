﻿CREATE PROCEDURE [rpt].[Inject_Chem_OutputFacilitiesFeedFuelIndex]
(
	@fctSubmissionId		INT,
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Text]
		)
		SELECT
			[SubmissionId]	= @fctSubmissionId,
			[MethodologyId]	= [ante].[Return_MethodologyId]('2015'),
			[CurrencyId]	= [dim].[Return_CurrencyId]('USD'),
			[EntityId]		= [rpt].[Return_EntityId]('FeedFuelIdx'),
			[AttributeId]	= [rpt].[Return_AttributeId]('FeedFuelIdx'),
			[Value_Float]	= [yld].[LowerHeatingValue_GJDay] / SUM([eii].[UtilizedCapacity] * [Std].[EnergyStandard_GJTonne]) * 100.0
		FROM
			[xls].[YieldAllocation]				[yld]
		INNER JOIN
			[xls].[Eii]							[eii]
				ON	[eii].[SubmissionId]			= [yld].[SubmissionId]
		INNER JOIN
			[etl].[ProcessUnits]				[e]
				ON	[e].[XlsProcessUnitDetail]	= [eii].[ProcessUnitDetail]
				AND	[e].[ProcessUnitTag]		IN ('OSAmmoniaRefrig', 'OSMethanolDist', 'OSUrea')
		INNER JOIN
			[rpt].[Entities]					[r]
				ON	[r].[EntityTag]				= [e].[ProcessUnitTag]
				AND	[r].[EntityTag]				IN ('OSAmmoniaRefrig', 'OSMethanolDist', 'OSUrea')
		INNER JOIN
			[ante].[EnergyStandard]				[std]
				ON	[std].[EntityId]			= [r].[EntityId]
				AND	[std].AttributeId			= [rpt].[Return_AttributeId]('EnergyConsGJDay')
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]			[o]
				ON	[o].[SubmissionId]	= @fctSubmissionId
				AND	[o].[MethodologyId]	= [ante].[Return_MethodologyId]('2015')
				AND	[o].[CurrencyId]	= [dim].[Return_CurrencyId]('USD')
				AND	[o].[AttributeId]	= [rpt].[Return_AttributeId]('FeedFuelIdx')
				AND	[o].[EntityId]		= [rpt].[Return_EntityId]('FeedFuelIdx')
		WHERE
				[yld].[SubmissionId]		= @stgSubmissionId
			AND	[yld].[ComponentDetail]		= 'Average Molecular Weight and Average LHV'
			AND	[o].[OutputFacilitiesId]	IS NULL
		GROUP BY
			[yld].[LowerHeatingValue_GJDay];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;