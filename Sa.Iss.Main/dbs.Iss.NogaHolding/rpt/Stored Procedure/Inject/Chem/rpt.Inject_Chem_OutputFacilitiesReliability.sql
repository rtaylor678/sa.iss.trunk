﻿CREATE PROCEDURE [rpt].[Inject_Chem_OutputFacilitiesReliability]
(
	@fctSubmissionId		INT,
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[u].[EntityId],
			[a].[AttributeId],
			[u].[Value_Float]
		FROM(
			SELECT
				[t].[SubmissionId],
					[MethodologyId]			= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]			= [dim].[Return_CurrencyId]('USD'),
					[CurrencyName]			= 'USD',
				[r].[EntityId],
				[r].[EntityName],
					[RelRegulatory_Hours]	= [t].[Regulatory_Hours],
					[RelMechanical_Hours]	= [t].[Mechanical_Hours],
					[RelTotal_Hours]		= [t].[Total_Hours]
			FROM
				[xls].[Reliability]					[t]
			INNER JOIN
				[etl].[ProcessUnits]				[e]
					ON	[e].[XlsProcessUnitDetail]	= [t].[ProcessUnitDetail]
			INNER JOIN
				[rpt].[Entities]					[r]
					ON	[r].[EntityTag]				= [e].[ProcessUnitTag]
			WHERE
				[t].[SubmissionId]		= @stgSubmissionId
			) [t]
			UNPIVOT (
				[Value_Float] FOR [AttributeTag] IN ([RelRegulatory_Hours], [RelMechanical_Hours], [RelTotal_Hours])
			) [u]
		INNER JOIN
			[rpt].[Attributes]	[a]
				ON	[a].[AttributeTag]	= [u].[AttributeTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [a].[AttributeId]
				AND	[r].[EntityId]		= [u].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Text]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[u].[EntityId],
			[a].[AttributeId],
			[u].[Value]
		FROM(
			SELECT
				[t].[SubmissionId],
					[MethodologyId]				= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]				= [dim].[Return_CurrencyId]('USD'),
					[CurrencyName]				= 'USD',
					[EntityId]					= [rpt].[Return_EntityId]('MaintRout'),
					[EntityName]				= 'MaintRout',
					[RelRegulatory_HoursEdc]	= [t].[EdcRegulatory_Hours],
					[RelMechanical_HoursEdc]	= [t].[EdcMechanical_Hours],
					[RelTotal_HoursEdc]			= [t].[EdcTotal_Hours]
			FROM
				[xls].[ReliabilityAllocation_Total]		[t]
			WHERE
				[t].[SubmissionId]		= @stgSubmissionId
			) [t]
			UNPIVOT (
				[Value] FOR [AttributeTag] IN ([RelRegulatory_HoursEdc], [RelMechanical_HoursEdc], [RelTotal_HoursEdc])
			) [u]
		INNER JOIN
			[rpt].[Attributes]	[a]
				ON	[a].[AttributeTag]	= [u].[AttributeTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [a].[AttributeId]
				AND	[r].[EntityId]		= [u].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Text]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[u].[EntityId],
			[a].[AttributeId],
			[u].[Value]
		FROM(
			SELECT
				[t].[SubmissionId],
					[MethodologyId]				= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]				= [dim].[Return_CurrencyId]('USD'),
					[CurrencyName]				= 'USD',
					[EntityId]					= [rpt].[Return_EntityId]('MaintTa'),
					[EntityName]				= 'MaintTa',
					[RelMechanical_HoursEdc]	= [t].[EdcDowntime_HoursAlloc],
					[RelTotal_HoursEdc]			= [t].[EdcDowntime_HoursAlloc]
			FROM
				[xls].[TurnaroundEdc_Total]		[t]
			WHERE	[t].[Duration_Type]	= 'Annualized'
				AND	[t].[SubmissionId]	= @stgSubmissionId
			) [t]
			UNPIVOT (
				[Value] FOR [AttributeTag] IN ([RelMechanical_HoursEdc], [RelTotal_HoursEdc])
			) [u]
		INNER JOIN
			[rpt].[Attributes]	[a]
				ON	[a].[AttributeTag]	= [u].[AttributeTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [a].[AttributeId]
				AND	[r].[EntityId]		= [u].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Text]
		)
		SELECT
				[SubmissionId]	= @fctSubmissionId,
			[u].[MethodologyId],
			[u].[CurrencyId],
			[u].[EntityId],
			[a].[AttributeId],
			[u].[Value]
		FROM(
			SELECT
				[r].[SubmissionId],
					[MethodologyId]			= [ante].[Return_MethodologyId]('2015'),
					[CurrencyId]			= [dim].[Return_CurrencyId]('USD'),
					[CurrencyName]			= 'USD',
					[EntityId]				= [rpt].[Return_EntityId]('OpExMaintRoutTA'),
					[EntityName]			= 'OpExMaintRoutTA',
				[RelRegulatory_HoursEdc]	= [r].[EdcRegulatory_Hours],
				[RelMechanical_HoursEdc]	= [r].[EdcMechanical_Hours]	+ [t].[EdcDowntime_HoursAlloc],
				[RelTotal_HoursEdc]			= [r].[EdcTotal_Hours]		+ [t].[EdcDowntime_HoursAlloc]
			FROM
				[xls].[ReliabilityAllocation_Total]		[r]
			INNER JOIN
				[xls].[TurnaroundEdc_Total]				[t]
					ON	[t].[SubmissionId]				= [r].[SubmissionId]
					AND	[t].[Duration_Type]				= 'Annualized'
			WHERE
				[t].[SubmissionId]		= @stgSubmissionId
			) [t]
			UNPIVOT (
				[Value] FOR [AttributeTag] IN ([RelRegulatory_HoursEdc], [RelMechanical_HoursEdc], [RelTotal_HoursEdc])
			) [u]
		INNER JOIN
			[rpt].[Attributes]	[a]
				ON	[a].[AttributeTag]	= [u].[AttributeTag]
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [u].[MethodologyId]
				AND	[r].[CurrencyId]	= [u].[CurrencyId]
				AND	[r].[AttributeId]	= [a].[AttributeId]
				AND	[r].[EntityId]		= [u].[EntityId]
		WHERE	[r].[OutputFacilitiesId] IS NULL;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;