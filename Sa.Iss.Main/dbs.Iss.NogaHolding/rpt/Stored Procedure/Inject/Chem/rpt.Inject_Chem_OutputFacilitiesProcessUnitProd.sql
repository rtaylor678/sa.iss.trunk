﻿CREATE PROCEDURE [rpt].[Inject_Chem_OutputFacilitiesProcessUnitProd]
(
	@fctSubmissionId		INT,
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
			[SubmissionId]		= @fctSubmissionId,
			[MethodologyId]		= [ante].[Return_MethodologyId]('2015'),
			[CurrencyId]		= [dim].[Return_CurrencyId]('USD'),
		[e].[EntityId],
			[AttributeId]		= [rpt].[Return_AttributeId]('Prod'),
			[Value_Float]		= [t].[CapacityUtilization_Unit]
		FROM
			[stg].[ChemProcessUnits]			[t]
		INNER JOIN
			[etl].[ProcessUnits]				[e]
				ON	[e].[XlsProcessUnitDetail]	= [t].[ProcessUnitName]
				AND	[e].[ProcessUnitTag]		IN ('OSAmmoniaRefrig', 'OSMethanolDist', 'OSUrea')
		LEFT OUTER JOIN
			[rpt].[OutputFacilities]	[r]
				ON	[r].[SubmissionId]	= @fctSubmissionId
				AND	[r].[MethodologyId]	= [ante].[Return_MethodologyId]('2015')
				AND	[r].[CurrencyId]	= [dim].[Return_CurrencyId]('USD')
				AND	[r].[AttributeId]	= [rpt].[Return_AttributeId]('Prod')
				AND	[r].[EntityId]		= [e].[EntityId]
		WHERE
				[t].[SubmissionId]		= @stgSubmissionId
			AND	[r].[OutputFacilitiesId] IS NULL;

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;