﻿CREATE PROCEDURE [rpt].[Inject_Chem_OutputFacilities_EiiWeighting]
(
	@fctSubmissionId		INT,
	@stgSubmissionId		INT
)
AS
BEGIN

	DECLARE @ProcedureDesc	NVARCHAR(4000) = N'EXECUTE ' + OBJECT_SCHEMA_NAME(@@PROCID) + N'.' + OBJECT_NAME(@@PROCID) + N' (' + CONVERT(VARCHAR, SYSDATETIME(), 20) + N')';

	SET NOCOUNT ON;

	BEGIN TRY

		INSERT INTO [rpt].[OutputFacilities]
		(
			[SubmissionId],
			[MethodologyId],
			[CurrencyId],
			[EntityId],
			[AttributeId],
			[Value_Float]
		)
		SELECT
			[SubmissionId]	= @fctSubmissionId,
			[MethodologyId]	= [ante].[Return_MethodologyId]('2015'),
			[CurrencyId]	= [dim].[Return_CurrencyId]('USD'),
			[EntityId]		= [rpt].[Return_EntityId]('Eii'),
			[AttributeId]	= [rpt].[Return_AttributeId]('Weighting'),
			[Value_Float]	= [e].[Consumption_GJHvc] / ([e].[Consumption_GJDay] / [i].[EiiStandardEnergy_GJDay]) * SUM([d].[Value])
		FROM
			[xls].[Divisors]					[d]
		INNER JOIN
			[etl].[Divisors]					[x]
				ON	[x].[XlsDivisorDetail]		= [d].[DivisorDetail]
				AND	[x].[DivisorTag]			IN ('Ammonia', 'Methanol', 'DivUreaBulk')
		INNER JOIN
			[xls].[EnergyAllocation]			[e]
				ON	[e].[SubmissionId]			= [d].[SubmissionId]
		INNER JOIN
			[etl].[Accounts]					[y]
				ON	[y].[XlsAccountDetail]		= [e].[AccountDetail]
				AND	[y].[AccountTag]			= 'OpExEnergy'
		INNER JOIN
			[xls].[Eii]							[i]
				ON	[i].[SubmissionId]			= [d].[SubmissionId]
		INNER JOIN
			[etl].[ProcessUnits]				[z]
				ON	[z].[XlsProcessUnitDetail]	= [i].[ProcessUnitDetail]
				AND	[z].[ProcessUnitTag]		= 'ProcUnits'
		WHERE
			[d].[SubmissionId]			= @stgSubmissionId
		GROUP BY
			[d].[SubmissionId],
			[e].[Consumption_GJHvc],
			[e].[Consumption_GJDay],
			[i].[EiiStandardEnergy_GJDay];

	END TRY
	BEGIN CATCH

		DECLARE @XActState	INT = XACT_STATE();
		SET @stgSubmissionId = COALESCE(@stgSubmissionId, -1);
		EXECUTE [audit].[Insert_LogError] @XActState, @ProcedureDesc, @stgSubmissionId;
		THROW;

		RETURN ERROR_NUMBER();

	END CATCH;

END;