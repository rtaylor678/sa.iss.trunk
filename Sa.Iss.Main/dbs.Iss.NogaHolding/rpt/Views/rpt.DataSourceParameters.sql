﻿CREATE VIEW [rpt].[DataSourceParameters]
AS
SELECT
	[s].[DataSourceId],
	[s].[TvfSchema],
	[s].[TvfName],
	[s].[Tvf],
	[p].[ParameterId],
	[p].[ParameterName],
	[p].[ParameterType],
	[p].[ParameterLength],
	[p].[ParameterScale],
	[p].[ParameterHasDefault],
	[p].[ParameterDefaultValue]
FROM
	[rpt].[DataSource]		[s]
CROSS APPLY
	[rpt].[Select_DataSourceParameters]([s].[TvfSchema], [s].[TvfName]) [p];