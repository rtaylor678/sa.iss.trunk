﻿CREATE VIEW [rpt].[AttributesMonths]
WITH SCHEMABINDING
AS
SELECT
	[a].[AttributeId],
	[a].[AttributeTag],
	[a].[AttributeName],
	[a].[AttributeDetail],
		[DataMonth]	= [t].[SortKey],
	[t].[SortKey]
FROM
	[rpt].[Attributes]	[a]
INNER JOIN (VALUES
	('Jan',  1),
	('Feb',  2),
	('Mar',  3),
	('Apr',  4),
	('May',  5),
	('Jun',  6),
	('Jul',  7),
	('Aug',  8),
	('Sep',  9),
	('Oct', 10),
	('Nov', 11),
	('Dec', 12)
	) [t]([AttributeTag], [SortKey])
		ON	[t].[AttributeTag]	= [a].[AttributeTag]
WHERE
	[a].[AttributeTag] IN ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');