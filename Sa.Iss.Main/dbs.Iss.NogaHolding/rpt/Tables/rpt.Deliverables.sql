﻿CREATE TABLE [rpt].[Deliverables]
(
	[DeliverableId]						INT					NOT	NULL	IDENTITY(1, 1),

	[DeliverableTag]					VARCHAR(12)			NOT	NULL,	CONSTRAINT [CL_Deliverables_DeliverableTag]		CHECK([DeliverableTag] <> ''),
																		CONSTRAINT [UK_Deliverables_DeliverableTag]		UNIQUE NONCLUSTERED([DeliverableTag] ASC),
	[DeliverableName]					VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_Deliverables_DeliverableName]	CHECK([DeliverableName] <> N''),
																		CONSTRAINT [UK_Deliverables_DeliverableName]	UNIQUE NONCLUSTERED([DeliverableName] ASC),
	[DeliverableDetail]					VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_Deliverables_DeliverableDetail]	CHECK([DeliverableDetail] <> N''),
																		CONSTRAINT [UK_Deliverables_DeliverableDetail]	UNIQUE NONCLUSTERED([DeliverableDetail] ASC),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Deliverables_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Deliverables_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Deliverables_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Deliverables_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Deliverables_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Deliverables]		PRIMARY KEY NONCLUSTERED([DeliverableId] ASC)
);
GO

CREATE TRIGGER [rpt].[t_Deliverables_u]
ON	[rpt].[Deliverables]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[rpt].[Deliverables]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[rpt].[Deliverables].[DeliverableId]	= [i].[DeliverableId];

END;