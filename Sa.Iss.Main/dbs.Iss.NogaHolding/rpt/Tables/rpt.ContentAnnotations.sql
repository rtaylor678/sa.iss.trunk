﻿CREATE TABLE [rpt].[ContentAnnotations]
(
	[ContentAnnotationId]				INT					NOT	NULL	IDENTITY(1, 1),

	[ContentId]							INT					NOT	NULL	CONSTRAINT [FK_ContentAnnotations_Content]			REFERENCES [rpt].[Content]([ContentId]),
	[AnnotationLevelId]					INT					NOT	NULL	CONSTRAINT [FK_ContentAnnotations_AnnotationsLevel]	REFERENCES [rpt].[AnnotationsLevel]([AnnotationsLevelId]),
	[AnnotationTextId]					INT					NOT	NULL	CONSTRAINT [FK_ContentAnnotations_AnnotationsText]	REFERENCES [rpt].[AnnotationsText]([AnnotationsTextId]),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_ContentAnnotations_tsModified]		DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ContentAnnotations_tsModifiedHost]	DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ContentAnnotations_tsModifiedUser]	DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_ContentAnnotations_tsModifiedApp]	DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_ContentAnnotations_tsModifiedGuid]	DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_ContentAnnotations]	PRIMARY KEY NONCLUSTERED([ContentAnnotationId] ASC),
	CONSTRAINT [UK_ContentAnnotations]	UNIQUE CLUSTERED([ContentId] ASC, [AnnotationLevelId] ASC),
	CONSTRAINT [UX_ContentAnnotations]	UNIQUE NONCLUSTERED([ContentId] ASC, [AnnotationTextId] ASC)
);
GO

CREATE TRIGGER [rpt].[t_ContentAnnotations_u]
ON	[rpt].[ContentAnnotations]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[rpt].[ContentAnnotations]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[rpt].[ContentAnnotations].[ContentAnnotationId]	= [i].[ContentAnnotationId];

END;