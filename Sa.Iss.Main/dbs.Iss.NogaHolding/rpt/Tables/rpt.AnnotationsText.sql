﻿CREATE TABLE [rpt].[AnnotationsText]
(
	[AnnotationsTextId]					INT					NOT	NULL	IDENTITY(1, 1),

	[AnnotationsTextTag]				VARCHAR(48)			NOT	NULL	CONSTRAINT [UK_AnnotationsText_AnnotationsTextTag]		UNIQUE CLUSTERED([AnnotationsTextTag] ASC),
																		CONSTRAINT [CL_AnnotationsText_AnnotationsTextTag]		CHECK([AnnotationsTextTag] <> ''),
	[AnnotationsTextName]				VARCHAR(48)			NOT	NULL	CONSTRAINT [UX_AnnotationsText_AnnotationsTextName]		UNIQUE NONCLUSTERED([AnnotationsTextName] ASC),
																		CONSTRAINT [CL_AnnotationsText_AnnotationsTextName]		CHECK([AnnotationsTextName] <> N''),
	[AnnotationsTextDetail]				VARCHAR(48)			NOT	NULL	CONSTRAINT [UX_AnnotationsText_AnnotationsTextDetail]	UNIQUE NONCLUSTERED([AnnotationsTextDetail] ASC),
																		CONSTRAINT [CL_AnnotationsText_AnnotationsTextDetail]	CHECK([AnnotationsTextDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_AnnotationsText_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AnnotationsText_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AnnotationsText_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_AnnotationsText_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_AnnotationsText_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_AnnotationsText]		PRIMARY KEY NONCLUSTERED([AnnotationsTextId] ASC)
);
GO

CREATE TRIGGER [rpt].[t_AnnotationsText_u]
ON	[rpt].[AnnotationsText]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[rpt].[AnnotationsText]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[rpt].[AnnotationsText].[AnnotationsTextId]	= [i].[AnnotationsTextId];

END;