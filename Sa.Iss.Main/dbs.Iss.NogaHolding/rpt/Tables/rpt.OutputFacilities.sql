﻿CREATE TABLE [rpt].[OutputFacilities]
(
	[OutputFacilitiesId]				INT					NOT	NULL	IDENTITY(1, 1),

	[SubmissionId]						INT					NOT	NULL	CONSTRAINT [FK_OutputFacilities_Submissions]		REFERENCES [fact].[Submissions]([SubmissionId]),
	[MethodologyId]						INT					NOT	NULL	CONSTRAINT [FK_OutputFacilities_Methodology_LookUp]	REFERENCES [ante].[Methodology]([MethodologyId]),
	[CurrencyId]						INT					NOT	NULL	CONSTRAINT [FK_OutputFacilities_Currency_LookUp]	REFERENCES [dim].[Currency_LookUp]([CurrencyId]),

	[EntityId]							INT					NOT	NULL	CONSTRAINT [FK_OutputFacilities_Entity_LookUp]		REFERENCES [rpt].[Entities]([EntityId]),
	[UnitId]							INT						NULL,

	[AttributeId]						INT					NOT	NULL	CONSTRAINT [FK_OutputFacilities_Attribute_LookUp]	REFERENCES [rpt].[Attributes]([AttributeId]),

	[Value_Float]						FLOAT					NULL,
	[Value_Text]						NVARCHAR(256)			NULL,
	[Value_Date]						DATE					NULL,
	[Value]								AS COALESCE(CONVERT(NVARCHAR, [Value_Float]), [Value_Text], LEFT(CONVERT(NVARCHAR, [Value_Date], 102), 10), N'<No Value>')
										PERSISTED			NOT	NULL,

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_OutputFacilities_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_OutputFacilities_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_OutputFacilities_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_OutputFacilities_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_OutputFacilities_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_OutputFacilities]	PRIMARY KEY NONCLUSTERED([OutputFacilitiesId] ASC),
	CONSTRAINT [UK_OutputFacilities]	UNIQUE CLUSTERED([SubmissionId] ASC, [MethodologyId] ASC, [CurrencyId] ASC, [EntityId] ASC, [UnitId] ASC, [AttributeId] ASC)
);
GO

CREATE TRIGGER [rpt].[t_OutputFacilities_u]
ON	[rpt].[OutputFacilities]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[rpt].[OutputFacilities]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[rpt].[OutputFacilities].[OutputFacilitiesId]	= [i].[OutputFacilitiesId];

END;