﻿CREATE TABLE [rpt].[Entities]
(
	[EntityId]							INT					NOT	NULL	IDENTITY(1, 1),

	[EntityTag]							VARCHAR(48)			NOT	NULL	CONSTRAINT [UK_Entities_EntityTag]			UNIQUE CLUSTERED([EntityTag] ASC),
																		CONSTRAINT [CL_Entities_EntityTag]			CHECK([EntityTag] <> ''),
	[EntityName]						NVARCHAR(96)		NOT	NULL	CONSTRAINT [UX_Entities_EntityName]			UNIQUE NONCLUSTERED([EntityName] ASC),
																		CONSTRAINT [CL_Entities_EntityName]			CHECK([EntityName] <> N''),
	[EntityDetail]						NVARCHAR(96)		NOT	NULL,	--CONSTRAINT [UX_Entities_EntityDetail]		UNIQUE NONCLUSTERED([EntityDetail] ASC),
																		CONSTRAINT [CL_Entities_EntityDetail]		CHECK([EntityDetail] <> N''),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Entities_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Entities_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Entities_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Entities_tsModifiedApp]		DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Entities_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Entities]			PRIMARY KEY NONCLUSTERED([EntityId] ASC)
);
GO

CREATE TRIGGER [rpt].[t_Entities_u]
ON	[rpt].[Entities]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[rpt].[Entities]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[rpt].[Entities].[EntityId]	= [i].[EntityId];

END;