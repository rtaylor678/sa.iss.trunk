﻿CREATE TABLE [rpt].[Content]
(
	[ContentId]							INT					NOT	NULL	IDENTITY(1, 1),

	[ContentTag]						VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_Content_ContentTag]				CHECK([ContentTag] <> ''),
																		CONSTRAINT [UK_Content_ContentTag]				UNIQUE NONCLUSTERED([ContentTag] ASC),
	[ContentName]						VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_Content_ContentName]				CHECK([ContentName] <> N''),
																		--CONSTRAINT [UK_Content_ContentName]				UNIQUE NONCLUSTERED([ContentName] ASC),
	[ContentDetail]						VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_Content_ContentDetail]			CHECK([ContentDetail] <> N''),
																		CONSTRAINT [UK_Content_ContentDetail]			UNIQUE NONCLUSTERED([ContentDetail] ASC),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Content_tsModified]				DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Content_tsModifiedHost]			DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Content_tsModifiedUser]			DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Content_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Content_tsModifiedGuid]			DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Content]				PRIMARY KEY NONCLUSTERED([ContentId] ASC)
);
GO

CREATE TRIGGER [rpt].[t_Content_u]
ON	[rpt].[Content]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[rpt].[Content]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[rpt].[Content].[ContentId]	= [i].[ContentId];

END;