﻿CREATE TABLE [rpt].[Style]
(
	[StyleId]							INT					NOT	NULL	IDENTITY(1, 1),

	[StyleTag]							VARCHAR(24)			NOT	NULL,	CONSTRAINT [CL_Style_StyleTag]				CHECK([StyleTag] <> ''),
																		CONSTRAINT [UK_Style_StyleTag]				UNIQUE NONCLUSTERED([StyleTag] ASC),
	[StyleName]							VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_Style_StyleName]				CHECK([StyleName] <> N''),
																		CONSTRAINT [UK_Style_StyleName]				UNIQUE NONCLUSTERED([StyleName] ASC),
	[StyleDetail]						VARCHAR(48)			NOT	NULL,	CONSTRAINT [CL_Style_StyleDetail]			CHECK([StyleDetail] <> N''),
																		CONSTRAINT [UK_Style_StyleDetail]			UNIQUE NONCLUSTERED([StyleDetail] ASC),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_Style_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Style_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Style_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_Style_tsModifiedApp]			DEFAULT(APP_NAME()),
	[tsModifiedGuid]					UNIQUEIDENTIFIER	NOT	NULL	CONSTRAINT [DF_Style_tsModifiedGuid]		DEFAULT(NEWSEQUENTIALID())	ROWGUIDCOL,

	CONSTRAINT [PK_Style]				PRIMARY KEY NONCLUSTERED([StyleId] ASC)
);
GO

CREATE TRIGGER [rpt].[t_Style_u]
ON	[rpt].[Style]
AFTER UPDATE
AS
BEGIN

	SET NOCOUNT ON;

	UPDATE
		[rpt].[Style]
	SET	[tsModified]		= SYSDATETIMEOFFSET(),
		[tsModifiedHost]	= HOST_NAME(),
		[tsModifiedUser]	= SUSER_SNAME(),
		[tsModifiedApp]		= APP_NAME()
	FROM
		INSERTED	[i]
	WHERE
		[rpt].[Style].[StyleId]	= [i].[StyleId];

END;