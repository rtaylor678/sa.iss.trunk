﻿CREATE TABLE [rpt].[DeliverablesContent]
(
	[DeliverablesContentId]				INT					NOT	NULL	IDENTITY(1, 1),

	[DeliverableId]						INT					NOT	NULL	CONSTRAINT [FK_DeliverablesContent_Deliverables]		REFERENCES [rpt].[Deliverables]([DeliverableId]),
	[ItemNumber]						INT					NOT	NULL,
	[ContentId]							INT					NOT	NULL	CONSTRAINT [FK_DeliverablesContent_Content]				REFERENCES [rpt].[Content]([ContentId]),

	[tsModified]						DATETIMEOFFSET(7)	NOT	NULL	CONSTRAINT [DF_DeliverablesContent_tsModified]			DEFAULT(SYSDATETIMEOFFSET()),
	[tsModifiedHost]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_DeliverablesContent_tsModifiedHost]		DEFAULT(HOST_NAME()),
	[tsModifiedUser]					NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_DeliverablesContent_tsModifiedUser]		DEFAULT(SUSER_SNAME()),
	[tsModifiedApp]						NVARCHAR(128)		NOT	NULL	CONSTRAINT [DF_DeliverablesContent_tsModifiedApp]		DEFAULT(APP_NAME()),

	CONSTRAINT [PK_DeliverablesContent]	PRIMARY KEY([DeliverablesContentId] ASC),
	CONSTRAINT [UK_DeliverablesContent]	UNIQUE CLUSTERED([DeliverableId] ASC, [ItemNumber] ASC)
);