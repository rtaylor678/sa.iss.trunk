﻿CREATE TYPE [rpt].[TypeAttributes] AS TABLE
(
	[AttributeId]		INT		NOT	NULL,
	[AttributeSortKey]	INT		NOT	NULL,
	PRIMARY KEY CLUSTERED([AttributeId])
);