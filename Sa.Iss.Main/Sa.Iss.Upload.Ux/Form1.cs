﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sa.Iss.Ux
{
	public partial class Form1 : Form
	{
		Sa.Iss.CpaFile inputFile;

		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{

		}

		private void btnSummary_Click(object sender, EventArgs e)
		{
			Deployment.GPIC sum = new Deployment.GPIC();
			sum.UploadSummary();
		}

		/////////////////////////////////////////////////////////////////////////////////

		private void btnPickUploadfile_Click(object sender, EventArgs e)
		{
			string filePath = SelectFile();

			if (filePath != string.Empty)
			{
				this.inputFile = new CpaFile(filePath, "lonestar");
				lblInputForm.Text = inputFile.Path + "\r\n" + inputFile.FileNameExtension;
				btnUpload.Text = "Upload\r\n" + inputFile.FileNameExtension;
			}
		}

		private string SelectFile()
		{
			string f = string.Empty;

			OpenFileDialog file = new OpenFileDialog();

			file.Filter = "Excel Files (*.xls,*.xlsx,*.xlsm)|*.xls;*.xlsx;*.xlsm";

			if (file.ShowDialog() == DialogResult.OK)
			{
				f = file.FileName;
			}

			return f;
		}

		private void btnUpload_Click(object sender, EventArgs e)
		{
			if (this.inputFile.CpaFormType != CpaForm.CpaFormType.None)
			{
				IStgUpload iForm = null;
				IXlsUpload cForm = null;

				switch (this.inputFile.CpaFormType)
				{
					case Sa.Iss.CpaForm.CpaFormType.Chem:
						iForm = new Chem.InputForm(this.inputFile);
						cForm = new Chem.Calculations(this.inputFile);
						//MessageBox.Show("CHEM");
						break;
					case Sa.Iss.CpaForm.CpaFormType.Fuel:
						iForm = new Fuel.InputForm(this.inputFile);
						cForm = new Fuel.Calculations(this.inputFile);
						MessageBox.Show("FUEL");
						break;
					case Sa.Iss.CpaForm.CpaFormType.Lube:
						iForm = new Lube.InputForm(this.inputFile);
						cForm = new Lube.Calculations(this.inputFile);
						MessageBox.Show("LUBE");
						break;
				}

				this.UploadFiles(iForm, cForm);
			}
		}

		private void UploadFiles(IStgUpload input, IXlsUpload calcs)
		{
			Submission submission = input.UploadStudyFile();

			calcs.UploadStudyFile(submission);

			MessageBox.Show("Done");
		}

		private void btnCreateReport_Click(object sender, EventArgs e)
		{
			string itm = this.lbSubmissions.SelectedItem.ToString();
			int idx = int.Parse(itm.Split('|')[0]);

			Submission s = new Submission();
			s.GetFactSubmission(idx);

			Deliverable d = new Deliverable();
			d.Id = s.deliverableId;
			d.MethodologyId = 1;
			d.CurrencyId = 1;

			Sa.Iss.Report rpt = new Sa.Iss.Report(d, s);
            string reportFilePath = SelectFile();
            if (reportFilePath == String.Empty)
            {
                MessageBox.Show("No report xls file chosen. Exiting.");
                return;
            }
            rpt.Create(reportFilePath);
		}

        //private String GetReportFilePath()
        //{
        //    OpenFileDialog fileDialog = new OpenFileDialog();
        //    fileDialog.Filter= "xlsx files (*.xlsx)|*.xlsx|All files (*.*)|*.*"
        //    fileDialog.FilterIndex=2;
        //    fileDialog.Title = "Please select xls report file to use.";
        //    if(fileDialog.ShowDialog()==DialogResult.OK)
        //    {
        //        return fileDialog.FileName;
        //    }
        //    else
        //        return String.Empty;
        //}

		private void PopulateListBox(ListBox lb)
		{
			lb.Items.Clear();

			using (SqlConnection cn = new SqlConnection(Common.cnString()))
			{
				using (SqlCommand cmd = new SqlCommand("[fact].[Select_Submissions]", cn))
				{
					cmd.CommandType = CommandType.StoredProcedure;

					cn.Open();

					using (SqlDataReader rdr = cmd.ExecuteReader())
					{
						while (rdr.Read())
						{
							string groupName = rdr.GetString(rdr.GetOrdinal("GroupName"));
							int submissionId = rdr.GetInt32(rdr.GetOrdinal("SubmissionId"));
							int dataYear = rdr.GetInt16(rdr.GetOrdinal("DataYear"));
							int dataMonth = rdr.GetByte(rdr.GetOrdinal("DataMonth"));

							string itm = submissionId.ToString() + "|" + groupName + " (" + dataYear.ToString() + "-" + dataMonth.ToString() + ")";

							lb.Items.Add(itm);
						}
					}
					cn.Close();
				}
			}
		}

		private void btnRefresh_Click(object sender, EventArgs e)
		{
			PopulateListBox(this.lbSubmissions);
		}
	}
}
